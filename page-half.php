<?php
/*
Template Name: Page Half Sidebar
*/
?>
<?php get_header();?>
        <div class="inner-page-half">
                <div class="wrapper">
                        <?php do_action('avoskin_navside');?>
                        <article class="main">
                                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                        <div class="content">
                                                <h1 class="the-title"><?php the_title();?></h1>
                                                <div class="format-text">
                                                        <?php the_content();?>
                                                </div><!-- end of format text -->
                                        </div><!-- end of content -->
                                <?php endwhile;?>
                                <?php else : ?>
                                        <div class="format-text">
                                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                        </div>
                                <?php endif;?>
                        </article>
                        <div class="clear"></div>
                </div><!--end of wrapper -->
        </div><!-- end of inner page half -->
<?php get_footer();?>