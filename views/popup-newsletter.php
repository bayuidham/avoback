<?php
        $home_id = get_option('page_on_front');
        $meta = [
                'show' => 'optin_popup_show',
                'img' => 'optin_popup_img',
                'title' => 'optin_popup_title',
                'text' => 'optin_popup_text',
                'info' => 'optin_popup_info',
                'delay' => 'optin_popup_delay',
                'api' => 'optin_sendgrid_api',
		'list' => 'optin_sendgrid_list',
		'url' => 'optin_kirim_url',
        ];
        extract(avoskin_get_meta($home_id, $meta));
        if($show == 'yes' && is_page_template('page-homepage.php')):
                $credential = [
                        //'api' => $api,
                        //'list' => $list,
			'url' => $url
                ];
?>
        
        <div class="popup-newsletter" data-delay="<?php echo $delay ;?>">
                <div class="layer">
                        <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
                        <div class="rowflex">
                                <?php if($img != ''):?>
                                        <figure><img src="<?php echo $img ;?>" /></figure>
                                <?php endif;?>
                                <div class="caption">
                                        <h2><?php echo $title ;?></h2>
                                        <div class="txt">
                                                <?php echo $text ;?>
                                        </div><!-- end of txt -->
                                        <div class="form-basic dark centered">
                                                <form>
                                                        <input type="email" value="" name="email" placeholder="<?php _e('Your Email Address','avoskin');?>" required="required">
                                                        <input type="hidden" name="credential"  value='<?php echo base64_encode(wp_json_encode($credential));?>'>
                                                        <button type="submit" class="button btn-white slimy has-loading"><?php _e('Subscribe','avoskin');?></button>
                                                </form>
                                        </div><!-- end of form basic -->
                                        <div class="txt">
                                                <?php echo $info ;?>
                                        </div><!-- end of txt -->
                                </div><!-- end of caption -->
                        </div><!-- end of rowflex -->
                </div><!-- end of layer -->
        </div><!-- end of popup credential -->
<?php endif;?>