<?php
        $home_id = get_option('page_on_front');
        $meta = [
                'ltitle' => 'login_title',
                'ltext' => 'login_text',
                'linfo' => 'login_info',
                'rtitle' => 'register_title',
                'rtext' => 'register_text',
                'rinfo' => 'register_info',
                'ftitle' => 'forgot_title',
                'ftext' => 'forgot_text',
                'finfo' => 'forgot_info',
        ];
        extract(avoskin_get_meta($home_id, $meta));
        
?>
<div class="popup-credential">
        <div class="layer">
                <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
                <div id="tab-login" class="item active">
                        <div class="txt">
                                <h2><?php echo $ltitle ;?></h2>
                                <?php echo $ltext ;?>
                        </div>
                        <div class="form-basic">
                                <!-- ::PENTEST:: -->
                                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                <form id="form-popup-login" <?php echo (is_page_template('page-checkout.php')) ? 'class="reload"' : '';?>>
                                        <input type="email" name="user_login" value="" placeholder="<?php _e('Email','avoskin');?>" required="required" autocomplete="username" />
                                        <div class="pass-field">
                                                <input type="password" name="user_password" value="" placeholder="<?php _e('Password','avoskin');?>" required="required" autocomplete="current-password"  />
                                                <b></b>
                                        </div><!-- end of pass field -->
                                        <a href="#" class="forgot"><?php _e('Forgot Password?','avoskin');?></a><br/>
                                        <div class="g-recaptcha" data-sitekey="6Le9A80bAAAAAJb3kIEABiFBQzCkQJSdir38jNbI"></div>
                                        <input type="hidden" name="token" value="<?php echo base64_encode(WC_Geolocation::get_ip_address()) ;?>"/>
                                        <button type="submit" class="button btn-fullwidth has-loading"><?php _e('Login','avoskin');?></button>
                                </form>
                                <div class="extra">
                                        <?php echo $linfo ;?>
                                </div>
                        </div><!-- end of form basic -->
                </div><!-- end of tab login -->
                
                <div id="tab-register" class="item">
                        <div class="txt">
                                <h2><?php echo $rtitle ;?></h2>
                                <?php echo $rtext ;?>
                        </div>
                        <div class="form-basic">
                                <form id="form-popup-register">
                                        <!-- ::CRLOYALTY:: -->
                                        <input type="text" value="" placeholder="<?php _e('Full Name','avoskin');?>" name="first_name" required="required" />
                                        <input type="hidden" value="" placeholder="<?php _e('Last Name','avoskin');?>" name="last_name" />
                                        <input type="email" name="user_email" value="" placeholder="<?php _e('Email','avoskin');?>" required="required" autocomplete="username"  />
                                        <input type="tel" name="loyalty_phone" value="" placeholder="<?php _e('Phone Number','avoskin');?>" required="required" />
                                        <div class="pass-field">
                                                <input type="password" name="user_password" value="" placeholder="<?php _e('Password','avoskin');?>" required="required" autocomplete="current-password"  />
                                                <b></b>
                                        </div><!-- end of pass field -->
                                        <button type="submit" class="button btn-fullwidth has-loading"><?php _e('Register','avoskin');?></button>
                                </form>
                                <div class="extra">
                                        <?php echo $rinfo ;?>
                                </div>
                        </div><!-- end of form basic -->
                </div><!-- end of tab register -->
                
                <div id="tab-forgot" class="item">
                        <div class="txt">
                                <h2><?php echo $ftitle ;?></h2>
                                <?php echo $ftext ;?>
                        </div>
                        <div class="form-basic">
                                <form id="form-popup-forgot-password">
                                        <input type="email" name="email" value="" placeholder="<?php _e('Email','avoskin');?>" required="required" />
                                        <button type="submit" class="button btn-fullwidth has-loading"><?php _e('Submit','avoskin');?></button>
                                </form>
                                <br/>
                                <div class="extra">
                                        <?php echo $finfo ;?>
                                </div>
                        </div><!-- end of form basic -->
                </div><!-- end of tab register -->
        </div><!-- end of layer -->
</div><!-- end of popup credential -->