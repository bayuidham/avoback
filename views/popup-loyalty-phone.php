<div class="popup-loyalty"> <!-- ::CRLOYALTY:: -->
        <div class="layer">
                <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
                <div class="txt">
                        <h2><?php _e('Input OTP Number','avoskin');?></h2>
                </div><!-- end of txt --><br/>
                <div class="form-basic">
                        <form id="submit-otp">
                                <div class="lineup-input">
                                        <?php for($i=1;$i<=4;$i++):?><input type="text" value="" maxlength="1" required="required" name="otp_number[]" /><?php endfor;?>
                                </div><!-- end of lineuup input -->
                                <div class="expired" data-time=""><span>00:00</span></div>
                                <input type="hidden" name="user" value="<?php echo get_current_user_id() ;?>" />
                                <button type="submit" class="button btn-fullwidth slimy has-loading"><?php _e('Submit','avoskin');?></button>
                        </form>
                </div><!-- end of form basic -->
        </div><!-- end of layer -->
</div><!-- end o popup password -->