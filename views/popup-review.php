<div class="popup-review">
        <div class="layer">
                <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
                <div class="caption">
                        <h2><?php _e('Write a Review','avoskin');?></h2>
                        <div class="format-text">
                                <p><?php _e('Your email address will not be published. Required fields are marked','avoskin');?> <sup>*</sup></p>
                        </div><!-- end of format text -->
                </div><!-- end of caption -->
                <div class="form-basic">
                        <form>
                                <div class="rowflex">
                                        <div class="fgroup">
                                                <label><?php _e('Name','avoskin');?><sup>*</sup></label>
                                                <input type="text" value="" name="name" required="required"/>
                                        </div><!-- end of fgroup -->
                                        <div class="fgroup">
                                                <label><?php _e('Email','avoskin');?><sup>*</sup></label>
                                                <input type="email" value="" name="email" required="required" />
                                        </div><!-- end of fgroup -->
                                        <div class="fgroup">
                                                <label><?php _e('Rating','avoskin');?><sup>*</sup></label>
                                                <div class="star-rate">
                                                        <input type="radio" id="star5" name="rating" value="5" />
                                                        <label for="star5" title="5 <?php _e('stars','avoskin');?>">5 <?php _e('stars','avoskin');?></label>
                                                        <input type="radio" id="star4" name="rating" value="4" />
                                                        <label for="star4" title="4 <?php _e('stars','avoskin');?>">4 <?php _e('stars','avoskin');?></label>
                                                        <input type="radio" id="star3" name="rating" value="3" />
                                                        <label for="star3" title="3 <?php _e('stars','avoskin');?>">3 <?php _e('stars','avoskin');?></label>
                                                        <input type="radio" id="star2" name="rating" value="2" />
                                                        <label for="star2" title="2 <?php _e('stars','avoskin');?>">2 <?php _e('stars','avoskin');?></label>
                                                        <input type="radio" id="star1" name="rating" value="1" checked="checked" />
                                                        <label for="star1" title="1 <?php _e('star','avoskin');?>">1 <?php _e('star','avoskin');?></label>
                                                </div><!-- end of star rate -->
                                                <div class="clear"></div>
                                        </div><!-- end of fgroup -->
                                </div><!-- end of rowflex -->
                                <div class="fgroup">
                                        <label><?php _e('Review','avoskin');?><sup>*</sup></label>
                                        <textarea name="review_text"  required="required"></textarea>
                                </div><!-- end of fgroup -->
                                <div class="fgroup">
                                        <label><?php _e('What is your skin type?','avoskin');?> <sup>*</sup></label>
                                        <div class="boxcheck">
                                                <?php
                                                        $chk = [
                                                                __('Oily', 'avoskin'),
                                                                __('Normal', 'avoskin'),
                                                                __('Dry', 'avoskin'),
                                                                __('Combo', 'avoskin'),
                                                                __('Acne-Prone', 'avoskin'),
                                                                __('Sensitive', 'avoskin')
                                                        ];
                                                        $n=1;
                                                        foreach($chk as $c):
                                                ?>
                                                        <label>
                                                                <input type="checkbox" name="skin_type[]" value="<?php echo $c ;?>" <?php echo ($n == 1) ? 'checked="checked"' : '';?> />
                                                                <span><?php echo $c ;?></span>
                                                        </label>
                                                <?php $n++;endforeach;?>
                                        </div><!-- end of box check -->
                                </div><!-- end of fgroup -->
                                <div class="fgroup">
                                        <label><?php _e('How long you wear it?','avoskin');?> <sup>*</sup></label>
                                        <div class="boxcheck">
                                                <?php
                                                        $chk = [
                                                                __('Less than A Week', 'avoskin'),
                                                                __('A Week', 'avoskin'),
                                                                __('A Month', 'avoskin'),
                                                                __('3-6 Months', 'avoskin'),
                                                                __('6 Month-1 year', 'avoskin'),
                                                                __('More Than A Year', 'avoskin')
                                                        ];
                                                        $n=1;
                                                        foreach($chk as $c):
                                                ?>
                                                        <label>
                                                                <input type="radio" name="use_duration" value="<?php echo $c ;?>" <?php echo ($n == 1) ? 'checked="checked"' : '';?>  />
                                                                <span><?php echo $c ;?></span>
                                                        </label>
                                                <?php $n++;endforeach;?>
                                        </div><!-- end of box check -->
                                </div><!-- end of fgroup -->
                                <div class="fgroup">
                                        <label><?php _e('Package Quality?','avoskin');?> <sup>*</sup></label>
                                        <div class="boxcheck">
                                                <?php
                                                        $chk = [
                                                                __('Poor', 'avoskin'),
                                                                __('Can Be Improved', 'avoskin'),
                                                                __('Okay', 'avoskin'),
                                                                __('Good', 'avoskin'),
                                                                __('Perfect', 'avoskin')
                                                        ];
                                                        $n=1;
                                                        foreach($chk as $c):
                                                ?>
                                                        <label>
                                                                <input type="radio" name="package_quality" value="<?php echo $c ;?>"  <?php echo ($n == 1) ? 'checked="checked"' : '';?>  />
                                                                <span><?php echo $c ;?></span>
                                                        </label>
                                                <?php $n++;endforeach;?>
                                        </div><!-- end of box check -->
                                </div><!-- end of fgroup -->
                                <div class="fgroup">
                                        <label><?php _e('How Is The Product Price?','avoskin');?> <sup>*</sup></label>
                                        <div class="boxcheck">
                                                <?php
                                                        $chk = [
                                                                __('Expensive', 'avoskin'),
                                                                __('Just Right', 'avoskin'),
                                                                __('Value For Money', 'avoskin')
                                                        ];
                                                        $n=1;
                                                        foreach($chk as $c):
                                                ?>
                                                        <label>
                                                                <input type="radio" name="product_price" value="<?php echo $c ;?>" <?php echo ($n == 1) ? 'checked="checked"' : '';?>  />
                                                                <span><?php echo $c ;?></span>
                                                        </label>
                                                <?php $n++;endforeach;?>
                                        </div><!-- end of box check -->
                                </div><!-- end of fgroup -->
                                <div class="rowflex">
                                        <div class="fgroup">
                                                <label><?php _e('Are You Recommend This Product?','avoskin');?> <sup>*</sup></label>
                                                <div class="boxcheck">
                                                        <label>
                                                                <input type="radio" name="recommend" value="<?php _e('Yes','avoskin');?>"  checked="checked" />
                                                                <span><?php _e('Yes','avoskin');?></span>
                                                        </label>
                                                        <label>
                                                                <input type="radio" name="recommend" value="<?php _e('No','avoskin');?>" />
                                                                <span><?php _e('No','avoskin');?></span>
                                                        </label>
                                                </div><!-- end of box check -->
                                        </div><!-- end of fgroup -->
                                        <div class="fgroup">
                                                <label><?php _e('Will You Repurchase This Product?','avoskin');?> <sup>*</sup></label>
                                                <div class="boxcheck">
                                                        <label>
                                                                <input type="radio" name="repurchase" value="<?php _e('Yes','avoskin');?>" checked="checked" />
                                                                <span><?php _e('Yes','avoskin');?></span>
                                                        </label>
                                                        <label>
                                                                <input type="radio" name="repurchase" value="<?php _e('No','avoskin');?>" />
                                                                <span><?php _e('No','avoskin');?></span>
                                                        </label>
                                                </div><!-- end of box check -->
                                        </div><!-- end of fgroup -->
                                </div><!-- end of rowflex -->
                                <div class="action">
                                        <input type="hidden" value="<?php echo get_the_ID();?>" name="product_id" />
                                        <button type="submit" class="button slimy has-loading"><?php _e('Submit','avoskin');?></button>
                                </div>
                        </form>
                </div><!-- end of form basic -->
        </div><!-- end of layer -->
</div><!-- end of popup credential -->