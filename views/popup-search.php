<div class="popup-search">
        <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
        <div class="holder">
                <a href="<?php echo home_url('/');?>">
                        <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" />
                </a>
                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET">
                        <input type="text" value="" placeholder="<?php _e('Search product or category','avoskin');?>" name="s" required="required">
                        <button type="submit"><i class="fa-search"></i></button>
                </form><!-- end of search -->
        </div><!-- end of holder -->
</div><!-- end of popup credential -->