<?php if(is_page_template('page-partner.php')):?>
        <script type="text/javascript">
                var map,
                        locations = $('.location .item').map(function(i,v){ return $(v).data('item'); }).get(),
                        gmarkers = [];
                
                function createMarker(myLatLng, popup, infowindow) {
                        var marker = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        icon:  '<?php echo avoskin_dir();?>/assets/img/icon/marker.svg'
                                });
                        
                         google.maps.event.addListener(marker, 'click', function() {
                                infowindow.setContent(popup);
                                infowindow.open(map, marker);
                        });
                         
                        return marker;
                }
                
                function initMap(){
                        var infowindow = new google.maps.InfoWindow({
                                maxWidth: 400
                        });
                        
                        map = new google.maps.Map(document.getElementById('maps'), {
                                zoom: 5,
                                center: new google.maps.LatLng(-7.919017, 110.378548),
                                mapTypeControl: false,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        
                        for (var i = 0; i < locations.length; i++) {
                                var loc = locations[i]['loc'];
				
				if ( typeof(loc) !== "undefined" && loc !== null ){ 
					var myLatLng = new google.maps.LatLng(loc[0], loc[1]),
						popup = '<div class="map-txt"><h3><span>'+locations[i]['title']+'</span> <b>'+locations[i]['subtitle']+'</b></h3>'+
								'<div class="txt">' +locations[i]['text']+'</div></div>';
					gmarkers[locations[i]['title']] = createMarker(myLatLng, popup, infowindow);
				}
                        }
                }
                
                $('.inner-partner .store .item').on('click', function(){
                        var item = $(this).data('item'),
                                index = item['title'];
                                
                        google.maps.event.trigger(gmarkers[index],'click');
                        var center = new google.maps.LatLng(item['loc'][0],item['loc'][1]);
			map.panTo(center);
                });
                
                $('.map-search input[type="text"]').on('keyup', function(){
                        var searchVal = $(this).val(),
                                filterItems = $('[data-filter-item]');
                        
                        if ( searchVal != '' ) {
                                filterItems.addClass('hidden');
                                var selected = $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]');
                                selected.removeClass('hidden');
                                if(selected.length) {
                                        google.maps.event.trigger(gmarkers[selected.data('item')['title']],'click');
                                        var center = new google.maps.LatLng(selected.data('item')['loc'][0],selected.data('item')['loc'][1]);
                                        map.panTo(center);
                                }
                        } else {
                                filterItems.removeClass('hidden');
                        }
                });
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlaj9lcEBFozGWo53xvVDC_vxjUUEw0uU&callback=initMap" async defer></script>
<?php endif;?>

<?php if(is_page_template('page-checkout.php')):
	// ::CRLOYALTY::
	get_template_part('views/popup-loyalty-phone');
?>
	<script src="<?php  echo get_template_directory_uri() . '/assets/js/dev/encrypt.js';?>"></script> <!--  ::PENTEST:: -->
	<script type="text/javascript">
		// ::CRLOYALTY::
		(function($) {
			$.fn.inputFilter = function(inputFilter) {
				return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
					if (inputFilter(this.value)) {
						this.oldValue = this.value;
						this.oldSelectionStart = this.selectionStart;
						this.oldSelectionEnd = this.selectionEnd;
					} else if (this.hasOwnProperty("oldValue")) {
						this.value = this.oldValue;
						this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
					} else {
						this.value = "";
					}
				});
			};
		}(jQuery));
		;(function(e){
			
			var checkout = {
				parent: $('.inner-checkout .user-data .layer .form-basic'),
				country: $('#checkout-country'),
				state: $('#checkout-state'),
				city: $('#checkout-city'),
				cityName: $('#checkout-city-name'),
				postal: $('#checkout-postal'),
				address: $('#checkout-address'),
				weight: parseInt($('#product-weight').val()),
				shipping: $('#shipping-courier .form-basic'),
				summary: $('#checkout-summary table'),
				autoCoupon: $('#auto-coupon-wrap'),
				appliedCoupon: $('#applied-coupon'),
				products: $('.inner-checkout .order-data .product-list'),
				coupon: $('#coupon-code'),
				timeout: null,
				email: $('#user-email'),
				param: function (name, string) {
					var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(string);
					if (results == null) {
					     return 0;
					}
					return results[1] || 0;
				},
				addLoading: function(){
					$('<div class="avoskin-loading">'+
							'<div class="centered">'+
								'<div class="ldr">'+
									'<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>'+
								'</div><!-- end of ldr -->'+
							'</div><!-- end of centered -->'+
						'</div><!-- end of avoskin loading -->').prependTo('body');
				},
				removeLoading: function(){
					$('body').find('.avoskin-loading').remove();
				},
				validateOrder: function(data){ // ::CRWFC::
					var valid = true;
					$.each(data, function(k,v){
						if (v.name != 'notes' && v.name != 'tax' && v.name != 'last_name' && v.name != 'loyalty_point') {
							if (v.value != '') {
								if (v.name == 'shipping' || v.name == 'grand_total' || v.name == 'subtotal') {
									if (v.name != 'shipping') {
										if (
										    parseInt(v.value) <= 0
										    && !$('#applied-coupon').hasClass('wfc-freeship')
										    && !$('#applied-coupon li.type-avo_wfc').length
										) {
											valid = false;
										}
									}else{
										if (
											parseInt(v.value) <= 0 && $('input[name="coupon_code"]').val() == ''
											|| parseInt(v.value) <= 0 && !$('#applied-coupon').hasClass('wfc-freeship') && !$('#applied-coupon li.type-avo_wfc').length
										) {
											valid = false;
										}
									}
								}
							}else{
								valid = false;
							}
						}
					});
					
					return valid;
				},
				processPoint: function(data){
					var parent = $('.lfwrap');
					if (data.hasPoint == 'yes') {
						parent.find('input').attr('readonly', 'readonly');
						parent.find('.btn-loyalty-apply').text('Remove');
						parent.find('.btn-loyalty-apply').addClass('has-point');
					}else{
						parent.find('input').removeAttr('readonly');
						parent.find('input').val('');
						parent.find('.btn-loyalty-apply').text('Use Point');
						parent.find('.btn-loyalty-apply').removeClass('has-point');
						parent.find('.btn-loyalty-apply').removeClass('remove-point');
					}
					parent.find('.btn-loyalty-apply').attr('data-point', data.userPoint);
					parent.find('p b').text(data.userPoint);
					if (data.pointOverlap == 'yes') {
						avoskinApp.fn.alert({
							"title": "Error",
							"content": "You don't have enough point."
						},1000);	
					}
				},
				switchPaymentMethod: function(data){ // ::CRWFC::
					if (data.paymentMethod.type == 'free') {
						$('.inner-checkout .order-data .payment').replaceWith(data.paymentMethod.content);
						$('.inner-checkout .order-data .action button.has-loading').text('Placed Order');
					}else{
						$('input#free-wfc').replaceWith(data.paymentMethod.content);
						$('.inner-checkout .order-data .action button.has-loading').text('Pay Now');
					}
					var cart = $('.cart-sidebar'),
						count = $('.bot-head .util .cart > a b');
						
					cart.html(data.paymentMethod.cart);
                                        count.html(data.paymentMethod.count);
				},
				// ::PENTEST::
				encrypt: function(data){
					var encrypt = new JSEncrypt();
					var serialize = [];
					encrypt.setPublicKey(atob(avoskin_data.public));
					$.each(data, function(k, v){
						var key = encrypt.encrypt(String(k));
							value = encrypt.encrypt(String(v));
							
						serialize.push({
							key: key,
							val: value
						});
					});
					return serialize;
				}
			};
			
			$(document).ready(function(){
				
				if (avoskin_data.user == 'nope') {
					var data = {
						shipping: '',
						shipping_name: '',
						user: avoskin_data.user,
						hasCoupon: checkout.appliedCoupon.find('input[name="coupon_code"]').val(),
						payment: $('#form-checkout-product .payment input:checked').val(),
					};
					checkout.addLoading();
					axios.post( avoskin_data.api_url + "generate_checkout_summary/", {data:checkout.encrypt(data)} ).then( response => { //::PENTEST::
						checkout.removeLoading();
						if (response.status == 200) {
							checkout.summary.html(response.data.content);
							checkout.products.html(response.data.products);
							checkout.autoCoupon.html(response.data.autoCoupon);
							checkout.appliedCoupon.html(response.data.appliedCoupon);
							if (response.data.coupon == 'removed') {
								avoskinApp.fn.alert({
									"title": "Info",
									"content": "Coupon removed from cart!"
								},10000);
							}
						}else{ // ::PENTEST::
							avoskinApp.fn.alert({
								"title": "Info",
								"content": "Invalid shipping data!"
							},2000);
						}
					}).catch( error => console.log( error ) );
				}

				checkout.country.on('change', function(){
					var self = $(this);
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "get_states/", {country:self.val()}  ).then( response => {
							self.removeClass('fetching');
							checkout.removeLoading();
							if (response.status == 200) {
								checkout.city.html('');
								checkout.state.html(response.data);
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				checkout.state.on('change', function(){
					var self = $(this);
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "get_cities/", {state:self.val()}  ).then( response => {
							self.removeClass('fetching');
							checkout.removeLoading();
							if (response.status == 200) {
								checkout.city.html(response.data);
								checkout.postal.html('<option value="empty" selected="selected">Choose postal code</option>');
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				checkout.city.on('change', function(){
					var self = $(this),
						data = {
							state: checkout.state.val(),
							current: ''
						};
						
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "get_postcode/", data ).then( response => {
							if (response.status == 200) {
								checkout.cityName.val(self.find('option:selected').html());
								checkout.postal.html(response.data);
								self.removeClass('fetching');
								checkout.removeLoading();
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				checkout.postal.on('change', function(){
					var self = $(this),
						data = {
							destination: checkout.city.val(),
							weight: checkout.weight
						};
					
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "get_couriers/", {data:checkout.encrypt(data)} ).then( response => { // ::PENTEST::
							checkout.removeLoading();
							if (response.status == 200) {
								checkout.shipping.html(response.data);
								var dataShipping = {
									shipping: checkout.shipping.find('input:checked').val(),
									shipping_name: checkout.shipping.find('input:checked').closest('label').find('> span').text(),
									hasCoupon: checkout.appliedCoupon.find('input[name="coupon_code"]').val(),
									payment: $('#form-checkout-product .payment input:checked').val(),
									user: avoskin_data.user
								};
								axios.post( avoskin_data.api_url + "generate_checkout_summary/", {data:checkout.encrypt(dataShipping)}  ).then( response => { // ::PENTEST
									self.removeClass('fetching');
									checkout.removeLoading();
									if (response.status == 200) {
										checkout.summary.html(response.data.content);
										checkout.products.html(response.data.products);
										checkout.autoCoupon.html(response.data.autoCoupon);
										checkout.appliedCoupon.html(response.data.appliedCoupon);
										setTimeout(function(){
											$('#shipping-courier input:checked').trigger('click');
										},200);
										//if (response.data.coupon == 'removed') {
										//	avoskinApp.fn.alert({
										//		"title": "Info",
										//		"content": "Coupon removed from cart!"
										//	},10000);
										//}
									}else{ // ::PENTEST::
										avoskinApp.fn.alert({
											"title": "Info",
											"content": "Invalid shipping data!"
										},2000);
									}
								}).catch( error => console.log( error ) );
							}else{ // ::PENTEST::
								avoskinApp.fn.alert({
									"title": "Info",
									"content": "Invalid shipping data!"
								},2000);
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				$(document).on('click', '#shipping-courier input', function(){
					var self = $(this),
						data = {
							shipping: self.val(),
							shipping_name: self.closest('label').find('span').html(),
							hasCoupon: checkout.appliedCoupon.find('input[name="coupon_code"]').val(),
							payment: $('#form-checkout-product .payment input:checked').val(),
							user: avoskin_data.user,
							point: $('.loyalty-field input[name="loyalty_point"]').val(), // ::CRLOYALTY::
							removePoint: ($('.loyalty-field .button').hasClass('remove-point')) ? 'yes' : 'no',
							grandTotal: $('#checkout-summary input[name="grand_total"]').val()
						};
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "generate_checkout_summary/", {data:checkout.encrypt(data)} ).then( response => { // ::PENTEST::
							self.removeClass('fetching');
							checkout.removeLoading();
							if (response.status == 200) {
								checkout.summary.html(response.data.content);
								checkout.products.html(response.data.products);
								checkout.autoCoupon.html(response.data.autoCoupon);
								checkout.appliedCoupon.html(response.data.appliedCoupon);
								if (response.data.coupon == 'removed') {
									avoskinApp.fn.alert({
										"title": "Info",
										"content": "Coupon removed from cart!"
									},10000);
								}
								checkout.processPoint(response.data); // ::CRLOYALTY::
								checkout.switchPaymentMethod(response.data); // ::WFC::
								
								
								// ::CRWFC::
								var channel = ['channel-gopay','channel-shopeepay', 'channel-bni_va', 'channel-alfamart'];
								if (parseInt($('input[name="grand_total"]').val()) < 10000) {
									$('.inner-checkout .order-data .payment .dotdio label').hide();
									var hasClick = false;
									$.each(channel, function(k, v){
										if($('.inner-checkout .order-data .payment .dotdio label.'+v).find('input:checked').length){
											hasClick = true;
										}
										$('.inner-checkout .order-data .payment .dotdio label.'+v).show();
									});
									if (!hasClick) {
										$('.inner-checkout .order-data .payment .dotdio label.'+channel[0]).trigger('click');
									}
								}else{
									$('.inner-checkout .order-data .payment .dotdio label').show();
								}
								
							}else{ // ::PENTEST::
								avoskinApp.fn.alert({
									"title": "Info",
									"content": "Invalid shipping data!"
								},2000);
							}
						}).catch( error => console.log( error ) );
					}
					
				});
				
				checkout.coupon.find('.button').on('click', function(e){
					e.preventDefault();
					var self = $(this),
						parent = self.closest('.form-basic'),
						data = {
							coupon: parent.find('input.dummy-coupon').val(),
							shipping: '',
							shipping_name: '',
							user: avoskin_data.user,
							payment: $('#form-checkout-product .payment input:checked').val(),
							removeCoupon: 'no',
							hasCoupon: checkout.appliedCoupon.find('input[name="coupon_code"]').val(),
							point: $('.loyalty-field input[name="loyalty_point"]').val(), // ::CRLOYALTY::
							removePoint: ($('.loyalty-field .button').hasClass('remove-point')) ? 'yes' : 'no',
							grandTotal: $('#checkout-summary input[name="grand_total"]').val()
						};
					
					if (checkout.shipping.find('input:checked').length) {
						data.shipping = checkout.shipping.find('input:checked').val();
						data.shipping_name = checkout.shipping.find('input:checked').closest('label').find('span').html()
					}
						
					if (data.coupon != '') {
						if (!self.hasClass('fetching')) {
							self.addClass('fetching');
							checkout.addLoading();
							axios.post( avoskin_data.api_url + "generate_checkout_summary/", {data:checkout.encrypt(data)}  ).then( response => { // ::PENTEST::
								self.removeClass('fetching');
								checkout.removeLoading();
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},10000);
								if (response.data.coupon == 'ok') {
									checkout.summary.html(response.data.content);
									checkout.products.html(response.data.products);
									checkout.autoCoupon.html(response.data.autoCoupon);
									checkout.appliedCoupon.html(response.data.appliedCoupon);
									checkout.coupon.find('input').val('');
									checkout.coupon.find('input').removeAttr('name');
									checkout.processPoint(response.data); // ::CRLOYALTY::
									checkout.switchPaymentMethod(response.data); // ::WFC::
								}
							}).catch( error => console.log( error ) );
						}
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.coupon_alert.title,
							"content": avoskin_data.coupon_alert.msg
						},500);
					}
				});
				
				$(document).on('click', '#applied-coupon li a', function(e){
					e.preventDefault();
					var self = $(this),
						parent = self.closest('li'),
						ancestor = parent.closest('#applied-coupon'),
						data = {
							coupon: self.attr('data-coupon'),
							removeCoupon: 'yes',
							shipping: '',
							shipping_name: '',
							user: avoskin_data.user,
							payment: $('#form-checkout-product .payment input:checked').val(),
							hasCoupon: checkout.appliedCoupon.find('input[name="coupon_code"]').val(),
							point: $('.loyalty-field input[name="loyalty_point"]').val(), // ::CRLOYALTY::
							removePoint: ($('.loyalty-field .button').hasClass('remove-point')) ? 'yes' : 'no',
							grandTotal: $('#checkout-summary input[name="grand_total"]').val()
						};
						
					if (checkout.shipping.find('input:checked').length) {
						data.shipping = checkout.shipping.find('input:checked').val();
						data.shipping_name = checkout.shipping.find('input:checked').closest('label').find('span').html()
					}
						
					
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						checkout.addLoading();
						axios.post( avoskin_data.api_url + "generate_checkout_summary/", {data:checkout.encrypt(data)}  ).then( response => {  //::PENTEST::
							self.removeClass('fetching');
							checkout.removeLoading();
							if (response.data.coupon == 'ok') {
								checkout.summary.html(response.data.content);
								checkout.products.html(response.data.products);
								checkout.autoCoupon.html(response.data.autoCoupon);
								checkout.appliedCoupon.html(response.data.appliedCoupon);
								checkout.processPoint(response.data); // ::CRLOYALTY::
								checkout.switchPaymentMethod(response.data); // ::WFC::
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},2000);
								
								// ::CRWFC::
								if (!$('.product-list table').length) {
									window.location.reload();
								}
							}
						});
					}
				});
				
				$(document).on('click', '.auto-coupon li a', function(e){
					e.preventDefault();
					var self = $(this);
					if (checkout.coupon.hasClass('has-coupon-applied')) {
						avoskinApp.fn.alert({
							"title": "Info",
							"content": "Please remove current coupon first!"
						},500);
					}else{
						checkout.coupon.find('input').val(self.find('strong').html());
						checkout.coupon.find('.button').trigger('click');
					}
				});
				
				// ::CRWFC::
				if (checkout.email.hasClass('not-log') ) {
					var oldEmail = '';
					checkout.timeout =  setInterval(function(){
						if (checkout.email.val() != '' && oldEmail != checkout.email.val() && !checkout.email.hasClass('fetching')) {
							checkout.email.addClass('fetching');
							oldEmail = checkout.email.val();
							axios.post( avoskin_data.api_url + "check_user_exists/", {email:checkout.email.val()}  ).then( response => {
								checkout.email.removeClass('fetching');
								if (response.data.status == 'ok') {
									clearInterval(checkout.timeout);
									avoskinApp.fn.alert({
										"title": response.data.title,
										"content": response.data.msg
									},1000);
									setTimeout(function(){
										$('.popup-credential .item').removeClass('active');
										$('.popup-credential').find('#tab-login').addClass('active');
										$('body').addClass('open-cred');
									},1500);
								}else if (response.data.status == 'removed_coupon') {
									avoskinApp.fn.alert({
										"title": response.data.title,
										"content": response.data.msg
									},2000);
									setTimeout(function(){
										if ($('.inner-checkout .order-data .product-list tbody tr').length > 1) {
											window.location.reload();
										}else{
											window.location.href = avoskin_data.site_url;
										}
									},2500);
								}
							}).catch( error => console.log( error ) );
						}
					}, 1000);
				}
				
				$(document).on('click', '#form-checkout-product .payment input', function(){
					$('#shipping-courier input:checked').trigger('click');
				});
				
				$('#form-checkout-product').on('submit', function(e){
					e.preventDefault();
					var self = $(this),
						data = self.serialize(),
						dataArray = self.serializeArray() // ::PENTEST::
						
					if (checkout.validateOrder(self.serializeArray())) {
						if (checkout.param('do_agree', data) != 0) {
							if ( parseFloat(self.find('.summary table td input[name="shipping"]').val()) <= 0 && !self.find('#applied-coupon input[name="coupon_code"]').length) {
								avoskinApp.fn.alert({
									"title": "Info",
									"content": "Something went wrong, please try again later"
								},800);
							}else{
								if ($('#checkout-address').val().length > 10) {
									if (!self.hasClass('fetching')) {
										clearInterval(checkout.timeout);
										
										// ::PENTEST::
										var encrypt = new JSEncrypt();
										var serialize = [];
										var product_data = [];
										encrypt.setPublicKey(atob(avoskin_data.public));
										$.each(dataArray, function(k, v){
											var key = encrypt.encrypt(v.name);
												value = encrypt.encrypt(v.value);
											if (v.name != 'product_data[]') {
												serialize.push({
													key: key,
													val: value
												});
											}else{
												product_data.push(v.value);
											}
										});
										serialize.push({
											key: encrypt.encrypt('product_data')	,
											val: JSON.stringify(product_data)
										});
										
										self.addClass('fetching');
										checkout.addLoading();
										axios.post( avoskin_data.api_url + "submit_checkout/", {data:serialize}  ).then( response => { // ::PENTEST::
											self.removeClass('fetching');
											checkout.removeLoading();
											
											if (response.data.result != 'invalid_postcode') {
												if (response.data.result == 'success') {
													window.location.href = response.data.redirect;
												}else{ window.location.reload(); }	
											}else{
												avoskinApp.fn.alert({
													"title": response.data.title,
													"content": response.data.msg
												},1000);
											}
										}).catch(  function(error){
											avoskinApp.fn.alert({
												"title": "Notice",
												"content": "Pembayaran kamu belum dapat di proses. Silahkan login dan lakukan pembelanjaan ulang."
											},3000);
											setTimeout(function(){
												window.location.href = avoskin_data.account
											}, 4000);
										} );
									}
								}else{
									avoskinApp.fn.alert({
										"title": "Notice",
										"content": "Silahkan masukkan alamat yang valid!"
									},2000);
								}
							}
						}else{
							avoskinApp.fn.alert({
								"title": avoskin_data.checkout_alert.title,
								"content": avoskin_data.checkout_alert.msg
							},800);
						}
					}else{
						avoskinApp.fn.alert({
							"title": "Notice",
							"content": "Order tidak valid!"
						},2000);
					}
				});
				
				//If city already selected, trigger change to calculate shipping
				if (checkout.postal.val() != '' && checkout.postal.val() != 'empty') {
					checkout.postal.trigger('change');
				}
				
				// ::CRLOYALTY::
				//$('.loyalty-field h3 a').on('click', function(e){
				//	e.preventDefault();
				//	var self = $(this),
				//		parent = self.closest('.loyalty-field');
				//	
				//	if (parent.hasClass('showing') && parent.find('input').val() != '' && parent.find('.btn-loyalty-apply').hasClass('has-point')) {
				//		parent.find('.btn-loyalty-apply').trigger('click');
				//	}
				//	parent.toggleClass('showing');
				//	$(this).closest('.loyalty-field').find('input').val('');
				//	
				//});
				
				$('.btn-loyalty-verify').on('click', function(e){
					e.preventDefault();
					var button = $(this);
						
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "generate_loyalty_otp/", {new_phone:button.attr('data-phone'),old_phone:button.attr('data-phone-old'), user: avoskin_data.user}  ).then( response => {
							button.removeClass('fetching');
							if (response.data.status == 'ok') {
								//Expired verification
								if ($('.popup-loyalty .expired').length) {
									$('.popup-loyalty .expired').each(function(){
										var self = $(this),
											exp = new Date().getTime() + (1000 * 180);
										$('.popup-loyalty .txt').html("<h2>Input OTP Number</h2>");
										 self.countdown(exp).on('update.countdown', function(event) {
											var $this = self.html(event.strftime('<span>%M:%S</span>'));
										}).on('finish.countdown',function(event){
											window.location.reload();
										});
									});
								}
								$('.popup-loyalty .lineup-input input').val('');
								$('<input type="hidden" name="new_phone" value="'+button.attr('data-phone')+'"/>').appendTo('.popup-loyalty #submit-otp');
								$('body').addClass('open-loyalty');
							}else{
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},1000);	
							}
						}).catch( error => console.log( error ) );	
					}
				});
				
				//Verification field move
				$('.form-basic .lineup-input input').on('keyup', function(){
					if ($(this).val() != '' && $(this).next('input').length) {
						$(this).next('input').focus();
					}
				});
				
				$('.popup-loyalty .layer .cls').on('click', function(e){
					e.preventDefault();
					$('body').removeClass('open-loyalty');
				});
				
				$('form#submit-otp').on('submit', function(e){
					e.preventDefault();
					var self = $(this),
						button = self.find('button'),
						data = self.serialize();
					
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "validate_loyalty_otp/", {data:data}  ).then( response => {
							button.removeClass('fetching');
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},1000);	
							if (response.data.status == 'ok') {
								setTimeout(function(){
									window.location.reload();
								},1200);
							}
						}).catch( error => console.log( error ) );	
					}
				});
				
				$(".loyalty-field .lfapply input").inputFilter(function(value) {
					return /^\d*$/.test(value);    
				});
				
				$('.loyalty-field .lfapply .btn-loyalty-apply').on('click', function(e){
					e.preventDefault();
					var self = $(this),
						hasPoint = parseInt(self.attr('data-point')),
						parent = self.closest('.lfapply'),
						input = parent.find('input'),
						total = $('#checkout-summary input[name="grand_total"]').val(),
						max =  parseInt(total) / 2,
						point = (max >= hasPoint ) ? hasPoint : total / 2;
					
					input.val(point);
					if (self.hasClass('has-point')) {
						if($('#shipping-courier input:checked').length){
							self.addClass('remove-point');
							$('#shipping-courier input:checked').trigger('click');
						}
					}else{
						if (input.val() != '' && parseInt(input.val()) > 0) {
							if (input.val() > parseInt(self.attr('data-point'))) {
								avoskinApp.fn.alert({
									"title": "Error",
									"content": "You don't have enough point."
								},1000);	
							}else{
								if (parseInt(input.val()) > max) {
									avoskinApp.fn.alert({
										"title": "Error",
										"content": "Maximum point usage is " + max
									},1000);	
								}else{
									if($('#shipping-courier input:checked').length){
										$('#shipping-courier input:checked').trigger('click');
									}
								}
							}
						}else{
							avoskinApp.fn.alert({
								"title": "Error",
								"content": "Please input point amount!"
							},1000);	
						}	
					}
					
				});
				
			});
			
		}(jQuery));
	</script>
<?php endif;?>
<?php if(is_page_template('page-tracking-order.php')):?>
	<script type="text/javascript">
		;(function($){
			$('#form-tracking-order').on('submit', function(e){
				e.preventDefault();
				var self = $(this),
					data = self.serialize(),
					button = self.find('.button');
				
				if (!button.hasClass('fetching')) {
					button.addClass('fetching');
					axios.post( avoskin_data.api_url + "track_order/", {data:data} ).then( response => {
						if (response.data.status != 'ok') {
							button.removeClass('fetching');
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},300);	
						}else{
							window.location.href = response.data.redirect
						}
					}).catch( error => console.log( error ) );
				}
			});
		}(jQuery));
	</script>
<?php endif;?>

<?php if(is_page_template('page-my-account.php')):?>

	<?php
		//Add popup change password field
		get_template_part('views/popup-password');
		
		// ::CRLOYALTY::
		get_template_part('views/popup-loyalty-phone');
	?>
	
	<script type="text/javascript">
		;(function($){
			$('.navacc-mobile select').on('change', function(){
				window.location.href = $(this).val();
			});
			$('#form-profile').on('submit', function(e){
				e.preventDefault();
				var self = $(this),
					button = self.find('.has-loading'),
					data = self.serialize();
				
				if (self.find('input[name="loyalty_phone"]').val().length > 8 && self.find('input[name="loyalty_phone"]').val().length < 15 && /^\d*(\.\d+)?$/.test(self.find('input[name="loyalty_phone"]').val()) ) {
					if (self.find('input[name="loyalty_phone"]').val() != self.find('input[name="loyalty_phone_old"]').val()){
						button.addClass('fetching');
						$('.btn-verify-phone').trigger('click');
					}else{
						if (!button.hasClass('fetching')) {
							button.addClass('fetching');
							axios.post( avoskin_data.api_url + "update_user_info/", {data:data}  ).then( response => {
								button.removeClass('fetching');
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},1000);
								// ::CRLOYALTY::
								setTimeout(function(){
									if (response.data.reload == 'yep') {
										window.location.reload();
									}
								},1200);
							}).catch( error => console.log( error ) );
						}		
					}
				}else{
					avoskinApp.fn.alert({
						"title": "Error",
						"content": "Invalid phone number"
					},1000);
				}
			});
			
			//Update Password
			$('.btn-change-password').on('click', function(e){
				e.preventDefault();
				$('body').addClass('open-password');
			});
			
			$('.popup-password .layer .cls').on('click', function(e){
				e.preventDefault()
				$('body').removeClass('open-password');
			});
			
			$('form#update-password').on('submit', function(e){
				e.preventDefault();
				var self = $(this),
					button = self.find('.has-loading'),
					newPass = self.find('input[name="new_password"]'),
					newPassConfirm = self.find('input[name="new_password_confirm"]'),
					data = self.serialize();
			
				// ::PENTEST::
				if (newPass.val() === newPassConfirm.val()){
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "update_user_password/", {data:data}  ).then( response => {
							button.removeClass('fetching');
							if (response.data.status == 'ok') {
								$('body').removeClass('open-password');
							}
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},1000);
						}).catch( error => console.log( error ) );
					}
				}else{
					avoskinApp.fn.alert({
						"title": avoskin_data.profile_alert.title,
						"content": avoskin_data.profile_alert.msg.pass_not_match
					},1000);
				}	
			});
			
			// ::CRLOYALTY::
			$('.loyalty-phone .btn-verify-phone').on('click', function(e){
				e.preventDefault();
				var button = $(this),
					parent = button.closest('.loyalty-phone'),
					oldPhone = parent.find('input[name="loyalty_phone_old"]'),
					newPhone = parent.find('input[name="loyalty_phone"]');
					
				if (newPhone.val() != '') {
					if (newPhone.val().length > 8 && /^\d*(\.\d+)?$/.test(newPhone.val()) ) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "generate_loyalty_otp/", {new_phone:newPhone.val(),old_phone:oldPhone.val(), user: avoskin_data.user}  ).then( response => {
							button.removeClass('fetching');
							if (response.data.status == 'ok') {
								//Expired verification
								if ($('.popup-loyalty .expired').length) {
									$('.popup-loyalty .expired').each(function(){
										var self = $(this),
											exp = new Date().getTime() + (1000 * 180);
										
										$('.popup-loyalty .txt').html("<h2>Input OTP Number</h2>");
										 self.countdown(exp).on('update.countdown', function(event) {
											var $this = self.html(event.strftime('<span>%M:%S</span>'));
										}).on('finish.countdown',function(event){
											window.location.reload();
										});
									});
								}
								$('#form-profile button.has-loading').removeClass('fetching');
								$('.popup-loyalty .lineup-input input').val('');
								$('<input type="hidden" name="new_phone" value="'+newPhone.val()+'"/>').appendTo('.popup-loyalty #submit-otp');
								
								$('body').addClass('open-loyalty');
							}else{
								$('#form-profile button.has-loading').removeClass('fetching');
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},1000);	
							}
						}).catch( error => console.log( error ) );	
					}else{
						avoskinApp.fn.alert({
							"title": "Info",
							"content": "Invalid phone number!"
						},1000);
					}
				}else{
					avoskinApp.fn.alert({
						"title": "Info",
						"content": "Please input phone number!"
					},1000);
				}
			});
			
			//Verification field move
			$('.form-basic .lineup-input input').on('keyup', function(){
				if ($(this).val() != '' && $(this).next('input').length) {
					$(this).next('input').focus();
				}
			});
			
			$('.popup-loyalty .layer .cls').on('click', function(e){
				e.preventDefault();
				$('body').removeClass('open-loyalty');
			});
			
			$('form#submit-otp').on('submit', function(e){
				e.preventDefault();
				var self = $(this),
					button = self.find('button'),
					data = self.serialize();
				
				if (!button.hasClass('fetching')) {
					button.addClass('fetching');
					axios.post( avoskin_data.api_url + "validate_loyalty_otp/", {data:data}  ).then( response => {
						button.removeClass('fetching');
						avoskinApp.fn.alert({
							"title": response.data.title,
							"content": response.data.msg
						},1000);	
						if (response.data.status == 'ok') {
							if ($('body').hasClass('woocommerce-account')) {
								$('#form-profile input[name="loyalty_phone_old"]').val(response.data.phone),
								$('#form-profile input[name="loyalty_phone"]').val(response.data.phone);
								$('#form-profile button.has-loading').trigger('click');
							}else{
								setTimeout(function(){
									window.location.reload();
								},1200);	
							}
						}
					}).catch( error => console.log( error ) );	
				}
			});
			
			// ::CRLOYALTY:: END
			
			
			//SAVE ADDRESS
			var account = {
				parent: $('.inner-checkout .account-address-edit .form-basic'),
				country: $('#checkout-country'),
				state: $('#checkout-state'),
				postal: $('#checkout-postal'),
				city: $('#checkout-city'),
				cityName: $('#checkout-city-name'),
				addLoading: function(){
					$('<div class="avoskin-loading">'+
							'<div class="centered">'+
								'<div class="ldr">'+
									'<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>'+
								'</div><!-- end of ldr -->'+
							'</div><!-- end of centered -->'+
						'</div><!-- end of avoskin loading -->').prependTo('body');
				},
				removeLoading: function(){
					$('body').find('.avoskin-loading').remove();
				}
			};
			
			$(document).ready(function(){
				
				account.country.on('change', function(){
					var self = $(this);
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						account.addLoading();
						axios.post( avoskin_data.api_url + "get_states/", {country:self.val()} ).then( response => {
							self.removeClass('fetching');
							account.removeLoading();
							if (response.status == 200) {
								account.city.html('');
								account.state.html(response.data);
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				account.state.on('change', function(){
					var self = $(this);
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						account.addLoading();
						axios.post( avoskin_data.api_url + "get_cities/", {state:self.val()}  ).then( response => {
							self.removeClass('fetching');
							account.removeLoading();
							if (response.status == 200) {
								account.city.html(response.data);
								account.postal.html('<option value="empty" selected="selected">Choose postal code</option>');
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				account.city.on('change', function(){
					var self = $(this),
					data = {
						state: account.state.val(),
						current: ''
					};
					
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						account.addLoading();
						axios.post( avoskin_data.api_url + "get_postcode/", data ).then( response => {
							if (response.status == 200) {
								account.cityName.val(self.find('option:selected').html());
								account.postal.html(response.data);
								self.removeClass('fetching');
								account.removeLoading();
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				// ::PENTEST::
				$('#form-billing-shipping').on('submit', function(e){
					e.preventDefault();
					var self = $(this),
						button = self.find('button.has-loading'),
						data = self.serialize();
						
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						account.addLoading();
						axios.post( avoskin_data.api_url + "update_user_address/", {data:data} ).then( response => {
							button.removeClass('fetching');
							account.removeLoading();
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},1000);
							if (response.data.status == 'ok') {
								window.location.href = response.data.redirect;
							}
						}).catch( error => console.log( error ) );
					}
				});
				
			});
			
			$('#comment-sorting select').on('change', function(){
				var self = $(this),
					parent = self.closest('#comment-sorting'),
					form = parent.find('form'),
					data = JSON.parse(self.val());
					
				form.find('input[name="orderby"]').val(data.orderby);
				form.find('input[name="order"]').val(data.order);
				form.trigger('submit');
				
			});
			
			//Upload profile pircure
			$('.upload-profile').on('click', function(e){
				e.preventDefault();
				$('#profile_picture').trigger('click');
			});
			
			$('#profile_picture').on('change', function(){
				if (this.files && this.files[0]) {
					var reader = new FileReader(),
						target = $('.inner-account .account-detail .user-profile figure img');
						
					if (
						this.files[0]['type'] == 'image/png'
						|| this.files[0]['type'] == 'image/jpeg'
						|| this.files[0]['type'] == 'image/jpg'
					) {
						var result = {
							name: this.files[0]['name'],
							type: this.files[0]['type']
						};
						if (this.files[0]['size'] < 2000000) {
							reader.onload = function (e) {
								target.attr('src', e.target.result);
								result.src = e.target.result;
								$('#profile_picture_src').val(JSON.stringify(result));
							}
							reader.readAsDataURL(this.files[0]);
						}else{
							avoskinApp.fn.alert({
								"title": avoskin_data.profile_alert.title,
								"content": avoskin_data.profile_alert.msg.invalid_size
							},1500);
							
							this.value = null;	
						}
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.profile_alert.title,
							"content": avoskin_data.profile_alert.msg.invalid_file
						},1500);
						
						this.value = null;
					}
				}
			});
			
			$('#woo-forgot-password').on('click', function(e){
				e.preventDefault();
				$('.popup-credential .item').removeClass('active');
				$('#tab-forgot').addClass('active');
				$('.popup-credential').addClass('to-forgot');
				$('body').addClass('open-cred');
			});
			
			$('#pay-order').on('click', function(e){
				e.preventDefault();
				$('body').addClass('open-order');
			});
			
			$('.popup-order .layer .cls, .btn-close-popup').on('click', function(e){
				e.preventDefault();
				$('body').removeClass('open-order');
			});
			
			$('#order-received').on('click', function(e){
				e.preventDefault();
				var self = $(this),
					order = self.attr('data-order');
				
				if (!self.hasClass('fetching')) {
					self.addClass('fetching');
					axios.post( avoskin_data.api_url + "user_order_received/", {order:order} ).then( response => {
						self.removeClass('fetching');
						avoskinApp.fn.alert({
							"title": response.data.title,
							"content": response.data.msg
						},1500);
						if (response.data.status == 'ok') {
							setTimeout(function(){
								window.location.reload();
							},1500);
						}
					}).catch( error => console.log( error ) );
				}
			});
			
		}(jQuery));
		
	</script>
<?php endif;?>


<?php if(is_page_template('page-join.php')):?>
	<script type="text/javascript">
		;(function(e){
			
			$(document).ready(function(){
				$('form#form-join').on('submit', function(e){
					e.preventDefault();
					var self = $(this),
						button = self.find('button.has-loading'),
						data = self.serialize();
					
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "join_avostore/", {data:data} ).then( response => {
							button.removeClass('fetching');
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},2000);
							if (response.data.status == 'ok') {
								window.location.href = response.data.redirect;
							}
						}).catch( error => console.log( error ) );
					}
				});
			});
			
		}(jQuery));
	</script>
<?php endif;?>
<script type="text/javascript">
	;(function(e){
		function elementLoaded(el, cb) {
			if ($(el).length) {
				// Element is now loaded.
				cb($(el));
			} else {
				// Repeat every 500ms.
				setTimeout(function() {
					elementLoaded(el, cb)
				}, 500);
			}
		};
	    
		elementLoaded('.qcw-trigger-btn > div', function(el) {
			$('.qcw-trigger-btn > div').html('<?php echo esc_attr(get_post_meta(get_option('page_on_front'), 'qiscus_text', true));?>');
		});
		
		
		$(document).ready(function(){
			if (avoskin_data.advisor != 'nope' && $('.advisor-link').length) {
				$('.advisor-link').each(function(){
					var anchor = $(this).find(' > a');
					
					anchor.attr('href', anchor.attr('href')+'?data='+avoskin_data.advisor);
				});
			}
		});
	}(jQuery));
</script>
<style type='text/css'>
        .embeddedServiceHelpButton .helpButton .uiButton {
            background-color: #3b5868;
            font-family: "Comic Sans MS", sans-serif;
        }
        .embeddedServiceHelpButton .helpButton .uiButton:focus {
            outline: 1px solid #3b5868;
        }
</style>  
<script type='text/javascript' src='https://service.force.com/embeddedservice/5.0/esw.min.js'></script>
<script type='text/javascript'>
        var initESW = function(gslbBaseURL) {
            embedded_svc.settings.displayHelpButton = true; //Or false
            embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'
    
            embedded_svc.settings.defaultMinimizedText = 'Chat With Us';
            embedded_svc.settings.disabledMinimizedText = 'All Agent Offline'; 

            embedded_svc.settings.loadingText = 'Connecting...........'; //(Defaults to Loading) 
            
            //overrided the case source. Chat from web will be recognize from web     
                embedded_svc.settings.extraPrechatFormDetails = [{
                            "label":"Case Origin",
                            "value":"Website",
                            "displayToAgent": true
                    }, {
                            "value":"Website",
							"transcriptFields":["Origin__c"] //pass default data to chat transcript fields
					}, {	
							"label":"Subject",
							"transcriptFields":["Subject__c"] //pass default data to chat transcript fields
					}, {
							"label":"Type",
							"transcriptFields":["Type__c"] //pass default data to chat transcript fields
					}, {
                            "label":"RecordTypeId",
                            "value":"0125g000000Xx0NAAS",
                            "displayToAgent": false			
                    }];
            
                embedded_svc.settings.extraPrechatInfo = [{
                "entityName":"Contact",
	            "saveToTranscript":"ContactId",
	            "showOnCreate":true,
				"entityFieldMaps": [{
							"doCreate":true,
							"doFind":true,
							"fieldName":"LastName",
							"isExactMatch":false,
							"label":"Last Name"
					}, {
							"doCreate":false, //prevent Create First Name
							"doFind":false, //prevent Create First Name
							"fieldName":"FirstName",
							"isExactMatch":true,
							"label":"First Name"
					}, {
							"doCreate":false,
							"doFind":true,
							"fieldName":"Phone",
							"isExactMatch":true,
							"label":"Phone"					
					}]
				}, {
				"entityName":"Account",
	            "showOnCreate":true,
	            "saveToTranscript":"AccountId",
				"entityFieldMaps": [{
							"isExactMatch":false, //working previous false
							"fieldName":"LastName",
							"doCreate":true,		//working previous true
							"doFind":true,			//working previous false
							"label":"Last Name"							
					}, {
                            //"isExactMatch":false, //working previous false
							"fieldName":"FirstName",
							"doCreate":true,		//working previous true
							//"doFind":true,			//working previous false
							"label":"First Name"
                    }, {    
						    "fieldName":"PersonEmail",
							"doCreate":true,
							"label":"Email"
					}, {    
                            "doFind":true,
                            "isExactMatch":false,
							"fieldName":"Phone",
							"doCreate":true,
							"label":"Phone"
					}, {		
							"isExactMatch":false,
							"fieldName":"RecordTypeId",
							"doCreate":true,
							"doFind":false,
							"label":"RecordTypeId"								
                        }]
                    
                        
                }];	
    
            
            embedded_svc.settings.enabledFeatures = ['LiveAgent'];
		    embedded_svc.settings.entryFeature = 'LiveAgent';
    
            embedded_svc.init(
			'https://ap24.salesforce.com',
			'https://avochatagent.secure.force.com/liveAgentSetupFlow',
			gslbBaseURL,
			'00D5g000003d7cy',
			'Avo_Chat_Team_Case_02',
			{
				baseLiveAgentContentURL: 'https://c.la2-c1-ukb.salesforceliveagent.com/content',
				deploymentId: '5725g0000010xx2',
				buttonId: '5735g0000010yQp',
				baseLiveAgentURL: 'https://d.la2-c1-ukb.salesforceliveagent.com/chat',
				eswLiveAgentDevName: 'Avo_Chat_Team_Case_02',
				isOfflineSupportEnabled: false
			}
		);
	};

	if (!window.embedded_svc) {
		var s = document.createElement('script');
		s.setAttribute('src', 'https://ap24.salesforce.com/embeddedservice/5.0/esw.min.js');
		s.onload = function() {
			initESW(null);
		};
		document.body.appendChild(s);
	} else {
		initESW('https://service.force.com');
	}
</script>