<div class="menu-layer"><!--<a href="#"></a>--></div>
<div class="menu-sidebar">
        <div class="menu-logo">
                <a href="<?php echo home_url('/');?>">
                        <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" />
                </a>
        </div><!-- end of menu logo -->
        <div class="menu-mobile">
                <?php
                        $args= [
                                'theme_location'	=> 'primary_mobile',
                                'container'		=> false,
                                'menu_class'		=> '',
                                'walker' => new avoskin_nav_walker_mobile(),
                        ];
                        wp_nav_menu($args);
                ?>
        </div><!-- end of menu mobile -->
        <div class="util">
                <div class="location">
                        <?php $code = (ICL_LANGUAGE_CODE == 'en') ? '' : 'id_';?>
                        <a href="<?php avoskin_get_page($code.'avostore');?>"><span><?php _e('Avostore','avoskin');?></span></a>
                </div><!-- end of location-->
                <?php if(!is_user_logged_in()):
                        $track = get_theme_mod('avoskin_'.$code.'tracking_form_page');
                ?>
                        <div class="track">
                                <a href="<?php echo get_permalink($track);?>"><?php _e('Track Order','avoskin');?></a>
                        </div><!-- end of track -->
                        <div class="credential">
                                <a href="#tab-login"><?php _e('Login','avoskin');?></a>
                                <a href="#tab-register"><?php _e('Register','avoskin');?></a>
                        </div><!-- end of credential -->
                <?php else:?>
                        <div class="user-nav">
                                <span><?php _e('Welcome','avoskin');?> <?php echo get_user_meta(get_current_user_id(), 'first_name', true) ;?></span>
                                <ul>
                                        <?php
                                                $args= [
                                                        'theme_location'	=> 'account_login',
                                                        'container'		=> false,
                                                        'menu_class'		=> '',
                                                        'items_wrap' => '%3$s' 
                                                ];
                                                wp_nav_menu($args);
                                        ?>
                                        <li class="logout"><a href="<?php echo wp_logout_url( home_url('/') ); ?>"><?php _e('Logout','avoskin');?></a></li>
                                </ul>
                        </div>
                <?php endif;?>
        </div><!-- end of util -->
</div><!-- end of menu sidebar -->