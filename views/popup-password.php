<div class="popup-password">
        <div class="layer">
                <a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
                <div class="txt">
                        <h2><?php _e('Change Password','avoskin');?></h2>
                </div><!-- end of txt --><br/>
                <div class="form-basic">
                        <form id="update-password">
                                <div class="fgroup">
                                        <label><?php _e('Old Password','avoskin');?></label>
                                        <div class="pass-field">
                                                <input type="password" name="old_password" value="" required="required" autocomplete="current-password"  />
                                                <b></b>
                                        </div><!-- end of pass field -->
                                </div>
                                <div class="fgroup">
                                        <label><?php _e('New Password','avoskin');?></label>
                                        <div class="pass-field">
                                                <input type="password" name="new_password" value="" required="required" autocomplete="current-password"  />
                                                <b></b>
                                        </div><!-- end of pass field -->
                                </div>
                                <div class="fgroup">
                                        <label><?php _e('Confirm New Pasword','avoskin');?></label>
                                        <div class="pass-field">
                                                <input type="password" name="new_password_confirm" value="" required="required" autocomplete="current-password"  />
                                                <b></b>
                                        </div><!-- end of pass field -->
                                </div>
                                <input type="hidden" name="user" value="<?php echo avoskin_user_data();?>" />
                                <button type="submit" class="button btn-fullwidth slimy has-loading"><?php _e('Update','avoskin');?></button>
                        </form>
                </div><!-- end of form basic -->
        </div><!-- end of layer -->
</div><!-- end o popup password -->