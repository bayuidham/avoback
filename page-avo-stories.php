<?php
/*
Template Name: Page Avo Stories
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-avo-stories">
                <?php if(have_posts()) : while(have_posts()) : the_post();
                        $meta = [
                                'title' => 'intro_title',
                                'text' => 'intro_text',
                                'slide' => 'slide_item',
                        ];
                        extract(avoskin_get_meta(get_the_ID(), $meta));
                ?>
                        <div class="section-title centered">
                                <div class="wrapper">
                                        <div class="pusher">
                                                <h2 class="line-title"><?php echo $title ;?></h2>
                                                <?php echo $text ;?>
                                        </div><!-- end of pusher -->
                                </div><!-- end of wrapper -->
                        </div><!-- end of section title -->
                        
                        <?php if(is_array($slide) && !empty($slide)):?>
                                <div class="slide">
                                        <?php if(count($slide) > 3):?>
                                                <div class="slidenav">
                                                        <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
                                                        <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
                                                </div><!-- end of slidenav -->
                                        <?php endif;?>
                                        <div class="wrapper">
                                                <div class="slick-carousel">
                                                        <?php foreach($slide as $s):
                                                                $image = avoskin_resize( $s['thumb'], 380, 260, true, true, true ); //resize & crop img
                                                        ?>
                                                                <div class="item">
                                                                        <a href="<?php echo $s['url'] ;?>">
                                                                                <figure>
                                                                                        <img src="<?php echo $image ;?>" alt="<?php the_title_attribute();?>" />
                                                                                        <figcaption><?php echo $s['title'] ;?></figcaption>
                                                                                </figure>
                                                                        </a>
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div><!-- end of slick carousel -->
                                        </div><!-- end of wrapper -->
                                </div><!-- end of slide -->
                        <?php endif;?>
                
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
                
        </div><!-- end of inner avo-stories -->
<?php get_footer();?>