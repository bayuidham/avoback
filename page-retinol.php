<?php
/*
Template Name: Page Miraculous Retinol Series
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        
        <div class="inner-retinol">
                
                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                
                        <?php
                                /*
                               * Functions hooked into avoskin_retinol action
                               * @hooked avoskin_banner_retinol          5
                               * @hooked avoskin_video_retinol              10
                               * @hooked avoskin_copy_retinol               15
                               * @hooked avoskin_benefit_retinol          20
                               * @hooked avoskin_action_retinol            25
                               */
                               do_action('avoskin_retinol');
                       ?>
                
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text centered">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
                
        </div><!-- endof inner retinol -->
        
<?php get_footer();?>