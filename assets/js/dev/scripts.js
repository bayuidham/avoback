;(function( $ ) {
	'use strict'
	
	window.avoskinApp = {
		el: {
			window : $(window),
                        document : $(document)
		},
		fn: {
			alert : function(data, timer = 300){
				$.alert({
					title: data.title,
					content: data.content,
					animateFromElement: false,
					buttons: {close: {btnClass: 'btn-hide',action: function(){}}},
					autoClose: "close|"+timer+"s",
					backgroundDismiss : true,
					animation: 'scale',
					closeAnimation: 'zoom',
					animationBounce:1
				});
			},
			equalHeight: function(container){
				var currentTallest = 0,
					currentRowStart = 0,
					rowDivs = new Array(),
					currentDiv,
					$el,
					topPosition = 0;
				$(container).each(function() {
			       
					$el = $(this);
					$($el).height('auto');
					topPosition = $el.position().top;
				     
					if (currentRowStart != topPosition) {
						for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
						  rowDivs[currentDiv].height(currentTallest);
						}
						rowDivs.length = 0; // empty the array
						currentRowStart = topPosition;
						currentTallest = $el.height();
						rowDivs.push($el);
					} else {
						rowDivs.push($el);
						currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
					}
					for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
						rowDivs[currentDiv].height(currentTallest);
					}
				});
			},
			normalizeHeight: function(){
				setTimeout(function(){
					avoskinApp.fn.equalHeight('.inner-home .flash-sale .layer');
					avoskinApp.fn.equalHeight('.inner-product .equal');
				},300);
			},
			imgSVG: function(){
				$("img.svg").each(function () {
					// Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
					var $img = $(this);
					// Get all the attributes.
					var attributes = $img.prop("attributes");
					// Get the image's URL.
					var imgURL = $img.attr("src");
					// Fire an AJAX GET request to the URL.
					$.get(imgURL, function (data) {
						// The data you get includes the document type definition, which we don't need.
						// We are only interested in the <svg> tag inside that.
						var $svg = $(data).find('svg');
						// Remove any invalid XML tags as per http://validator.w3.org
						$svg = $svg.removeAttr('xmlns:a');
						// Loop through original image's attributes and apply on SVG
						$.each(attributes, function() {
							$svg.attr(this.name, this.value);
						});
						// Replace image with new SVG
						$img.replaceWith($svg);
					});
				});
			},
			currency: function(){
				numeral.register('locale', 'id', {
					delimiters: {
						thousands: '.',
						decimal: ','
					},
					abbreviations: {
						thousand: 'k',
						million: 'm',
						billion: 'b',
						trillion: 't'
					},
					currency: {
						symbol: 'Rp '
					}
				});
				return numeral;
			},
			calcCart: function(parent, target, currency){
				var ancestor = parent.closest('tr'),
					price = currency(ancestor.find('.price').html()).value(),
					total = ancestor.find('.total b');
					
				parent.closest('.summary').addClass('updated');
				
				total.html(currency(parseInt(target.html()) * parseInt(price)).format('$ 0,0'));
				
			},
			textCut: function(elem, height){
				$(elem).dotdotdot({
					ellipsis: "...",
					height: height,
					truncate: "letter"
				});
			},
			sendRequest: function(form, route, timer = 1000, redirect = true){
				var button = form.find('.button'),
					data = form.serialize();
				
				if (!button.hasClass('fetching')) {
					button.addClass('fetching');
					axios.post( avoskin_data.api_url + route +"/", {data:data} ).then( response => {
						button.removeClass('fetching');
						
						avoskinApp.fn.alert({
							"title": response.data.title,
							"content": response.data.msg
						},timer);
						
						if (response.data.status == 'ok') {
							setTimeout(function(){
								if (redirect) {
									window.location.href = response.data.redirect;
								}else{
									window.location.reload();	
								}
							}, 2000);
						}
					}).catch( error => console.log( error ) );
				}
			},
			isEmail: function(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				return regex.test(email);
			},
			recordReferral: function(referral, visitor){
				axios.post( avoskin_data.api_url + "record_referral/", {referral: referral, visitor: visitor}  ).then( response => {}).catch( error => console.log( error ) );
			},
			headerGap: function(){
				if ($('#top').hasClass('has-popup')) {
					var top = $('.popup-info').outerHeight(),
						style = '<style type="text/css" id="inject-style">#top.has-popup .is-sticky .bot-head{top:'+top+'px !important}</style>';
						
					$('#top').css({paddingTop: top});
					$('#inject-style').remove();
					$(style).appendTo('head');
				}
			},
                        stickyHalfSidebar: function(){
                                var topSpacing = 50,
                                        bottomSpacing =  $('#bottom').outerHeight() + 100;
                                
                                if ($(window).width() < 1000) {
                                        topSpacing += 60;
                                }else{
                                        topSpacing += $('#top .bot-head').outerHeight() 
                                }
                                
                                if ($('#top').hasClass('has-popup')) {
                                        topSpacing += $('.popup-info').outerHeight();
                                }
                                
                                $(".inner-page-half .navside ul").unstick();
                                $(".inner-page-half .navside ul").sticky({
                                        topSpacing: topSpacing,
                                        bottomSpacing: bottomSpacing,
                                });
                        }
		},
		run: function(){
                        //WINDOW LOAD
			avoskinApp.el.window.on('load',function(){
				avoskinApp.fn.normalizeHeight();
				avoskinApp.fn.textCut('.section-blog .item h3', 60);
				avoskinApp.fn.textCut('.section-blog .item .txt p', 120);
				avoskinApp.fn.headerGap();
				
				$('<a href="#" class="dropmobile"><i class="fa-angle-right"></i></a>').appendTo('.menu-mobile ul li.menu-item-has-children');
				
				$('.menu-mobile > ul > li.menu-item-has-children').each(function(){
					var self = $(this),
						target = self.find('.sub-holder h4 b'),
						src = self.find(' > a:first-child').html();
						
					target.html(src);
				});
				
			});
			
			//WINDOW RESIZE
			avoskinApp.el.window.on('resize',function(){
				avoskinApp.fn.normalizeHeight();
				setTimeout(function(){
                                        avoskinApp.fn.stickyHalfSidebar();
					avoskinApp.fn.headerGap();
				}, 200);
			});
			
			//WINDOW READY
                        avoskinApp.el.document.ready(function(){
				avoskinApp.fn.imgSVG();
				
				var currency = avoskinApp.fn.currency();
				currency.locale('id');
				
				$("#top .bot-head").sticky({topSpacing:0})
                                avoskinApp.fn.stickyHalfSidebar();
                                
				$('.popup-info .cls').on('click', function(e){
					e.preventDefault();
					$('#top').css({paddingTop: 0});
					$('#inject-style').remove();
					$(this).closest('.popup-info').slideUp(200);
					$('#top').removeClass('has-popup');
                                        avoskinApp.fn.stickyHalfSidebar(true);
				});
				
				$('#top .account > a').on('click', function(e){
					e.preventDefault();
					$(this).closest('.account').toggleClass('active');
				});
				
				$('.bot-head .util .cart > a').on('click', function(e){
					e.preventDefault();
					$('body').addClass('open-cart');
				});
				
				$(document).on('click','.cart-sidebar h3 a, .cart-layer', function(e){
					e.preventDefault();
					$('body').removeClass('open-cart');
				});
				
				$(document).on('click','.cart-sidebar .item .rmv', function(e){ 
					e.preventDefault();
					var self = $(this),
						parent = self.closest('.item'),
						ancestor = self.closest('.cart-sidebar'),
						subtotal = ancestor.find('.info .subtotal > strong'),
						count = $('.bot-head .util .cart > a b');
					
					if (!parent.hasClass('fetching')) {
						parent.addClass('fetching');
						axios.post( avoskin_data.api_url + "remove_cart_item/", {id:self.attr('data-id')} ).then( response => {
							subtotal.html(response.data.subtotal);
							count.html(response.data.count);
							parent.remove();
							if ($('body').hasClass('page-template-page-cart') || $('body').hasClass('page-template-page-checkout')) {
								window.location.reload();
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				$('.totop').on('click', function(e){
					e.preventDefault();
					$('html, body').animate({
						scrollTop: $('#top').offset().top
					}, 500);
				});
				
				$('.accord-item .acc-head').on('click', function(){
					var self = $(this),
						parent = self.closest('.accord-item'),
						target = parent.find('.acc-body');
					
					target.stop().slideToggle('fast', function(){
						parent.toggleClass('expanded');
					});
				});
				
				$('.form-basic .pass-field b').on('click', function(){
					var self = $(this),
                                                target = self.closest('.pass-field').find('input');
					
					if (target.attr('type') == 'password') {
						target.attr('type', 'text');
                                                self.addClass('merem');
					}else{
						target.attr('type', 'password');
                                                self.removeClass('merem');
					}
				});
				
				$('.popup-credential .extra a').click(function(e){
					var self = $(this),
						parent = self.closest('.popup-credential'),
						item = parent.find('.active').attr('id');
					
					if (item != 'tab-forgot') {
						e.preventDefault();
						parent.find('.item').removeClass('active');
						if (item =='tab-login') {
							parent.find('#tab-register').addClass('active');
						}else if (item == 'tab-register' ) {
							parent.find('#tab-login').addClass('active');
						}
						parent.removeClass('to-forgot');
					}
					
				});
				
				$('#top .account li a, .inner-checkout .txt a, .menu-sidebar .credential a').on('click', function(e){
					var self = $(this);
					
					if (self.attr('href') == '#tab-login' || self.attr('href') == '#tab-register') {
						e.preventDefault();
						$('.popup-credential .item').removeClass('active');
						$('.popup-credential').find(self.attr('href')).addClass('active');
						$('body').addClass('open-cred');
					}
				});
				
				$('.popup-credential .layer .cls').on('click', function(e){
					e.preventDefault()
					$('body').removeClass('open-cred');
				});
				
				$('#tab-login .forgot').on('click', function(e){
					e.preventDefault();
					$('.popup-credential .item').removeClass('active');
					$('#tab-forgot').addClass('active');
					$('.popup-credential').addClass('to-forgot');
				});
				
				$('.inner-avo-stories .slide .slick-carousel').slick({
					infinite: true,
					slidesToShow: 3,
					slidesToScroll: 1,
					dots: false,
					prevArrow: $('.inner-avo-stories .slide .slidenav .prev'),
					nextArrow: $('.inner-avo-stories .slide .slidenav .next'),
					responsive: [
						{
							breakpoint: 768,
							settings: {
								arrows: false,
								dots: true,
								slidesToShow: 1
							}
						}
					]
				});
                                
                                $('.inner-lady-boss .slide .slick-carousel').slick({
					infinite: true,
					slidesToShow: 3,
					slidesToScroll: 1,
					dots: true,
					arrows: false,
					responsive: [
                                                {
							breakpoint: 960,
							settings: {
								arrows: false,
								dots: true,
								slidesToShow: 2
							}
						},
						{
							breakpoint: 768,
							settings: {
								arrows: false,
								dots: true,
								slidesToShow: 1
							}
						}
					]
				});
				
				$('.tab-basic .tab-head a').on('click', function(e){
					e.preventDefault();
					var self = $(this),
						parent = self.closest('.tab-basic');
					
					parent.find('.tab-head a').removeClass('active');
					parent.find('.tab-item').removeClass('active');
					
					self.addClass('active');
					$(self.attr('href')).addClass('active');
				});
				
				$('.detail-product .desc .tab-head .dropselect select').on('change', function(){
					var self = $(this),
						parent = self.closest('.tab-basic');
					
					parent.find('.tab-item').removeClass('active');
					$(self.val()).addClass('active');
				});
				
				//Countdown
				if ($('.timing').length) {
					$('.timing').each(function(){
						var sender = $(this),
                                                        theTime =  moment.tz(sender.attr('data-time'), "Asia/Jakarta");
						 sender.countdown(theTime.toDate()).on('update.countdown', function(event) {
							var $this = $(this).html(event.strftime(''
								+ '<span><strong>%H</strong> hour :</span>'
								+ '<span><strong>%M</strong> minute :</span>'
								+ '<span><strong>%S</strong> second</span>'));
						}).on('finish.countdown',function(event){
							window.location.href = sender.attr('data-order');
						});
					});
				}
				
				if ($('.ftime').length) {
					$('.ftime').each(function(){
						var sender = $(this);
						 $(this).countdown($(this).attr('data-time')).on('update.countdown', function(event) {
							var $this = $(this).html(event.strftime(''
								+ '<span><strong>%D</strong> :</span> '
								+ '<span><strong>%H</strong> :</span> '
								+ '<span><strong>%M</strong> :</span> '
								+ '<span><strong>%S</strong></span>'));
						}).on('finish.countdown',function(event){
							sender.closest('.flash-sale').remove();
						});
					});
				}
				
				$('.calc a').on('click', function(e){
					e.preventDefault();
					var self = $(this),
						parent = self.closest('.calc'),
						target = parent.find('span'),
						targetValue = parseInt(target.html());
						
					if (self.hasClass('min') && targetValue != 1) {
						target.html(targetValue - 1);
					}else if (self.hasClass('plus')) {
						target.html(targetValue + 1);
					}
					
					
					//SINGLE PRODUCT
					if ($('.detail-product').length) {
						$('.detail-product .info .util .act-add-to-cart').attr('data-quantity', target.html());
                                                $('.sticky-product .act-add-to-cart').attr('data-quantity', target.html());
                                                $('.sticky-product .act-buy-now').attr('href', $('.sticky-product .act-buy-now').attr('data-url') + target.html());
                                                $('.sticky-product .act-buy-now').attr('data-qty',target.html());
                                                
                                                if (parent.hasClass('on-sticky')) {
                                                        $('.detail-product .calc.calc-hollow > span').html(target.html());
                                                }else{
                                                        $('.sticky-product .calc.calc-hollow > span').html(target.html());
                                                }
                                                
					}
					
					//PAGE CART
					if ($('.inner-cart').length) {
						avoskinApp.fn.calcCart(parent, target, currency);
					}
					
				});
                                
                                $('.sticky-product .act-buy-now').on('click', function(e){
                                        if (parseInt($(this).attr('data-qty')) > parseInt($(this).attr('data-stock'))) {
                                                e.preventDefault();
                                                avoskinApp.fn.alert({
                                                        "title": "Info",
                                                        "content": "Stock product not available!"
                                                },1000);
                                        }
                                });
				
				$('.inner-cart .summary table .rmv').on('click', function(e){ 
					e.preventDefault();
					var self = $(this),
						parent = self.closest('tr');
					
					parent.closest('.summary').addClass('updated');
					parent.remove();
					if (!$('.inner-cart .summary table tbody tr').length) {
						axios.post( avoskin_data.api_url + "emptying_cart/" ).then( response => {
							window.location.reload();
						});
					}
				});
				
				$('.inner-cart .summary .action .update-cart').on('click', function(e){
					e.preventDefault();
					var self = $(this),
						ancestor = self.closest('.summary'),
						table = ancestor.find('table'),
						data = [];
						
					table.find('tbody tr').each(function(){
						data.push({
							'product' : $(this).find('.calc span').attr('data-id'),
							'quantity' : $(this).find('.calc span').html()
						});
					});
					
					if (data.length && !self.hasClass('fetching')) {
						self.addClass('fetching');
						axios.post( avoskin_data.api_url + "update_cart_items/", {data:data} ).then( response => {
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},1000);
							setTimeout(function(){
								if (response.data.status == 'ok') {
									window.location.reload();
								}
							},1500);
						}).catch( error => console.log( error ) );
					}
				});
				
				$('.account-wishlist .rmv').on('click', function(e){ 
					e.preventDefault();
					var self = $(this),
						parent = self.closest('tr');
					
					if (!self.hasClass('fetching')) {
						self.addClass('fetching');
						var data = {
							product: self.attr('data-product'),
							user: avoskin_data.user
						};
						axios.post( avoskin_data.api_url + "add_to_wish/", {data:data} ).then( response => {
							self.removeClass('fetching');
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},500);
							parent.remove();
							if (!$('.account-wishlist .table-basic tbody tr').length) {
								window.location.reload();
							}
						}).catch( error => console.log( error ) );
					}
				});
				
				$('.scrollbar-inner').scrollbar();
				
				avoskinApp.fn.textCut('.widget_product h4', 20);
				
				$('.detail-product .gallery .big .slick-carousel').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					fade: true,
					asNavFor: '.detail-product .gallery .thumb .slick-carousel'
				});
				
				$('.detail-product .gallery .thumb .slick-carousel').slick({
					slidesToShow: 4,
					slidesToScroll: 1,
					asNavFor: '.detail-product .gallery .big .slick-carousel',
					dots: false,
					centerMode: false,
					arrows: false,
					focusOnSelect: true
				});
				
				$('.popup-review .layer .cls').on('click', function(e){
					e.preventDefault()
					$('body').removeClass('open-review');
				});
				
				$('a[href="#popup-review"]').on('click', function(e){
					e.preventDefault();
					if (avoskin_data.user != 'nope') {
						$('body').addClass('open-review');
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.login_alert.title,
							"content": avoskin_data.login_alert.msg
						},1500);	
					}
				});
				
				$('.popup-review form').on('submit', function(e){ 
					e.preventDefault();
					var self = $(this),
						button = self.find('.button'),
						data = self.serialize();
						
					if (!button.hasClass('fetching')) {
						button.addClass('fetching');
						axios.post( avoskin_data.api_url + "submit_review/", {data:data, user: avoskin_data.user}  ).then( response => {
							button.removeClass('fetching');
							avoskinApp.fn.alert({
								"title": response.data.title,
								"content": response.data.msg
							},1000);
							setTimeout(function(){
								$('body').removeClass('open-review');
							}, 1500);
						}).catch( error => console.log( error ) );
					}
				})
				
				$('#review-skin-type select, #review-skin-type-mobile select').on('change', function(){
					$(this).closest('form').trigger('submit');
				});
				
				
				$('.inner-home .hero .slick-carousel').slick({
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false,
					prevArrow: $('.inner-home .hero .slidenav .prev'),
					nextArrow: $('.inner-home .hero .slidenav .next'),
					responsive: [
						{
							breakpoint: 768,
							settings: {
								dots: true
							}
						}
					]
				});
				
				$('.inner-home .testimonial .said .slick-carousel').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					fade: true,
					asNavFor: '.inner-home .testimonial .from .slick-carousel',
					infinite: false,
					draggable: false
				});
				
				$('.inner-home .testimonial .from .slick-carousel').slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					asNavFor: '.inner-home .testimonial .said .slick-carousel',
					dots: false,
					centerMode: false,
					arrows: false,
					focusOnSelect: true,
					infinite: false,
					draggable: false,
					responsive: [
						{
							breakpoint: 768,
							settings: {
								slidesToShow: 5,
								slidesToScroll: 5,
								variableWidth: true,
								infinite: true,
								draggable: true,
							}
						}
					]
				});
				
				$('.inner-home .flash-sale .slick-carousel').slick({
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 1,
					dots: false,
					prevArrow: $('.inner-home .flash-sale .slidenav .prev'),
					nextArrow: $('.inner-home .flash-sale .slidenav .next'),
					responsive: [
						{
							breakpoint: 1259,
							settings: {
								slidesToShow: 4
							}
						},
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 3
							}
						},
						{
							breakpoint: 768,
							settings: {
								arrows: false,
								slidesToShow: 3,
								variableWidth: true,
							}
						}
					]
				});
				
				$('.section-blog .slick-carousel').slick({
					infinite: true,
					slidesToShow: 3,
					slidesToScroll: 1,
					dots: false,
					prevArrow: $('.section-blog .slidenav .prev'),
					nextArrow: $('.section-blog .slidenav .next'),
					responsive: [
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 2
							}
						},
						{
							breakpoint: 768,
							settings: {
								arrows: false,
								slidesToShow: 1,
								dots: true
							}
						},
					]
				});
				
				$('.button.disabled').on('click', function(e){
					e.preventDefault();
				});
				
				//LOGIN
				$('#form-popup-login').on('submit', function(e){
					e.preventDefault();
					var reload = $(this).hasClass('reload') ? false : true;
					avoskinApp.fn.sendRequest($(this), 'user_login', 2000, reload);
				});
				
				//REGISTER
				$('#form-popup-register').on('submit', function(e){
					e.preventDefault();
					avoskinApp.fn.sendRequest($(this), 'user_register', 2000, false);
				});
				
				//FORGOT PASSWORD
				$('#form-popup-forgot-password').on('submit', function(e){
					e.preventDefault();
					avoskinApp.fn.sendRequest($(this), 'user_forgot_password', 2000, false);
				});
				
				$('select.perpage').on('change', function(){
					$(this).closest('form').trigger('submit');
				});
				
				$(document).on('click','.act-add-to-cart', function(e){
					e.preventDefault();
					var self = $(this),
						cart = $('.cart-sidebar'),
						count = $('.bot-head .util .cart > a b'),
                                                stock = self.attr('data-stock'),
                                                quantity = self.attr('data-quantity');
					
					if (parseInt(quantity) > parseInt(stock)) {
                                                avoskinApp.fn.alert({
                                                        "title": avoskin_data.nostock.title,
                                                        "content": avoskin_data.nostock.msg
                                                },300);
                                        }else{
                                                if (!self.hasClass('fetching')) {
                                                        self.addClass('fetching');
                                                        axios.post( avoskin_data.api_url + "add_to_cart/", {id:self.attr('data-id'), quantity:quantity} ).then( response => {
                                                                self.removeClass('fetching');
                                                                if (response.data.status == 'ok') {
                                                                        avoskinApp.fn.alert({
                                                                                "title": response.data.title,
                                                                                "content": response.data.msg
                                                                        }, 300 );
                                                                        cart.html(response.data.cart);
                                                                        count.html(response.data.count);
                                                                }else{
                                                                        avoskinApp.fn.alert({
                                                                                "title": avoskin_data.nostock.title,
                                                                                "content": avoskin_data.nostock.msg
                                                                        },300);
                                                                }
                                                        }).catch( error => console.log( error ) );
                                                }
                                        }
				});
				
				var delay = 2000;
				if ($('.popup-newsletter').length) {
					delay = parseInt($('.popup-newsletter').attr('data-delay')) * 1000;
					setTimeout(function(){	
						$('body').addClass('open-newsletter');
					},delay);
				}
				
				$('.popup-newsletter .layer .cls').on('click', function(e){
					e.preventDefault()
					$('body').removeClass('open-newsletter');
				});
				
				$('.popup-review .boxcheck input[type="checkbox"]').on('click', function(){
					var self = $(this),
						parent = self.closest('.boxcheck');
						
					if (!self.prop('checked') && !parent.find('input[type="checkbox"]:checked').length ) {
						self.prop("checked", true);
					}
				});
				
				$(document).on('click','.add-to-wish', function(e){
					e.preventDefault();
					var self = $(this);
					
					if (avoskin_data.user != 'nope') {
						var data = {
							product: self.attr('data-product'),
							user: avoskin_data.user
						};
						if (!self.hasClass('fetching')) {
							self.addClass('fetching');
							axios.post( avoskin_data.api_url + "add_to_wish/", {data:data}).then( response => {
								self.removeClass('fetching');
								avoskinApp.fn.alert({
									"title": response.data.title,
									"content": response.data.msg
								},500);
								
								if (response.data.action == 'added') {
									self.addClass('onlist');
                                                                        $('a[data-id="'+self.attr('data-id')+'"]').addClass('onlist');
								}else{
									self.removeClass('onlist');
                                                                        $('a[data-id="'+self.attr('data-id')+'"]').removeClass('onlist');
								}
								
							}).catch( error => console.log( error ) );
						}
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.wishlist_alert.title,
							"content": avoskin_data.wishlist_alert.msg
						},1200);
					}
				});
				
				if (avoskin_data.user != 'nope' && !$.isEmptyObject(avoskin_data.wishlist)) {
					$.each(avoskin_data.wishlist, function(i, v){
						$('.add-to-wish[data-product="'+v+'"]').addClass('onlist');
					});
				}
				
				$('.optin form, .popup-newsletter form').on('submit', function(e){
					e.preventDefault();
					var self = $(this),
						button = self.find('.button'),
						email = self.find('input[type="email"]'),
						data = self.serialize();
					
					if (email.val() != '') {
						if (!avoskinApp.fn.isEmail(email.val())) {
							avoskinApp.fn.alert({
								"title": avoskin_data.optin.title.notice,
								"content": avoskin_data.optin.msg.invalid
							},800);
						}else{
							if (!button.hasClass('fetching')) {
								button.addClass('fetching');
								axios.post( avoskin_data.api_url + "subscribe_optin/", {data:data} ).then( response => {
                                                                        button.removeClass('fetching');
                                                                        if (response.data.status == 'ok') {
                                                                                $('body').removeClass('open-newsletter');
                                                                                avoskinApp.fn.alert({
                                                                                        "title": avoskin_data.optin.title.success,
                                                                                        "content": avoskin_data.optin.msg.success
                                                                                },800);
                                                                        }else{
                                                                                avoskinApp.fn.alert({
                                                                                        "title": response.data.title,
                                                                                        "content": response.data.msg
                                                                                },800);
                                                                        }
                                                                }).catch( error => console.log( error ) );
							}
						}
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.optin.title.notice,
							"content": avoskin_data.optin.msg.empty
						},800);
					}
				});
				
				setTimeout(function(){
					if (avoskin_data.ref != '') {
						Cookies.set('avoskin_referral', avoskin_data.ref, { expires: 1 });
						avoskinApp.fn.recordReferral(avoskin_data.ref, avoskin_data.visitor);
					}else{
						var hasRef =  Cookies.get('avoskin_referral');
						if(typeof hasRef !== "undefined" && hasRef !== ''){
							avoskinApp.fn.recordReferral(hasRef, avoskin_data.visitor);
						}
					}
                                        
                                        if (avoskin_data.avosbrf != 'nope') {
                                                Cookies.set('avosbrf', avoskin_data.avosbrf, { expires: 1 });
                                        }
				}, 300);
				
				
				$('.mobile-menu-trigger').on('click', function(e){
					e.preventDefault();
					$('body').addClass('open-menuside');
				});
				$('.menu-layer a, .menu-layer').on('click', function(e){
					e.preventDefault();
					$('body').removeClass('open-menuside');
					$('.menu-mobile li').removeClass('expanded');
				});
				
				$(document).on('click', '.menu-mobile li .dropmobile', function(e){
					e.preventDefault();
					$(this).closest('li').addClass('expanded');
				});
				
				$(document).on('click', '.menu-mobile .sub-holder h4 a', function(e){
					e.preventDefault();
					$(this).closest('li.expanded').removeClass('expanded');
				});
				
				$('.inner-page-half .navside .dropselect select').on('change', function(){
					window.location.href = $(this).val();
				});
				
                                //POPPUP SEARCH
                                $('.bot-head .util .search a').on('click', function(e){
                                        e.preventDefault();
                                        $('body').addClass('open-search');
                                });
                                $('.popup-search .cls').on('click', function(e){
                                        e.preventDefault();
                                        $('body').removeClass('open-search');
                                });
                                
                                //POPPUP WAITLIST
                                $('a.add-to-waitlist').on('click', function(e){
                                        e.preventDefault();
                                        $('body').addClass('open-waitlist');
                                });
                                $('.popup-waitlist .cls').on('click', function(e){
                                        e.preventDefault();
                                        $('body').removeClass('open-waitlist');
                                });
                                
			});
			
			//WINDOW SCROLL
			avoskinApp.el.window.on('scroll',function(){
				// ::CRSTICKY::
                                if ($('.detail-product .info .util').length) {
                                        
                                        var $window = $(window),
                                                viewport_top = $window.scrollTop(),
                                                viewport_height = $window.height(),
                                                viewport_bottom = viewport_top + viewport_height,
                                                $elem = $('.detail-product .info .util'),
                                                top = $elem.offset().top,
                                                height = $elem.height(),
                                                bottom = top + height;
                                
                                        if( viewport_top > bottom ){
                                                $('body').addClass('open-sticky-product');
                                        }else{
                                                $('body').removeClass('open-sticky-product');
                                        }
                                }				
			});
		}
	}
	
	avoskinApp.run();
	
}(jQuery));