;(function($){
        'use strict'
        
        function avocalc(){
                if(window.matchMedia("only screen and (max-width: 768px)").matches){
                        $('.inner-bae .person .cat-holder').each(function(){
                                var width = 0,
                                        self = $(this);
                                
                                self.find('a').each(function(){
                                        width += $(this).outerWidth() + 30;
                                });
                                
                                self.find('.xwrap').css({width:width});
                        });        
                }else{
                        $('.inner-bae .person .cat-holder .xwrap').css({width:'auto'});
                }
        }
        $(window).on('load', function(){
                var botspace = $('.inner-bae').outerHeight() - ($('.inner-bae .intro').outerHeight() + $('.inner-bae .variant').outerHeight()) + 280;
                
                $(".inner-bae .intro figure .forstick").sticky({
                        topSpacing:200,
                        bottomSpacing: botspace
                });
                
                
                avocalc();
                
                $('.detail-bae .cta a').on('click', function(e){
                        e.preventDefault();
                        $('.qcw-trigger-btn').trigger('click');
                });
                
                $('.inner-home .hero').removeClass('unloaded');
        });
        
        $(window).on('resize', function(){
                avocalc();
        });
        $(document).ready(function(){
                $('.inner-bae .theprod .slide').each(function(){
                        var self = $(this),
                                slide = self.attr('data-slide'),
                                dots = $('.inner-bae .theprod .dots-'+slide);
                        
                        self.find('.slick-carousel').slick({
                                infinite: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                dots: false,
                                arrows:true,
                                fade: true,
                                prevArrow: self.find('.slidenav .prev'),
                                nextArrow: self.find('.slidenav .next'),
                        });
                        
                        
                        // On before slide change
                        self.find('.slick-carousel').on('beforeChange',  function(event, slick, currentSlide, nextSlide){
                                
                                dots.each(function(){
                                        $(this).find('a').removeClass('active');
                                        $(this).find('a').eq(nextSlide).addClass('active');        
                                });
                                
                        });
                        
                        dots.each(function(){
                                $(this).find('a').on('click', function(e){
                                        e.preventDefault();
                                        self.find('.slick-carousel').slick('slickGoTo', $(this).index());
                                });
                        })
                        
                });
                
                $('.inner-bae .theprod .slide-dots .nav a').on('click', function(e){
                        e.preventDefault();
                        var self = $(this),
                                slide = self.attr('data-dot');
                                
                        if (!self.hasClass('active')) {
                                $('.inner-bae .theprod .slide-dots .dots').removeClass('active');
                                $('.inner-bae .theprod .slide-dots .nav a').removeClass('active');
                                $('.inner-bae .theprod .slide').removeClass('active');
                                
                                $('a[data-dot="'+slide+'"]').addClass('active');
                                $('#'+slide).css({opacity:'0'});
                                $('#'+slide).addClass('active');
                                $('.inner-bae .theprod .slide-dots .dots-'+slide).css({opacity:'0'});
                                $('.inner-bae .theprod .slide-dots .dots-'+slide).addClass('active');
                                $('.inner-bae .theprod .slide-dots .dots-'+slide).find('a:last-child').trigger('click');
                                setTimeout(function(){
                                        $('.inner-bae .theprod .slide-dots .dots-'+slide).find('a:first-child').trigger('click');
                                        $('.inner-bae .theprod .slide-dots .dots-'+slide).css({opacity:'1'});
                                        $('#'+slide).css({opacity:'1'});
                                },600);
                        }
                });
                
                $('.inner-bae .ingre .slide').each(function(){
                        var self = $(this);
                        
                         self.find('.slick-carousel').slick({
                                infinite: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                dots: false,
                                arrows:true,
                                fade: true,
                                prevArrow: self.find('.slidenav .prev'),
                                nextArrow: self.find('.slidenav .next'),
                        });
                });
                $(".inner-bae .navi").sticky({topSpacing:0})
                
                $('.inner-bae .navi .wrapper > a').on('click',function(e){
                        e.preventDefault();
                        $('html, body').animate({
                                scrollTop: $('#top').offset().top
                        }, 500);
                });
                
                   $('.inner-bae .proof').each(function(){
                        var self = $(this);
                        
                        self.find('.slick-carousel').slick({
                                infinite: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                dots: (parseInt(self.attr('data-count') > 1)) ? true : false,
                                arrows:false,
                                speed: 800,
                                fade: true,
                        });
                        
                });
                   
                $('.inner-bae .person .cat-holder a').on('click', function(e){
                        e.preventDefault();
                        var self = $(this),
                                parent = self.closest('.cat-holder'),
                                ances = parent.closest('.person'),
                                target = ances.find('.list.rowflex'),
                                product = [];
                        
                        $.each(parent.data('item'), function(k,v){
                                product.push(v.product);
                        });
                        
                        if (!ances.hasClass('fetching')) {
                                ances.addClass('fetching');
                                axios.post( avoskin_data.api_url + "skinbae_product/", {cat:self.attr('data-cat'), product: product} ).then( response => {
                                        target.html(response.data);
                                        $('.inner-bae .person .cat-holder a').removeClass('active');
                                        self.addClass('active');
                                        ances.removeClass('fetching');
                                }).catch( error => console.log( error ) );
                        }
                });
                
                $('.inner-thank .copy-input a, .popup-order .copy-input a').on('click', function(e){
                        e.preventDefault();
                        var self = $(this),
                                parent = self.closest('.copy-input'),
                                input = parent.find('input');
                                
                        var $temp = $("<input>");
                        $("body").append($temp);
                        $temp.val(input.val()).select();
                        document.execCommand("copy");
                        $temp.remove();
                        avoskinApp.fn.alert({
                                "title": "Info",
                                "content": "Text successfully copied."
                        },500);
                });
                
                new WOW().init();
        });
}(jQuery));