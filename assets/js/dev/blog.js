;(function($){
        'use strict'
        
        function avoblog_calc_header(){
                var top = 0;
                if ($('#top').hasClass('has-popup')) {
                        top = $('.popup-info').outerHeight() + $('.bot-head').outerHeight() ;
                }else{
                        top =  $('.bot-head').outerHeight() ;
                }
                top = top - 2;
                $('.navi-blp').css({
                        marginTop: top
                })
        }
        $(document).ready(function(){
		  $('.lazy').Lazy({
			// your configuration goes here
			scrollDirection: 'vertical',
			effect: 'fadeIn',
			visibleOnly: true,
			onError: function(element) {
			    console.log('error loading ' + element.data('src'));
			}
		});
                $('.inner-article .category .slider').each(function(){
                        var self = $(this);
                        
                        self.find('.slick-carousel').slick({
                                infinite: true,
                                slidesToShow: 5,
                                slidesToScroll: 1,
                                autoplay: false,
                                dots: false,
                                variableWidth: true,
                                arrows: false
                        });	
                });
                $(".navi-blp").sticky({
                        topSpacing: 0
                });
                
                $('.navi-blp').on('sticky-start', function() {
                        avoblog_calc_header();
                });
                $('.navi-blp').on('sticky-end', function() {
                        $('.navi-blp').css({
                             marginTop:'0'
                        });
                });
                
                $('.popup-info .cls').on('click', function(){
                        avoblog_calc_header();
                });
                
                
                $('.inner-home2 .hero').each(function(){
                   var self = $(this);
                   
                        self.find('.slick-carousel').slick({
                                infinite: true,
				lazyLoad: 'ondemand',
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                autoplay: false,
                                speed: 800,
                                prevArrow: self.find('.slidenav .prev'),
				nextArrow: self.find('.slidenav .next'),
                                dots: false,
                                variableWidth: false,
                                responsive: [
                                        {
                                                breakpoint: 768,
                                                settings: {
                                                        arrows: false,
                                                        dots: true
                                                }
                                        }
                                ]
                        });     
                });
                
                $('.inner-home2 .series').each(function(){
                   var self = $(this);
                   
                        self.find('.slick-carousel').slick({
                                infinite: true,
				lazyLoad: 'ondemand',
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                autoplay: false,
                                speed: 800,
                                prevArrow: self.find('.slidenav .prev'),
				nextArrow: self.find('.slidenav .next'),
                                dots: false,
                                variableWidth: false,
                                responsive: [
                                        {
                                                breakpoint: 768,
                                                settings: {
                                                        arrows: false,
                                                        variableWidth: true,
                                                        slidesToShow: 1
                                                }
                                        }
                                ]
                        });     
                });
                
                $('.inner-home2 .flash .timer').each(function(){
                        var self = $(this),
                                theTime =  moment.tz(self.attr('data-time'), "Asia/Jakarta");
                         self.countdown(theTime.toDate()).on('update.countdown', function(event) {
                                var $this = self.html(event.strftime('%D : %H : %M : %S'));
                        }).on('finish.countdown',function(event){
                                setTimeout(function(){
                                        window.location.reload();
                                },500);
                        });
                });
		
		$('.inner-friday .flash .timer').each(function(){
                        var self = $(this),
                                theTime =  moment.tz(self.attr('data-time'), "Asia/Jakarta");
                         self.countdown(theTime.toDate()).on('update.countdown', function(event) {
                                var $this = self.html(event.strftime('%D : %H : %M : %S'));
                        }).on('finish.countdown',function(event){
                                setTimeout(function(){
                                        window.location.reload();
                                },500);
                        });
                });
                
                $('.flash-scroll').each(function(){
                        var self = $(this);
                        self.scrollbar({
                                "autoScrollSize": false,
                                "scrollx": self.next('.scrollerx'),
                        });
                });
                $('.rec-scroll').each(function(){
                        var self = $(this);
                        self.scrollbar({
                                "autoScrollSize": false,
                                "scrollx": self.next('.scrollerx'),
                        });
                });
                
                $('.inner-home2 .recom .rechead a').on('click',function(e){
                        e.preventDefault();
                        $('.inner-home2 .recom .rechead a, .inner-home2 .recom .item').removeClass('current');
                        $(this).addClass('current');
                        $($(this).attr('href')).addClass('current');
                        $('.inner-home2 .recom .thbmob').html($('.inner-home2 .recom .item.current .thumb').html());
                });
                
                $('.inner-home2 .quote').each(function(){
                        var self = $(this),
                                total = self.attr('data-total');
                        
                        self.find('.quotext .slick-carousel').slick({
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false,
                                fade: true,
                                autoplay:true,
                                speed:500,
                                swipe: false,
                                swipeToSlide:false,
                                touchMove:false,
                        });
                        
                        self.find('.qlogo .slick-carousel').slick({
                                slidesToShow: 5,
                                slidesToScroll: 1,
                                asNavFor: '.inner-home2 .quote .quotext .slick-carousel',
                                dots: false,
                                centerMode: false,
                                arrows: false,
                                autoplay:false,
                                focusOnSelect: true
                        });
                        
                });
                
                $('.inner-home2 .caro').each(function(){
                   var self = $(this);
                   
                        self.find('.slick-carousel').slick({
                                infinite: true,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                autoplay: false,
                                speed: 800,
                                prevArrow: self.find('.slidenav .prev'),
				nextArrow: self.find('.slidenav .next'),
                                dots: false,
                                variableWidth: false,
                                responsive: [
                                        {
                                                breakpoint: 991,
                                                settings: {
                                                        slidesToShow: 2,
                                                        dots: true
                                                }
                                        },
                                        {
                                                breakpoint: 768,
                                                settings: {
                                                        slidesToShow: 1,
                                                        dots: true
                                                }
                                        }
                                ]
                        });     
                });
                
                $('.inner-home2 .subs').each(function(){
                        var self = $(this);
                        setTimeout(function(){
                                $('.inner-home2 .subs').removeClass('notload');
                        },parseInt(self.attr('data-delay')));
                });
                
                
                $('.inner-home2 .subs .cls').on('click', function(e){
                        e.preventDefault();
                        $(this).closest('.subs').addClass('unload');
                });
                
                var hWidth = 0;
                $('.inner-home2 .recom .rechead .holder a').each(function(){
                        hWidth += $(this).outerWidth() + 15;
                });
                $('.inner-home2 .recom .rechead .holder').css({width: hWidth});
                
                
                $('.inner-home2 .subs form').on('submit', function(e){
                        e.preventDefault();
                        var self = $(this),
                                button = self.find('.button'),
                                email = self.find('input[type="email"]'),
                                data = self.serialize();
                        
                        if (email.val() != '') {
                                if (!avoskinApp.fn.isEmail(email.val())) {
                                        avoskinApp.fn.alert({
                                                "title": avoskin_data.optin.title.notice,
                                                "content": avoskin_data.optin.msg.invalid
                                        },800);
                                }else{
                                        if (!button.hasClass('fetching')) {
                                                button.addClass('fetching');
                                                axios.post( avoskin_data.api_url + "subscribe_optin/", {data:data} ).then( response => {
                                                        button.removeClass('fetching');
                                                        if (response.data.status == 'ok') {
                                                                $('body').removeClass('open-newsletter');
                                                                avoskinApp.fn.alert({
                                                                        "title": avoskin_data.optin.title.success,
                                                                        "content": avoskin_data.optin.msg.success
                                                                },800);
                                                                setTimeout(function(){
                                                                        $('.inner-home2 .subs').addClass('unload');
                                                                },1000);
                                                        }else{
                                                                avoskinApp.fn.alert({
                                                                        "title": response.data.title,
                                                                        "content": response.data.msg
                                                                },800);
                                                        }
                                                }).catch( error => console.log( error ) );
                                        }
                                }
                        }else{
                                avoskinApp.fn.alert({
                                        "title": avoskin_data.optin.title.notice,
                                        "content": avoskin_data.optin.msg.empty
                                },800);
                        }
                });
		
		// ::CRFRIDAY::
		$('.inner-friday .hero').each(function(){
                   var self = $(this);
                   
                        self.find('.slick-carousel').slick({
                                infinite: true,
				lazyLoad: 'ondemand',
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                autoplay: false,
                                speed: 800,
                                prevArrow: self.find('.slidenav .prev'),
				nextArrow: self.find('.slidenav .next'),
                                dots: false,
                                variableWidth: false,
                                responsive: [
                                        {
                                                breakpoint: 768,
                                                settings: {
                                                        arrows: false,
                                                        dots: true
                                                }
                                        }
                                ]
                        });     
                });
		
		// ::CRWFC::
		$('form#wfc-redeem').on('submit', function(e){
			e.preventDefault();
                        var self = $(this),
                                button = self.find('.button'),
                                data = self.serialize(),
				confirm = true;
                        
			if (self.find('input[name="applied"]').val() != 'nope') {
				if (self.find('input[name="applied"]').val() != self.find('input[name="code"]').val().toLowerCase()) {
					$.confirm({
						title: 'Confirm',
						animateFromElement: false,
						content: '<p>Apakah anda ingin mengganti coupon Waste for Change '+ self.find('input[name="applied"]').val().toUpperCase() +' dengan ' + self.find('input[name="code"]').val().toUpperCase() + ' ?</p>',
						buttons: {
							apply: {
								text: 'Apply',
								btnClass: 'button btn-tiny smalltall has-loading btn-wfc-redeem',
								action: function (event) {
									var popup = this;
									popup.buttons.apply.addClass('fetching');
									axios.post( avoskin_data.api_url + "wfc_redeem/", {data:data, user: avoskin_data.user} ).then( response => {
										popup.buttons.apply.removeClass('fetching');
										avoskinApp.fn.alert({
											"title": response.data.title,
											"content": response.data.msg
										},800);
										if (response.data.status == 'ok') {
											$('<div class="avoskin-loading">'+
												'<div class="centered">'+
													'<div class="ldr">'+
														'<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>'+
													'</div><!-- end of ldr -->'+
												'</div><!-- end of centered -->'+
											'</div><!-- end of avoskin loading -->').prependTo('body');
											setTimeout(function(){
												window.location.href = response.data.redirect;
											}, 1500);
										}
										popup.$$cancel.trigger('click');
									}).catch( error => console.log( error ) );
									return false;
								}
							},
							cancel: {
								text: 'Cancel',
								action: function(event){
									
								}
							}
						}
					});
				}else{
					window.location.href = self.find('input[name="redeem"]').val();
				}
			}else{
				if (!button.hasClass('fetching')) {
					button.addClass('fetching');
					axios.post( avoskin_data.api_url + "wfc_redeem/", {data:data, user: avoskin_data.user} ).then( response => {
						button.removeClass('fetching');
						avoskinApp.fn.alert({
							"title": response.data.title,
							"content": response.data.msg
						},800);
						if (response.data.status == 'ok') {
							$('<div class="avoskin-loading">'+
								'<div class="centered">'+
									'<div class="ldr">'+
										'<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>'+
									'</div><!-- end of ldr -->'+
								'</div><!-- end of centered -->'+
							'</div><!-- end of avoskin loading -->').prependTo('body');
							setTimeout(function(){
								window.location.href = response.data.redirect;
							}, 1500);
						}
					}).catch( error => console.log( error ) );
				}	
			}
                        
		});
		
		$('.btn-add-to-cart-wfc').on('click', function(e){
			e.preventDefault();
			var self = $(this),
				cart = $('.cart-sidebar'),
				count = $('.bot-head .util .cart > a b');
				
			if (!self.hasClass('fetching')) {
				self.addClass('fetching');
				axios.post( avoskin_data.api_url + "add_to_cart_wfc/", {id:self.attr('data-id'), code: self.attr('data-code')} ).then( response => {
					self.removeClass('fetching');
					if (response.data.status == 'ok') {
						avoskinApp.fn.alert({
							"title": response.data.title,
							"content": response.data.msg
						}, 300 );
						cart.html(response.data.cart);
						count.html(response.data.count);
						$('.lazy').Lazy({
							scrollDirection: 'vertical',
							effect: 'fadeIn',
							visibleOnly: true,
							onError: function(element) {
								console.log('error loading ' + element.data('src'));
							}
						});
					}else{
						avoskinApp.fn.alert({
							"title": avoskin_data.nostock.title,
							"content": avoskin_data.nostock.msg
						},300);
					}
				}).catch( error => console.log( error ) );
			}
		});
        });
        $(window).on('load', function(){
                $('.inner-home2 .hero').removeClass('notload');
		$('.inner-friday .hero').removeClass('notload');// ::CRFRIDAY::
        });
	
	$(window).on('scroll', function(){// ::CRFRIDAY::
		if ($('.inner-friday .loadmore.hide').length) {
			var paginate =  $('.inner-friday .loadmore'),
				hT = $('.inner-friday .loadmore').offset().top,
				hH = $('.inner-friday .loadmore').outerHeight(),
				wH = $(window).height(),
				wS = $(window).scrollTop(),
				target = $('.inner-friday .best .prodlist'),
				self = paginate.find('span'),
				page = parseInt(self.attr('data-page')),
				data = {
					product: self.data('product'),
					page: page,
					total: self.attr('data-total')
				};
			if ( wS > (hT+hH-wH) ) {
				paginate.removeClass('hide');
				axios.post( avoskin_data.api_url + "get_best_product/", data ).then( response => {
					$(response.data).appendTo(target);
					 $('.lazy').Lazy({
						// your configuration goes here
						scrollDirection: 'vertical',
						effect: 'fadeIn',
						visibleOnly: true,
						onError: function(element) {
							console.log('error loading ' + element.data('src'));
						}
					});
					if (  target.find('.proditem').length < parseInt(self.attr('data-max')) ) {
						paginate.addClass('hide');
						self.attr('data-page', page + 1);
					}else{
						paginate.remove();
					}
				}).catch( error => console.log( error ) );
			}
		}
	});
}(jQuery));