<?php
/*
Template Name: Page About
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-about">
                
               <?php if(have_posts()) : while(have_posts()) : the_post();?>
               
                        <?php
                                /*
                               * Functions hooked into avoskin_about action
                               * @hooked avoskin_intro_about                 5
                               * @hooked avoskin_discover_about          10
                               * @hooked avoskin_cruelty_about             15
                               * @hooked avoskin_love_about                  20
                               */
                               do_action('avoskin_about');
                       ?>
                
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
                
        </div><!-- end of inner about -->
<?php get_footer();?>