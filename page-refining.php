<?php
/*
Template Name: Page Miraculous Refining Series
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        
        <div class="inner-mira">
                
                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                
                        <?php
                                /*
                               * Functions hooked into avoskin_refining action
                               * @hooked avoskin_banner_refining          5
                               * @hooked avoskin_copy_refining               10
                               * @hooked avoskin_benefit_refining          15
                               * @hooked avoskin_action_refining            20
                               */
                               do_action('avoskin_refining');
                       ?>
                
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text centered">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
                
        </div><!-- end of inner mira -->
        
<?php get_footer();?>