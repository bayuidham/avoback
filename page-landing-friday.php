<?php
/*
Template Name: Page Landing Friday
*/

// ::CRFRIDAY::
?>
<?php get_header();?>
        <?php

        if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
                        'order' => 'section_order',
                        'visibility' => 'visibility',
                        'slider' => 'slide_item',
                        'pamflet' => 'pamflet_img',
                        'mpamflet' => 'pamflet_mimg',
                        'purl' => 'pamflet_url',
                        'trio' => 'trio_item',
                        'btitle' => 'best_title',
                        'btext' => 'best_text',
                        'bpagi' => 'best_pagi',
                        'bproduct' => 'best_product',
                        
                        'icon' => 'flas_icon',
			'img' => 'flas_img',
			'mimg' => 'flas_mimg',
                        'start' => 'flash_start',
			'end' => 'flash_end',
			'btn' => 'flash_button',
			'url' => 'flash_url',
			'item' => 'flash_product',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
        ?>
                <div class="inner-friday">
                        <?php if(is_array($order) && !empty($order)):?>
                                <?php foreach($order as $ord):?>
                                        <?php if($ord == 'slider'):?>
                                                <?php if(is_array($slider) && !empty($slider) && is_array($visibility) && in_array('slider', $visibility)):?>
                                                        <div class="hero notload">
                                                                <div class="slider">
                                                                        <div class="slick-carousel">
                                                                                <?php
                                                                                foreach($slider as $i):
                                                                                        $desktop = avoskin_resize( $i['img'] , 2560, 960, true, true, true );
                                                                                        $mobile = avoskin_resize( $i['mobile_img'] , 830, 512, true, true, true );
                                                                                        $mobile = ( $mobile != '') ? $mobile : avoskin_resize( $i['img'] , 830, 512, true, true, true );
                                                                                        $has_url = ($i['link'] != '') ? true : false;
                                                                                        $link = (substr($i['link'], 0, 4) == "http") ? $i['link'] : 'http://' . $i['link'];
                                                                                ?>
                                                                                        <div class="item">
                                                                                                <?php if($has_url):?>
                                                                                                <a href="<?php echo $link;?>">
                                                                                                <?php endif;?>
                                                                                                        <img src="<?php echo $desktop ;?>" alt="home"
                                                                                                                srcset="
                                                                                                                        <?php echo $mobile ;?>  500w,
                                                                                                                        <?php echo $desktop ;?>  800w
                                                                                                                "
                                                                                                                sizes="(min-width: 769px) 100vw, 30vw"
                                                                                                        />
                                                                                                        <div class="holder">
                                                                                                                <div class="wrapper">
                                                                                                                        <h2><?php echo $i['title'] ;?></h2>
                                                                                                                        <p><?php echo $i['subtitle'] ;?></p>
                                                                                                                </div><!-- end of wrapper -->
                                                                                                        </div><!-- end of holder -->
                                                                                                <?php if($has_url):?>
                                                                                                </a>
                                                                                                <?php endif;?>
                                                                                        </div><!-- end of item -->
                                                                                <?php endforeach;?>
                                                                        </div><!-- end of slick carousel -->
                                                                        <?php if(count($slider) > 1):?>
                                                                                <div class="slidenav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>
                                                                        <?php endif;?>
                                                                </div><!-- end of slider -->
                                                        </div><!-- end of hero -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'pamflet'):?>
                                                <?php if(is_array($visibility) && in_array('pamflet', $visibility)):
                                                        $has_url = ($purl != '') ? true : false;
                                                        $purl = (substr($purl, 0, 4) == "http") ? $purl : 'http://' . $purl;
                                                        $mpamflet = ($mpamflet  != '' ) ? $mpamflet : $pamflet;
                                                ?>
                                                        <div class="pamflet">
                                                                <?php if($has_url):?>
                                                                <a href="<?php echo $purl ;?>">
                                                                <?php endif;?>
                                                                        <img src="<?php echo $pamflet ;?>" alt="home"
                                                                                srcset="
                                                                                        <?php echo $mpamflet ;?>  500w,
                                                                                        <?php echo $pamflet ;?>  800w
                                                                                "
                                                                                sizes="(min-width: 769px) 100vw, 30vw"
                                                                        />
                                                                <?php if($has_url):?>
                                                                </a>
                                                                <?php endif;?>
                                                        </div><!-- end of pamlet -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'thumbnail'):?>
                                                <?php if(is_array($trio) && !empty($trio) && is_array($visibility) && in_array('trio', $visibility)): ?>
                                                       <div class="trio">
                                                               <div class="wrapper">
                                                                       <div class="rowflex">
                                                                               <?php foreach($trio as $t):
                                                                                       $has_url = ($t['url'] != '') ? true : false;
                                                                                       $link = (substr( $t['url'], 0, 4) == "http") ?  $t['url'] : 'http://' .  $t['url'];
                                                                               ?>
                                                                                       <figure><?php if($has_url):?><a href="<?php echo $link ;?>"><?php endif;?><img src="<?php echo $t['img'] ;?>" /><?php if($has_url):?></a><?php endif;?></figure>
                                                                               <?php endforeach;?>
                                                                       </div><!-- end of rowlfex -->
                                                               </div><!-- end of wrapper -->
                                                       </div><!-- end of trio -->
                                               <?php endif;?>
                                        <?php elseif($ord == 'flash'):?>
                                                <?php if( is_array($visibility) && in_array('flash', $visibility)):
                                                if((int)$end >= (int)current_time('timestamp')):
                                                        // ::CRFLASH::  
                                                        $time = ($start != '' && (int)$start > (int)current_time('timestamp')) ?  $start : $end;
                                                        $label = ($start != '' && (int)$start > (int)current_time('timestamp')) ?  __('Flash Sale Starting in', 'avoskin') : __('Flash Sale End in', 'avoskin');
                                                        $time = $time - (7 * 60 * 60);
                                                        $time = date("Y/m/d H:i:s", $time);
                                                ?>
                                                <div class="flash">
                                                        <div class="wrapper">
                                                                <div class="hen">
                                                                        <?php if($icon != ''):?>
                                                                                <figure><img src="<?php echo $icon ;?>" width="215" /></figure>
                                                                        <?php endif;?>
                                                                        <div class="info">
                                                                                <p><?php echo $label ;?></p>
                                                                                <div class="timer" data-time="<?php echo $time ;?>"></div>
                                                                        </div><!-- end of info -->
                                                                        <div class="clear"></div>
                                                                </div><!-- end of hen -->
                                                        </div>
                                                        <div class="wrapflash">
                                                                <?php if($img != ''):?>
                                                                        <div class="thumb">
                                                                                <?php if(wp_is_mobile()):?>
                                                                                        <img src="<?php echo $mimg ;?>" alt="home" />
                                                                                <?php else:?>
                                                                                        <img src="<?php echo $img ;?>" alt="home" />
                                                                                <?php endif;?>
                                                                        </div><!-- end of thumb -->
                                                                <?php endif;?>
                                                                <?php if(is_array($item) && !empty($item)):?>
                                                                        <div class="flashlist">
                                                                                <div class="flash-scroll">
                                                                                        <div class="prodlist">
                                                                                                <?php foreach($item as $i ):
                                                                                                        $product = wc_get_product((int)$i['product']);
                                                                                                        $id = $product->get_id();
                                                                                                        $price = $product->get_regular_price();
                                                                                                        $sale = $product->get_sale_price();
                                                                                                        $percent = '';
                                                                                                        if(!$product->is_on_sale()){
                                                                                                                $sale = 0;
                                                                                                        }
                                                                                                        if($sale > 0){
                                                                                                                $percent = ceil((($price - $sale )  / $price ) * 100);
                                                                                                        }
                                                                                                        $stock = ceil(get_post_meta( $id, '_stock', true ));
                                                                                                ?>
                                                                                                        <div class="proditem">
                                                                                                                <figure>
                                                                                                                        <a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"></a>
                                                                                                                        <?php if($percent != ''):?>
                                                                                                                                <span class="disc">-<?php echo $percent ;?>%</span>
                                                                                                                        <?php endif;?>
                                                                                                                        <a href="<?php echo get_permalink($id);?>">
                                                                                                                                <?php echo $product->get_image([600, 520]);?>
                                                                                                                       </a>
                                                                                                                       <?php if($product->get_stock_status() != 'instock'):?>
                                                                                                                                <span class="stock-empty"><?php avoskin_oos_text($id);?></span>
                                                                                                                        <?php endif;?>
                                                                                                                </figure>
                                                                                                                <div class="caption">
                                                                                                                        <h3><a href="<?php echo $product->get_permalink() ;?>"><?php echo $product->get_name() ;?></a></h3>
                                                                                                                        <div class="cats">
                                                                                                                                <?php echo wc_get_product_category_list($id);?>
                                                                                                                        </div><!-- end of cats -->
                                                                                                                        <div class="act">
                                                                                                                                <div class="pricing"><?php echo $product->get_price_html(); ?></div>
                                                                                                                                <a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"></a>
                                                                                                                                <div class="clear"></div>
                                                                                                                        </div><!--end of act -->
                                                                                                                        <div class="stockey">
                                                                                                                                <?php
                                                                                                                                        $low = wc_get_low_stock_amount($product);
                                                                                                                                        $quantity = ($product->get_stock_quantity() != 0) ? $product->get_stock_quantity() : 1;
                                                                                                                                        $percent  =  100 - $quantity;
                                                                                                                                ?>
                                                                                                                                <span class="bar"><b style="width:<?php echo $percent ;?>%"></b></span>
                                                                                                                                <?php if($product->get_stock_quantity()  > 0):?>
                                                                                                                                        <?php if($product->get_stock_quantity() > (int)$low):?>
                                                                                                                                                <small><?php _e('Sisa','avoskin');?> <?php echo $quantity;?></small>
                                                                                                                                        <?php else:?>
                                                                                                                                                <small><?php _e('Hampir Habis','avoskin');?></small>
                                                                                                                                        <?php endif;?>
                                                                                                                                <?php else:?>
                                                                                                                                        <small><?php _e('Stock Habis','avoskin');?></small>
                                                                                                                                <?php endif;?>
                                                                                                                        </div><!-- end of stockey -->
                                                                                                                </div><!-- end of caption -->
                                                                                                        </div><!-- end of item -->
                                                                                                <?php endforeach;?>
                                                                                                <div class="clear"></div>
                                                                                        </div><!-- end of prodlist -->
                                                                                </div><!-- end of flash scroll -->
                                                                                <div class="scrollerx light">
                                                                                        <div class="scrollerx-wrapper">
                                                                                                <div class="scroll-element_size"></div>
                                                                                                <div class="scroll-element_track"></div>
                                                                                                <div class="scroll-bar"></div>
                                                                                        </div>
                                                                                </div>
                                                                        </div><!-- end of flash list -->
                                                                <?php endif;?>
                                                        </div><!-- end of wrap flash -->
                                                        <?php if($btn != '' && $url != ''):?>
                                                                <div class="centered">
                                                                        <a href="<?php echo $url ;?>" class="button"><?php echo $btn ;?></a>
                                                                </div><!-- end of centered -->
                                                        <?php endif;?>
                                                </div><!-- end of flash -->
                                                <?php endif;?>
                                                <?php endif;?>
                                        <?php elseif($ord == 'best'):?>
                                                <?php if(is_array($visibility) && in_array('best', $visibility)):?>
                                                        <div class="best">
                                                                <div class="wrapper">
                                                                        <div class="hentry">
                                                                        <?php if($btitle != ''):?>
                                                                                <h2><?php echo $btitle ;?></h2>
                                                                        <?php endif;?>
                                                                        <?php if($btext != ''):?>
                                                                        <div class="txt">
                                                                                <?php echo $btext ;?>
                                                                        </div><!-- end of txt -->
                                                                        <?php endif;?>
                                                                        </div><!-- end of hentry -->
                                                                        <?php if(is_array($bproduct) && !empty($bproduct)):?>
                                                                                <div class="prodlist rowflex">
                                                                                        <?php
                                                                                                echo avoskin_request('get_best_product',[
                                                                                                        'product' => $bproduct,
                                                                                                        'page' =>1,
                                                                                                        'total' => (int)$bpagi
                                                                                                ],'POST');
                                                                                                ?>
                                                                                </div><!-- end of prod list -->
                                                                                <?php if($total < count($bproduct)):?>
                                                                                        <div class="loadmore hide">
                                                                                                <span data-page="2" data-product='<?php echo wp_json_encode($bproduct) ;?>' data-total="<?php echo $bpagi ;?>" data-max="<?php echo count($bproduct) ;?>"><?php _e('Sedang Memuat','avoskin');?></span>
                                                                                        </div>
                                                                                <?php endif;?>
                                                                        <?php endif;?>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of best -->
                                                <?php endif;?>
                                        <?php endif;?>
                                <?php endforeach;?>
                        <?php endif;?>
                        
                        
                </div><!-- end of inner friday -->
        
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>