<?php
/*
Template Name: Page Your Skin Bae
*/
?>
<?php get_header();?>
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <div class="inner-bae">
                          <?php
                                /*
                               * Functions hooked into avoskin_skinbae action
                               * @hooked avoskin_navi_skinbae                   2
                               * @hooked avoskin_intro_skinbae                   5
                               * @hooked avoskin_variant_skinbae             10
                               * @hooked avoskin_hero_skinbae                   15
                               * @hooked avoskin_product_skinbae            20
                               * @hooked avoskin_ingre_skinbae                 25
                               * @hooked avoskin_benefit_skinbae             30
                               * @hooked avoskin_function_skinbae          35
                               * @hooked avoskin_proof_skinbae               40
                               * @hooked avoskin_person_skinbae            45
                               */
                               do_action('avoskin_skinbae');
                       ?>
                        
                </div><!-- end of inner bae -->
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>