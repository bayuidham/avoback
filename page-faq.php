<?php
/*
Template Name: Page FAQ
*/
?>
<?php get_header();?>
        <div class="inner-page-half">
                <div class="wrapper">
                        <?php do_action('avoskin_navside');?>
                        <article class="main">
                                <?php if(have_posts()) : while(have_posts()) : the_post();
                                        $meta = [
                                                'title' => 'intro_title',
                                                'text' => 'intro_text',
                                                'ftitle' => 'faq_title',
                                                'fsubtitle' => 'faq_subtitle',
                                                'item' => 'faq_item'
                                        ];
                                        extract(avoskin_get_meta(get_the_ID(), $meta));
                                ?>
                                        <div class="content">
                                                <h1 class="the-title"><?php echo $title ;?></h1>
                                                <div class="format-text">
                                                        <?php echo $text ;?>
                                                </div><!-- end of format text -->
                                                <br/><br/>
                                                <h2 class="the-title"><?php echo $ftitle ;?></h2>
                                                <div class="format-text">
                                                        <h3><?php echo $fsubtitle ;?></h3>
                                                </div><!-- end of format text -->
                                                <?php if(is_array($item) && !empty($item)):?>
                                                        <br/>
                                                        <?php  $n=1;foreach($item as $i):?>
                                                              <div class="accord-item <?php echo ($n == 1) ? 'active expanded' : '';?>">
                                                                      <div class="acc-head">
                                                                              <h3><?php echo $i['title'] ;?></h3>
                                                                      </div><!-- end of acc head -->
                                                                      <div class="acc-body">
                                                                              <div class="format-text">
                                                                                      <?php echo $i['text'] ;?>
                                                                              </div><!-- end of format text -->
                                                                      </div><!-- end of acc body -->
                                                              </div><!-- end fo accord item -->
                                                      <?php $n++;endforeach;?>
                                                <?php endif;?>
                                        </div><!-- end of content -->
                                <?php endwhile;?>
                                <?php else : ?>
                                        <div class="format-text">
                                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                        </div>
                                <?php endif;?>
                        </article>
                        <div class="clear"></div>
                </div><!--end of wrapper -->
        </div><!-- end of inner page half -->
<?php get_footer();?>