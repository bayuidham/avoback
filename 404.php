<?php get_header();?>
        <?php
                $code = (ICL_LANGUAGE_CODE == 'en') ? '' : 'id_';
                $error = get_theme_mod('avoskin_'.$code.'error_404_page');
                $error = get_post((int)$error, ARRAY_A);
                if(is_array($error) && !empty($error)):
                        $thumb = get_post_thumbnail_id($error['ID']);
                        $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
        ?>
                <div class="inner-error">
                        <div class="wrapper">
                                <div class="pusher">
                                        <img src="<?php echo $img_url ;?>" width="450"/>
                                        <div class="caption">
                                                <?php echo $error['post_content'] ;?>
                                        </div><!-- end of caption -->
                                        <div class="action">
                                                <a href="<?php echo home_url('/');?>" class="button btn-hollow"><?php _e('Back to Home','avoskin');?></a>
                                                <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button"><?php _e('Go to Product','avoskin');?></a>
                                        </div><!-- end of action -->
                                </div><!-- end of pusher -->
                        </div><!-- end of wrapper -->
                </div><!-- end of inner page half -->
        <?php endif;?>
<?php get_footer();?>