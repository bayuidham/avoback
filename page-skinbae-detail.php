<?php
/*
Template Name: Page Your Skin Bae Detail
*/
?>
<?php get_header();?>
        <?php if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
			'your' => 'navi_your',
			'over' => 'navi_over',
			'buy' => 'navi_buy',
                        'iimg' => 'intro_img',
			'ititle' => 'intro_title',
			'isubtitle' => 'intro_subtitle',
			'itext' => 'intro_text',
                        'fitem' => 'function_item',
                        'cimg' => 'cta_img',
                        'ctxt' => 'cta_text',
                        'caction' => 'cta_action',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
        ?>
                <div class="inner-bae detail-bae">
                        <?php if($your != '' || $over != '' ||  $buy != '' ):?>
                                <div class="navi">
                                        <div class="wrapper">
                                                  <?php if($your != ''):?>
                                                          <a href="#"><?php echo $your ;?></a>
                                                <?php endif;?>
                                                <div class="action">
                                                          <?php if($over != ''):?>
                                                                  <p><?php echo $over ;?></p>
                                                        <?php endif;?>
                                                        <?php if($buy != ''):?>
                                                                  <a href="<?php echo $buy ;?>" class="button btn-hollow"><?php _e('Buy','avosin');?></a>
                                                        <?php endif;?>
                                                </div>
                                                <div class="clear"></div>
                                        </div>
                                </div><!-- end of navi -->
                        <?php endif;?>
                        
                        <div class="reason">
                                <div class="wrapper">
                                        <div class="hentry">
                                                <?php if($isubtitle != ''):?>
                                                        <b><?php echo $isubtitle ;?></b>
                                                <?php endif;?>
                                                <?php if($ititle != ''):?>
                                                        <h2><?php echo $ititle ;?></h2>
                                                <?php endif;?>
                                                <?php if($itext != ''):?>
                                                        <div class="format-text">
                                                                <?php echo $itext ;?>
                                                        </div><!-- end of format text -->
                                                <?php endif;?>
                                        </div><!-- end of hentry -->
                                        <?php if($iimg != ''):?>
                                                <figure><img src="<?php echo $iimg ;?>" /></figure>
                                        <?php endif;?>
                                </div>
                        </div><!-- end of reaso -->
                        
                          <div class="function">
                                <div class="wrapper">
                                        <?php if(is_array($fitem) && !empty($fitem)):?>
                                                <div class="list rowflex">
                                                        <?php foreach($fitem as $i):?>
                                                                <div class="item">
                                                                        <figure><img src="<?php echo $i['img'] ;?>"  width="190"/></figure>
                                                                        <h3><?php echo $i['title'] ;?></h3>
                                                                        <div class="format-text">
                                                                                <?php echo $i['text'] ;?>
                                                                        </div><!-- end of txt -->
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div><!-- end of list -->
                                        <?php endif;?>
                                </div><!-- endf of wrapper -->
                        </div><!-- end of benefit -->
                        
                        <div class="cta">
                                <div class="wrapper">
                                        <div class="holder">
                                                <div class="fhalf">
                                                        <?php if(is_array($cimg) && !empty($cimg)):?>
                                                                <?php foreach($cimg as $c):?>
                                                                        <img src="<?php echo $c ;?>" />
                                                                <?php endforeach;?>
                                                        <?php endif;?>
                                                </div>
                                                <div class="ahalf">
                                                        <?php if($ctxt != ''):?>
                                                                <p><?php echo $ctxt ;?></p>
                                                        <?php endif;?>
                                                        <?php if($caction != ''):?>
                                                                <a href="#"><?php echo $caction ;?></a>
                                                        <?php endif;?>
                                                </div>
                                        </div><!-- end of holder -->
                                </div><!-- end of wrapper -->
                        </div><!-- end of cta -->
                        
                </div><!-- end of inner bae -->
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>