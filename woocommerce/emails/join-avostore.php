<?php
/**
 * Customer note email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-note.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php _e('Hi admin. The following person has requested to join avostore.','avoskin');?></p>
<div style="margin-bottom: 50px">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('Name','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['name'] ;?></td>
                </tr>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('No. KTP','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['no_ktp'] ;?></td>
                </tr>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('Email','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['email'] ;?></td>
                </tr>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('Date of Birth','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['dob_day'] ;?>/<?php echo $args['dob_month'] ;?>/<?php echo $args['dob_year'] ;?></td>
                </tr>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('Phone','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['phone'] ;?></td>
                </tr>
                <?php if(isset($args['add_contact']) && $args['add_contact'] != ''):?>
                        <tr>
                                <td style="border: 1px solid #e5e5e5;"><?php _e('Additional Contact','avoskin');?></td>
                                <td style="border: 1px solid #e5e5e5;"><?php echo $args['add_contact'] ;?></td>
                        </tr>
                <?php endif;?>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('City','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['city'] ;?></td>
                </tr>
                <tr>
                        <td style="border: 1px solid #e5e5e5;"><?php _e('Address','avoskin');?></td>
                        <td style="border: 1px solid #e5e5e5;"><?php echo $args['address'] ;?></td>
                </tr>
	</table>
</div>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
