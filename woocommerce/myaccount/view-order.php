<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

defined( 'ABSPATH' ) || exit;

$notes = $order->get_customer_order_notes();
$tracking = avoskin_tracking_data(get_post_meta($order->get_id(), 'resi_order', true), get_post_meta($order->get_id(), 'shipping_courier', true));
?>
<div class="account-detail acc-order-detail">
	<div class="status">
		<p>
			<?php
				printf(
					/* translators: 1: order number 2: order date 3: order status */
					esc_html__( 'Order %1$s was placed on %2$s and is currently %3$s.', 'avoskin' ),
					'<strong>#' . $order->get_order_number() . '</strong>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					'<small>' . wc_format_datetime( $order->get_date_created() ) . '</small>', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					'<strong>' . wc_get_order_status_name( $order->get_status() ) . '</strong>' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				);
			?>
		</p>
		<?php if($order->has_status( apply_filters( 'woocommerce_valid_order_statuses_for_order_again', [ 'completed' ] ))):?>
			<a href="<?php echo wp_nonce_url( add_query_arg( 'order_again', $order->get_id(), wc_get_cart_url() ), 'woocommerce-order_again' );?>" class="button btn-hollow slimy"><?php _e('Re-Order','avoskin');?></a>
		<?php elseif($order->has_status('pending') || $order->has_status('on-hold')):?>
			<a id="pay-order" href="#" class="button slimy has-icon"><i class="fa-credit-card"></i> <span><?php _e('Pay Now','avoskin');?></span></a>
		<?php elseif($order->has_status('on-delivery')):?>
			<a id="order-received" href="#" data-order="<?php echo $order->get_id();?>" class="button slimy has-loading"><?php _e('Order Received','avoskin');?></a>
		<?php else:?>
		<a href="#" class="button disabled" style="opacity: 0;">&nbsp;</a>
		<?php endif;?>
		
		<div class="clear"></div>
	</div><!-- end of info -->
	
	<div class="detail">
		<div class="rowflex">
			<div class="item">
				<h3><?php _e('Order Detail','avoskin');?></h3>
				<table>
					<thead>
						<tr>
							<th><?php _e('Shipping','avoskin');?></th>
							<th><?php _e('Total','avoskin');?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($order->get_items() as $item_id => $item_data):
							$product = $item_data->get_product();
						?>
							<tr>
								<td><?php echo $product->get_name() ;?> <b>x<?php echo $item_data->get_quantity() ;?></b></td>
								<td><?php echo wc_price($item_data->get_subtotal()) ;?></td>
							</tr>
						<?php endforeach;?>
					</tbody>
					<tfoot>
						<tr>
							<td><?php _e('Subtotal','avoskin');?></td>
							<td><?php echo strip_tags(wc_price($order->get_subtotal())) ;?></td>
						</tr>
						<?php if($order->get_discount_total() != 0):?>
							<tr>
								<td><?php _e('Voucher/Coupon','avoskin');?></td>
								<td><i>- <?php echo strip_tags(wc_price($order->get_discount_total()));?></i></td>
							</tr>
						<?php endif;?>
						<?php // ::CRLOYALTY::
						$fees = $order->get_fees();
						if(is_array($fees) && !empty($fees)){
							foreach($fees as $f):
								$label = $f->get_name();
								$value = $f->get_total();
								if($label == 'Loyalty Point'):
							?>
							<tr>
								<td><?php echo $label; ?></td>
								<td><i><?php echo strip_tags(wc_price($value)); ?></i></td>
							</tr>
							<?php endif;endforeach;
						}?>
						<?php if($order->get_total_tax() != 0):?>
							<tr>
								<td><?php _e('Tax','avoskin');?></td>
								<td><?php echo wc_price($order->get_total_tax());?></td>
							</tr>
						<?php endif;?>
						<tr>
							<td><?php _e('Shipping','avoskin');?></td>
							<td><?php echo wc_price($order->get_shipping_total());?></td>
						</tr>
						
						<tr>
							<td><?php _e('Grand Total','avoskin');?></td>
							<td><b><?php echo strip_tags(wc_price($order->get_total()));?></b></td>
						</tr>
					</tfoot>
				</table>
			</div><!-- end of item -->
			<div class="shipping">
				<!-- ::CRWFC:: -->
				<h3><?php _e('Billing Address','avoskin');?></h3>
				<div class="txt">
					<p><?php echo $order->get_formatted_billing_address();?></p>
				</div><!-- end of txt -->
				<h3><?php _e('Shipping Address','avoskin');?></h3>
				<div class="txt">
					<p><?php echo $order->get_formatted_shipping_address();?></p>
				</div><!-- end of txt -->
			</div><!-- end of shipping -->
		</div><!-- end of rowflex -->
	</div><!-- end of detail -->
	
	<div class="tracking">
		<h3><?php _e('Tracking','avoskin');?></h3>
		<?php if(is_array($tracking) && !empty($tracking) && isset($tracking['status']) && $tracking['status'] == 'ok' && $tracking['data']['status']['code'] === 200):
			$result = $tracking['data']['result'];
		?>
			<div class="table-basic <?php echo( count($result['manifest']) < 2) ? 'only-one' : '';?>">
				<table>
					<thead>
						<tr>
							<th><?php _e('Status','avoskin');?></th>
							<th><?php _e('City','avoskin');?></th>
							<th><?php _e('Time','avoskin');?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($result['manifest'] as $m):
							$time = strtotime($m['manifest_date'] . ' ' . $m['manifest_time']) ;
                                                        $newformat = date('d-M-Y, H:i:s',$time);
						?>
							<tr>
								<td><?php echo $m['manifest_description'] ;?></td>
								<td>
									<?php if(isset($m['city_name']) && $m['city_name'] != ''):?>
										<?php echo $m['city_name'];?>
									<?php else:?>
										<?php echo avoskin_get_string_between($m['manifest_description']) ;?>
									<?php endif;?>
								</td>
								<td><?php echo $newformat;?></td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div><!-- end of table basic -->
		<?php else:?>
			<p><?php _e('Tracking info is not available yet. ','avoskin')?></p>
		<?php endif;?>
	</div><!-- end of tracking -->
</div><!-- end of account detail -->
<?php if($order->has_status('pending') || $order->has_status('on-hold')):
	$xp = get_option('woocommerce_midtrans_settings');
	$additional = (isset($xp['custom_expiry']) && $xp['custom_expiry'] != '' ) ? $xp['custom_expiry'] : '24 hours';
?>
	<div class="popup-order">
		<div class="layer">
			<a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" /></a>
			<div class="caption">
				<h2><?php _e('Please Complete Order','avoskin');?></h2>
				<div class="format-text">
					<p>
						<?php
							printf(
								/* translators: 1: order number 2: order date 3: order status */
								esc_html__( 'Hi %1$s  thanks for shopping and choosing Avoskin as your skin partner', 'avoskin' ),
								'<strong>'.$order->get_billing_first_name()  . ' ' . $order->get_billing_last_name().'</strong>'
							);
						?>
					</p>
				</div><!-- end of format text -->
			</div><!-- end of caption -->
			
			<div class="order-payment">
				<?php if($additional != 'disabled'):
						// ::FEEDBACK::
					     $expired = date( 'Y/m/d H:i:s',  strtotime('+'.$additional, $order->get_date_created ()->getOffsetTimestamp()) - (7 * 60 * 60));
				  ?>
					     <div class="countdown">
						     <h3><?php _e('Make a Payment Immediately Before','avoskin');?></h3>
						     <div class="timing" data-order="<?php echo $order->get_cancel_order_url_raw();?>" data-time="<?php echo $expired ;?>"></div><!-- end of timing -->
						     <small><?php _e('Transactions will be automatically canceled if payment has not been made','avoskin');?></small>
					     </div><!-- end of countdown -->
				  <?php endif;?>
				
				<div class="transfer">
					<?php
					     $template = [
						      'bca_va' => ['text' => 'Bank BCA','width' => '63'],
						      'echannel' => ['text' => 'Bank Mandiri','width' => '68'],
						      'bni_va' => ['text' => 'Bank BNI','width' => '62'],
						      'bri_va' => ['text' => 'Bank BRI','width' => '80'],
						      'permata_va' => ['text' => 'Bank Permata','width' => '99'],
						      'credit_card' => ['text' => 'Credit Card','width' => '90'],
						      'gopay' => ['text' => 'GoPay/QRIS','width' => '100'],
						      'alfamart' => ['text' => 'Alfamart/Alfamidi/Dan+Dan','width' => '50'],
						      'akulaku' => ['text' => 'Akulaku','width' => '95'],
						      'shopeepay' => ['text' => 'ShopeePay','width' => '100']
					     ];
					     $channel = get_post_meta($order->get_id(),'payment_channel', true) ;
					     $vanumber = get_post_meta($order->get_id(),'bank_va_number', true);
					     $midtrans = get_option('woocommerce_midtrans_settings');
					     if($channel != ''):
					     ?>
						      <div class="payment-channel">
							       <h3><?php _e('Transfer To Account Number','avoskin');?></h3>
							       <figure>
									<img src="<?php avoskin_dir();?>/assets/img/bank/<?php echo $channel ;?>.png" width="<?php echo $template[$channel]['width'];?>"/>
									<figcaption><?php echo $vanumber ;?></figcaption>
									<div class="copy-input">
										 <a href="#" title="Copy"><i class="fa fa-copy"></i>Copy</a>
										 <input type="hidden" value="<?php echo $vanumber ;?>" />
									</div><!-- end of copy iput -->
							       </figure>
							       
						      </div><!-- end of payment channel -->
						      <div class="amount">
							       <h3><?php _e('Amount To Be Paid','avoskin');?></h3>
							       <strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						       </div><!-- end of amount -->
					     <?php else:?>
						      <h3><?php _e('Click button below to make a payment','avoskin');?></h3>
						      <div class="trf-action">
							      <a href="<?php echo esc_url($order->get_meta('_mt_payment_url')) ;?>" class="button slimy has-icon"><i class="fa-credit-card"></i> <span><?php _e('Pay Now','avoskin');?></span></a>
						      </div><!-- end of trf action -->
						      <div class="amount">
							       <h3><?php _e('Amount To Be Paid','avoskin');?></h3>
							       <strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						       </div><!-- end of amount -->
					     <?php endif;?>
					     <?php if($channel != '') do_action('avoskin_payment_instruction', $order->get_id()); ?>
				</div><!-- end of tranfer -->
				<div class="centered">
					<a href="#" class="button btn-hollow slimy btn-close-popup"><?php _e('Close','avoskin');?></a>
				</div><!-- end of centered -->
			</div><!-- end of order payment -->
		      
		</div><!-- end of layer -->
	</div><!-- end of popup credential -->
<?php endif;?>