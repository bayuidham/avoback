<div class="account-detail account-wishlist">
        <div class="info">
                <h3><?php _e('Wishlist','avoskin');?></h3>
        </div>
        <?php
                $wishlist = get_user_meta(get_current_user_id(), 'wishlist', true);
                if(is_array($wishlist) && !empty($wishlist)):
        ?>
                <div class="table-basic">
                        <table>
                                <thead>
                                        <tr>
                                                <th><?php _e('Product','avoskin');?></th>
                                                <th><?php _e('Unit Price','avoskin');?></th>
                                                <th><?php _e('Stock Status','avoskin');?></th>
                                                <th><?php _e('Actions','avoskin');?></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        <?php foreach($wishlist as $w):
                                                $product = wc_get_product($w);
                                                if($product):
                                                        $price = $product->get_regular_price();
                                                        $sale = $product->get_sale_price();
                                                        $stock = ceil(get_post_meta( $product->get_id(), '_stock', true ));
                                        ?>
                                                        <tr>
                                                                <td>
                                                                        <a href="#" class="rmv" data-product="<?php echo $product->get_id();?>"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" class="svg" /></a>
                                                                        <?php echo $product->get_image([46,65]) ;?>
                                                                        <a href="<?php echo get_permalink($product->get_id());?>">
                                                                                <?php echo $product->get_name() ;?>
                                                                                <b><?php echo ($product->get_stock_status() == 'instock') ? __('In Stock','avoskin') : __('Out of Stock','avoskin')  ;?></b>
                                                                        </a>
                                                                </td>
                                                                <td><?php echo ($sale > 0) ? wc_price($sale) : wc_price($price) ;?></td>
                                                                <td><b><?php echo ($product->get_stock_status() == 'instock') ? __('In Stock','avoskin') : __('Out of Stock','avoskin')  ;?></b></td>
                                                                <td><a href="#" class="button btn-hollow slimy act-add-to-cart has-loading" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><?php _e('Add to Cart','avoskin');?></a></td>
                                                        </tr>
                                                <?php endif;?>
                                        <?php endforeach;?>
                                </tbody>
                        </table>
                </div><!-- end of table basic -->
        <?php else:?>
                <p><?php _e('You don\'t have product on your wishlist.','avoskin');?></p>
        <?php endif;?>
        
</div><!-- end of account detail -->