<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>
<?php if ( $has_orders ) : ?>
	<div class="account-detail acc-order">
		<div class="info">
			<h3><?php _e('Order','avoskin');?></h3>
		</div>
		<div class="tab-basic">
			<?php
				$tabs = [
					'current' => [
						'title' => __('Current Order', 'avoskin'),
						'item' => []
					],
					'complete' => [
						'title' => __('Completed Order', 'avoskin'),
						'item' => []
					],
					'cancelled' => [
						'title' => __('Canceled Order', 'avoskin'),
						'item' => []
					],
				];
				foreach ( $customer_orders->orders as $customer_order ) {
					$order = wc_get_order( $customer_order );
					$item_count = $order->get_item_count() - $order->get_item_count_refunded();
					$item = [
						'detail_url' => esc_url( $order->get_view_order_url() ),
						'order_id' => esc_html( _x( '#', 'hash before order number', 'avoskin' ) . $order->get_order_number() ),
						'products' =>  $order->get_items(),
						'date' => esc_html( wc_format_datetime( $order->get_date_created() ) ),
						'total' => $order->get_formatted_order_total() ,
						'quantity' =>$item_count
					];
					if($order->get_status() == 'cancelled' || $order->get_status() == 'refunded'){
						$tabs['cancelled']['item'][] = $item;
					}elseif($order->get_status() == 'completed'){
						$tabs['complete']['item'][] = $item;
					}else{
						$tabs['current']['item'][] = $item;
					}
				}
			?>
			
			<div class="tab-head">
				<?php $i=1;foreach($tabs as $t => $v):?>
					<a href="#<?php echo $t ;?>" <?php echo ($i == 1) ? 'class="active"' : '' ;?>><?php echo $v['title'] ;?></a>
				<?php $i++;endforeach;?>
			</div><!-- end of tab head -->
			
			<?php $n=1;foreach($tabs as $t => $v):?>
				<div id="<?php echo $t ;?>" class="tab-item <?php echo ($n == 1) ? 'active' : '' ;?>">
					<?php if(is_array($v['item']) && !empty($v['item'])):?>
						<div class="table-basic">
							<table>
								<thead>
									<tr>
										<th><?php _e('Order ID','avoskin');?></th>
										<th><?php _e('Products','avoskin');?></th>
										<th><?php _e('Date','avoskin');?></th>
										<th><?php _e('Total','avoskin');?></th>
										<th><?php _e('Actions','avoskin');?></th>
									</tr>
								</thead>
								<tbody>
									
									<?php foreach($v['item'] as $order_item):?>
										<tr <?php echo (count($order_item['products']) > 1) ? 'class="topped"' : '';?>>
											<td><a href="<?php echo $order_item['detail_url'] ;?>"><?php echo $order_item['order_id'] ;?></a></td>
											<td>
												<?php foreach($order_item['products'] as $product):
													$thumb = get_post_thumbnail_id($product['product_id']);
													$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
													$image = avoskin_resize( $img_url, 40, 40, true, true, true ); //resize & crop img
												?>
													<div class="order-product">
														<a href="<?php echo get_permalink($product['product_id']) ;?>">
															<img src="<?php echo $image ;?>" />
															<span>
																<strong><?php echo $order_item['order_id'] ;?></strong>
																<?php echo get_the_title($product['product_id']) ;?>
																<em><?php echo $order_item['date'] ;?></em>
															</span>
														</a>
													</div>
												<?php endforeach;?>
											</td>
											<td class="on-desktop"><?php echo $order_item['date'] ;?></td>
											<td>
												<b><?php echo $order_item['total'] ;?></b> <?php _e('for','avoskin');?> <?php echo $order_item['quantity'] ;?> <?php _e('item','avoskin');?>
											</td>
											<td><a href="<?php echo $order_item['detail_url'] ;?>" class="button btn-hollow btn-eye"><?php _e('View','avoskin');?></a></td>
										</tr>
									<?php endforeach;?>
								</tbody>
							</table>
						</div><!-- end of table basic -->
					<?php else:?>
						<div class="order-not-found">
							<img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/empty-order.svg" />
						</div><!-- end of order not found -->
					<?php endif;?>
				</div><!-- end of tab item -->
			<?php $n++;endforeach;?>
			
		</div><!-- end of order tab -->
		
	</div><!-- end of account detail -->
<?php else:?>
	<div class="account-detail acc-order">
		<div class="order-not-found">
			<img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/empty-order.svg" />
		</div><!-- end of order not found -->
	</div><!-- end of account detail -->
<?php endif;?>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>
