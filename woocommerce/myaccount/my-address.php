<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$customer_id = get_current_user_id();
if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		[
			'billing'  => __( 'Billing address', 'avoskin' ),
			'shipping' => __( 'Shipping address', 'avoskin' ),
		],
		$customer_id
	);
} else {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		[
			'billing' => __( 'Billing address', 'avoskin' ),
		],
		$customer_id
	);
}
?>
<div class="account-detail account-address">
	<div class="info">
		<h3><?php _e('Billing Address','avoskin');?></h3>
	</div><!-- end of info -->
	
	<div class="address">
		<div class="wrap">
			<div class="item">
				<h3><?php _e('Billing Information','avoskin');?></h3>
				<p>
					<?php
						$billing = wc_get_account_formatted_address( 'billing');
						echo $billing;
					;?>
				</p>
				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'billing' ) ); ?>" class="edit"><?php echo $billing ? esc_html__( 'Change Address', 'avoskin' ) : esc_html__( 'Add Address', 'avoskin' ); ?></a>
			</div><!-- end of item -->
			<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
				<div class="item">
					<h3><?php _e('Shipping Information','avoskin');?></h3>
					<p>
					<?php
						$shipping = wc_get_account_formatted_address( 'shipping');
						echo $shipping;
					;?>
				</p>
				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'shipping' ) ); ?>" class="edit"><?php echo $shipping ? esc_html__( 'Change Address', 'avoskin' ) : esc_html__( 'Add Address', 'avoskin' ); ?></a>
				</div><!-- end of item -->
			<?php endif;?>
		</div><!-- end of wrap -->
	</div><!--end of address -->
    
</div><!-- end of account detail -->