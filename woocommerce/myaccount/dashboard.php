<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php
	//update_user_meta($current_user->ID, 'loyalty_phone_old', '');
	//update_user_meta($current_user->ID, 'loyalty_phone_verified', 'nope');
	//update_user_meta($current_user->ID, 'loyalty_phone', '');
?>
<div class="account-detail">
	<div class="info user-profile">
		<figure>
			<?php echo avoskin_avatar($current_user->ID); ?>
			<a href="#" class="upload-profile"><span><?php _e('upload','avoskin');?></span></a>
		</figure>
		<div class="caption">
			<h3 class="<?php avoskin_is_verified($current_user->ID) ;?>"><?php echo get_user_meta($current_user->ID, 'first_name', true);?> <?php echo get_user_meta($current_user->ID, 'last_name', true);?></h3>
			<span><?php if(get_user_meta($current_user->ID, 'avo_is_verified', true) === 'yes'):?><b style="display: block;margin-bottom: 8px;"><?php _e('Verified Account','avoskin');?></b><?php endif;?></span>
			<span><?php _e('Member since','avoskin');?> <?php echo date('d-m-Y', strtotime($current_user->user_registered));?></span>
			<div class="change-password-mobile">
				<a href="#" class="button btn-hollow btn-change-password"><?php _e('Change Password','avoskin');?></a>
			</div><!-- end of fgroup -->
		</div>
	</div><!-- end of info -->
	<div class="form-basic">
		<form id="form-profile">
			<!-- ::CRLOYALTY:: -->
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Full Name','avoskin');?><sup>*</sup></label>
					<input type="text" value="<?php echo get_user_meta($current_user->ID, 'first_name', true);?>"  placeholder="Jorge" name="first_name" required="required"/>
				</div><!-- end of fgroup -->
				<div class="fgroup loyalty-phone">
					<label><?php _e('Phone Number','avoskin');?><sup>*</sup></label>
					<div>
						<input type="tel" value="<?php echo get_user_meta($current_user->ID, 'loyalty_phone', true);?>"  placeholder="<?php _e('input your mobile phone','avoskin');?>" name="loyalty_phone" required="required" minlength="8" maxlength="14"/>
						<input type="hidden" value="<?php echo get_user_meta($current_user->ID, 'loyalty_phone_old', true);?>"   name="loyalty_phone_old"  minlength="8" maxlength="14"/>
						<?php if( get_user_meta($current_user->ID, 'loyalty_phone_verified', true) != 'yep'):?>
							<span class="unverif"><?php _e('Unverified','avoskin');?></span>
						<?php else:?>
							<span><?php _e('Verified','avoskin');?></span>
						<?php endif;?>
						<a href="#" class="button btn-hollow btn-verify-phone has-loading" style="display: none;"><?php _e('Verify','avoskin');?></a>
					</div>
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			<input type="hidden" name="last_name" value=""/>
			<?php /*
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('First Name','avoskin');?><sup>*</sup></label>
					<input type="text" value="<?php echo get_user_meta($current_user->ID, 'first_name', true);?>"  placeholder="Jorge" name="first_name" required="required"/>
				</div><!-- end of fgroup -->
				<div class="fgroup">
					<label><?php _e('Last Name','avoskin');?><sup>*</sup></label>
					<input type="text" value="<?php echo get_user_meta($current_user->ID, 'last_name', true);?>"  placeholder="Beker" name="last_name" required="required" />
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			*/?>
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Date of Birth','avoskin');?></label>
					<div class="rowflex">
						<div class="item">
							<div class="dropselect">
								<select name="dob_day">
									<?php for($i =1;$i<=31;$i++):?>
										<option value="<?php echo $i ;?>" <?php selected($i, get_user_meta($current_user->ID, 'dob_d', true));?>><?php echo $i ;?></option>
									<?php endfor;?>
								</select>
							</div><!-- end of drop select -->
						</div><!--end of item -->
						<div class="item">
							<div class="dropselect">
								<select name="dob_month">
									<?php
										$month = [
											'1' => __('January', 'avoskin'),
											'2' => __('February', 'avoskin'),
											'3' => __('March', 'avoskin'),
											'4' => __('April', 'avoskin'),
											'5' => __('May', 'avoskin'),
											'6' => __('June', 'avoskin'),
											'7' => __('July', 'avoskin'),
											'8' => __('August', 'avoskin'),
											'9' => __('September', 'avoskin'),
											'10' => __('October', 'avoskin'),
											'11' => __('November', 'avoskin'),
											'12' => __('December', 'avoskin')
										];
										foreach($month as $m => $v):?>
										<option value="<?php echo $m ;?>" <?php selected($m, get_user_meta($current_user->ID, 'dob_m', true));?>><?php echo $v ;?></option>
									<?php endforeach;?>
								</select>
							</div><!-- end of drop select -->
						</div><!-- end of item -->
						<div class="item">
							<div class="dropselect">
								<select name="dob_year">
									<?php for($i =1960;$i<=current_time('Y') - 13 ;$i++):?>
										<option value="<?php echo $i ;?>" <?php selected($i, get_user_meta($current_user->ID, 'dob_y', true));?>><?php echo $i ;?></option>
									<?php endfor;?>
								</select>
							</div><!-- end of drop select -->
						</div><!--end of item -->
					</div><!-- end of rowflex -->
				</div><!-- end of fgroup -->
				<div class="fgroup">
					<label><?php _e('Gender','avoskin');?></label>
					<div class="dotdio">
						<label><input type="radio" name="gender" value="male"  <?php checked('male', get_user_meta($current_user->ID, 'gender', true));?> /><span><?php _e('Male','avoskin');?></span></label>
						<label><input type="radio" name="gender" value="female" <?php checked('female', get_user_meta($current_user->ID, 'gender', true));?> /><span><?php _e('Female','avoskin');?></span></label>
					</div><!--e nd of dotdio -->
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Email','avoskin');?><sup>*</sup></label>
					<input type="email" value="<?php echo $current_user->data->user_email;?>" placeholder="<?php _e('Email','avoskin');?>" required="required" name="email" />
				</div><!-- end of fgroup-->
				<div class="fgroup change-password-desktop">
					<label>&nbsp;</label>
					<a href="#" class="button btn-hollow btn-change-password"><?php _e('Change Password','avoskin');?></a>
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			<input type="file" accept="image/*" id="profile_picture" style="display: none;" />
			<input type="hidden" name="profile_picture" value="" id="profile_picture_src" />
			<input type="hidden" name="user" value="<?php echo avoskin_user_data();?>" />
			<button type="submit" class="button has-loading"><?php _e('Save','avoskin');?></button>
		</form>
	</div><!-- end of form basic -->
</div><!-- end of account detail -->