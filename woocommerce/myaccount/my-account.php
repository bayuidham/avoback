<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
 ?>
 <div class="intro">
        <div class="wrapper">
                <h2><?php the_title();?></h2>
                <div class="format-text">
                        <p><?php
	printf(
		__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'avoskin' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'my-account' ) )
	);
?></p>
                </div><!-- end of format text -->
        </div><!-- end of wrapper -->
</div><!-- end of intro -->
<div class="content">
	<div class="wrapper">
		<div class="layer">
			<?php do_action( 'woocommerce_account_navigation' );?>
			<article class="main">
				<?php  do_action( 'woocommerce_account_content' );?>
			</article><!-- end of main -->
			<div class="clear"></div>
		</div><!-- end of layer -->
	</div><!-- end of wrapper -->
</div><!--end of content -->
