<div class="account-detail account-review">
        <?php
                define('DEFAULT_COMMENTS_PER_PAGE', 3 ); 
                $orderby = (isset($_GET['orderby']) && $_GET['orderby'] != '') ? $_GET['orderby'] : 'comment_date_gmt';
                $order = (isset($_GET['order']) && $_GET['order'] != '') ? $_GET['order'] : 'DESC';
		$page = (get_query_var('review')) ? get_query_var('review') : 1;
		$limit = DEFAULT_COMMENTS_PER_PAGE;
		$offset = ($page * $limit) - $limit;	
		$param = [
			'status' => 'approve',
			'offset' => $offset,
			'number' => $limit,
                        'author__in' => [get_current_user_id()],
                        'orderby' => $orderby ,
			'order' => $order,
		];
		$total_comments = get_comments([
			'orderby' => $orderby ,
			'order' => $order,
			'status' => 'approve',
			'parent' => 0,
                        'author__in' => [get_current_user_id()]
		]);
	
		$pages = ceil( count($total_comments) / DEFAULT_COMMENTS_PER_PAGE );
		$comments = get_comments($param );
                $sorting = [
                        [
                                'title' => __('Date New-Old', 'avoskin'),
                                'sort' => [
                                        'orderby' => 'comment_date_gmt',
                                        'order' => 'DESC'
                                ]
                        ],
                        [
                                'title' => __('Date Old-New', 'avoskin'),
                                'sort' => [
                                        'orderby' => 'comment_date_gmt',
                                        'order' => 'ASC'
                                ]
                        ],
                        [
                                'title' => __('Product A-Z', 'avoskin'),
                                'sort' => [
                                        'orderby' => 'comment_post_ID',
                                        'order' => 'ASC'
                                ]
                        ],
                        [
                                'title' => __('Product Z-A', 'avoskin'),
                                'sort' => [
                                        'orderby' => 'comment_post_ID',
                                        'order' => 'DESC'
                                ]
                        ]
                ];
        ?>
        <div class="info">
                <h3><?php _e('Review','avoskin');?></h3>
                <div id="comment-sorting" class="dropselect dark medium">
                        <select name="sorting">
                                <?php foreach($sorting as $s):
                                        $current = ($s['sort']['orderby'] == $orderby && $s['sort']['order'] == $order) ? 'selected="selected"' : '';
                                ?>
                                        <option value='<?php echo wp_json_encode($s['sort']) ;?>' <?php echo $current ;?>><?php echo $s['title'] ;?></option>
                                <?php endforeach;?>
                        </select>
                        <form action="" method="GET">
                                <input type="hidden" value="" name="orderby" />
                                <input type="hidden" value="" name="order" />
                        </form>
                </div><!-- end of dropselect -->
                <div class="clear"></div>
        </div><!-- end of info -->
        
        <div class="review">
                <?php if(is_array($comments) && !empty($comments)):?>
                        <div class="wrap">
                                <?php foreach($comments as $c):
                                        $thumb = get_post_thumbnail_id($c->comment_post_ID);
                                        $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                                        $image = avoskin_resize( $img_url, 40, 57, true, true, true ); //resize & crop img
                                ?>
                                        <div class="item">
                                                <div class="rowflex">
                                                        <div class="product">
                                                                <figure><a href="<?php echo get_permalink($c->comment_post_ID);?>">
                                                                        <img src="<?php echo $image ;?>"/>
                                                                </a></figure>
                                                                <h3><a href="<?php echo get_permalink($c->comment_post_ID) ;?>"><?php echo get_the_title($c->comment_post_ID) ;?></a></h3>
                                                        </div><!-- end of prouct -->
                                                        <div class="feedback">
                                                                <div class="meta">
                                                                        <?php echo avoskin_rating(get_comment_meta($c->comment_ID, 'rating', true));?>
                                                                        <span><?php _e('Skin Type','avoskin');?></span>
                                                                        <?php
                                                                                $skin_type = get_comment_meta($c->comment_ID, 'skin_type', true);
                                                                                foreach($skin_type as $s):
                                                                        ?>
                                                                                <strong><?php echo $s;?></strong>
                                                                        <?php endforeach;?>
                                                                </div><!-- end of meta -->
                                                                <div class="format-text">
                                                                        <p><?php echo $c->comment_content ;?></p>
                                                                </div><!-- end of format text -->
                                                        </div><!-- end feedback -->
                                                </div><!-- end of rowflex -->
                                        </div><!-- end of item -->
                                <?php endforeach;?>
                        </div><!-- end of wrap -->
                <?php else:?>
                        <div class="item">
                                <div class="format-text">
                                        <p><?php _e('No review found.','avoskin');?></p>
                                </div><!-- end of format text -->
                        </div><!-- end of item -->
                <?php endif;?>
                <?php if ($pages > 1):?>
                        <div class="page-pagination">
                                <?php
                                        echo paginate_links([
                                                'base'         => get_permalink( get_option('woocommerce_myaccount_page_id') ).'review/%#%',
                                                'format'       => '/%#%',
                                                'show_all'     => false,
                                                'current' => $page,  
                                                'total' => $pages,  
                                                'prev_text' => '<i class="fa-angle-left"></i>',  
                                                'next_text' => '<i class="fa-angle-right"></i>'  ,
                                                'end_size'     => 1,
                                                'mid_size'     => 2,
                                        ]);  
                                ?>
                        </div><!--end of page pagination -->
                <?php endif;?>
        </div><!--end of review -->
    
</div><!-- end of account detail -->