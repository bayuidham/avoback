<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}?>
<div class="wrapper account-cred">
	<div class="pusher">
		<div class="layer">
			<div class="form-basic">

<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

<div class="u-columns col2-set" id="customer_login">
	

	<div class="u-column1 col-1">

<?php endif; ?>
		<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
		<?php if(isset($_GET['password-reset']) && $_GET['password-reset'] != '' && $_GET['password-reset'] == 'true'):?>
			<div class="avo-notice">
				<p><?php _e('Your password successfully updated.','avoskin');?></p>
			</div>
		<?php endif;?>
		<div class="txt">
			<h2 style="text-align: center"><?php esc_html_e( 'Login', 'avoskin' ); ?></h2>
			<!-- ::CRWFC:: -->
			<?php if(!is_user_logged_in() && avoskin_has_wfc_coupon() != 'nope' ):?>
				<p style="text-align: center;"><?php _e('Please login before using coupon code: ','avoskin');?><b style="font-weight: 700;color: #88a184;"><?php echo strtoupper(avoskin_has_wfc_coupon()) ;?></b></p>
			<?php endif;?>
		</div>
		<!--  ::PENTEST :: -->
		<div style="text-align: center;margin: 20px 0 0;">
			<a href="#" class="button btn-my-account-login" style="color: #fff;"><?php _e('Click here to login','avoskin');?></a>
			<script type="text/javascript">
				(function($){
					$(function(){
						$('.btn-my-account-login').on('click', function(e){
							e.preventDefault();
							$('.popup-credential .item').removeClass('active');
							$('.popup-credential').find('#tab-login').addClass('active');
							$('body').addClass('open-cred');
						});
					});
				}(jQuery));
			</script>
		</div>
		
		<?php /*
		<form class="woocommerce-form woocommerce-form-login login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="fgroup">
				<label for="username"><?php esc_html_e( 'Username or email address', 'avoskin' ); ?>&nbsp;<sup class="required">*</sup></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" required="required"/><?php // @codingStandardsIgnoreLine ?>
			</div>
			<div class="fgroup">
				<label for="password"><?php esc_html_e( 'Password', 'avoskin' ); ?>&nbsp;<sup class="required">*</sup></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password"  required="required" />
			</div>

			<?php do_action( 'woocommerce_login_form' ); ?>

			
				<div class="checky">
					<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'avoskin' ); ?></span>
				</label>
				</div>
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<button type="submit" class="woocommerce-button button slimy btn-fullwidth woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'avoskin' ); ?>"><?php esc_html_e( 'Log in', 'avoskin' ); ?></button>
			<p class="woocommerce-LostPassword lost_password">
				<a id="woo-forgot-password" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'avoskin' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
		*/?>

<?php if ( 'yes' === get_option( 'woocommerce_enable_myaccount_registration' ) ) : ?>

	</div>

	<div class="u-column2 col-2">

		<h2><?php esc_html_e( 'Register', 'avoskin' ); ?></h2>

		<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<div class="fgroup woocommerce- woocommerce---wide  -wide">
					<label for="reg_username"><?php esc_html_e( 'Username', 'avoskin' ); ?>&nbsp;<span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</div>

			<?php endif; ?>

			<div class="fgroup woocommerce- woocommerce---wide  -wide">
				<label for="reg_email"><?php esc_html_e( 'Email address', 'avoskin' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
			</div>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="woocommerce- woocommerce---wide  -wide">
					<label for="reg_password"><?php esc_html_e( 'Password', 'avoskin' ); ?>&nbsp;<span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
				</p>

			<?php else : ?>

				<p><?php esc_html_e( 'A password will be sent to your email address.', 'avoskin' ); ?></p>

			<?php endif; ?>

			<?php do_action( 'woocommerce_register_form' ); ?>

			<p class="woocommerce-FormRow ">
				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
				<button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'avoskin' ); ?>"><?php esc_html_e( 'Register', 'avoskin' ); ?></button>
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
			</div><!-- end of form basic -->
			</div><!-- end of layer -->
	</div><!-- end of pusher -->
</div><!-- end of wrapper -->
