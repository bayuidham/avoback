<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
$user_id = get_current_user_id();
$page_title = ( 'billing' === $load_address ) ? esc_html__( 'Billing address', 'avoskin' ) : esc_html__( 'Shipping address', 'avoskin' );
do_action( 'woocommerce_before_edit_account_address_form' ); ?>

<?php if ( ! $load_address ) : ?>
	<?php wc_get_template( 'myaccount/my-address.php' ); ?>
<?php else :?>
	<div class="account-detail account-address-edit">
		<div class="info">
			<h3><?php echo $page_title ;?></h3>
		</div><!-- end of info -->
		<div class="form-basic">
			<form id="form-billing-shipping">
				<div class="rowflex">
					<div class="fgroup">
						<label><?php _e('Full Name','avoskin');?><sup>*</sup></label>
						<input type="text" value="<?php echo get_user_meta($user_id , $load_address.'_first_name', true)?>" placeholder="<?php _e('Enter your name','avoskin');?>" name="<?php echo $load_address;?>_first_name" required="required" />
					</div><!-- end of fgroup -->
					<div class="fgroup">
						<label><?php _e('Email Address','avoskin');?><sup>*</sup></label>
						<input  type="email" value="<?php echo get_user_meta($user_id , $load_address.'_email', true);?>" placeholder="<?php _e('Enter email address','avoskin');?>" name="<?php echo $load_address ;?>_email" required="required" />
					</div><!-- end of fgroup -->
					<?php /*
					<div class="fgroup">
						<label><?php _e('Last Name','avoskin');?><sup>*</sup></label>
						<input type="text" value="<?php echo get_user_meta($user_id , $load_address.'_last_name', true);?>" placeholder="<?php _e('Enter your name','avoskin');?>" name="<?php echo $load_address ;?>_last_name" required="required" />
					</div><!-- end of fgroup -->
					*/?>
				</div><!-- end of rowflex -->
				<input type="hidden" name="<?php echo $load_address ;?>_last_name" value="" />
				<input type="hidden" name="<?php echo $load_address ;?>_country" value="ID" />
				<?php /*
				<div class="fgroup">
					<label><?php _e('Email Address','avoskin');?><sup>*</sup></label>
					<input  type="email" value="<?php echo get_user_meta($user_id , $load_address.'_email', true);?>" placeholder="<?php _e('Enter email address','avoskin');?>" name="<?php echo $load_address ;?>_email" required="required" />
				</div><!-- end of fgroup -->
				
				<div class="fgroup">
					<?php
						$countries_obj = new WC_Countries();
						$countries = $countries_obj->get_allowed_countries();
					?>
					<label><?php _e('Country','avoskin');?><sup>*</sup></label>
					<div class="dropselect large">
						<select id="checkout-country" name="<?php echo $load_address ;?>_country" required="required">
							<?php if(is_array($countries) && !empty($countries)):?>
								<?php foreach($countries as $c => $v):?>
									<option value="<?php echo $c ;?>" <?php selected($c, get_user_meta($user_id , $load_address.'_country', true));?>><?php echo $v ;?></option>
								<?php endforeach;?>
							<?php endif;?>
						</select>
					</div><!-- end of dropselect -->
				</div><!-- end of fgroup -->
				*/?>
				<div class="rowflex">
					<div class="fgroup">
						<label><?php _e('Province','avoskin');?><sup>*</sup></label>
						<div class="dropselect large">
							<select name="<?php echo $load_address ;?>_state" required="required" id="checkout-state">
								<?php
									$args_state = [
										'country' => 'ID',
										'current' =>  get_user_meta($user_id , $load_address.'_state', true)
									];
									echo avoskin_request('get_states', $args_state , 'POST');
								?>
							</select>
						</div><!-- end of dropselect -->
					</div><!-- end of fgroup -->
					<div class="fgroup">
						<label><?php _e('City','avoskin');?><sup>*</sup></label>
						<div class="dropselect medium">
							<select name="<?php echo $load_address ;?>_city_id" required="required"  id="checkout-city">
								<?php
									$args_city = [
										'state' => get_user_meta($user_id , $load_address.'_state', true),
										'current' =>  get_user_meta($user_id , $load_address.'_city_id', true)
									];
									echo avoskin_request('get_cities', $args_city , 'POST');
								?>
							</select>
						</div><!-- end of dropselect -->
						<input type="hidden" value="<?php echo get_user_meta($user_id , $load_address.'_city', true) ;?>" name="<?php echo $load_address ;?>_city" id="checkout-city-name" />
					</div><!-- end of fgroup -->
				</div>
				<div class="rowflex">
					<div class="fgroup">
						<label><?php _e('Postal Code','avoskin');?><sup>*</sup></label>
						<div class="dropselect medium">
							<select name="<?php echo $load_address ;?>_postcode" required="required"  id="checkout-postal">
								<?php
									$args_postcode = [
										'state' => get_user_meta($user_id , $load_address.'_state', true),
										'current' =>  get_user_meta($user_id , $load_address.'_postcode', true) 
									];
									echo avoskin_request('get_postcode', $args_postcode , 'POST');
								?>
							</select>
						</div><!-- end of dropselect -->
					</div><!-- end of fgroup -->
					<div class="fgroup">
						<label><?php _e('Phone Number','avoskin');?><sup>*</sup></label>
						<input type="tel" value="<?php echo get_user_meta($user_id , $load_address.'_phone', true) ;?>" placeholder="<?php _e('Enter your phone number','avoskin');?>" name="<?php echo $load_address ;?>_phone" required="required" />
					</div><!-- end of fgroup -->
				</div><!-- end of rowflex -->
				<div class="fgroup">
					<label><?php _e('Address','avoskin');?><sup>*</sup></label>
					<textarea placeholder="<?php _e('Enter your address','avoskin');?>" name="<?php echo $load_address ;?>_address_1" required="required" id="checkout-address"><?php echo get_user_meta($user_id , $load_address.'_address_1', true) ;?></textarea>
				</div><!-- end of fgroup -->
				<input type="hidden" name="user" value="<?php echo avoskin_user_data();?>" />
				<button type="submit" class="button slimy has-loading"><?php _e('Save','avoskin');?></button>
			</form>
		</div><!-- end of form basic -->
	</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
