<?php
/**
* Checkout billing information form
*
* This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see     https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.6.0
* @global WC_Checkout $checkout
*/

defined( 'ABSPATH' ) || exit;
$user_id = get_current_user_id();
$firstname = (isset($_GET['fname']) && $_GET['fname'] != '') ? $_GET['fname'] :  get_user_meta($user_id , 'billing_first_name', true);
$lastname = (isset($_GET['lname']) && $_GET['lname'] != '') ? ' '.$_GET['lname'] :  ' '.get_user_meta($user_id , 'billing_last_name', true);
$fullname = $firstname.$lastname;
$avmail = (isset($_GET['avmail']) && $_GET['avmail'] != '') ? $_GET['avmail'] :  get_user_meta($user_id , 'billing_email', true);
$advisor = (isset($_GET['advisor']) && $_GET['advisor'] != '') ? $_GET['advisor'] :  0;
?>
<div class="user-data">
	<div class="layer">
		<h3><?php _e('Billing Detail','avoskin');?></h3>
		<div class="form-wrap">
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Full Name','avoskin');?><sup>*</sup></label>
					<input type="text" value="<?php echo $fullname; ?>" placeholder="<?php _e('Enter your name','avoskin');?>" name="first_name" required="required" />
				</div><!-- end of fgroup -->
				<div class="fgroup">
					<label><?php _e('Email Address','avoskin');?><sup>*</sup></label>
					<input id="user-email" <?php echo (!is_user_logged_in()) ? 'class="not-log"' : '';?> type="email" value="<?php echo $avmail ;?>" placeholder="<?php _e('Enter email address','avoskin');?>" name="email" required="required" />
				</div><!-- end of fgroup -->
				<?php /*
				<div class="fgroup">
					<label><?php _e('Last Name','avoskin');?><sup>*</sup></label>
					<input type="text" value="<?php echo $lastname;?>" placeholder="<?php _e('Enter your name','avoskin');?>" name="last_name" required="required" />
				</div><!-- end of fgroup -->
				*/?>
			</div><!-- end of rowflex -->
			<?php /*
			<div class="fgroup">
				<label><?php _e('Email Address','avoskin');?><sup>*</sup></label>
				<input id="user-email" <?php echo (!is_user_logged_in()) ? 'class="not-log"' : '';?> type="email" value="<?php echo get_user_meta($user_id , 'billing_email', true);?>" placeholder="<?php _e('Enter email address','avoskin');?>" name="email" required="required" />
			</div><!-- end of fgroup -->
			*/?>
			<?php /*
			<div class="fgroup">
				<?php
					$countries_obj = new WC_Countries();
					$countries = $countries_obj->get_allowed_countries();
				?>
				<label><?php _e('Country','avoskin');?><sup>*</sup></label>
				<div class="dropselect large">
					<select id="checkout-country" name="country" required="required">
						<?php if(is_array($countries) && !empty($countries)):?>
							<?php foreach($countries as $c => $v):?>
								<option value="<?php echo $c ;?>" <?php selected($c, get_user_meta($user_id , 'billing_country', true));?>><?php echo $v ;?></option>
							<?php endforeach;?>
						<?php endif;?>
					</select>
				</div><!-- end of dropselect -->
			</div><!-- end of fgroup -->
			*/?>
			<input type="hidden" name="last_name" value="" />
			<input type="hidden" name="country" value="ID" />
			<input type="hidden" name="advisor" value="<?php echo $advisor ;?>" />
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Province','avoskin');?><sup>*</sup></label>
					<div class="dropselect large">
						<select name="state" required="required" id="checkout-state">
							<?php
								$args_state = [
									'country' => 'ID',
									'current' =>  get_user_meta($user_id , 'billing_state', true)
								];
								echo avoskin_request('get_states', $args_state , 'POST');
							?>
						</select>
					</div><!-- end of dropselect -->
				</div><!-- end of fgroup -->
				<div class="fgroup">
					<label><?php _e('City','avoskin');?><sup>*</sup></label>
					<div class="dropselect medium">
						<select name="city_id" required="required"  id="checkout-city">
							<?php
								$args_city = [
									'state' => get_user_meta($user_id , 'billing_state', true),
									'current' =>  get_user_meta($user_id , 'billing_city_id', true)
								];
								echo avoskin_request('get_cities', $args_city , 'POST');
							?>
						</select>
					</div><!-- end of dropselect -->
					<input type="hidden" value="<?php echo get_user_meta($user_id , 'billing_city', true) ;?>" name="city" id="checkout-city-name" />
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			
			
			<div class="rowflex">
				<div class="fgroup">
					<label><?php _e('Postal Code','avoskin');?><sup>*</sup></label>
					<div class="dropselect medium">
						<select name="postcode" required="required"  id="checkout-postal">
							<?php
								$args_postcode = [
									'state' => get_user_meta($user_id , 'billing_state', true),
									'current' =>  get_user_meta($user_id , 'billing_postcode', true)
								];
								echo avoskin_request('get_postcode', $args_postcode , 'POST');
							?>
						</select>
					</div><!-- end of dropselect -->
				</div><!-- end of fgroup -->
				<div class="fgroup">
					<label><?php _e('Phone Number','avoskin');?><sup>*</sup></label>
					<input type="tel" value="<?php echo get_user_meta($user_id , 'billing_phone', true) ;?>" placeholder="<?php _e('Enter your phone number','avoskin');?>" name="phone" required="required" />
					<?php echo apply_filters('loyalty_mobile_field_info', '');?><!-- ::CRLOYALTY:: -->
				</div><!-- end of fgroup -->
			</div><!-- end of rowflex -->
			<div class="fgroup">
				<label><?php _e('Address','avoskin');?><sup>*</sup></label>
				<textarea placeholder="<?php _e('Enter your address','avoskin');?>" name="address_1" required="required" id="checkout-address"><?php echo get_user_meta($user_id , 'billing_address_1', true) ;?></textarea>
			</div><!-- end of fgroup -->
			<div class="fgroup">
				<label><?php _e('Note (optional)','avoskin');?></label>
				<textarea placeholder="<?php _e('Notes about your order or for the courier','avoskin');?>" name="notes"></textarea>
			</div><!-- end of fgroup -->
			<?php /*
			<div class="checky">
				<!-- ::TEMP:: HIDE THIS FOR REGISTERED USER -->
				<label><input type="checkbox" name="do_register" value="1" /><span><?php _e('Register a new account?','avoskin');?></span></label>
			</div><!--e nd of checky -->
			*/?>
			<input type="hidden" value="<?php echo (isset($_COOKIE['_ga']) && $_COOKIE['_ga'] != '') ? $_COOKIE['_ga'] : 0  ;?>" name="dummy_ga" />
		</div><!-- end of form wrap -->
	</div><!-- end of layer -->
</div><!-- end of user data -->