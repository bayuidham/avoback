<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="txt">
	<h1><?php the_title();?></h1>
	<?php if(!is_user_logged_in()):?>
		<p><?php _e('Already have an account?','avoskin');?> <a href="#tab-login"><?php _e('Login now','avoskin');?></a></p>
	<?php endif;?>
</div>

<div class="process">
	<div class="form-basic">
		<form id="form-checkout-product" class="rowflex">
			<?php if ( $checkout->get_checkout_fields() ) : ?>
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
				<div class="order-data">
					<?php
						/*
					       * Functions hooked into woo_avoskin_checkout action
					       * @hooked woo_avoskin_your_order_checkout          5
					       * @hooked woo_avoskin_shipping_checkout               10
					       * @hooked woo_avoskin_payment_checkout               15
					       * @hooked woo_avoskin_coupon_checkout                 20
					       * @hooked woo_avoskin_summary_checkout             25
					       * @hooked woo_avoskin_action_checkout                   30
					       */
					       do_action('woo_avoskin_checkout');
				       ?>
				</div><!-- end of order data -->
			<?php endif; ?>
		</form>
	</div><!-- end of form basic -->
	<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
</div><!-- end of process -->

