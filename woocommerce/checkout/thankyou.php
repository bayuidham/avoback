<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
	 $home = get_option('page_on_front');
?>

<div class="inner-thank">
	 <div class="pusher">
		 <img src="<?php echo get_post_meta($home, 'thank_img', true);?>" class="svg" />

		  <?php if ( $order ):
			  $status = $order->get_status();
		  ?>
			   <?php if($status == 'pending' || $status == 'on-hold'):
				    $xp = get_option('woocommerce_midtrans_settings');
				    $additional = (isset($xp['custom_expiry']) && $xp['custom_expiry'] != '' ) ? $xp['custom_expiry'] : '24 hours';
			   ?>
				  <div class="txt">
					     <?php echo do_shortcode(get_post_meta($home, 'thank_text', true));?>
				  </div><!-- end of txt -->
				  <?php if($additional != 'disabled'):
					     // ::FEEDBACK::
					     $expired = date( 'Y/m/d H:i:s',  strtotime('+'.$additional, $order->get_date_created ()->getOffsetTimestamp()) - (7 * 60 * 60));
				  ?>
					     <div class="countdown">
						     <h3><?php _e('Make a Payment Immediately Before','avoskin');?></h3>
						     <div class="timing" data-order="<?php echo $order->get_cancel_order_url_raw();?>" data-time="<?php echo $expired ;?>"></div><!-- end of timing -->
						     <small><?php _e('Transactions will be automatically canceled if payment has not been made','avoskin');?></small>
					     </div><!-- end of countdown -->
				  <?php endif;?>
				  
				  <div class="transfer">
					     <?php
					     $template = [
						      'bca_va' => ['text' => 'Bank BCA','width' => '63'],
						      'echannel' => ['text' => 'Bank Mandiri','width' => '68'],
						      'bni_va' => ['text' => 'Bank BNI','width' => '62'],
						      'bri_va' => ['text' => 'Bank BRI','width' => '80'],
						      'permata_va' => ['text' => 'Bank Permata','width' => '99'],
						      'credit_card' => ['text' => 'Credit Card','width' => '90'],
						      'gopay' => ['text' => 'GoPay/QRIS','width' => '100'],
						      'alfamart' => ['text' => 'Alfamart/Alfamidi/Dan+Dan','width' => '50'],
						      'akulaku' => ['text' => 'Akulaku','width' => '95'],
						      'shopeepay' => ['text' => 'ShopeePay','width' => '100']
					     ];
					     $channel = get_post_meta($order->get_id(),'payment_channel', true) ;
					     $vanumber = get_post_meta($order->get_id(),'bank_va_number', true);
					     $midtrans = get_option('woocommerce_midtrans_settings');
					     if($channel != ''):
					     ?>
						      <div class="payment-channel">
							       <h3><?php _e('Transfer To Account Number','avoskin');?></h3>
							       <figure>
									<img src="<?php avoskin_dir();?>/assets/img/bank/<?php echo $channel ;?>.png" width="<?php echo $template[$channel]['width'];?>"/>
									<figcaption><?php echo $vanumber ;?></figcaption>
									<div class="copy-input">
										 <a href="#" title="Copy"><i class="fa fa-copy"></i>Copy</a>
										 <input type="hidden" value="<?php echo $vanumber ;?>" />
									</div><!-- end of copy iput -->
							       </figure>
							       
						      </div><!-- end of payment channel -->
						      <div class="amount">
							       <h3><?php _e('Amount To Be Paid','avoskin');?></h3>
							       <strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						       </div><!-- end of amount -->
					     <?php else:?>
						      <h3><?php _e('Click button below to make a payment','avoskin');?></h3>
						      <div class="trf-action">
							      <a href="<?php echo esc_url($order->get_meta('_mt_payment_url')) ;?>" class="button slimy has-icon"><i class="fa-credit-card"></i> <span><?php _e('Pay Now','avoskin');?></span></a>
						      </div><!-- end of trf action -->
						      <div class="amount">
							       <h3><?php _e('Amount To Be Paid','avoskin');?></h3>
							       <strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						       </div><!-- end of amount -->
					     <?php endif;?>
					     <?php if($channel != '') do_action('avoskin_payment_instruction', $order->get_id()); ?>
				  </div><!-- end of tranfer -->
				  <a href="<?php echo home_url();?>" class="button btn-hollow slimy"><?php _e('Back to Home','avoskin');?></a>
			  <?php else:?>
				  <div class="txt">
					  <p><?php _e('Thank you, your payment has been received.','avoskin');?></p>
				  </div><!-- end of txt -->
				  <a href="<?php echo home_url('/');?>" class="button slimy"><?php _e('Back to home','avoskin');?></a>
			  <?php endif;?>
		  <?php else:?>
			  <div class="txt">
				  <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'avoskin' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			  </div><!-- end of txt -->
			  <a href="<?php echo home_url();?>" class="button slimy"><?php _e('Back to Home','avoskin');?></a>
		  <?php endif;?>
	</div><!-- end of pusher -->
	<?php do_action('after_woocommerce_pay');?>
</div><!-- end of inner  thank -->