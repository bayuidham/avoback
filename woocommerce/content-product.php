<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$id = $product->get_id();
$price = $product->get_regular_price();
$sale = $product->get_sale_price();
if(!$product->is_on_sale()){
	$sale = 0;
}
$percent = '';
if($sale > 0){
	$percent = ceil((($price - $sale )  / $price ) * 100);
}
$tagline = get_post_meta($id, 'product_tagline', true);
$stock = ceil(get_post_meta( $product->get_id(), '_stock', true ));
$hide_price = (get_post_meta($product->get_id(), 'product_hide_price', true) == 'yes' ) ? 'hide-price' : '';
?>

<div class="product-item <?php echo $hide_price ;?>  post-<?php echo $id ;?>">
	<?php do_action( 'woocommerce_before_shop_loop_item' );?>
	<div class="holder">
		<div class="img-box">
			<?php if($product->get_stock_status() != 'instock'):?>
				<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
			<?php endif;?>
			<?php if($percent != '' || $tagline != ''):?>
				<span class="disc"><?php echo ($percent == '' || $tagline != '' && is_product() || $tagline != '' && is_page_template('page-homepage.php')) ? $tagline : $percent.'%' ;?></span>
			<?php endif;?>
			<?php
				$img_size = (!is_shop() && !is_tax() ) ? [285,285] :  [193,193] ;
				echo $product->get_image($img_size) ;
			?>
			<div class="action">
				<a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
				<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
				<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
			</div><!-- end of action -->
		</div><!-- end of img box -->
		<div class="cat">
			<?php echo wc_get_product_category_list($product->get_id());?>
		</div><!-- end of cat -->
		<h2 class="product-title"><a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a></h2>
		<?php echo avoskin_rating($product->get_average_rating());?>
		<?php if($percent != '') :?>
			<span class="sale"><?php echo wc_price($price) ;?></span>
		<?php endif;?>
		<strong class="price"><?php echo ($percent != '') ? wc_price($sale) : wc_price($price) ;?></strong>
	</div><!-- endof holder-->
</div><!-- end of item -->