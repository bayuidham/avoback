<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
	$items = WC()->cart->get_cart();
	if(is_array($items) && !empty($items)):
?>

	<div class="rowflex">
		<div class="summary">
			<div class="table-basic">
				<table>
					<thead>
						<tr>
							<th><?php _e('Product','avoskin');?></th>
							<th><?php _e('Price','avoskin');?></th>
							<th><?php _e('Quantity','avoskin');?></th>
							<th><?php _e('Total','avoskin');?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($items as $item => $values):
							$product =  wc_get_product( $values['data']->get_id() );
							$price = $product->get_regular_price();
							$sale = $product->get_sale_price();
							if(!$product->is_on_sale()){
								$sale = 0;
							}
							$quantity = $values['quantity'];
							$total =  ($sale > 0) ? (float)$sale * $quantity : (float)$price * $quantity;
						?>
							<tr>
								<td>
									<a href="#" class="rmv" data-id="<?php echo $product->get_id();?>">
										<img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg" class="svg" />
									</a>
									<?php echo $product->get_image([46,66]) ;?>
									<?php if(!isset($values['wfc_code'])): // ::CRWFC:: ?>
										<a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a>
									<?php else:?>
										<?php echo $product->get_name() ;?>
									<?php endif;?>
								</td>
								<td class="price"><?php echo ($sale > 0) ? strip_tags(wc_price($sale)) : strip_tags(wc_price($price)) ;?></td>
								<td>
									<?php if(!isset($values['wfc_code'])): // ::CRWFC:: ?>
										<div class="calc">
											<a href="#" class="min"><i class="fa-minus"></i></a><!--
											--><span data-id="<?php echo $product->get_id();?>"><?php echo $quantity;?></span><!--
											--><a href="#" class="plus"><i class="fa-plus"></i></a>
										</div>
									<?php else:?>
										&nbsp;
									<?php endif;?>
								</td>
								<td class="total"><em><?php _e('Total Price:','avoskin');?> </em><b><?php echo strip_tags(wc_price($total)) ;?></b></td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div><!-- end of table basic -->
			<div class="action">
				<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ;?>" class="button btn-hollow slimy has-icon"><i class="fa-angle-left"></i><span><?php _e('Continue Shopping','avoskin');?></span></a>
				<a href="#" class="button slimy update-cart has-loading"><?php _e('Update','avoskin');?></a>
				<div class="clear"></div>
			</div><!-- end of action -->
		</div><!-- end of summary -->
		
		<?php
			/**
			 * Cart collaterals hook.
			 *
			 * @hooked woocommerce_cart_totals - 10
			 */
			do_action( 'woocommerce_cart_collaterals' );
		?>
	
	</div><!-- end of rowflex -->

<?php else:?>
	<div class="format-text">
		<p><?php _e('Your bag is empty.','avoskin');?></p>
	</div>
<?php endif;?>