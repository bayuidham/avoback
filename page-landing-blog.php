<?php
/*
Template Name: Page Landing Blog
*/
?>
<?php get_header();?>

        <?php
        
        if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
                        'stitle' => 'story_title',
                        'spost' => 'story_post',
                        'ctitle' => 'cat_title',
                        'citem' => 'cat_item',
                        'ltitle' => 'latest_title',
                        'lshow' => 'latest_show',
                        
                        'pimg' => 'product_img',
                        'ptitle' => 'product_title',
                        'psub' => 'product_subtitle',
                        'pbtn' => 'product_btn',
                        'purl' => 'product_url',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
        ?>
        
        <div class="inner-article">
                <?php if(is_array($spost) && !empty($spost)):?>
                        <div class="story">
                                <div class="wrapper">
                                        <?php if($stitle != ''):?>
                                                <div class="hentry">
                                                        <h2><?php echo $stitle ;?></h2>
                                                </div><!-- end of hentry -->
                                        <?php endif;?>
                                        <div class="rowflex">
                                                <?php
                                                $stories = get_posts(
                                                         [
                                                               'numberposts' => 4,
                                                               'post__in' => $spost,
                                                               'orderby'=> 'post__in'
                                                        ]
                                                );
                                                if( $stories ) $x=1;foreach( $stories as $post ):
                                                setup_postdata($post);
                                                        $thumb = get_post_thumbnail_id();
                                                        $img_url = wp_get_attachment_url( $thumb,'full'); 
                                                
                                                ?>
                                                        <?php if($x == 1):
                                                                $image = avoskin_resize( $img_url, 1356, 548, true, true, true );
                                                        ?>
                                                                <div class="big">
                                                                       <div class="item">
                                                                               <figure>
                                                                                       <a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a>
                                                                               </figure>
                                                                               <div class="caption">
                                                                                       <div class="cat"><?php the_category(', ');?></div>
                                                                                       <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                                                       <div class="meta">
                                                                                               <?php the_author_posts_link();?>
                                                                                                <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                                                       </div><!-- end of meta -->
                                                                               </div><!-- end of caption -->
                                                                       </div><!-- end of item -->
                                                               </div><!-- end of big -->
                                                               <div class="smaller">
                                                        <?php else:
                                                                $image = avoskin_resize( $img_url, 258, 254, true, true, true );
                                                        ?>
                                                                <div class="item">
                                                                        <figure>
                                                                                <a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a>
                                                                        </figure>
                                                                        <div class="caption">
                                                                                <div class="cat"><?php the_category(', ');?></div>
                                                                                <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                                                <div class="meta">
                                                                                        <?php the_author_posts_link();?>
                                                                                         <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                                                </div><!-- end of meta -->
                                                                        </div><!-- end of caption -->
                                                                </div><!-- end of item -->
                                                        <?php endif;?>
                                                <?php $x++;endforeach;
                                                wp_reset_postdata(); 
                                                ?>
                                                </div><!-- end of small -->
                                        </div><!-- end of rowflex -->
                                </div><!-- end of wrapper -->
                        </div><!-- ed of story -->
                <?php endif;?>
                
                <?php if(is_array($citem) && !empty($citem)):?>
                        <div class="category">
                                <div class="wrapper">
                                        <?php if($ctitle != ''):?>
                                        <div class="hentry">
                                                <h2><?php echo $ctitle ;?></h2>
                                        </div><!-- end of hentry -->
                                        <?php endif;?>
                                        <div class="slider">
                                                <div class="slick-carousel">
                                                        <?php foreach($citem as $c):
                                                                $image = avoskin_resize( $c['thumb'], 770, 208, true, true, true );
                                                                $term = get_term_by('term_id', (int)$c['id'], 'category');
                                                        ?>
                                                                <div class="item">
                                                                        <a href="<?php echo get_term_link((int)$c['id']) ;?>" style="background-image: url('<?php echo $image ;?>');"><span><?php echo $term->name;?></span></a>
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div>
                                        </div>
                                </div><!-- end of wrapper -->
                        </div><!-- end of category -->
                <?php endif;?>
                
                <?php if($lshow == 'yes'):?>
                        <div class="story">
                                <div class="wrapper">
                                        <?php if($ltitle != ''):?>
                                                <div class="hentry">
                                                        <h2><?php echo $ltitle ;?></h2>
                                                </div><!-- end of hentry -->
                                        <?php endif;?>
                                       
                                        <div class="rowflex">
                                                <?php
                                                $x=1;
                                                $loop = new WP_Query([
                                                        'post_type' => 'post',
                                                        'orderby' => 'date',
                                                        'order' => 'DESC',
                                                        'posts_per_page' => 4,
                                                ]);?>
                                                
                                                <?php if($loop->have_posts()) : while($loop->have_posts()):$loop->the_post();
                                                        $thumb = get_post_thumbnail_id();
                                                        $img_url = wp_get_attachment_url( $thumb,'full');
                                                ?>
                                                           <?php if($x == 1):
                                                                $image = avoskin_resize( $img_url, 1356, 548, true, true, true );
                                                        ?>
                                                                <div class="big">
                                                                       <div class="item">
                                                                               <figure>
                                                                                       <a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a>
                                                                               </figure>
                                                                               <div class="caption">
                                                                                       <div class="cat"><?php the_category(', ');?></div>
                                                                                       <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                                                       <div class="meta">
                                                                                               <?php the_author_posts_link();?>
                                                                                                <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                                                       </div><!-- end of meta -->
                                                                               </div><!-- end of caption -->
                                                                       </div><!-- end of item -->
                                                               </div><!-- end of big -->
                                                               <div class="smaller">
                                                        <?php else:
                                                                $image = avoskin_resize( $img_url, 258, 254, true, true, true );
                                                        ?>
                                                                <div class="item">
                                                                        <figure>
                                                                                <a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a>
                                                                        </figure>
                                                                        <div class="caption">
                                                                                <div class="cat"><?php the_category(', ');?></div>
                                                                                <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                                                <div class="meta">
                                                                                        <?php the_author_posts_link();?>
                                                                                         <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                                                </div><!-- end of meta -->
                                                                        </div><!-- end of caption -->
                                                                </div><!-- end of item -->
                                                        <?php endif;?>
                                                <?php $x++;endwhile; ?>
                                                <?php endif;?>
                                                <?php wp_reset_query(); ?>
                                                               </div>
                                        </div><!-- end of rowflex -->
                                </div><!-- end of wrapper -->
                        </div><!-- ed of story -->
                <?php endif;?>
                
                <div class="banner">
                        <div class="wrapper">
                                <div class="item" <?php echo ($pimg != '') ? 'style="background-image: url(\''.$pimg.'\');"' : '' ;?>>
                                        <?php if($psub != ''):?>
                                        <span><?php echo $psub ;?></span>
                                        <?php endif;?>
                                        <?php if($ptitle != ''):?>
                                                <h2><a href="<?php echo $purl ;?>"><?php echo $ptitle ;?></a></h2>
                                        <?php endif;?>
                                        <?php if($pbtn != '' && $purl != ''):?>
                                                <a href="<?php echo $purl ;?>" class="button btn-hollow"><?php echo $pbtn ;?></a>
                                        <?php endif;?>
                                </div><!-- end of item -->
                        </div><!-- endo f wrapper -->
                </div><!-- end of banner -->
                
        </div><!-- end of article -->
        
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>

<?php get_footer();?>