<?php
/*
Template Name: Page Skin Advisor
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-skin-advisor">
                <?php if(have_posts()) : while(have_posts()) : the_post();
                        $meta = [
                                'title' => 'page_title',
                                'text' => 'page_text',
                                'btn' => 'btn_txt',
                                'url' => 'btn_url',
                                'enable' => 'btn_enable',
                        ];
                        extract(avoskin_get_meta(get_the_ID(), $meta));
                        // ::TEMP::
                ?>
                
                        <div class="wrapper">
                                <div class="pusher">
                                        <div class="rowflex">
                                                <figure>
                                                        <img src="<?php avoskin_dir();?>/assets/img/advisor/1.png" alt="advisor" width="160"/>
                                                        <img src="<?php avoskin_dir();?>/assets/img/advisor/2.png" alt="advisor" width="178"/>
                                                </figure>
                                                <div class="caption">
                                                        <h2 class="line-title"><?php echo $title ;?></h2>
                                                        <div class="format-text">
                                                                <?php echo $text ;?>
                                                        </div><!-- end of format text -->
                                                        <a href="<?php echo $url ;?>" class="button slimy <?php echo ($enable == 'no') ? 'disabled' : '' ;?>"><?php echo $btn ;?></a>
                                                </div><!-- end of caption -->
                                        </div><!-- end of rowflex -->
                                </div><!-- end of pusher -->
                        </div><!-- end of wrapper -->
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
        </div><!-- end of inner skin advisor -->
<?php get_footer();?>