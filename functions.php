<?php
/**
 * Avoskin engine room
 *
 * @package avoskin
 *
 */
 
 // Assign theme version to a var
$theme = wp_get_theme('avoskin');
$avoskin_version = $theme['Version'];

//Set content width based on design stylesheet
if ( ! isset( $content_width ) ) {
        $content_width = 1200; /* pixels */
}

require_once get_parent_theme_file_path( '/inc/avoskin-nav-walker.php' );
require_once get_parent_theme_file_path( '/inc/avoskin-functions.php' );
require_once get_parent_theme_file_path( '/inc/avoskin-template-hooks.php' );
require_once get_parent_theme_file_path( '/inc/avoskin-template-functions.php' );

require_once get_parent_theme_file_path( '/inc/class-controller-avoskin.php' );
require_once get_parent_theme_file_path( '/inc/class-member-avoskin.php' );
require_once get_parent_theme_file_path( '/inc/class-model-avoskin.php' );
require_once get_parent_theme_file_path( '/inc/class-loyalty-avoskin.php' );// ::CRLOYALTY::

require_once get_parent_theme_file_path( '/inc/widget/contact.php' );
require_once get_parent_theme_file_path( '/inc/widget/social.php' );
require_once get_parent_theme_file_path( '/inc/widget/popblog.php' );

//Libs
require_once get_parent_theme_file_path( '/inc/libs/Medoo.php' );
require_once get_parent_theme_file_path( '/inc/libs/img-resizer.php' );
require_once get_parent_theme_file_path( '/inc/libs/vendor/raiym/instagram-php-scraper/src/InstagramScraper.php' );
require_once get_parent_theme_file_path( '/inc/metabox/metabox.php' );


$avoskin = (object) [
        'version' => $avoskin_version,
        'main' => require_once get_parent_theme_file_path( '/inc/class-avoskin.php' )
];


if(class_exists( 'WooCommerce' )){
	require_once get_parent_theme_file_path( '/inc/rajaongkir/tro-data.php' );
	require_once get_parent_theme_file_path( '/inc/airwaybill/awb-data.php' );
	
	$avoskin->woocommerce = require 'inc/woocommerce/class-avoskin-woocommerce.php';
	require_once get_parent_theme_file_path( '/inc/woocommerce/avoskin-woocommerce-template-hooks.php' );
	require_once get_parent_theme_file_path( '/inc/woocommerce/avoskin-woocommerce-template-functions.php' );
}