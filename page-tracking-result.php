<?php
/*
Template Name: Page Tracking Result
*/
?>
<?php get_header();?>
        <?php // ::PENTEST:: ?>
        <div class="inner-tracking-result">
                <?php if(isset($_GET['data']) && $_GET['data'] != '' && avoskin_validate_order($_GET['data'])):
                        $data = json_decode(base64_decode($_GET['data']), true);
                        $order = wc_get_order($data['order_id']);
                        $tracking = avoskin_tracking_data($data['resi'], $data['courier']);
                ?>
                        <div class="section-title centered">
                                <div class="wrapper">
                                        <div class="pusher">
                                                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                                        <h2 class="line-title"><?php the_title();?></h2>
                                                        <?php if(is_user_logged_in()):?>
                                                                <?php the_content();?>
                                                        <?php else:?>
                                                                <p><?php _e('Please login to track your order!','avoskin');?></p>
                                                        <?php endif;?>
                                                <?php endwhile;?>
                                                <?php else : ?>
                                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>                                                
                                                <?php endif;?>
                                        </div><!-- end of pusher -->
                                </div><!-- end of wrapper -->
                        </div><!-- end of section title -->
                        <?php if(is_user_logged_in()):?>
                                <div class="tracking-info">
                                        <div class="order-detail">
                                                <div class="wrapper">
                                                        <div class="rowflex">
                                                                <div class="item">
                                                                        <h3><?php _e('Order Detail','avoskin');?></h3>
                                                                        <table>
                                                                                <thead>
                                                                                        <tr>
                                                                                                <th><?php _e('Shipping','avoskin');?></th>
                                                                                                <th><?php _e('Total','avoskin');?></th>
                                                                                        </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                        <?php foreach ($order->get_items() as $item_id => $item_data):
                                                                                                $product = $item_data->get_product();
                                                                                        ?>
                                                                                                <tr>
                                                                                                        <td><?php echo $product->get_name() ;?> <b>x<?php echo $item_data->get_quantity() ;?></b></td>
                                                                                                        <td><?php echo wc_price($item_data->get_subtotal()) ;?></td>
                                                                                                </tr>
                                                                                        <?php endforeach;?>
                                                                                </tbody>
                                                                                <tfoot>
                                                                                        <tr>
                                                                                                <td><?php _e('Subtotal','avoskin');?></td>
                                                                                                <td><?php echo strip_tags(wc_price($order->get_subtotal())) ;?></td>
                                                                                        </tr>
                                                                                        <?php if($order->get_discount_total() != 0):?>
                                                                                                <tr>
                                                                                                        <td><?php _e('Voucher/Coupon','avoskin');?></td>
                                                                                                        <td><i>- <?php echo strip_tags(wc_price($order->get_discount_total()));?></i></td>
                                                                                                </tr>
                                                                                        <?php endif;?>
                                                                                        <?php if($order->get_total_tax() != 0):?>
                                                                                                <tr>
                                                                                                        <td><?php _e('Tax','avoskin');?></td>
                                                                                                        <td><?php echo wc_price($order->get_total_tax());?></td>
                                                                                                </tr>
                                                                                        <?php endif;?>
                                                                                        <tr>
                                                                                                <td><?php _e('Shipping','avoskin');?></td>
                                                                                                <td><?php echo wc_price($order->get_shipping_total());?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                                <td><?php _e('Grand Total','avoskin');?></td>
                                                                                                <td><b><?php echo strip_tags(wc_price($order->get_total()));?></b></td>
                                                                                        </tr>
                                                                                </tfoot>
                                                                        </table>
                                                                </div><!-- end of item -->
                                                                <div class="item">
                                                                        <h3><?php _e('Billing Address','avoskin');?></h3>
                                                                        <div class="txt">
                                                                                <p><?php echo $order->get_formatted_billing_address();?></p>
                                                                        </div><!-- end of txt -->
                                                                </div>
                                                        </div><!-- end of rowflex -->
                                                </div><!-- end of wrapper -->
                                        </div><!-- end of order detail -->
                                        
                                        <?php if(is_array($tracking) && !empty($tracking) && isset($tracking['status']) && $tracking['status'] == 'ok' && $tracking['data']['status']['code'] === 200):
                                                $result = $tracking['data']['result'];
                                        ?>
                                                <div class="tracking-data">
                                                        <h3 class="for-mobile"><?php _e('Tracking','avoskin');?></h3>
                                                       <div class="wrapper">
                                                               <div class="table-basic <?php echo( count($result['manifest']) < 2) ? 'only-one' : '';?>">
                                                                       <table>
                                                                               <thead>
                                                                                       <tr>
                                                                                               <th><?php _e('Status','avoskin');?></th>
                                                                                               <th><?php _e('City','avoskin');?></th>
                                                                                               <th><?php _e('Time','avoskin');?></th>
                                                                                       </tr>
                                                                               </thead>
                                                                               <tbody>
                                                                                        <?php foreach($result['manifest'] as $m):
                                                                                                $time = strtotime($m['manifest_date'] . ' ' . $m['manifest_time']) ;
                                                                                                $newformat = date('d-M-Y, H:i:s',$time);
                                                                                        ?>
                                                                                                <tr>
                                                                                                        <td><?php echo $m['manifest_description'] ;?></td>
                                                                                                        <td>
                                                                                                                <?php if(isset($m['city_name']) && $m['city_name'] != ''):?>
                                                                                                                        <?php echo $m['city_name'];?>
                                                                                                                <?php else:?>
                                                                                                                        <?php echo avoskin_get_string_between($m['manifest_description']) ;?>
                                                                                                                <?php endif;?>
                                                                                                        </td>
                                                                                                        <td><?php echo $newformat ;?></td>
                                                                                                </tr>
                                                                                        <?php endforeach;?>
                                                                               </tbody>
                                                                       </table>
                                                               </div><!-- end of table basic -->
                                                       </div><!-- end of wrapper -->
                                               </div><!-- end of tracking data -->
                                        <?php else:?>
                                                <div class="section-title">
                                                        <div class="wrapper">
                                                                <p><?php _e('Tracking info is not available yet. ','avoskin')?></p>
                                                        </div><!-- end of wrapper -->
                                                </div><!-- end of section title -->
                                        <?php endif;?>
                                </div><!-- end of tracking info -->
                        <?php endif;?>
                <?php endif;?>
             
        </div><!-- end of inner order-tracking-->
<?php get_footer();?>