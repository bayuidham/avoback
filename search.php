<?php get_header();?>
       <?php woocommerce_breadcrumb();?>
        <div class="inner-product">
                <div class="wrapper">
                        <article class="main">
                                <div class="filter">
                                        <span><?php _e('Search result for','avoskin');?>: <strong><?php echo get_search_query() ;?></strong></span>
                                        <div class="sort">
                                                <form class="woocommerce-ordering" method="get">
                                                        <div class="dropselect dark medium">
                                                                <select name="perpage" class="perpage">
                                                                        <?php
                                                                                $selected = (isset($_GET['perpage']) && $_GET['perpage'] != '') ? $_GET['perpage'] : get_option( 'posts_per_page' );
                                                                                $total = [12, 24, 48, 96,  -1];
                                                                        ?>
                                                                        <?php foreach($total as $t):?>
                                                                                <option value="<?php echo $t ;?>" <?php selected($selected, $t);?>>
                                                                                        <?php _e('Show','avoskin');?> <?php echo ($t != -1) ? $t : __('all', 'avoskin') ;?> <?php _e('products','avoskin');?>
                                                                                        </option>
                                                                        <?php endforeach;?>
                                                                </select>
                                                        </div><!-- end of dropselect -->
                                                        <?php
                                                                $orderby = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
                                                                
                                                                $catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', [
                                                                        'relevance' => __( 'Relevance', 'avoskin' ),  
                                                                        'popularity' => __( 'Sort by popularity', 'avoskin' ),  
                                                                        'rating' => __( 'Sort by average rating', 'avoskin' ),  
                                                                        'date' => __( 'Sort by latest', 'avoskin' ),  
                                                                        'price' => __( 'Sort by price: low to high', 'avoskin' ),  
                                                                        'price-desc' => __( 'Sort by price: high to low', 'avoskin' ),  
                                                                ] ); 
                                                        ?>
                                                        <div class="dropselect dark medium">
                                                                <select name="orderby" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'avoskin' ); ?>">
                                                                        <?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
                                                                                <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
                                                                        <?php endforeach; ?>
                                                                </select>
                                                        </div>
                                                        <?php wc_query_string_form_fields( null, [  'perpage','orderby', 'submit', 'paged', 'product-page' ] ); ?>
                                                </form>
                                        </div><!-- end of sort-->
                                        <div class="clear"></div>
                                </div><!-- end of filter -->
                                
                                <div class="rowflex">
                                <?php
                                        if(have_posts()) : while(have_posts()) : the_post();
                                                $product = wc_get_product(get_the_ID());
                                                if($product):
                                                $id = $product->get_id();
                                                $price = $product->get_regular_price();
                                                $sale = $product->get_sale_price();
                                                $percent = '';
                                                if($sale > 0){
                                                        $percent = ceil((($price - $sale )  / $price ) * 100);
                                                }
                                                $tagline = get_post_meta($id, 'product_tagline', true);
                                                $stock = ceil(get_post_meta( $id, '_stock', true ));
                                                $hide_price = (get_post_meta($id, 'product_hide_price', true) == 'yes' ) ? 'hide-price' : '';
                                        ?>
                                                <div class="product-item <?php echo $hide_price ;?>">
                                                        <div class="holder">
                                                                <div class="img-box">
                                                                        <?php if($percent != '' || $tagline != ''):?>
                                                                                <span class="disc"><?php echo ($percent == '' || $tagline != '' && is_product() || $tagline != '' && is_page_template('page-homepage.php')) ? $tagline : $percent.'%' ;?></span>
                                                                        <?php endif;?>
                                                                        <?php echo $product->get_image([193,193]) ;?>
                                                                        <div class="action">
                                                                                <a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
                                                                                <a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
                                                                                <a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
                                                                        </div><!-- end of action -->
                                                                </div><!-- end of img box -->
                                                                <div class="cat">
                                                                        <?php echo wc_get_product_category_list($product->get_id());?>
                                                                </div><!-- end of cat -->
                                                                <h2 class="product-title"><a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a></h2>
                                                                <?php echo avoskin_rating($product->get_average_rating());?>
                                                                <?php if($percent != '') :?>
                                                                        <span class="sale"><?php echo wc_price($price) ;?></span>
                                                                <?php endif;?>
                                                                <strong class="price"><?php echo ($percent != '') ? wc_price($sale) : wc_price($price) ;?></strong>
                                                        </div><!-- endof holder-->
                                                </div><!-- end of item -->
                                        <?php endif;?>
                                        <?php endwhile;?>
                                        <?php else : ?>
                                                <div class="format-text" style="flex: 100% !important;max-width:none;">
                                                        <p><?php _e('Sorry, no product matched your criteria. Try something else. ','avoskin')?></p>
                                                </div>
                                        <?php endif;?>
                                </div><!-- end of rowflex -->
                                <div class="page-pagination">
                                        <?php
                                                global $wp_query;  
                                                $total_pages = $wp_query->max_num_pages;  
                                                if ($total_pages > 1){  
                                                        $current_page = max(1, get_query_var('paged'));
                                                        $args = [
                                                                'base' => preg_replace('/\?.*/', '', get_pagenum_link(1)) . '%_%',
                                                                'format' => 'page/%#%',  
                                                                'current' => $current_page,  
                                                                'total' => $total_pages,  
                                                                'prev_text' => '<i class="fa fa-angle-left"></i>',  
                                                                'next_text' => '<i class="fa fa-angle-right"></i>' 
                                                        ];
                                                        if(get_query_var('perpage') != '' && get_query_var('orderby') != ''){
                                                                $args['add_args'] = [
                                                                        'perpage' => get_query_var('perpage'),
                                                                        'orderby' =>  get_query_var('orderby')
                                                                ];
                                                        }
                                                        echo paginate_links($args);
                                                } 
                                        ?>
                                </div><!--end of page pagination -->
                        </article><!-- end of main -->
                        <?php get_sidebar();?>
                        <div class="clear"></div>
                </div><!-- end of wrapper -->
        </div><!-- end of inner product -->
<?php get_footer();?>