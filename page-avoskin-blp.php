<?php
/*
Template Name: Page Avoskin x BLP
*/
?>
<?php get_header();?>
        <?php
        
        if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
                        'visibility' => 'blp_visibility',
                        'ctitle' => 'cta_title',
                        'cbtn' => 'cta_btn',
                        'curl' => 'cta_url',
                        
                        'iimg' => 'intro_img',
                        'ititle' => 'intro_title',
                        'isub' => 'intro_subtitle',
                        'itxt' => 'intro_text',
                        
                        'video' => 'video_item',
                        
                        'atitle' => 'article_title',
                        'apost' => 'article_post',
                        'alink' => 'article_url',
                        
                        'ptitle' => 'product_title',
                        'pitem' => 'product_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
                $visibility = (is_array($visibility)) ?  $visibility : [];
        ?>
                <?php if(in_array('cta', $visibility) ):?>
                        <div class="navi-blp">
                                <div class="wrapper">
                                        <?php if($ctitle != ''):?>
                                        <span><?php echo $ctitle ;?></span>
                                        <?php endif;?>
                                        <?php if($cbtn != '' && $curl != ''):?>
                                        <a href="<?php echo $curl ;?>" class="button btn-hollow"><?php echo $cbtn ;?></a>
                                        <?php endif;?>
                                        <div class="clear"></div>
                                </div>
                        </div><!-- end of navi -->
                <?php endif;?>
                <div class="inner-blp">
                        <?php if(in_array('intro', $visibility) ):?>
                                <div class="intro">
                                        <div class="wrapper">
                                                <?php if($iimg != ''):?>
                                                        <figure><img src="<?php echo $iimg ;?>" width="459" /></figure>
                                                <?php endif;?>
                                                <div class="caption">
                                                        <?php if($ititle != ''):?>
                                                        <h2 class="wow fadeInUp" data-wow-delay="500ms"><?php echo $ititle ;?></h2>
                                                        <?php endif;?>
                                                        <?php if($isub != ''):?>
                                                                <span class="wow fadeInUp" data-wow-delay="1000ms"><?php echo $isub ;?></span>
                                                        <?php endif;?>
                                                        <?php if($itxt != ''):?>
                                                                <div class="wow fadeInUp txt" data-wow-delay="1200ms">
                                                                        <?php echo $itxt ;?>
                                                                </div><!-- end of txt -->
                                                        <?php endif;?>
                                                </div><!-- end of caption -->
                                        </div><!-- end of wrapper -->
                                </div><!-- end of intro -->
                        <?php endif;?>
                        
                        <?php if(in_array('video', $visibility) && is_array($video) && !empty($video) ):?>
                                <div class="video">
                                        <div class="wrapper">
                                                <div class="rowflex">
                                                        <?php $x=1;foreach($video as $v):
                                                                $image = ($x == 1 ) ? avoskin_resize( $v['thumb'], 2400, 900, true, true, true ) : avoskin_resize( $v['thumb'], 242, 246, true, true, true );
                                                        ?>
                                                                <div class="item">
                                                                        <figure>
                                                                                <img src="<?php echo $image ;?>"/>
                                                                        </figure>
                                                                        <div class="caption">
                                                                                <h2><?php echo $v['title'] ;?></h2>
                                                                                <a href="<?php echo $v['video'] ;?>"  data-fancybox class="play"><?php _e('Play','avoskin');?></a>
                                                                        </div><!-- end of caption -->
                                                                </div><!-- end of item -->
                                                        <?php $x++;endforeach;?>
                                                </div><!-- end of rowflex -->
                                        </div><!-- end of wrapper -->
                                </div><!-- end of video -->
                        <?php endif;?>
                        
                        <?php if(in_array('article', $visibility) && is_array($apost) && !empty($apost) ):?>
                                <div class="article">
                                        <div class="wrapper">
                                                <?php if($atitle != ''):?>
                                                        <div class="hentry">
                                                                <h2><?php echo $atitle ;?></h2>
                                                        </div><!-- end of henty -->
                                                <?php endif;?>
                                                <div class="rowflex">
                                                        <?php
                                                        $x = 1;
                                                        $aindex = 0;
                                                        
                                                           $article = get_posts(
                                                                        [
                                                                                'post_type'=> 'post',
                                                                                'numberposts' => 4,
                                                                                'post__in' => $apost,
                                                                                'orderby'=> 'post__in'
                                                                       ]
                                                               );
                                                               if( $article ) foreach( $article as $post ):
                                                               unset($apost[$aindex]);
                                                               $aindex++;
                                                               setup_postdata($post);
                                                                       $thumb = get_post_thumbnail_id();
                                                                       $img_url = wp_get_attachment_url( $thumb,'full');
                                                                       $cat = get_the_category($post->ID);
                                                                       $mimg = avoskin_resize( $img_url, 640, 360, true, true, true );
                                                               ?>
                                                               <?php if($x == 1):
                                                                $image = avoskin_resize( $img_url, 1022, 1286, true, true, true );
                                                               ?>
                                                                <div class="item big">
                                                                        <a href="<?php echo the_permalink();?>">
                                                                                <img src="<?php echo $image;?>" alt="home"
                                                                                        srcset="
                                                                                                <?php echo $mimg ;?>  500w,
                                                                                                <?php echo $image ;?>  800w
                                                                                        "
                                                                                        sizes="(min-width: 769px) 100vw, 30vw"
                                                                                />
                                                                                <div class="caption">
                                                                                        <span><?php foreach($cat as $c):?>
                                                                                        <?php echo $c->name ;?>
                                                                                <?php endforeach;?></span>
                                                                                        <h3><?php the_title();?></h3>
                                                                                        <div class="meta">
                                                                                                <b><?php echo get_the_author_meta('display_name') ;?></b>
                                                                                                <b><?php the_time('F d, Y'); ?></b>
                                                                                        </div><!-- end of meta -->
                                                                                </div>
                                                                        </a>
                                                                </div><!-- end of item big -->
                                                                <div class="smaller">
                                                                <div class="holder">
                                                                <?php else:
                                                                        $image = ($x != 4) ? avoskin_resize( $img_url, 640, 628, true, true, true ) : avoskin_resize( $img_url, 1326, 608, true, true, true );
                                                                ?>
                                                                        <div class="item">
                                                                                <a href="<?php echo the_permalink();?>">
                                                                                        <img src="<?php echo $image;?>" alt="home"
                                                                                                srcset="
                                                                                                        <?php echo $mimg ;?>  500w,
                                                                                                        <?php echo $image ;?>  800w
                                                                                                "
                                                                                                sizes="(min-width: 769px) 100vw, 30vw"
                                                                                        />
                                                                                        <div class="caption">
                                                                                                <span><?php foreach($cat as $c):?>
                                                                                                <?php echo $c->name ;?>
                                                                                        <?php endforeach;?></span>
                                                                                                <h3><?php the_title();?></h3>
                                                                                                <div class="meta">
                                                                                                        <b><?php echo get_the_author_meta('display_name') ;?></b>
                                                                                                        <b><?php the_time('F d, Y'); ?></b>
                                                                                                </div><!-- end of meta -->
                                                                                        </div>
                                                                                </a>
                                                                        </div><!-- end of item -->
                                                                <?php endif;?>
                                                        <?php $x++;endforeach;
                                                        wp_reset_postdata(); 
                                                        ?>
                                                        </div>
                                                                        </div>
                                                        
                                                </div><!-- end of rowflex -->
                                                       <?php if(!empty($apost) ):?>
                                                       <div class="rowflex spanned">
                                                       <?php
                                                                $article = get_posts(
                                                                        [
                                                                                'post_type'=> 'post',
                                                                                'numberposts' => -1,
                                                                                'post__in' => $apost,
                                                                                'orderby'=> 'post__in'
                                                                       ]
                                                               );
                                                               if( $article ) foreach( $article as $post ):
                                                               setup_postdata($post);
                                                                       $thumb = get_post_thumbnail_id();
                                                                       $img_url = wp_get_attachment_url( $thumb,'full');
                                                                       $cat = get_the_category($post->ID);
                                                                       $image = avoskin_resize( $img_url, 640, 628, true, true, true ) ;
                                                                       $mimg = avoskin_resize( $img_url, 640, 360, true, true, true );
                                                               ?>
                                                                  <div class="item">
                                                                        <a href="<?php echo the_permalink();?>">
                                                                                <img src="<?php echo $image;?>" alt="home"
                                                                                        srcset="
                                                                                                <?php echo $mimg ;?>  500w,
                                                                                                <?php echo $image ;?>  800w
                                                                                        "
                                                                                        sizes="(min-width: 769px) 100vw, 30vw"
                                                                                />
                                                                                <div class="caption">
                                                                                        <span><?php foreach($cat as $c):?>
                                                                                        <?php echo $c->name ;?>
                                                                                <?php endforeach;?></span>
                                                                                        <h3><?php the_title();?></h3>
                                                                                        <div class="meta">
                                                                                                <b><?php echo get_the_author_meta('display_name') ;?></b>
                                                                                                <b><?php the_time('F d, Y'); ?></b>
                                                                                        </div><!-- end of meta -->
                                                                                </div>
                                                                        </a>
                                                                </div><!-- end of item -->
                                                               <?php endforeach;?>
                                                               </div>
                                                        <?php endif;?>
                                                        <?php if($alink != ''):?>
                                                <div class="centered">
                                                        <a href="<?php echo $alink ;?>" class="button btn-hollow"><?php _e('Show More','avoskin');?></a>
                                                </div><!-- end of centered -->
                                                <?php endif;?>
                                        </div><!-- end of wrapper -->
                                </div><!-- end of article -->
                        
                        <?php endif;?>
                        
                        <?php if(in_array('product', $visibility) && is_array($pitem) && !empty($pitem) ):?>
                                <div class="products">
                                        <div class="wrapper">
                                                <?php if($ptitle != ''):?>
                                                <div class="hentry">
                                                        <h2><?php echo $ptitle ;?></h2>
                                                </div><!-- end of hentry  -->
                                                <?php endif;?>
                                                <div class="rowflex">
                                                        <?php foreach($pitem as $p):
                                                                $product = wc_get_product($p);
                                                                $id = $product->get_id();
                                                                $price = $product->get_regular_price();
                                                                $sale = $product->get_sale_price();
                                                                $percent = '';
                                                                if($sale > 0){
                                                                        $percent = ceil((($price - $sale )  / $price ) * 100);
                                                                }
                                                                $stock = ceil(get_post_meta( $id, '_stock', true ));
                                                                $thumb = get_post_thumbnail_id($id);
                                                                $img_url = wp_get_attachment_url( $thumb,'full');
                                                                $image = avoskin_resize( $img_url, 438, 438, true, true, true );
                                                        ?>
                                                                 <div class="item">
                                                                        <figure>
                                                                                <?php if($product->get_stock_status() != 'instock'):?>
                                                                                        <span class="stock-empty"><?php avoskin_oos_text($id);?></span>
                                                                                <?php endif;?>
                                                                                <?php if($percent != '' ):?>
                                                                                        <span class="disc"><?php echo ($percent == '' ) ? '' : $percent.'%' ;?></span>
                                                                                <?php endif;?>
                                                                                <img src="<?php echo $image ;?>"/>
                                                                                <div class="action">
                                                                                        <a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
                                                                                        <a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
                                                                                        <a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
                                                                                </div><!-- end of action -->
                                                                        </figure>
                                                                        <div class="caption">
                                                                                <h3><a href="<?php echo get_permalink($id) ;?>"><?php echo $product->get_name() ;?></a></h3>
                                                                                <?php /*
                                                                                <span><?php echo $product->get_short_description() ;?></span>
                                                                                */?>
                                                                        </div><!-- end of caption -->
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div><!-- end of rowflex -->
                                        </div><!-- end of wrapper -->
                                </div><!-- end of products -->
                        <?php endif;?>
                        
                </div><!-- end of inner blp -->
        
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>