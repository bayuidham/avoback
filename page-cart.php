<?php
/*
Template Name: Page Cart
*/
?>
<?php get_header();?>
        <?php if(have_posts()) : while(have_posts()) : the_post();?>
                <div class="inner-cart">
                        <div class="wrapper">
                                <h1><?php the_title()?></h1>
                                <?php the_content();?>
                        </div><!-- end of wrapper -->
                </div><!-- end of inner cart -->
        
        <?php endwhile;?>
        <?php else : ?>
                <div class="format-text centered">
                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                </div>
        <?php endif;?>
<?php get_footer();?>