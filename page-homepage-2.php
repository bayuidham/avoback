<?php
/*
Template Name: Page Homepage 2
*/
?>
<?php get_header();?>

        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <div class="inner-home2">
                        <?php
                                /*
                               * Functions hooked into avoskin_homepage2 action
                               * @hooked avoskin_slider_homepage2                 5
                               * @hooked avoskin_series_homepage2                10
                               * @hooked avoskin_flash_homepage2                  15
                               * @hooked avoskin_best_homepage2                   20
                               * @hooked avoskin_recom_homepage2               25
                               * @hooked avoskin_testi_homepage2                   30
                               * @hooked avoskin_caro_homepage2                   35
                               * @hooked avoskin_subs_homepage2                   40
                               */
                               do_action('avoskin_homepage2');
                       ?>
                </div><!-- end og inner home 2 -->
        
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>

<?php get_footer();?>