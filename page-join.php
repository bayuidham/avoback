<?php
/*
Template Name: Page Join Avostore
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-page inner-join">
                <div class="wrapper">
                        <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                <h1 class="the-title"><?php the_title();?></h1>
                                <div class="pusher">
                                        <div class="format-text">
                                                <?php the_content();?>
                                        </div><!-- end of format text -->
                                        
                                        <div class="layer">
                                                <div class="form-basic">
                                                        <form id="form-join">
                                                                <div class="rowflex">
                                                                        <div class="fgroup">
                                                                                <label><?php _e('Your Name','avoskin');?><sup>*</sup></label>
                                                                                <input type="text" value="" placeholder="" name="name" required="required"/>
                                                                        </div><!-- end of fgroup -->
                                                                        <div class="fgroup">
                                                                                <label><?php _e('No. KTP','avoskin');?><sup>*</sup></label>
                                                                                <input type="text" value="" placeholder="" name="no_ktp" required="required"/>
                                                                        </div><!-- end of fgroup -->
                                                                </div><!-- end of rowflex -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('Email','avoskin');?><sup>*</sup></label>
                                                                        <input type="email" placeholder=""  name="email" required="required" />
                                                                </div><!-- end of fgroup -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('Date of birth','avoskin');?><sup>*</sup></label>
                                                                        <div class="rowflex">
                                                                                <div class="item">
                                                                                        <div class="dropselect medium">
                                                                                                <select name="dob_day" required="required">
                                                                                                        <option value=""><?php _e('Day','avoskin');?></option>
                                                                                                        <?php for($i =1;$i<=31;$i++):?>
                                                                                                                <option value="<?php echo $i ;?>"><?php echo $i ;?></option>
                                                                                                        <?php endfor;?>
                                                                                                </select>
                                                                                        </div><!-- end of drop select -->
                                                                                </div><!--end of item -->
                                                                                <div class="item">
                                                                                        <div class="dropselect medium">
                                                                                                <select name="dob_month" required="required">
                                                                                                        <option value=""><?php _e('Month','avoskin');?></option>
                                                                                                        <?php
                                                                                                                $month = [
                                                                                                                        '1' => __('January', 'avoskin'),
                                                                                                                        '2' => __('February', 'avoskin'),
                                                                                                                        '3' => __('March', 'avoskin'),
                                                                                                                        '4' => __('April', 'avoskin'),
                                                                                                                        '5' => __('May', 'avoskin'),
                                                                                                                        '6' => __('June', 'avoskin'),
                                                                                                                        '7' => __('July', 'avoskin'),
                                                                                                                        '8' => __('August', 'avoskin'),
                                                                                                                        '9' => __('September', 'avoskin'),
                                                                                                                        '10' => __('October', 'avoskin'),
                                                                                                                        '11' => __('November', 'avoskin'),
                                                                                                                        '12' => __('December', 'avoskin')
                                                                                                                ];
                                                                                                                foreach($month as $m => $v):?>
                                                                                                                <option value="<?php echo $m ;?>"><?php echo $v ;?></option>
                                                                                                        <?php endforeach;?>
                                                                                                </select>
                                                                                        </div><!-- end of drop select -->
                                                                                </div><!-- end of item -->
                                                                                <div class="item">
                                                                                        <div class="dropselect medium">
                                                                                                <select name="dob_year" required="required">
                                                                                                        <option value=""><?php _e('Year','avoskin');?></option>
                                                                                                        <?php for($i =1950;$i<=2010;$i++):?>
                                                                                                                <option value="<?php echo $i ;?>"><?php echo $i ;?></option>
                                                                                                        <?php endfor;?>
                                                                                                </select>
                                                                                        </div><!-- end of drop select -->
                                                                                </div><!--end of item -->
                                                                        </div><!-- end of rowflex -->
                                                                </div><!-- end of fgroup -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('Phone Number','avoskin');?><sup>*</sup></label>
                                                                        <input type="tel" value="" name="phone" required="required" />
                                                                </div><!-- end of fgroup -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('Additional Contact','avoskin');?></label>
                                                                        <textarea name="add_contact"></textarea>
                                                                </div><!-- end of fgroup -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('City','avoskin');?><sup>*</sup></label>
                                                                        <div class="dropselect large">
                                                                                <select name="city" required="required">
                                                                                        <option value=""><?php _e('Select City','avoskin');?></option>
                                                                                        <?php
                                                                                                $city = avoskin_request('get_all_city');
                                                                                                if(is_array($city) && !empty($city)):
                                                                                                foreach($city as $c => $v):
                                                                                        ?>
                                                                                                <option value="<?php echo $v['name'];?> (<?php echo $v['type'] ;?>)"><?php echo $v['name'];?> (<?php echo $v['type'] ;?>)</option>
                                                                                        <?php endforeach;?>
                                                                                        <?php endif;?>
                                                                                </select>
                                                                        </div><!-- end of dropselect -->
                                                                </div><!-- end of fgroup -->
                                                                <div class="fgroup">
                                                                        <label><?php _e('Address','avoskin');?><sup>*</sup></label>
                                                                        <textarea name="address" required="required"></textarea>
                                                                </div><!-- end of fgroup -->
                                                                <div class="centered">
                                                                        <button type="submit" class="button slimy has-loading"><?php _e('Join Us','avoskin');?></button>
                                                                </div>
                                                        </form>
                                                </div><!-- end of form basic -->
                                        </div><!-- end of layer -->
                                </div><!-- end of pusher -->
                        <?php endwhile;?>
                        <?php else : ?>
                                <div class="format-text">
                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                </div>
                        <?php endif;?>
                </div><!-- end of wrapper -->
        </div><!-- end of inner page -->
<?php get_footer();?>