<?php
/*
Template Name: Page My Account
*/
?>
<?php get_header();?>
        <div class="inner-account">
                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                        <?php the_content();?>
                <?php endwhile;?>
                <?php else : ?>
                        <div class="wrapper centered">
                                <div class="format-text">
                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                </div>
                        </div><!-- end of wrapper -->
                <?php endif;?>
        </div><!-- end of inner account -->
<?php get_footer();?>