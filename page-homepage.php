<?php
/*
Template Name: Page Homepage
*/
?>
<?php get_header();?>
        <div class="inner-home">
                
                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                        <?php
                                /*
                               * Functions hooked into avoskin_homepage action
                               * @hooked avoskin_slider_homepage                    5
                               * @hooked avoskin_feature_homepage                10
                               * @hooked avoskin_avostore_homepage             15
                               * @hooked avoskin_testimonial_homepage        20
                               * @hooked avoskin_flash_sale_homepage           25
                               * @hooked avoskin_best_seller_homepage          30
                               * @hooked avoskin_instagram_homepage           35
                               * @hooked avoskin_blog_homepage                       40
                               */
                               do_action('avoskin_homepage');
                       ?>
                <?php endwhile;?>
                <?php else : ?>
                        <div class="wrapper">
                                <div class="format-text">
                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                </div><!-- end of format text -->
                        </div><!-- endo of wrapper -->
                <?php endif;?>
                
        </div><!-- end of inner home -->
        
<?php get_footer();?>