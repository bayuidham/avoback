<?php
/**
 * The header for our theme.
 * @package avoskin
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<script>
!function (w, d, t) {
w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

ttq.load('C4B1ME6JGOC88M14OBI0');
ttq.page();
}(window, document, 'ttq');
</script>

</head>
<body <?php body_class(); ?>>
        <?php if(false)://if(is_page_template('page-homepage.php')):?>
                <div id="home-load">
                        <style type="text/css">.lds-ellipsis{display:inline-block;position:relative;width:80px;height:80px}.lds-ellipsis div{position:absolute;top:33px;width:13px;height:13px;border-radius:50%;background:#b6c6b3;animation-timing-function:cubic-bezier(0,1,1,0)}.lds-ellipsis div:nth-child(1){left:8px;animation:lds-ellipsis1 .6s infinite}.lds-ellipsis div:nth-child(2){left:8px;animation:lds-ellipsis2 .6s infinite}.lds-ellipsis div:nth-child(3){left:32px;animation:lds-ellipsis2 .6s infinite}.lds-ellipsis div:nth-child(4){left:56px;animation:lds-ellipsis3 .6s infinite}@keyframes lds-ellipsis1{0%{transform:scale(0)}100%{transform:scale(1)}}@keyframes lds-ellipsis3{0%{transform:scale(1)}100%{transform:scale(0)}}@keyframes lds-ellipsis2{0%{transform:translate(0,0)}100%{transform:translate(24px,0)}}@media (max-width: 768px) {.avoskin-loading img{max-width:150px !important;}}</style>
                        <div class="avoskin-loading" style="background: #fff;">
                                <div class="centered">
                                        <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" style="display: block;width: 300px;margin: 0 auto;" />
                                        <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                                </div><!-- end of centered -->
                        </div><!-- end of avoskin loading -->
                        <script type="text/javascript">;(function($){$(document).ready(function(){$('#home-load').fadeOut('fast',function(){$('#home-load').remove();   });})}(jQuery));</script>
                </div>
        <?php endif;?>
        <?php
                /*
                * Functions hooked into avoskin_header action
                * @hooked avoskin_before_head          5
                * @hooked avoskin_topbar_head        10
                * @hooked avoskin_botbar_head        15
                * @hooked avoskin_after_head            20
                */
               (avoskin_is_blog()) ? do_action('avoskin_blog_header') : do_action('avoskin_header');
        ?>