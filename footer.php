        <?php
                /*
                * Functions hooked into avoskin_footer action
                * @hooked avoskin_before_foot       5
                * @hooked avoskin_optin_foot         10
                * @hooked avoskin_widget_foot      15
                * @hooked avoskin_tribute_foot      20
                * @hooked avoskin_after_foot          25
                */
                (avoskin_is_blog()) ? do_action('avoskin_blog_footer') : do_action('avoskin_footer');
        ?>
</body>
</html>