<?php
/*
Template Name: Page Checkout
*/
?>
<?php get_header();?>
        <!-- ::CRWFC:: -->
        <?php if(!is_user_logged_in() && avoskin_has_wfc_coupon() != 'nope' ):
                wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ));
                exit;
        ?>
        <?php else:?>
                <div class="inner-checkout">
                        <div class="wrapper">
                                <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                        <?php the_content();?>                        
                                <?php endwhile;?>
                                <?php else : ?>
                                        <div class="format-text">
                                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                        </div>
                                <?php endif;?>
                        </div><!-- end of wrapper -->
                </div><!-- end of inner checkout -->
        <?php endif;?>
<?php get_footer();?>