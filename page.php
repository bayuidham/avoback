<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-page">
                <div class="wrapper">
                        <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                <h1 class="the-title"><?php the_title();?></h1>
                                <div class="pusher">
                                        <div class="format-text">
                                                <?php the_content();?>
                                        </div><!-- end of format text -->
                                </div><!-- end f pusher -->
                        <?php endwhile;?>
                        <?php else : ?>
                                <div class="format-text">
                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                </div>
                        <?php endif;?>
                </div><!-- end of wrapper -->
        </div><!-- end of inner page -->
<?php get_footer();?>