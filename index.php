<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-blog">
                <div class="wrapper">
                        <div class="hentry">
                                <?php
                                        $title = 'Blog';
                                        if(is_category()){
                                                $cat = get_the_category();
                                                $title = $cat[0]->name;
                                         }else if(is_author()){
                                                global $author;
                                                $userdata = get_userdata($author);
                                                $title = $userdata->display_name;
                                         }
                                ?>
                                <h2><?php echo $title ;?></h2>
                        </div><!-- end of hentry -->
                        <div class="list">
                                <?php

                                if(have_posts()) : while(have_posts()) : the_post();
                                        $thumb = get_post_thumbnail_id();
                                        $img_url = wp_get_attachment_url( $thumb,'full'); 
                                        $image = avoskin_resize( $img_url, 556, 546, true, true, true );
                                ?>
                                        <div class="item">
                                                <figure><a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a></figure>
                                                <div class="caption">
                                                        <div class="cat"><?php the_category(', ');?></div>
                                                        <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                        <div class="txt">
                                                        <?php echo '<p>' . apply_filters('the_content ', wp_trim_words( get_the_content(), 50,'&hellip;')) .'</p>' ; ?>
                                                        </div><!-- end of txt -->
                                                        <div class="meta">
                                                                <?php the_author_posts_link();?>
                                                                <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                        </div><!-- end of meta -->
                                                </div><!-- end of caption -->
                                        </div><!-- end of item -->
                                <?php endwhile;?>
                                <?php else : ?>
                                        <div class="inner-page">
                                                <div class="format-text wrapper">
                                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                                </div>
                                        </div>
                                <?php endif;?>
                        </div><!-- end of list -->                        
                        <div class="page-pagination squared">
                                <?php
                                        global $wp_query;  
                                        $total_pages = $wp_query->max_num_pages;  
                                        if ($total_pages > 1){  
                                                $current_page = max(1, get_query_var('paged'));  
                                                echo paginate_links([
                                                        'base' => get_pagenum_link(1) . '%_%',  
                                                        'format' => 'page/%#%',  
                                                        'current' => $current_page,  
                                                        'total' => $total_pages,  
                                                        'prev_text' => '<i class="fa fa-angle-left"></i>',  
                                                        'next_text' => '<i class="fa fa-angle-right"></i>'  
                                                ]);  
                                        } 
                                ?>
                        </div><!--end of page pagination -->
                </div><!-- end of wrapper -->
        </div><!-- end of inner blog -->
<?php get_footer();?>