<?php
/*
Template Name: Page Tracking Order
*/
?>
<?php get_header();?>
        <div class="inner-tracking-input">
                <?php // ::PENTEST:: ?>
                <div class="section-title centered">
                        <div class="wrapper">
                                <div class="pusher">
                                        <?php if(have_posts()) : while(have_posts()) : the_post();?>
                                                <h2 class="line-title"><?php the_title();?></h2>
                                                <?php if(is_user_logged_in()):?>
                                                        <?php the_content();?>
                                                <?php else:?>
                                                        <p><?php _e('Please login to track your order!','avoskin');?></p>
                                                <?php endif;?>
                                        <?php endwhile;?>
                                        <?php else : ?>
                                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                        <?php endif;?>
                                </div><!-- end of pusher -->
                        </div><!-- end of wrapper -->
                </div><!-- end of section title -->
                
                <?php if(is_user_logged_in()):?>
                        <div class="tracking-form">
                                <div class="wrapper">
                                        <div class="layer form-basic">
                                                <form id="form-tracking-order">
                                                        <div class="fgroup">
                                                                <label><?php _e('Order ID','avoskin');?></label>
                                                                <input type="text" value="" placeholder="<?php _e('Found in your order confirmation email','avoskin');?>" required="required" name="order_id" />
                                                        </div><!-- end of fgroup -->
                                                        <div class="fgroup">
                                                                <label><?php _e('Billing Email','avoskin');?></label>
                                                                <input type="email" value="" placeholder="<?php _e('Email you used during checkout','avoskin');?>" required="required" name="billing_email" />
                                                        </div><!-- end of fgroup -->
                                                        <?php
                                                                $code = (ICL_LANGUAGE_CODE == 'en') ? '' : 'id_';
                                                                $track = get_theme_mod('avoskin_'.$code.'tracking_result_page');
                                                        ?>
                                                        <input type="hidden" name="result" value="<?php echo $track ;?>" />
                                                        <button type="submit" class="button btn-fullwidth has-loading"><?php _e('Track','avoskin');?></button>
                                                </form>
                                        </div><!-- end of layer -->
                                </div><!-- end of wrapper -->
                        </div><!-- end of tracking data -->
                <?php endif;?>
             
        </div><!-- end of inner order-tracking-->
<?php get_footer();?>