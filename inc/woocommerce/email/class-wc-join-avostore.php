<?php
/**
 * Class WC_Join_Avostore file.
 *
 * @package WooCommerce\Emails
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'WC_Join_Avostore', false ) ) :

	/**
	 * Customer Note Order Email.
	 *
	 * Customer note emails are sent when you add a note to an order.
	 *
	 * @class       WC_Join_Avostore
	 * @version     3.5.0
	 * @package     WooCommerce/Classes/Emails
	 * @extends     WC_Email
	 */
	class WC_Join_Avostore extends WC_Email {
                
                public $args;

		/**
		 * Constructor.
		 */
		public function __construct() {
			$this->id             = 'join_avostore';
			$this->title          = __( 'Join Avostore', 'avoskin' );
			$this->description    = __( 'This is email that you\'ll receive if a visitor join Avostore.', 'avoskin' );
			$this->template_html  = 'emails/join-avostore.php';
			$this->template_plain = 'emails/join-avostore.php';

			// Triggers.
			add_action( 'woocommerce_join_avostore_notification', [ $this, 'trigger' ]);

			// Call parent constructor.
			parent::__construct();
                        
                        // Other settings.
                        $this->recipient = get_option( 'admin_email' );
		}

		/**
		 * Get email subject.
		 *
		 * @since  3.1.0
		 * @return string
		 */
		public function get_default_subject() {
			return __( 'Join Avostore Request', 'avoskin' );
		}

		/**
		 * Get email heading.
		 *
		 * @since  3.1.0
		 * @return string
		 */
		public function get_default_heading() {
			return __( 'Someone has requesting to join Avostore', 'avoskin' );
		}

		/**
		 * Trigger.
		 *
		 * @param array $args Email arguments.
		 */
		public function trigger( $args ) {
			$this->setup_locale();

			if ( ! empty( $args ) ) {
                                $this->args = $args;
			}

			if ( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();
		}

		/**
		 * Get content html.
		 *
		 * @return string
		 */
		public function get_content_html() {
			return wc_get_template_html(
				$this->template_html,
				[
					'args'              => $this->args,
					'email_heading'      => $this->get_heading(),
					'additional_content' => $this->get_additional_content(),
					'sent_to_admin'      => true,
					'plain_text'         => false,
					'email'              => $this,
				]
			);
		}

		/**
		 * Get content plain.
		 *
		 * @return string
		 */
		public function get_content_plain() {
			return wc_get_template_html(
				$this->template_plain,
				[
                                        'args'              => $this->args,
					'email_heading'      => $this->get_heading(),
					'additional_content' => $this->get_additional_content(),
					'sent_to_admin'      => true,
					'plain_text'         => false,
					'email'              => $this,
				]
			);
		}

		/**
		 * Default content to show below main email content.
		 *
		 * @since 3.7.0
		 * @return string
		 */
		public function get_default_additional_content() {
			return __( 'Join Avostore Information.', 'avoskin' );
		}
	}

endif;

return new WC_Join_Avostore();
