<?php
/**
 * Class WC_Customer_Resi_order file.
 *
 * @package WooCommerce\Emails
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'WC_Customer_Resi_order', false ) ) :

	/**
	 * Customer Note Order Email.
	 *
	 * Customer note emails are sent when you add a note to an order.
	 *
	 * @class       WC_Customer_Resi_order
	 * @version     3.5.0
	 * @package     WooCommerce/Classes/Emails
	 * @extends     WC_Email
	 */
	class WC_Customer_Resi_order extends WC_Email {

		/**
		 * Customer note.
		 *
		 * @var string
		 */
                
                public $resi_number;
                
                public $resi_courier;

		/**
		 * Constructor.
		 */
		public function __construct() {
			$this->id             = 'customer_resi_order';
			$this->customer_email = true;
			$this->title          = __( 'Resi Order', 'avoskin' );
			$this->description    = __( 'Resi order emails are sent when you add a resi number to an order.', 'avoskin' );
			$this->template_html  = 'emails/customer-resi-order.php';
			$this->template_plain = 'emails/plain/customer-resi-order.php';
			$this->placeholders   = [
				'{order_date}'   => '',
				'{order_number}' => '',
			];

			// Triggers.
			add_action( 'woocommerce_new_resi_info_notification', [ $this, 'trigger' ]);

			// Call parent constructor.
			parent::__construct();
		}

		/**
		 * Get email subject.
		 *
		 * @since  3.1.0
		 * @return string
		 */
		public function get_default_subject() {
			return __( 'Resi info added to your {site_title} order from {order_date}', 'avoskin' );
		}

		/**
		 * Get email heading.
		 *
		 * @since  3.1.0
		 * @return string
		 */
		public function get_default_heading() {
			return __( 'A resi info has been added to your order', 'avoskin' );
		}

		/**
		 * Trigger.
		 *
		 * @param array $args Email arguments.
		 */
		public function trigger( $args ) {
			$this->setup_locale();

			if ( ! empty( $args ) ) {
				$defaults = [
					'order_id'      => '',
				];

				$args = wp_parse_args( $args, $defaults );

				$order_id      = $args['order_id'];

				if ( $order_id ) {
					$this->object = wc_get_order( $order_id );

					if ( $this->object ) {
						$this->recipient                      = $this->object->get_billing_email();
						$this->resi_number                  = $args['resi'];
                                                $this->resi_courier                  = $args['courier'];
						$this->placeholders['{order_date}']   = wc_format_datetime( $this->object->get_date_created() );
						$this->placeholders['{order_number}'] = $this->object->get_order_number();
					}
				}
			}

			if ( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();
		}

		/**
		 * Get content html.
		 *
		 * @return string
		 */
		public function get_content_html() {
			return wc_get_template_html(
				$this->template_html,
				[
					'order'              => $this->object,
					'email_heading'      => $this->get_heading(),
					'additional_content' => $this->get_additional_content(),
					'resi_number'      => $this->resi_number,
                                        'resi_courier'      => $this->resi_courier,
					'sent_to_admin'      => false,
					'plain_text'         => false,
					'email'              => $this,
				]
			);
		}

		/**
		 * Get content plain.
		 *
		 * @return string
		 */
		public function get_content_plain() {
			return wc_get_template_html(
				$this->template_plain,
				[
					'order'              => $this->object,
					'email_heading'      => $this->get_heading(),
					'additional_content' => $this->get_additional_content(),
					'resi_number'      => $this->resi_number,
                                        'resi_courier'      => $this->resi_courier,
					'sent_to_admin'      => false,
					'plain_text'         => true,
					'email'              => $this,
				]
			);
		}

		/**
		 * Default content to show below main email content.
		 *
		 * @since 3.7.0
		 * @return string
		 */
		public function get_default_additional_content() {
			return __( 'Thanks for reading.', 'avoskin' );
		}
	}

endif;

return new WC_Customer_Resi_order();
