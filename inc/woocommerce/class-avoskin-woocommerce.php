<?php
/**
 * Avoskin WooCommerce Class
 *
 * @package  avoskin
 * @since    2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Avoskin_WooCommerce' ) ) {
        
	/**
	 * The Avoskin WooCommerce Integration class
	 */
	class Avoskin_WooCommerce {
                
		private $model, $loyalty; // ::CRLOYALTY::
		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			$this->model = \Avoskin\Model::get_instance();
			$this->loyalty = \Avoskin\Loyalty::get_instance(); // ::CRLOYALTY::
			
			add_action( 'after_setup_theme', [ $this, 'setup' ] );
			add_filter( 'body_class', [$this, 'woocommerce_body_class' ] );
			add_filter( 'woocommerce_output_related_products_args', [ $this, 'related_products_args' ] );
			add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
			add_action( 'pre_get_posts', [$this,'woocommerce_per_page'], 999 );
			
			//Add custom settings for shipping
			add_filter( 'woocommerce_get_sections_shipping' , [$this,'add_custom_shipping_settings_tab'] );
			add_filter( 'woocommerce_get_settings_shipping' , [$this,'custom_shipping_get_settings'] , 10, 2 );
			
			add_filter( 'woocommerce_get_sections_advanced' , [$this,'add_custom_advanced_settings_tab'] );
			add_filter( 'woocommerce_get_settings_advanced' , [$this,'custom_advanced_get_settings'] , 10, 2 );
			
			add_filter( 'woocommerce_get_sections_products' , [$this,'add_custom_products_settings_tab'] );
			add_filter( 'woocommerce_get_settings_products' , [$this,'custom_products_get_settings'] , 10, 2 );
			
			//Customize my account link
			add_filter ( 'woocommerce_account_menu_items', [$this,'my_account_menu'] );
			add_action( 'init', [$this, 'add_endpoint_my_account'] );
			
			add_action( 'woocommerce_account_review_endpoint', [$this,'review_display_my_account'] );
			add_action( 'woocommerce_account_wishlist_endpoint', [$this,'wishlist_display_my_account'] );
			add_action( 'woocommerce_account_loyalty_endpoint', [$this,'loyalty_display_my_account'] ); // ::CRLOYALTY::
			
			//Add review query var for my account review page
			add_filter( 'query_vars', [$this,'review_query_vars'], 0 );
			
			//Create custom email template for additional avoskin email
			add_filter( 'woocommerce_email_classes', [$this, 'add_woocommerce_custom_email'] );
			
			//Add space after currency symbol
			add_filter('woocommerce_currency_symbol', [$this,'currency_space'], 10, 2);
			
			//Include category as search term
			add_filter('posts_search', [$this,'search_with_category'], 500, 2);
			
			
			//Add on delivery order status
			add_action( 'init',  [$this,'register_on_delivery_order_status'] );
			add_filter( 'wc_order_statuses', [$this,'add_on_delivery_to_order_statuses'] );
			add_filter( 'bulk_actions-edit-shop_order', [$this,'add_register_bulk_action'] );
			//Make sure that "action name" in the hook is the same like the option value from the above function
			add_action( 'admin_action_mark_on_delivery', [$this,'bulk_process_on_delivery'] );
			add_action( 'admin_action_mark_packing', [$this,'bulk_process_packing'] );
			
			add_action('admin_notices', [$this,'on_delivery_status_notices']);
			//Add custom prefix to Order ID/Number
			add_filter( 'woocommerce_order_number', [$this, 'order_prefix'] );
			
			add_action( 'woocommerce_product_query', [$this,'hide_gift_product'], 10, 2 );
			
			add_filter( 'wc_twilio_sms_customer_sms_before_variable_replace', [$this,'sv_wc_twilio_sms_variable_replacement'], 10, 2 );
			
			//Add payment channel to midtrans
			add_filter('wc_midtrans_settings', [$this,'midtrans_payment_channel']);
			
			//Waitist text
			add_filter('wcwl_join_waitlist_message_text', [$this,'waitlist_join_intro']);
			add_filter('wcwl_join_waitlist_success_message_text', [$this,'waitlist_join_success']);
			add_filter('wcwl_join_waitlist_already_joined_message_text', [$this,'waitlist_join_success']);
			add_filter('wcwl_leave_waitlist_message_text', [$this,'waitlist_leave_msg']);
			add_filter('wcwl_leave_waitlist_success_message_text', [$this,'waitlist_leave_success']);
			add_filter('wcwl_account_tab_title', [$this,'waitlist_account_tab']);
			add_filter('wcwl_shortcode_title', [$this,'waitlist_account_tab']);
			add_filter('wcwl_shortcode_intro_text', '__return_empty_string');
			add_filter('wcwl_shortcode_archive_intro_text', '__return_empty_string');
			add_filter('wcwl_shortcode_archive_remove_text', '__return_empty_string');
			add_filter('wcwl_shortcode_archive_product_title', '__return_empty_string');
			add_filter('wcwl_shortcode_remove_text', [$this,'waitlist_account_remove']);
			
			//Add WA options to wilio
			add_filter('wc_twilio_sms_settings', [$this,'twilio_whatsapp_number']);
			
			// save WA settings
			add_action( 'woocommerce_update_options_twilio_sms', [ $this, 'save_twilio_settings' ] );
			
			add_action('admin_footer', [$this,'inject_script'], 999);
			
			//Add custom settings for shopback
			add_filter( 'woocommerce_get_sections_products' , [$this,'add_shopback_settings_tab'] );
			add_filter( 'woocommerce_get_settings_products' , [$this,'shopback_get_settings'] , 10, 2 );
			
			//Call shopback
			add_action( 'woocommerce_order_status_changed', [$this,'execute_shopback'], 10, 4);
			
			//Custom coupon
			add_filter( 'woocommerce_coupon_discount_types', [$this,'add_shipping_coupon'] );
			
			//FIlter related product
			add_filter('woocommerce_related_products', [$this, 'exclude_hidden_product'], 10, 3);
			
			add_filter('woocommerce_admin_reports', [$this, 'add_advisor_report']);
			
			//Set conversion for skin advisor
			add_action( 'woocommerce_order_status_pending_to_processing', [$this,'advisor_statistic_conversion']);
			add_action( 'woocommerce_order_status_on-hold_to_processing', [$this,'advisor_statistic_conversion']);
			
			//ADD/REMOVE SALE CATEGORY BASED ON PRICE
			add_action( 'updated_post_meta', [$this,'sale_product_category_invoker'], 10, 4 );
			
			//Record point when order status change to completed
			add_action( 'woocommerce_order_status_completed', [$this,'record_banking_point'], 20, 2); // ::CRLOYALTY::
			
			add_filter('woocommerce_product_get_image', [$this, 'lazyload_img'], 30, 6);
			
			add_action('woocommerce_product_query', [$this,'modify_product_query'], 99, 2);
			
			add_filter('woocommerce_apply_individual_use_coupon', [$this, 'exclude_shipping_coupon_removed'], 999, 3);
			
			// ::CRLOYALTY::
			add_action('woocommerce_admin_order_totals_after_discount', [$this,'add_fees_info_at_order_admin']);
			
		}
		
		/**
		 * Sets up theme defaults and registers support for various WooCommerce features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 *
		 * @since 2.4.0
		 * @return void
		 */
		public function setup() {
			add_theme_support(
				'woocommerce', apply_filters(
					'avoskin_woocommerce_args', [
						'single_image_width'    => 450,
						'thumbnail_image_width' => 183,
						'product_grid'          => [
							'default_columns' => 3,
							'default_rows'    => 4,
							'min_columns'     => 1,
							'max_columns'     => 3,
							'min_rows'        => 1,
						]
					]
				)
			);
		}
		/**
		 * Add WooCommerce specific classes to the body tag
		 *
		 * @param  array $classes css classes applied to the body tag.
		 * @return array $classes modified to include 'woocommerce-active' class
		 */
		public function woocommerce_body_class( $classes ) {
			
			// ::CRWFC::
			if(!is_page_template('page-checkout.php')){
				$applied =  WC()->cart->applied_coupons;
				if(is_array($applied) && !empty($applied)){
					$has_remove = false;
					foreach($applied as $a){
						$coupon = new WC_Coupon( $a);
						if($coupon->get_discount_type() != 'avo_wfc'){
							WC()->cart->remove_coupon( $a );
						}
					}
					if($has_remove){
					WC()->cart->calculate_totals();	
					}
				}
			}
			avoskin_remove_wfc_product();
			
			
			if(avoskin_is_blog()){
				$classes[] = 'avoskin-blog';
			}
			
			$classes[] = 'avoskin-woo';

			// Remove `no-wc-breadcrumb` body class.
			$key = array_search( 'no-wc-breadcrumb', $classes, true );

			if ( false !== $key ) {
				unset( $classes[ $key ] );
			}
                        
			return $classes;
		}
                
		/**
		 * Related Products Args
		 *
		 * @param  array $args related products args.
		 * @since 1.0.0
		 * @return  array $args related products args
		 */
		public function related_products_args( $args ) {
			$args = apply_filters(
				'avoskin_related_products_args', [
					'posts_per_page' => 4,
					'columns'        => 4,
				]
			);
                        
			return $args;
		}
                
		
		public function woocommerce_per_page($query){
			$per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
			if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'product' )){
				$query->set( 'posts_per_page', $per_page );
			}
			
			$orderby = filter_input(INPUT_GET, 'orderby');
			if( $query->is_main_query() && !is_admin() && is_search()){
				$query->set( 'orderby', $orderby );
				$query->set( 'posts_per_page', $per_page );
				$query =  WC()->query->product_query($query);
			}
			
			if( $query->query['post_type'] == 'product' && !is_admin() ){
				$sort = $query->get('orderby');
				if($sort != 'post__in'){
					$meta_query = $query->get('meta_query');
					$meta_query = (is_array($meta_query) && !empty($meta_query)) ? $meta_query : [];
					$meta_query[] = [
						'key' => 'product_gift',
						'value' => 'yes',
						'compare' => '!='
					];
					$query->set('meta_query', $meta_query);	
				}
			}
		}
		
		public function hide_gift_product($q, $query){
			$meta_query = $q->get('meta_query');
			$meta_query = (is_array($meta_query) && !empty($meta_query)) ? $meta_query : [];
			$meta_query[] = [
				'key' => 'product_gift',
				'value' => 'yes',
				'compare' => '!='
			];
			$q->set('meta_query', $meta_query);
		}
		
		public function search_with_category($search, $wp_query){
			global $wpdb;
			$cats = $this->model->search_product_by_cat($wp_query->query_vars['s']);
			
			if(empty($search) || empty($cats)) {
				return $search; // skip processing - no search term in query
			}
			
			$searchand = '';
			$search = "{$searchand}($wpdb->posts.post_title LIKE '%{$wp_query->query_vars['s']}%') OR " ;
			foreach ($cats as $id) {
				$id = esc_sql($wpdb->esc_like($id['id']));
				$search .= "{$searchand}($wpdb->posts.ID = '{$id}')";
				$searchand = ' OR ';
			}
			
			 if (!empty($search)) {
				$search = " AND ({$search}) ";
			}
			
			return $search;
		}
		
		public function add_custom_shipping_settings_tab($settings_tab){
			$settings_tab['rajaongkir_settings'] = __( 'Rajaongkir settings', 'avoskin' );
			$settings_tab['jne_awb_settings'] = __( 'JNE Air Waybill settings', 'avoskin' );
			$settings_tab['rpx_awb_settings'] = __( 'RPX Air Waybill settings', 'avoskin' );
			$settings_tab['jnt_awb_settings'] = __( 'J&T Air Waybill settings', 'avoskin' );
			$settings_tab['sicepat_awb_settings'] = __( 'SiCepat Air Waybill settings', 'avoskin' );
			$settings_tab['avo_package_status'] = __( 'Package status settings', 'avoskin' );
			return $settings_tab;
		}
		
		public function custom_shipping_get_settings($settings, $current_section){
			
			if( 'rajaongkir_settings' == $current_section ) {
				$rajaongkir_settings =  [
					[
						'name' => __( 'Rajaongkir Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup rajaongkir integration', 'avoskin' ),
						'id'   => 'tro_title' 
					],
					[
						'name' => __( 'API Key 1','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API key from Rajaonkir here. Only for pro', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'tro_apikey_1'	
					],
					[
						'name' => __( 'API Key 2','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API key from Rajaonkir here. Only for pro', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'tro_apikey_2'	
					],
					[
						'name' => __( 'City Origin', 'avoskin' ),
						'type' => 'select',
						'desc' => __( 'Choose your city origin'),
						'desc_tip' => true,
						'id'	=> 'tro_city_origin',
						'options' => $this->get_cities_origin()
					],
					
				];
				
				$couriers = TRO_Data::get_couriers();
				foreach($couriers as $id => $name) {
					$rajaongkir_settings[] = [
						'id' => 'tro_'.$id.'_services',
						'name' => $name	,
						'type' => 'multiselect',
						'class' => 'wc-enhanced-select',
						'description' => __("Choose allowed services by $name.", 'avoskin'),
						'options' => TRO_Data::get_services($id, true)
					];
				}
				
				$rajaongkir_settings[] = [ 'type' => 'sectionend', 'id' => 'rajaongkir_settings' ];
				
				return $rajaongkir_settings;
			
			}elseif( 'jne_awb_settings' == $current_section ) {
				$jne_awb_settings =  [
					[
						'name' => __( 'JNE Air Waybill Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup airwaybill integration for JNE', 'avoskin' ),
						'id'   => 'jne_awb_title' 
					],
					[
						'name' => __( 'API Endpoint URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Endpoint URL for JNE API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_endpoint_url'	
					],
					[
						'name' => __( 'Username','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Username for JNE API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_username'	
					],
					[
						'name' => __( 'API Key','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API Key for JNE API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_apikey'
					],
					[
						'name' => __( 'Customer Number','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input customer number for JNE API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_cust'	
					],
					[
						'name' => __( 'Shipper Name','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper name', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_shipper_name'
					],
					[
						'name' => __( 'Shipper Phone','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper phone', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jne_awb_shipper_phone'
					],
					[
						'name' => __('Branch', 'avoskin') ,
						'type' => 'select',
						'desc' => __( 'Input JNE branch location', 'avoskin'),
						'class' => 'wc-enhanced-select',
						'options' => AWB_Data::jne_get_branch(),
						'id'	=> 'jne_awb_branch'
					],
					[
						'name' => __('Origin', 'avoskin') ,
						'type' => 'select',
						'desc' => __( 'Input JNE origin location', 'avoskin'),
						'class' => 'wc-enhanced-select',
						'options' => AWB_Data::jne_get_origin(),
						'id'	=> 'jne_awb_origin'
					],
				];
				
				$jne_awb_settings[] = [ 'type' => 'sectionend', 'id' => 'jne_awb_settings' ];
				
				return $jne_awb_settings;
			
			}elseif( 'rpx_awb_settings' == $current_section ) {
				$rpx_awb_settings =  [
					[
						'name' => __( 'RPX Air Waybill Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup airwaybill integration for RPX', 'avoskin' ),
						'id'   => 'rpx_awb_title' 
					],
					[
						'name' => __( 'API Endpoint URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Endpoint URL for RPX API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_endpoint_url'	
					],
					[
						'name' => __( 'Username','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Username for RPX API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_username'	
					],
					[
						'name' => __( 'Password','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Password for RPX API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_password'
					],
					[
						'name' => __( 'Account Number','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input account number for RPX API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_account'	
					],
					[
						'name' => __( 'Company Name','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default company name', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_company_name'
					],
					[
						'name' => __( 'Shipper Name','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper name', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_shipper_name'
					],
					[
						'name' => __( 'Shipper Phone','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper phone', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'rpx_awb_shipper_phone'
					],
				];
				
				$rpx_awb_settings[] = [ 'type' => 'sectionend', 'id' => 'rpx_awb_settings' ];
				
				return $rpx_awb_settings;
			
			} elseif( 'jnt_awb_settings' == $current_section ) {
				$jnt_awb_settings =  [
					[
						'name' => __( 'J&T Air Waybill Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup airwaybill integration for J&T', 'avoskin' ),
						'id'   => 'jnt_awb_title' 
					],
					[
						'name' => __( 'API Endpoint URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Endpoint URL for JNT API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_endpoint_url'	
					],
					[
						'name' => __( 'Key','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Key for J&T API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_key'	
					],
					[
						'name' => __( 'Username','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Username for J&T API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_username'
					],
					[
						'name' => __( 'API Key','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API Key for J&T API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_api'
					],
					[
						'name' => __( 'Shipper Name','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper name', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_shipper_name'
					],
					[
						'name' => __( 'Shipper Phone','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper phone', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'jnt_awb_shipper_phone'
					],
				];
				
				$jnt_awb_settings[] = [ 'type' => 'sectionend', 'id' => 'jnt_awb_settings' ];
				
				return $jnt_awb_settings;
			}  elseif( 'sicepat_awb_settings' == $current_section ) {
				//$range = get_option('sicepat_awb_range');
				//$avail = 0;
				//if($range != ''){
				//	$range = explode('-', $range);
				//	if(is_array($range) && !empty($range)){
				//		$left = (int)$range[1] - (int)$range[0];
				//		$avail = $left - count(get_option('sicepat_used_awb', []));
				//	}
				//}
				$avail = count(get_option('sicepat_avail_awb', []));
				$sicepat_awb_settings =  [
					[
						'name' => __( 'SiCepat Air Waybill Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup airwaybill integration for SiCepat', 'avoskin' ),
						'id'   => 'sicepat_awb_title' 
					],
					[
						'name' => __( 'API Key','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Key for SiCepat API', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_key'	
					],
					[
						'name' => __('Mode', 'avoskin') ,
						'type' => 'select',
						'options' => [
							'testing' => 'Testing',
							'production' => 'Production'
						],
						'id'	=> 'sicepat_awb_mode'
					],
					[
						'name' => __( 'API Endpoint Testing URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Endpoint URL for SiCepat testing mode', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_endpoint_url_testing'	
					],
					[
						'name' => __( 'API Endpoint Production URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input Endpoint URL for SiCepat production mode', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_endpoint_url_production'	
					],
					[
						'name' => __( 'AWB Number Range','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Separate with dash. Available number left: <b style="font-weight:700;">'.$avail.'</b><a id="reset-sicepat-counter" href="#" class="button" style="margin:10px; 0 0;">Reset Counter</a>', 'avoskin'),
						'desc_tip' => false,
						'id'	=> 'sicepat_awb_range'	
					],
					[
						'name' => __( 'Shipper Name','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper name', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_name'
					],
					[
						'name' => __( 'Shipper Phone','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper phone', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_phone'
					],
					//[
					//	'name' => __( 'Shipper Pickup Origin','avoskin' ),
					//	'type' => 'select',
					//	'class' => 'wc-enhanced-select',
					//	'desc' => __( 'Input default shipper city for pickup', 'avoskin'),
					//	'desc_tip' => true,
					//	'options' => AWB_Data::sicepat_get_origin(),
					//	'id'	=> 'sicepat_awb_shipper_origin_pickup'
					//],
					[
						'name' => __( 'Shipper Pickup Origin Code','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper origin code for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_code_pickup'
					],
					[
						'name' => __( 'Shipper Pickup Address','avoskin' ),
						'type' => 'textarea',
						'desc' => __( 'Input default shipper address for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_address_pickup'
					],
					[
						'name' => __( 'Shipper Province','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper province for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_province_pickup'
					],
					[
						'name' => __( 'Shipper City','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper city for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_city_pickup'
					],
					[
						'name' => __( 'Shipper District','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper district for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_district_pickup'
					],
					[
						'name' => __( 'Shipper Postcode','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input default shipper postcode for pickup', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'sicepat_awb_shipper_zip_pickup'
					],
				];
				
				$sicepat_awb_settings[] = [ 'type' => 'sectionend', 'id' => 'sicepat_awb_settings' ];
				
				return $sicepat_awb_settings;
			}elseif( 'avo_package_status' == $current_section ) {
				$package_status =  [
					[
						'name' => __( 'Package Status Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup package status for order', 'avoskin' ),
						'id'   => 'avo_package_title' 
					],
					[
						'name' => __( 'Status Lists','avoskin' ),
						'type' => 'textarea',
						'css'       => 'height: 65px;',
						'desc' => __( 'Input status list one item per-line', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'avo_package_status_list'	
					],
				];
				
				$package_status[] = [ 'type' => 'sectionend', 'id' => 'avo_package_status' ];
				
				return $package_status;
			} else {
				return $settings;
			}
		}
		
		
		public function add_custom_advanced_settings_tab($settings_tab){
			$settings_tab['avo_point_settings'] = __( 'Point settings', 'avoskin' );
			$settings_tab['avo_enc_settings'] = __( 'Encryption settings', 'avoskin' ); // ::PENTEST::
			return $settings_tab;
		}
		
		public function custom_advanced_get_settings($settings, $current_section){
			if( 'avo_point_settings' == $current_section ) {
				$point_settings =  [
					[
						'name' => __( 'Point Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup avoskin banking point', 'avoskin' ),
						'id'   => 'avo_point_title' 
					],
					[
						'name' => __( 'API Base URL','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API Endpoint URL', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'avo_point_url'	
					],
					[
						'name' => __( 'API Key','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Input API Key ', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'avo_point_api_key'	
					],
					[
						'name' => __( 'SMS OTP URL','avoskin' ),
						'type' => 'text',
						'id'	=> 'avo_otp_url'	
					],
					[
						'name' => __( 'SMS OTP Username','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Username for https://smsviro.com/ ', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'avo_otp_username'	
					],
					[
						'name' => __( 'SMS OTP Password','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Password for https://smsviro.com/ ', 'avoskin'),
						'desc_tip' => true,
						'id'	=> 'avo_otp_password'	
					],
					[
						'name' => __( 'SMS OTP Message','avoskin' ),
						'type' => 'text',
						'desc' => __( 'Use {otp} to display OTP number at SMS message', 'avoskin'),
						'desc_tip' => false,
						'id'	=> 'avo_otp_msg'	
					],
				];
				
				$point_settings[] = [ 'type' => 'sectionend', 'id' => 'avo_point_settings' ];
				
				return $point_settings;
			}elseif( 'avo_enc_settings' == $current_section ) { // ::PENTEST::
				$enc_settings =  [
					[
						'name' => __( 'Encryption Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup avoskin checkout encryption', 'avoskin' ),
						'id'   => 'avo_enc_title' 
					],
					[
						'name' => __( 'RSA Private Key','avoskin' ),
						'type' => 'textarea',
						'desc_tip' => false,
						'id'	=> 'avo_enc_private'	
					],
					[
						'name' => __( 'RSA Public Key','avoskin' ),
						'type' => 'textarea',
						'desc_tip' => false,
						'id'	=> 'avo_enc_public'	
					],
				];
				
				$enc_settings[] = [ 'type' => 'sectionend', 'id' => 'avo_enc_settings' ];
				
				return $enc_settings;
			} else {
				return $settings;
			}
		}
		
		public function add_custom_products_settings_tab($settings_tab){
			$settings_tab['avo_products_settings'] = __( 'Product Settings', 'avoskin' );
			return $settings_tab;
		}
		
		public function custom_products_get_settings($settings, $current_section){
			if( 'avo_products_settings' == $current_section ) {
				$product_settings =  [
					[
						'name' => __( 'Products Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup avoskin product', 'avoskin' ),
						'id'   => 'avo_product_title'
					],
					[
						'name' => __('Sorting', 'avoskin') ,
						'type' => 'select',
						'desc' => __( 'Sorting product settings', 'avoskin'),
						'class' => 'wc-enhanced-select',
						'options' => [
							'default' => 'Default',
							'instock_latest' => 'Product In Stock, Sort by Latest',
							'onsale_instock' => 'Product On Sale, In Stock, Other',
							'latest_updated' => 'Latest Updated',
							'selected_product' => 'Selected Product, Latest Product',
						],
						'id'	=> 'avo_product_sorting'
					],
					[
						'name' => __('Choosed Product', 'avoskin') ,
						'type' => 'multiselect',
						'desc' => __( 'Choosed product settings', 'avoskin'),
						'class' => 'wc-enhanced-select',
						'options' => avoskin_get_cpt_en(),
						'id'	=> 'avo_product_choosed'
					],
				];
				
				$product_settings[] = [ 'type' => 'sectionend', 'id' => 'avo_products_settings' ];
				
				return $product_settings;
			} else {
				return $settings;
			}
		}
		
		private function get_cities_origin() {
			$country = wc_get_base_location();
			$prov_id = TRO_Data::get_province_id($country['state']);
		    
			// get cities data
			$cities_raw = TRO_Data::get_cities($prov_id);
		    
			// parse raw data
			$cities = [];
			foreach($cities_raw as $id => $value) {
				$cities[$id] = $value['city_name'];
			}
			
			return $cities;
		}
		
		public function my_account_menu($menu_links){
			$menu_links['dashboard'] = __('Account', 'avoskin');
			$menu_links['orders'] = __('Order', 'avoskin');
			$menu_links['edit-address'] = __('Address', 'avoskin');
			$menu_links = array_slice( $menu_links, 0, 5, true )  + [ 'loyalty' => __('History Point', 'avoskin') ] + array_slice( $menu_links, 5, NULL, true ); // ::CRLOYALTY::
			$menu_links = array_slice( $menu_links, 0, 5, true )  + [ 'wishlist' => __('Wishlist', 'avoskin') ] + array_slice( $menu_links, 5, NULL, true );
			$menu_links = array_slice( $menu_links, 0, 5, true )  + [ 'review' => __('Review', 'avoskin') ] + array_slice( $menu_links, 5, NULL, true ); 
			
			unset( $menu_links['edit-account'] ); // Remove Account details tab
			unset( $menu_links['downloads'] ); // Disable Downloads
			return $menu_links;
		}
		
		public function add_endpoint_my_account(){
			add_rewrite_endpoint( 'review', EP_PAGES );
			add_rewrite_endpoint( 'wishlist', EP_PAGES );
			add_rewrite_endpoint( 'loyalty', EP_PAGES );
		}
		
		public function review_display_my_account(){
			get_template_part('woocommerce/myaccount/review');
		}
		
		public function wishlist_display_my_account(){
			get_template_part('woocommerce/myaccount/wishlist');
		}
		
		public function loyalty_display_my_account(){ // ::CRLOYALTY::
			echo apply_filters('loyalty_display_my_account', '');
		}
		
		public function review_query_vars($vars){
			$vars[] = 'review';
			$vars[] = 'compage';
			return $vars;
		}
		
		public function add_woocommerce_custom_email($email_classes ){
			
			//Remove default Woo New Account Email
			//unset($email_classes['WC_Email_Customer_New_Account']);
			//Remove deefault Woo Reset Password Email
			//unset($email_classes['WC_Email_Customer_Reset_Password']);
			
			//Include new email for register
			require_once get_parent_theme_file_path( '/inc/woocommerce/email/class-wc-customer-register-new-account.php' );
			$email_classes['WC_Customer_Register_New_Account'] = new WC_Customer_Register_New_Account();
			
			//Include new email for reset password
			require_once get_parent_theme_file_path( '/inc/woocommerce/email/class-wc-customer-reset-password.php' );
			$email_classes['WC_Customer_Reset_Password'] = new WC_Customer_Reset_Password();
			
			//Include new email for resi number
			require_once get_parent_theme_file_path( '/inc/woocommerce/email/class-wc-customer-resi-order.php' );
			$email_classes['WC_Customer_Resi_order'] = new WC_Customer_Resi_order();
			
			//Include new email for join avostore
			require_once get_parent_theme_file_path( '/inc/woocommerce/email/class-wc-join-avostore.php' );
			$email_classes['WC_Join_Avostore'] = new WC_Join_Avostore();
		    
			return $email_classes;
		}
		
		public function currency_space($currency_symbol, $currency){
			return $currency_symbol.' ';
		}
		
		public function register_on_delivery_order_status(){
			register_post_status( 'wc-on-delivery', [
				'label'                     => __('On Delivery', 'avoskin'),
				'public'                    => true,
				'exclude_from_search'       => false,
				'show_in_admin_all_list'    => true,
				'show_in_admin_status_list' => true,
				'label_count'               => _n_noop( 'On delivery (%s)', 'On delivery (%s)' )
			] );
			
			register_post_status( 'wc-packing', [
				'label'                     => __('Packing', 'avoskin'),
				'public'                    => true,
				'exclude_from_search'       => false,
				'show_in_admin_all_list'    => true,
				'show_in_admin_status_list' => true,
				'label_count'               => _n_noop( 'Packing (%s)', 'Packing (%s)' )
			] );
		}
		
		public function add_on_delivery_to_order_statuses($order_statuses){
			$new_order_statuses = [];
 
			// add new order status after processing
			foreach ( $order_statuses as $key => $status ) {
				$new_order_statuses[ $key ] = $status;
				if ( 'wc-processing' === $key ) {
					$new_order_statuses['wc-packing'] = 'Packing';
					$new_order_statuses['wc-on-delivery'] = 'On delivery';
				}
			}
			return $new_order_statuses;
		}
		
		public function add_register_bulk_action($bulk_actions ){
			$bulk_actions['mark_packing'] = 'Change status to packing'; 
			$bulk_actions['mark_on_delivery'] = 'Change status to on delivery';
			return $bulk_actions;
		}
		
		public function bulk_process_on_delivery(){
			if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
				return;
		 
			foreach( $_REQUEST['post'] as $order_id ) {
		 
				$order = new WC_Order( $order_id );
				$order_note = 'That\'s what happened by bulk edit:';
				$order->update_status( 'wc-on-delivery', $order_note, true ); // "misha-shipment" is the order status name (do not use wc-misha-shipment)
		 
			}
		 
			// of course using add_query_arg() is not required, you can build your URL inline
			$location = add_query_arg( [
				'post_type' => 'shop_order',
				'marked_on_delivery' => 1, // markED_awaiting_shipment=1 is just the $_GET variable for notices
				'changed' => count( $_REQUEST['post'] ), // number of changed orders
				'ids' => join( $_REQUEST['post'], ',' ),
				'post_status' => 'all'
			], 'edit.php' );
		 
			wp_redirect( admin_url( $location ) );
			exit;
		}
		
		public function bulk_process_packing(){
			if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
				return;
		 
			foreach( $_REQUEST['post'] as $order_id ) {
		 
				$order = new WC_Order( $order_id );
				$order_note = 'That\'s what happened by bulk edit:';
				$order->update_status( 'wc-packing', $order_note, true ); 
		 
			}
		 
			// of course using add_query_arg() is not required, you can build your URL inline
			$location = add_query_arg( [
				'post_type' => 'shop_order',
				'marked_packing' => 1, // markED_awaiting_shipment=1 is just the $_GET variable for notices
				'changed' => count( $_REQUEST['post'] ), // number of changed orders
				'ids' => join( $_REQUEST['post'], ',' ),
				'post_status' => 'all'
			], 'edit.php' );
		 
			wp_redirect( admin_url( $location ) );
			exit;
		}
		
		public function on_delivery_status_notices(){
			global $pagenow, $typenow;
 
			if( $typenow == 'shop_order' 
				&& $pagenow == 'edit.php'
				&& isset( $_REQUEST['marked_on_delivery'] )
				&& $_REQUEST['marked_on_delivery'] == 1
				&& isset( $_REQUEST['changed'] ) )
			{
				$message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
				echo "<div class=\"updated\"><p>{$message}</p></div>";			 
			}
			
			if( $typenow == 'shop_order' 
				&& $pagenow == 'edit.php'
				&& isset( $_REQUEST['marked_packing'] )
				&& $_REQUEST['marked_packing'] == 1
				&& isset( $_REQUEST['changed'] ) )
			{
				$message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
				echo "<div class=\"updated\"><p>{$message}</p></div>";			 
			}
		}
		
		public function order_prefix($order_id){
			$prefix = 'AVS-';
			$new_order_id = $prefix . $order_id;
			return $new_order_id;
		}
		
		public function sv_wc_twilio_sms_variable_replacement( $message, $order ) {
			$message = str_replace( '%resi%', get_post_meta($order->get_id(), 'resi_order', true), $message );
			$message = str_replace( '%norek%', get_post_meta($order->get_id(), 'bank_va_number', true), $message );
			
			return $message;
		}
		
		public function midtrans_payment_channel($settings){
			foreach( $settings as $k => $values ){
				$new_settings[$k] = $values;
				if($k == 'enabled'){
					$options = [
						'bca_va'    => __( 'Bank BCA', 'avoskin' ),
						'echannel'   => __( 'Bank Mandiri', 'avoskin' ),
						'bni_va'   => __( 'Bank BNI', 'avoskin' ),
						'bri_va' => __('Bank BRI', 'avoskin'),
						'permata_va'   => __( 'Bank Permata', 'avoskin' ),
						'credit_card'   => __( 'Credit Card', 'avoskin' ),
						'gopay'   => __( 'GoPay', 'avoskin' ),
						'akulaku' => __('Akulaku', 'avoskin'),
						'alfamart' => __('Alfamart', 'avoskin'),
						'shopeepay' => __('ShopeePay', 'avoskin'),
					];
					$new_settings['midtrans_payment_enabled'] = [
						'title' => __( 'Enabled Payment', 'avoskin' ),
						'type' => 'multiselect',
						'class' => 'wc-enhanced-select',
						'description' => __( 'Select the available payment channel', 'avoskin' ),
						'options'   => $options
					];
					$midtrans = get_option('woocommerce_midtrans_settings');
					$channel = $midtrans['midtrans_payment_enabled'];
					if(is_array($channel) && !empty($channel)){
						foreach($channel as $c){
							$new_settings['midtrans_payment_inf0_'.$c] = [
								'title' => $options[$c] . ' Payment Info',
								'type' => 'textarea',
								'class' => 'regular-input',
								'css' => 'width:400px'
							];
						}
					}
				}
			}
			return $new_settings;
		}
		
		public function waitlist_join_intro(){
			if(is_user_logged_in()){
				return __('Tekan tombol “Join Waitlist” agar kami dapat mengirimkan pemberitahuan ketika produk ini tersedia kembali.', 'avoskin');	
			}else{
				return __('Masukkan alamat e-mailmu agar kami dapat mengirimkan pemberitahuan ketika produk ini tersedia kembali.', 'avoskin');	
			}
			
		}
		
		public function waitlist_join_success(){
			return __('Kami akan mengirimkan pemberitahuan ketika produk ini tersedia kembali.', 'avoskin');
		}
		
		public function waitlist_leave_msg(){
			return __('Anda sudah terdaftar pada waiting list untuk produk ini.', 'avoskin');
		}
		
		public function waitlist_leave_success(){
			return __('Anda telah dihapus dari waiting list untuk produk ini.', 'avoskin');
		}
		
		public function waitlist_account_tab(){
			return __('Waitlist','avoskin');	
		}
		
		public function waitlist_account_remove(){
			return '<img src="'.esc_url( get_template_directory_uri() ).'/assets/img/icon/close-circle-green.svg" width="24">';
		}
		
		public function twilio_whatsapp_number($settings){
			$key = 0;
			foreach( $settings as $values ){
				$new_settings[$key] = $values;
				$key++;
				if($values['id'] == 'wc_twilio_sms_from_number'){
					$new_settings[$key] = [
						'id'       => 'avoskin_disable_sms',
						'name'    => __('Disable SMS Notification'),
						'type'     => 'select',
						'options'  => [
							'no' => __( 'No', 'woocommerce-twilio-sms-notifications' ),
							'yes'   => __( 'Yes', 'woocommerce-twilio-sms-notifications' )
						],
					];
					$key++;
					$new_settings[$key] = [
						'id'       => 'avoskin_wa_twilio_number',
						'name'    => __('WhatsApp From Number'),
						'desc_tip'     => __('Enter the number to send WhatsApp messages from. This must be a purchased number from Twilio.'),
						'type'     => 'text',
					];
					$key++;
					
					$new_settings[$key] = [
						'id'       => 'avoskin_disable_wa',
						'name'    => __('Disable WhatsApp Notification'),
						'type'     => 'select',
						'options'  => [
							'no' => __( 'No', 'woocommerce-twilio-sms-notifications' ),
							'yes'   => __( 'Yes', 'woocommerce-twilio-sms-notifications' )
						],
					];
					$key++;
				}
			}
			return $new_settings;
		}
		
		public function save_twilio_settings(){
			if(isset($_POST['avoskin_wa_twilio_number'])){
				update_option('avoskin_wa_twilio_number', $_POST['avoskin_wa_twilio_number']);
			}
			
			if(isset($_POST['avoskin_disable_sms'])){
				update_option('avoskin_disable_sms', $_POST['avoskin_disable_sms']);
			}
			
			if(isset($_POST['avoskin_disable_wa'])){
				update_option('avoskin_disable_wa', $_POST['avoskin_disable_wa']);
			}
		}
		
		public function inject_script(){
			?>
				<script type="text/javascript">
					;(function($){
						if ($('.woocommerce_page_wc-settings').length) {
							$('.nav-tab-wrapper .nav-tab').each(function(){
								if($(this).text() == 'SMS'){
									$(this).text('SMS & WhatsApp');
								}
							});
						}
					})(jQuery);
				</script>
			<?php
		}
		
		public function add_shopback_settings_tab($settings_tab){
			$settings_tab['shopback_settings'] = __( 'Shopback', 'avoskin' );
			return $settings_tab;
		}
		
		public function shopback_get_settings($settings, $current_section){
			if( 'shopback_settings' == $current_section ) {
				$shopback_settings =  [
					[
						'name' => __( 'Shopback Settings', 'avoskin' ),
						'type' => 'title',
						'desc' => __( 'Setup Shopback integration', 'avoskin' ),
						'id'   => 'shopback_title' 
					],
					[
						'name' => __( 'Enable shopback integration?','avoskin' ),
						'type' => 'select',
						'options' => [
							'no' => 'No',
							'yes' => 'Yes'
						],
						'id'	=> 'shopback_enable'	
					],
					[
						'name' => __( 'Order Offer ID','avoskin' ),
						'type' => 'text',
						'id'	=> 'shopback_order_offer_id'	
					],
					[
						'name' => __( 'Validation Offer ID','avoskin' ),
						'type' => 'text',
						'id'	=> 'shopback_validation_offer_id'	
					],
					[
						'name' => __( 'Security Token','avoskin' ),
						'type' => 'text',
						'id'	=> 'shopback_security_token'	
					],
					[
						'name' => __( 'Affiliate ID','avoskin' ),
						'type' => 'text',
						'id'	=> 'shopback_aff_id'	
					],
					[
						'name' => __( 'Shopback referral field','avoskin' ),
						'type' => 'text',
						'id'	=> 'shopback_ref_field'
					]
				];
				
				$shopback_settings[] = [ 'type' => 'sectionend', 'id' => 'shopback_settings' ];
				
				return $shopback_settings;
			} else {
				return $settings;
			}
		}
		
		public function execute_shopback($order_id, $from_status, $to_status, $order){
			$sbrf = get_post_meta( $order_id, 'avosbrf', true);
			if( get_option('shopback_enable') == 'yes' && $sbrf != ''){
				$sbrf = json_decode(base64_decode($sbrf), true);
				$coupon = $order->get_used_coupons();
				$coupon = (is_array($coupon) && !empty($coupon) && isset($coupon[0]) && $coupon[0] != '') ? strtoupper($coupon[0]) : '';
				
				if($to_status == 'processing'){
					$default = [
						'offer_id' => get_option('shopback_order_offer_id'),
						'timezone' => 'Asia/Jakarta',
						'datetime' => wc_format_datetime( $order->get_date_created(), 'Y-m-d H:i:s' ),
						'transaction_id' => $sbrf['transaction_id'],
						'security_token' => get_option('shopback_security_token')
					];
					if($coupon != ''){
						$default['adv_unique1'] = $coupon;
					}
					foreach ( $order->get_items() as $item_id => $item ) {
						$product = $item->get_product();
						$sku = ($product->get_sku() != '') ? $product->get_sku() : $product->get_id();
						$category  = '';
						$cats = $product->get_category_ids();
						if(is_array($cats) && !empty($cats)){
							$n = 1;
							foreach($cats as $c){
								if( $term = get_term_by( 'id', $c, 'product_cat' ) ){
									$category .= ($n < count($cats)) ?  $term->name. ', ' :  $term->name;
								}
								$n++;
							}
						}
						$query = [
							'adv_sub' => $order->get_order_number() . '-'. $sku,
							'adv_sub2' => $item->get_quantity(),
							'adv_sub3' => $category,
							'adv_sub4' => $sku,
							'adv_sub5' => $product->get_name(),
							'amount' =>  $item->get_total(),
						];
						$url = 'http://shopback.go2cloud.org/aff_lsr?' . http_build_query(array_merge($query, $default));
						$call = wp_remote_get($url);
					}
				}
				
				if($from_status != 'on-hold' && $from_status != 'pending'){
					if($to_status == 'completed' || $to_status == 'cancelled' || $to_status == 'failed' || $to_status == 'refunded'){
						$approval = ($to_status == 'completed') ? 'approved' : 'rejected';
						$default = [
							'offer_id' => get_option('shopback_validation_offer_id'),
							'timezone' => 'Asia/Jakarta',
							'security_token' => get_option('shopback_security_token'),
							'aff_id' => get_option('shopback_aff_id'),
							'status' => $approval
						];
						if($coupon != ''){
							$default['adv_unique1'] = $coupon;
						}
						foreach ( $order->get_items() as $item_id => $item ) {
							$product = $item->get_product();
							$sku = ($product->get_sku() != '') ? $product->get_sku() : $product->get_id();
							$category  = '';
							$cats = $product->get_category_ids();
							if(is_array($cats) && !empty($cats)){
								$n = 1;
								foreach($cats as $c){
									if( $term = get_term_by( 'id', $c, 'product_cat' ) ){
										$category .= ($n < count($cats)) ?  $term->name. ', ' :  $term->name;
									}
									$n++;
								}
							}
							$query = [
								'adv_sub' => $order->get_order_number() . '-'. $sku,
								'adv_sub2' => $item->get_quantity(),
								'adv_sub3' => $category,
								'adv_sub4' => $sku,
								'adv_sub5' => $product->get_name(),
								'amount' =>  $item->get_total(),
							];
							$url = 'http://shopback.go2cloud.org/aff_lsr?' . http_build_query(array_merge($query, $default));
							$call = wp_remote_get($url);
						}
					}		
				}
			}
		}
		
		public function add_shipping_coupon( $types ){
			$types['avo_shipping'] = __( 'Shipping', 'avoskin' );
			$types['avo_wfc'] = __( 'Waste for Changes', 'avoskin' ); // ::CRWFC::
			return $types;
		}
		
		public function exclude_hidden_product($query, $product_id, $args){
			if(is_array($query) && !empty($query)){
				foreach($query as $key => $q){
					if(get_post_meta($q, 'product_gift', true) == 'yes'){
						unset($query[$key]);
					}
				}
			}
			return $query;
		}
		
		public function add_advisor_report($reports){
			$reports['skin_advisor'] = [
				'title'   => __( 'Skin Advisor', 'woocommerce' ),
				'reports' => [
					'general' => [
						'title'       => __( '', 'woocommerce' ),
						'description' => '',
						'hide_title'  => true,
						'callback'    => [$this, 'skin_advisor_report']
					]
				]
			];
			return $reports;
		}
		
		public function skin_advisor_report(){
			require_once get_parent_theme_file_path( '/inc/class-advisor-statistic-avoskin.php' );
			$report  = new Advisor_Statistic();
			$report->output_report();
		}
		
		public function advisor_statistic_conversion($order_id){
			$advisor = get_post_meta($order_id, 'advisor', true);
			if($advisor != '' && $advisor != 0){
				update_post_meta((int)$advisor, 'conversion', 'yes');
				update_post_meta((int)$advisor, 'order', $order_id);
			}
		}
		
		public function sale_product_category_invoker($meta_id, $post_id, $meta_key, $meta_value){
			if(get_post_type($post_id) == 'product'){
				$sale = get_term_by('slug', 'sale', 'product_cat');
				$product = wc_get_product($post_id);
				$cats = $product->get_category_ids();
				$cats = (is_array($cats)) ? $cats : [];
				
				if($product->get_price() == $product->get_regular_price()){
					
					if(in_array($sale->term_id, $cats)){
						$key = array_search($sale->term_id, $cats);
						unset($cats[$key]);
					}
				}else{
					if(!in_array($sale->term_id, $cats)){
						array_push($cats, $sale->term_id);
					}
				}
				if(!empty($cats)){
					$product->set_category_ids(array_values($cats));
					$product->save();
				}
			}
		}
		// ::CRLOYALTY::
		public function record_banking_point($order_id, $order){
			if(get_post_meta($order_id, 'recorded_point', true) != 'yes'){
				$country = $order->get_billing_country();
				$state = $order->get_billing_state();
				$courier = get_post_meta($order->get_id(), 'shipping_courier', true);
				$courier_service = get_post_meta($order->get_id(), 'shipping_service', true);
				$prov = WC()->countries->get_states( $country )[$state] ;
				$city = wp_strip_all_tags(esc_attr($order->get_billing_city()));
				$address =  wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_billing_address_1()))) . ' ' . $city . ' ' . $prov . ' ' . $order->get_billing_postcode();
				
				 if($courier == 'sicepat'){
					$sicepat_req = json_decode(get_post_meta($order->get_id(), 'sicepat_temp_request', true), true);
					$sicepat_req = $sicepat_req['PackageList'][0];
					$road = ($sicepat_req['recipient_address'] != '' ) ? wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $sicepat_req['recipient_address']))) :
																			wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_billing_address_1())));
					$district =  ($sicepat_req['recipient_district']  != '' ) ? $sicepat_req['recipient_district']  : '';
					$city = ($sicepat_req['recipient_city'] != '' ) ? $sicepat_req['recipient_city'] : $city ;
					$prov = ($sicepat_req['recipient_province'] != '') ? $sicepat_req['recipient_province'] : $prov ;
					$zip = ($sicepat_req['recipient_zip']  != '' ) ? $sicepat_req['recipient_zip'] : $order->get_billing_postcode();
					$address = $road. ' ' . $district . ' ' . $city  . ' ' . $prov. ' ' .  $zip ;
				}
				$products = [];
				foreach ( $order->get_items() as $item_id => $item ) {
					$product = $item->get_product();
					$products[] = [
						'sku' => $product->get_sku(),
						'name' => $item->get_name(),
						'qty' => $item->get_quantity(),
						'price' => (int)$item->get_subtotal() / (int)$item->get_quantity()
					];
				}
				$phone = str_replace('+', '', $order->get_billing_phone());
				$phone = ($phone[0] == 0) ? preg_replace('/0/', '62', $phone, 1) : $phone;
				
				$body = [
					'origin_order_id' => $order->get_order_number(),
					'status' => strtoupper($order->get_status()),
					'order_date' => wc_format_datetime( $order->get_date_created(), 'c' ),
					'shipping_address' => $address,
					'shipping_province' => $prov,
					'shipping_city' => $city,
					'shipping_method' => strtoupper($courier),
					'shipping_service' => strtoupper($courier_service),
					'shipping_price' => $order->get_shipping_total(),
					'grand_total' => $order->get_total(),
					'discount' => $order->get_total_discount(),
					'point_usage' => 0,
					'user_detail' => [
						'full_name' =>  wp_strip_all_tags(esc_attr($order->get_billing_first_name()) . ' ' . esc_attr($order->get_billing_last_name())),
						//'phone_number' => $phone,
						'phone_number' => $this->loyalty->format_phone($this->loyalty->get_loyalty_phone($order->get_user_id())),
						'email' => $order->get_billing_email(),
						'gender' => get_user_meta($order->get_user_id(), 'gender', true)
					],
					'products' => $products
				];
				$args = [
					'headers' => [
						'Content-Type' => 'application/json',
						'access-token' => get_option('avo_point_api_key')
					],
					'body' => wp_json_encode($body)
				];
				$url = get_option('avo_point_url').'pa/order/';
				$result = wp_remote_post($url, $args);
				if($result['response']['code'] == 200){
					update_post_meta($order->get_id(), 'recorded_point', 'yes');
				}
				update_post_meta($order->get_id(), 'record_point_param', wp_json_encode([
					'headers' => [
						'Content-Type' => 'application/json',
						'access-token' => get_option('avo_point_api_key')
					],
					'body' => $body
				]));
			}
		}
		
		public function lazyload_img($image, $product, $size, $attr, $placeholder, $img){
			preg_match( '@src="([^"]+)"@' , $image, $match );
			$src = array_pop($match);
			return '<img class="lazy" data-src="'.$src.'" width="'.$size[0].'" height="'.$size[1].'" />';
		}
		
		public function modify_product_query($q, $query){
			$sorting = get_option('avo_product_sorting');
			if($sorting == 'instock_latest'){
				$meta_query = $q->get('meta_query');
				$meta_query = (is_array($meta_query) && !empty($meta_query)) ? $meta_query : [];
				$meta_query[] = [
					'key' => '_stock_status',
					'value' => 'instock',
					'compare' => '='
				];
				if(count($meta_query) > 1 && !isset($meta_query['relation'])){
					$meta_query['relation'] = 'AND';
				}
				$q->set('meta_query', $meta_query);
				$q->set('orderby', 'date ID');
				$q->set('order', 'DESC');
			}elseif($sorting == 'onsale_instock'){
				$products = $this->model->get_product_sale_instock();
				$q->set('post__in', $products);
				$q->set('orderby', 'post__in');
			}elseif($sorting == 'latest_updated'){
				$q->set('orderby', 'modified');
			}elseif($sorting == 'selected_product'){
				$choosed = get_option('avo_product_choosed', []);
				$products = $this->model->get_product_choosed($choosed);
				$q->set('post__in', $products);
				$q->set('orderby', 'post__in');
			}
		}
		
		public function exclude_shipping_coupon_removed($exclude, $the_coupon, $coupons){ 
			foreach($coupons as $c){
				$coupon = new WC_Coupon($c);
				if($coupon->get_discount_type() == 'avo_shipping' || $coupon->get_discount_type() == 'avo_wfc'){
					$exclude[] = $c;
				}
			}
			return $exclude;
		}
		
		public function add_fees_info_at_order_admin($order_id){ // ::CRLOYALTY::
			$order = wc_get_order($order_id);
			$fees = $order->get_fees();
			if(is_array($fees) && !empty($fees)){
				foreach($fees as $f):
					$label = $f->get_name();
					$value = $f->get_total();
					if($label == 'Loyalty Point'):
				?>
				<tr>
					<td class="label"><?php echo $label; ?>:</td>
					<td width="1%"></td>
					<td class="custom-total"><?php echo wc_price($value); ?></td>
				</tr>
				<?php endif;endforeach;
			}
		}
	}
}

return new Avoskin_WooCommerce();
