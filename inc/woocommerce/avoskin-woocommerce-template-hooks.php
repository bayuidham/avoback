<?php

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
remove_action( 'woocommerce_thankyou', 'woocommerce_order_details_table', 10 );
remove_action( 'woocommerce_cart_is_empty', 'woocommerce_output_all_notices', 5 );
remove_action( 'woocommerce_shortcode_before_product_cat_loop', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_checkout_form_cart_notices', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_account_content', 'woocommerce_output_all_notices', 5 );
remove_action( 'before_woocommerce_pay', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

add_action('woo_avoskin_single_product', 'woo_avoskin_info_single_product', 5, 1);
add_action('woo_avoskin_single_product', 'woo_avoskin_sticky_single_product', 5, 1); //:: CRSTICKY::
add_action('woo_avoskin_single_product', 'woo_avoskin_desc_single_product', 10, 1);
add_action('woo_avoskin_single_product', 'woo_avoskin_review_single_product', 15, 1);

add_action('woo_avoskin_checkout', 'woo_avoskin_your_order_checkout', 5);
add_action('woo_avoskin_checkout', 'woo_avoskin_shipping_checkout', 10);
add_action('woo_avoskin_checkout', 'woo_avoskin_payment_checkout', 15);
add_action('woo_avoskin_checkout', 'woo_avoskin_coupon_checkout', 20);
add_action('woo_avoskin_checkout', 'woo_avoskin_summary_checkout', 25);
add_action('woo_avoskin_checkout', 'woo_avoskin_action_checkout', 30);

add_action('woocommerce_admin_order_totals_after_discount', 'woo_add_subtotal_on_order', 10, 1);

add_action('avoskin_payment_instruction', 'avoskin_payment_instruction_view',10,1);