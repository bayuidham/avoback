<?php
if ( ! function_exists( 'woo_avoskin_info_single_product' ) ) {
	function woo_avoskin_info_single_product($product) {
		$gallery = $product->get_gallery_image_ids();
		$thumb = get_post_thumbnail_id($product->get_id());
		array_unshift($gallery,$thumb);
		$stock = ceil(get_post_meta( $product->get_id(), '_stock', true ));
	?>
		<div class="info">
			<div class="wrapper">
				<?php if(is_array($gallery) && !empty($gallery)):?>
					<div class="gallery">
						<div class="big">
							<?php if($product->get_stock_status() != 'instock'):?>
								<span class="stock-empty"><?php avoskin_oos_text($product->get_id());?></span>
							<?php endif;?>
							<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/wishlist.svg" /></a>
							<div class="slick-carousel">
								<?php foreach($gallery as $g):
									$img_url = wp_get_attachment_url( $g,'full'); //get img URL
									$image = avoskin_resize( $img_url, 480, 480, true, true, true ); //resize & crop img
								?>
									<div class="item">
										<a href="<?php echo $img_url ;?>" data-fancybox="gallery">
											<img src="<?php echo $image ;?>" />        
										</a>
									</div>
								<?php endforeach;?>
							</div><!-- end of slick -->
						</div><!-- end of big -->
						<div class="thumb">
							<div class="slick-carousel">
								<?php foreach($gallery as $g):
									$img_url = wp_get_attachment_url( $g,'full'); //get img URL
									$image = avoskin_resize( $img_url, 120, 120, true, true, true ); //resize & crop img
								?>
									<div class="item">
										<span><img src="<?php echo $image ;?>" /></span>
									</div>
								<?php endforeach;?>
							</div>
						</div><!-- end of thumb -->
					</div><!-- end of gallery -->
				<?php endif;?>
				<div class="caption">
					<div class="cat">
						<?php echo wc_get_product_category_list($product->get_id());?>
					</div><!-- end of cat -->
					<h1 class="the-title"><?php echo $product->get_name() ;?></h1>
					<div class="meta">
						<?php echo avoskin_rating($product->get_average_rating());?>
						<span>(<?php echo $product->get_review_count() ;?> <?php _e('customer reviews','avoskin');?>)</span>
						<a href="#popup-review"><img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/review.svg" /><b><?php _e('Write a Review','avoskin');?></b></a>
					</div><!-- end of meta --->
					<div class="nominal">
						<?php
							$price = $product->get_regular_price();
							$sale = $product->get_sale_price();
							if(!$product->is_on_sale()){
								$sale = 0;
							}
						?>
						<?php echo ($sale > 0) ? wc_price($sale) : wc_price($price);?><?php echo ($sale > 0) ? '<span class="discounted">'.wc_price($price).'</span>' : '';?>
					</div><!-- endof nominal -->
					<div class="format-text">
						<?php echo $product->get_short_description();?>
					</div><!-- endof format text -->
					
					<div class="layer">
						<div class="util">
							<?php if($product->get_stock_status() == 'instock'):?>
								<div class="calc calc-hollow">
									<a href="#" class="min"><i class="fa-minus"></i></a><!--
									--><span>1</span><!--
									--><a href="#" class="plus"><i class="fa-plus"></i></a>
								</div>
								<a href="#" class="button slimy act-add-to-cart has-loading" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><?php _e('Add to Cart','avoskin');?></a>
							<?php else:?>
								<a href="#" class="button slimy add-to-waitlist" style="margin-left: 0;"><?php _e('NOTIFY ME WHEN IN STOCK','avoskin');?></a>
							<?php endif;?>
							<!-- ::CRSTICKY:: -->
							<a href="#" class="add-to-wish" data-id="wid-<?php echo $product->get_id() ;?>" data-product="<?php echo $product->get_id();?>"><img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/wishlist.svg" /></a>
						</div><!-- end of util -->
						<div class="spec">
							<table>
								<tr>
									<td><?php _e('Status','avoskin');?></td>
									<td><span><?php echo ($product->get_stock_status() == 'instock') ? __('In Stock','avoskin') : __('Sold Out','avoskin')  ;?></span></td>
								</tr>
								<tr>
									<td><?php _e('Stock','avoskin');?></td>
									<td><?php echo $product->get_stock_quantity() ;?></td>
								</tr>
								<tr>
									<td><?php _e('Category','avoskin');?></td>
									<td><?php echo wc_get_product_tag_list($product->get_id());?></td>
								</tr>
								<tr>
									<td><?php _e('Weight','avoskin');?></td>
									<td><?php echo $product->get_weight() ;?> <?php echo get_option('woocommerce_weight_unit') ;?></td>
								</tr>
								<?php
									$dimension = $product->get_dimensions(false);
									$dimension = []; // ::TEMP:: HIDE DIMENSION
									if(is_array($dimension)  && !empty($dimension)):
								?>
									<tr>
										<td><?php _e('Dimension','avoskin');?></td>
										<td>
											<?php $n=1;foreach($dimension as $d => $v):?>
												<?php echo $v ;?>  <?php echo ($n < count($dimension)) ? 'x' : '';?>
											<?php $n++;endforeach;?>
											<?php echo get_option('woocommerce_dimension_unit');?>
										</td>
									</tr>
								<?php endif;?>
							</table>
						</div><!-- end of spec -->
					</div><!-- end of layer -->
				</div><!-- end of caption -->
				<div class="clear"></div>
			</div><!-- end of wrapper -->
		</div><!-- end of info -->
		<div class="popup-waitlist">
			<div class="layer">
				<a href="#" class="cls"><img src="<?php avoskin_dir();?>/assets/img/icon/close-circle-green.svg"></a>
				<div class="holder <?php echo (is_user_logged_in()) ? '' : 'form-basic';?>">
					<h3><?php _e('NOTIFY WHEN IN STOCK','avoskin');?></h3>
					<div class="wait-product">
						<figure><?php echo $product->get_image([80,80]) ;?></figure>
						<div class="wcapt">
							<h4><?php echo $product->get_name() ;?></h4>
							<strong><?php _e('Weight','avoskin');?>: <?php echo $product->get_weight() ;?> <?php echo get_option('woocommerce_weight_unit') ;?></strong>
						</div><!-- end of wcapt -->
						<div class="clear"></div>
					</div><!-- end of wait product -->
					<?php  echo do_shortcode('[woocommerce_waitlist product_id='.$product->get_id().']');?>
				</div><!-- end of holder -->
			</div><!-- end of layer -->
		</div><!-- end of popup waitlist -->
	<?php
	}
}

// ::CRSTICKY::
if ( ! function_exists( 'woo_avoskin_sticky_single_product' ) ) {
	function woo_avoskin_sticky_single_product($product) {
	?>
		<div class="sticky-product">
			<div class="wrapper rowflex">
				<div class="caption">
					<h2>
						<b><?php echo $product->get_name() ;?></b>
						<a href="#" class="add-to-wish"  data-id="wid-<?php echo $product->get_id() ;?>" data-product="<?php echo $product->get_id();?>"><img class="svg" src="<?php avoskin_dir();?>/assets/img/icon/wishlist.svg" /></a>
					</h2>
					<div class="cat">
						<?php echo wc_get_product_category_list($product->get_id());?>
					</div><!-- end of cat -->
					
				</div><!-- end of caption -->
				<div class="action">
					<div class="nominal">
						<?php
							$price = $product->get_regular_price();
							$sale = $product->get_sale_price();
							$stock = ceil(get_post_meta( $product->get_id(), '_stock', true ));
							if(!$product->is_on_sale()){
								$sale = 0;
							}
						?>
						<?php echo ($sale > 0) ? wc_price($sale) : wc_price($price);?><?php echo ($sale > 0) ? '<span class="discounted">'.wc_price($price).'</span>' : '';?>
					</div><!-- endof nominal -->
					<div class="util">
						<?php if($product->get_stock_status() == 'instock'):?>
							<div class="calc calc-hollow on-sticky">
								<a href="#" class="min"><i class="fa-minus"></i></a><!--
								--><span>1</span><!--
								--><a href="#" class="plus"><i class="fa-plus"></i></a>
							</div>
							<a href="#" class="button slimy act-add-to-cart has-loading" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i><?php _e('Add to Cart','avoskin');?></a>
							<a
								href="<?php echo home_url('/') ;?>checkout/?add-to-cart=<?php echo $product->get_id() ;?>&quantity=1"
								data-url="<?php echo home_url('/') ;?>checkout/?add-to-cart=<?php echo $product->get_id() ;?>&quantity="
								data-stock="<?php echo $stock ;?>"
								data-qty="1"
								class="button slimy act-buy-now"><?php _e('Buy Now','avoskin');?></a>
						<?php else:?>
							<a href="#" class="button slimy add-to-waitlist"><?php _e('NOTIFY ME WHEN IN STOCK','avoskin');?></a>
						<?php endif;?>
					</div><!-- end of util -->
				</div>
				<a href="#" class="totop"></a>
			</div><!-- end of wrapper -->
		</div><!-- end of sticky action -->
        <?php
	}
}

if ( ! function_exists( 'woo_avoskin_desc_single_product' ) ) {
	function woo_avoskin_desc_single_product($product) {
		$tab = get_post_meta($product->get_id(), 'avotab_item', true);
		if(is_array($tab) && !empty($tab)):
	?>
		<div class="desc">
			<div class="wrapper">
				<div class="tab-basic">
					<div class="tab-head">
						<?php $i=1;foreach($tab as $t):?>
							<a href="#tab-<?php echo $i ;?>" <?php echo ($i == 1) ? 'class="active"' : '' ;?>><?php echo $t['title'] ;?></a>
						<?php $i++;endforeach;?>
						<div class="dropselect large">
							<select>
								<?php $i=1;foreach($tab as $t):?>
									<option value="#tab-<?php echo $i ;?>"><?php echo $t['title'] ;?></option>
								<?php $i++;endforeach;?>
							</select>
						</div><!-- end of drop select -->
					</div><!-- end of tab head -->
					
					<?php $n=1;foreach($tab as $t):
						$has_video = (isset($t['video']) && $t['video'] != '' && isset($t['thumbnail']) && $t['thumbnail'] != '' ) ? true : false;
					?>
						<div id="tab-<?php echo $n ;?>" class="tab-item <?php echo ($n == 1) ? 'active' : '' ;?> <?php echo ($has_video) ? 'has-video' : '';?>">
							<div class="rowflex <?php echo ($has_video) ? 'switch' : '';?>">
								<?php if ($has_video):?>
									<figure>
										<a href="<?php echo $t['video'] ;?>" data-fancybox>
											<b><i class="fa-play"></i></b>
											<img src="<?php echo $t['thumbnail'] ;?>" alt="product" />
										</a>
									</figure>
								<?php endif;?>
								<div class="format-text">
									<?php echo $t['text'] ;?>
								</div><!-- end of format text -->
							</div><!-- end of rowflex -->
						</div><!-- end of tab item -->
					<?php $n++;endforeach;?>
				</div><!-- end of tab basic -->
			</div><!-- end of wrapper -->
		</div><!-- end of description -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'woo_avoskin_review_single_product' ) ) {
	function woo_avoskin_review_single_product($product) {
		global $sitepress;
		remove_filter( 'comments_clauses', array( $sitepress, 'comments_clauses' ), 10, 2 );
		
		define('DEFAULT_COMMENTS_PER_PAGE', 3 ); 
		$page = (get_query_var('compage')) ? get_query_var('compage') : 1;
		$limit = DEFAULT_COMMENTS_PER_PAGE;
		$offset = ($page * $limit) - $limit;
		
		$EN = apply_filters( 'wpml_object_id', $product->get_id(), 'product', false, 'en' );
		$ID = apply_filters( 'wpml_object_id', $product->get_id(), 'product', false, 'id' );
		
		$com1 = get_comments([
			'fields' => 'ids',
			'post_id' => (int)$EN,
		]);
		$com2 = get_comments([
			'fields' => 'ids',
			'post_id' => (int)$ID,
		]);
		
		$param = [
			'status' => 'approve',
			'offset' => $offset,
			'post_id' => 0,
			'comment__in' => array_merge($com1, $com2),
			'number' => $limit,
		];
		$args = [
			'orderby' => 'post_date' ,
			'order' => 'DESC',
			'post_id'=> 0,
			'comment__in' => array_merge($com1, $com2),
			'status' => 'approve',
			'parent' => 0
		];
		
		$skin_type_query = '';
		if(isset($_GET['skin_type']) && $_GET['skin_type'] != ''){
			$skin_type_query = $_GET['skin_type'];
			$param['meta_query'] = [
				[
					'key' => 'skin_type',
					'value' => serialize( $skin_type_query ),
					'compare' => 'LIKE'
				]
			];
			$args['meta_query'] = $param['meta_query'];
		}
		
		$total_comments = get_comments($args);
		$pages = ceil( count($total_comments) / DEFAULT_COMMENTS_PER_PAGE );
		$comments = get_comments($param );
	?>
		<div class="review">
			<div class="wrapper">
				<div class="rev-head rowflex">
					<h2 class="line-title"><?php _e('Reviews','avoskin');?> (<?php echo count($total_comments);?>)</h2>
					<script type="text/javascript"> ;(function($){ $(document).ready(function(){ $('.detail-product .info .meta span').html('(<?php echo count($total_comments);?> customer reviews)'); }); }(jQuery));</script>
					<div class="rev-nav">
						<form id="review-skin-type" action="" method="GET">
							<div class="dropselect dark medium">
								<select name="skin_type">
									<option value="" <?php echo ($skin_type_query == '') ? 'selected="selected"' : '';?>><?php _e('Skin Type','avoskin');?></option>
									<?php
										$skin = [
											__('Oily', 'avoskin'),
											__('Normal', 'avoskin'),
											__('Dry', 'avoskin'),
											__('Combo', 'avoskin'),
											__('Acne-Prone', 'avoskin'),
											__('Sensitive', 'avoskin')
										];
										foreach($skin as $s):
									?>
										<option value="<?php echo $s ;?>" <?php selected($s, $skin_type_query);?>><?php echo $s ;?></option>
									<?php endforeach;?>
									
								</select>
							</div><!-- end of drop select -->
						</form>
						<a href="#popup-review" class="button slimy"><?php _e('Add Review','avoskin');?></a>
						<div class="clear"></div>
					</div><!-- end of rev nav -->
				</div><!-- end of rev head -->
				<div class="rowflex">
					<div class="rate-result">
						<?php
							$item = [
								5 => 'nope',
								4 => 'min1',
								3 => 'min2',
								2 => 'min3',
								1 => 'min4'
							];
							$counter = [];
							foreach([$ID,$EN] as $idn){
								$pdn = wc_get_product($idn);
								if($pdn){
									foreach($item as $i => $v){
										$counter[$i] = (isset($counter[$i])) ? $counter[$i] + $pdn->get_rating_count($i) : $pdn->get_rating_count($i) ;
									}	
								}
								
							}
							foreach($item as $i => $v):
							$percentage = 0;
							if($counter[$i] > 0){
								$percentage = ($counter[$i] / count($total_comments)) * 100;
							}
						?>
							<div class="item">
								<div class="rate <?php echo $v ;?>"><?php for($n=1;$n<=5;$n++):?><i class="fa-star"></i><?php endfor;?></div>
								<div class="bar"><span><b style="width:<?php echo $percentage.'%'; ?>"></b></span><strong><?php echo floor($percentage).'%' ;?></strong></div><!-- end of bar -->
							</div><!-- end of item -->
						<?php endforeach;?>
						<form id="review-skin-type-mobile" action="" method="GET">
							<div class="dropselect dark large">
								<select name="skin_type">
									<option value="" <?php echo ($skin_type_query == '') ? 'selected="selected"' : '';?>><?php _e('Skin Type','avoskin');?></option>
									<?php
										$skin = [
											__('Oily', 'avoskin'),
											__('Normal', 'avoskin'),
											__('Dry', 'avoskin'),
											__('Combo', 'avoskin'),
											__('Acne-Prone', 'avoskin'),
											__('Sensitive', 'avoskin')
										];
										foreach($skin as $s):
									?>
										<option value="<?php echo $s ;?>" <?php selected($s, $skin_type_query);?>><?php echo $s ;?></option>
									<?php endforeach;?>
									
								</select>
							</div><!-- end of drop select -->
						</form>
					</div><!-- end of rate result -->
					<div class="rate-group">
					       <div class="layer">
							<div class="wrap">
								<?php
									if(is_array($comments) && !empty($comments)):
										$opt = get_option('avoskin_review_settings', []);
										$elem = (isset($opt['element_review'])) ? $opt['element_review'] : [];
								?>
									<?php foreach($comments as $c):?>
										<div class="item">
											<div class="userinfo">
												<?php if(in_array('pict', $elem)):?>
													<figure><?php echo avoskin_avatar($c->user_id);?></figure>
												<?php endif;?>
												<div class="caption">
													<?php if(in_array('name', $elem)):?>
														<h3 class="<?php avoskin_is_verified($c->user_id) ;?>"><?php echo $c->comment_author ;?></h3>
													<?php endif;?>
													<?php if(in_array('date', $elem)):?>
														<span><?php echo get_comment_date( 'F d, Y', $c->comment_ID )?></span>
													<?php endif;?>
												</div><!-- end of caption -->
											</div><!-- end of userinfo -->
											<div class="meta">
												<?php if(in_array('rate', $elem)):?>
													<?php echo avoskin_rating(get_comment_meta($c->comment_ID, 'rating', true));?>
												<?php endif;?>
												<?php if(in_array('skin_type', $elem)):?>
													<span><?php _e('Skin Type','avoskin');?></span>
													<?php
														$skin_type = get_comment_meta($c->comment_ID, 'skin_type', true);
														foreach($skin_type as $s):
													?>
														<strong><?php echo $s;?></strong>
													<?php endforeach;?>
												<?php endif;?>
											</div><!-- end of meta -->
											<?php if(in_array('comment', $elem)):?>
												<div class="format-text">
													<p><?php echo $c->comment_content ;?></p>
												</div><!-- end of format text -->
											<?php endif;?>
										</div><!-- end of item -->
									<?php endforeach;?>
								<?php else:?>
									<div class="item">
										<div class="format-text">
											<p><?php _e('No review found.','avoskin');?></p>
										</div><!-- end of format text -->
									</div><!-- end of item -->
								<?php endif;?>
							</div><!-- end of wrap -->
							<?php if ($pages > 1):?>
								<div class="page-pagination">
									<?php
										echo paginate_links([
											'base'         => @add_query_arg('compage','%#%'),
											'format'       => '?compage=%#%',
											'show_all'     => false,
											'current' => $page,  
											'total' => $pages,  
											'prev_text' => '<i class="fa-angle-left"></i>',  
											'next_text' => '<i class="fa-angle-right"></i>'  ,
											'end_size'     => 1,
											'mid_size'     => 2,
										]);  
									?>
								</div><!--end of page pagination -->
							<?php endif;?>
					       </div><!-- end of layer -->
					</div><!-- end of rate group -->
				</div><!-- end of rowflex -->
			</div><!-- endo f wrapper -->
		</div><!-- end of review -->
	<?php
	}
}

if ( ! function_exists( 'woo_avoskin_your_order_checkout' ) ) {
	function woo_avoskin_your_order_checkout() {
		$items = WC()->cart->get_cart();
		$weight = 0;
		if(is_array($items) && !empty($items)):
	?>
		<div class="product-list">
			<h2><?php _e('Your Order','avoskin');?></h2>
			<table>
				<?php foreach($items as $item => $values):
					$product =  wc_get_product( $values['data']->get_id() );
					$is_gift = (isset($values['free_gift'])) ? true : false;
					$price = $product->get_regular_price();
					$sale = $product->get_sale_price();
					if(!$product->is_on_sale()){
						$sale = 0;
					}
					$quantity = $values['quantity'];
					
					
					// ::CRWFC::
					if(
						count($items) > 1
						&& isset($values['wfc_code'])
						&& $values['wfc_code'] != ''
						&& get_post_meta(wc_get_coupon_id_by_code($values['wfc_code']), 'wfc_freeship', true) == 'yes'
					){
						$weight += 0;
					}else{
						$weight += (int)$product->get_weight() * $quantity;	
					}
					
					
					$total =  ($sale > 0) ? (float)$sale * $quantity : (float)$price * $quantity;
					$product_data = [
						'id' => $product->get_id(),
						'quantity' => $quantity,
						'stupid' => ($is_gift) ? 'not_really' : 'nope' //It should be 'gift' but just a decoy so we call it 'stupid'
					];
				?>
					<tr>
						<td><?php echo $product->get_name() ;?> <strong>x<?php echo $quantity ;?></strong></td>
						<td>
							<?php echo ($is_gift) ? __('Free!','avoskin') : wc_price($total);?>
							<input type="hidden" name="product_data[]" value='<?php echo base64_encode(wp_json_encode($product_data));?>' />
						</td>
					</tr>
				<?php endforeach;?>
			</table>
			<input id="product-weight" type="hidden" name="product_weight" value="<?php echo $weight ;?>" />
		</div><!-- end of product item -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'woo_avoskin_shipping_checkout' ) ) {
	function woo_avoskin_shipping_checkout() {
	?>
		<div id="shipping-courier" class="shipping">
			<h3><?php _e('Shipping','avoskin');?></h3>
			<div class="form-basic">
				<p><?php _e('Input your billing address first.','avoskin');?></p>
			</div><!-- end of form basic -->
		</div><!-- end of shipping -->
	<?php
	}
}

if ( ! function_exists( 'woo_avoskin_payment_checkout' ) ) {
	function woo_avoskin_payment_checkout() {
		$payment =  WC()->payment_gateways->get_available_payment_gateways();
	?>
		<?php if(avoskin_is_wfc_freeship()): //::CRWFC::
			$method = ['name' => 'wfc_payment','title' => 'Payment Free'];
		?>
			<input id="free-wfc" type="hidden" name="payment" value='<?php echo wp_json_encode($method);?>' />
		<?php else:?>
			<?php echo avoskin_payment_method_html();?>
		<?php endif;?>
	<?php
	}
}

if ( ! function_exists( 'woo_avoskin_coupon_checkout' ) ) {
	function woo_avoskin_coupon_checkout() {
		$coupon = WC()->cart->get_applied_coupons();
		// ::CRWFC::
	?>
		<?php if(!avoskin_is_wfc_freeship()):?>
			<div id="auto-coupon-wrap"></div>
			<div id="coupon-code" class="coupon">
				<h3><img src="<?php avoskin_dir();?>/assets/img/icon/coupon.png" width="18" /><span><?php _e('Have a coupon?','avoskin');?></span></h3>
				<div class="form-basic">
					<input type="text" class="dummy-coupon" value="" placeholder="<?php _e('Enter coupon code','avoskin');?>" style="text-transform: uppercase;" />
					<br/>
					<a href="#" class="button btn-hollow btn-fullwidth has-loading"><?php _e('Apply Coupon','avoskin');?></a>
				</div><!-- end of form basic -->
			</div><!-- end of coupon -->
		<?php endif;?>
		<div id="applied-coupon" <?php echo (avoskin_is_wfc_freeship()) ? 'class="wfc-freeship"' : '' ;?>></div><!-- end of applied coupon -->
	<?php
		if(!avoskin_is_wfc_freeship()) do_action('loyalty_checkout_use_point'); // ::CRLOYALTY::
	}
}

if ( ! function_exists( 'woo_avoskin_summary_checkout' ) ) {
	function woo_avoskin_summary_checkout() {
	?>
		<div id="checkout-summary" class="summary">
			<table>
				<?php
					$summary = avoskin_request('generate_checkout_summary', ['data' => []], 'POST');
					echo $summary['content'];
				?>
			</table>
		</div><!-- end of summary -->
	<?php
	}
}

if ( ! function_exists( 'woo_avoskin_action_checkout' ) ) {
	function woo_avoskin_action_checkout() {
		$referral = (isset($_COOKIE['avoskin_referral']) && $_COOKIE['avoskin_referral'] != '') ? $_COOKIE['avoskin_referral'] : '';
		$avosbrf = (isset($_COOKIE['avosbrf']) && $_COOKIE['avosbrf'] != '') ? $_COOKIE['avosbrf'] : '';
	?>
		<div class="action">
			<div class="checky">
				<label><input type="checkbox" name="do_agree" value="1" /><span>
					<?php echo get_option('woocommerce_checkout_terms_and_conditions_checkbox_text');?>
					<a href="<?php echo get_permalink(get_option('woocommerce_terms_page_id'));?>"><?php _e('Terms and Conditions','avoskin');?></a>
				</span></label>
			</div><!--e nd of checky -->
			<?php if($referral != ''):?>
				<input type="hidden" name="referral" value="<?php echo $referral ;?>" />
				<input type="hidden" name="visitor" value="<?php echo base64_encode(WC_Geolocation::get_ip_address()) ;?>" />
			<?php endif;?>
			<?php if($avosbrf != ''):?>
				<input type="hidden" name="avosbrf" value="<?php echo $avosbrf ;?>" />
			<?php endif;?>
			<input type="hidden" name="user" value="<?php echo (!is_user_logged_in()) ? 'not_login' : avoskin_user_data();?>" />
			
			<?php if(avoskin_is_wfc_freeship()): //::CRWFC:: ?>
				<button type="submit" class="button btn-fullwidth has-loading"><?php _e('Placed Order','avoskin');?></button>
			<?php else:?>
				<button type="submit" class="button btn-fullwidth has-loading"><?php _e('Pay Now','avoskin');?></button>
			<?php endif;?>
		</div>
	<?php
	}
}

if ( ! function_exists( 'woo_add_subtotal_on_order' ) ) {
	function woo_add_subtotal_on_order($order_id) {
		$order = wc_get_order($order_id);
	?>
		<tr>
			<td class="label"><?php esc_html_e( 'Subtotal:', 'woocommerce' ); ?></td>
			<td width="1%"></td>
			<td class="total">
				<?php echo wc_price( $order->get_subtotal(), array( 'currency' => $order->get_currency() ) ); // WPCS: XSS ok. ?>
			</td>
		</tr>
	<?php
	}
}
if ( ! function_exists( 'avoskin_payment_instruction_view' ) ) {
	function avoskin_payment_instruction_view($order_id) {
		$instruction = avoskin_get_instruction_data($order_id);
	?>
		<div class="payment-info">
			<h3><?php _e('Payment Instruction','zahra');?></h3>
			<div class="wrapins">
				<?php $n=1;foreach($instruction as $i):?>
					<div class="accord-item">
						<div class="acc-head">
							<h3><?php echo $i['title'] ;?></h3>
						</div><!-- end of acc head -->
						<div class="acc-body">
							<div class="format-text">
								<?php echo $i['text'] ;?>
							</div><!-- end of format text -->
						</div><!-- end of acc body -->
					</div><!-- end fo accord item -->
				<?php $n++;endforeach;?>
			</div>
	       </div>
        <?php
	}
}