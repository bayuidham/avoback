<style type="text/css">
        .review-wrap{
                max-width: 800px;
        }
        .review-wrap.detail{
                max-width: 1200px;
        }
        .review-wrap h2{
                margin-bottom: 20px !important;
        }
        .review-wrap h2 a{
                font-size: 14px;
                font-style: italic;
                float: right;
                position: relative;
                top: 12px;
        }
        .table-basic table{
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border-radius: 6px;
                box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
                overflow: hidden;
                font-family: "Open Sans";
        }
        .table-basic th{
                background: #0073aa;
                color: #fff;
                padding: 18px 24px;
                font-weight: 600;
                text-align: left;
        }
        .table-basic tfoot td,
        .table-basic tbody td{
                padding: 15px 24px;
                vertical-align: top;
                border-top: 1px solid #eee;
                font-size: 13px;
                color: #22272b;
                border-left: 1px solid #eee;
        }
        .table-basic tr td.the-title,
        .table-basic tbody tr:first-child td{
                border: none;
        }
        .table-basic tbody td:first-child + td,
        .table-basic tbody td:first-child{
                border-left: none;
        }
        .table-basic tfoot strong{
                font-weight: 700;
                color: #000;
        }
        .table-basic tr.field-title td{
                font-weight: 700;
                background: #f5f6fa;
        }
        .table-basic tr td.the-title,
        .table-basic tbody tr.field-title td:first-child{
                background: #dfe4ea;
        }
        .table-basic tr.field-title td:first-child{
                width: 200px;
        }
        .table-basic p{
                line-height: 160%;
                margin: 0;
                padding: 0;
        }
        .table-basic ul{
                margin: 20px 0 0;
                padding: 0;
                width: 100%;
                list-style: disc inside;
                display: none;
        }
        .table-basic ul li{
                display: flex;
                margin-top: 8px;
                padding-left: 15px;
                position: relative;
        }
        .table-basic ul li:before{
                display: block;
                position: absolute;
                left: 0;
                top: 6px;
                width: 5px;
                height: 5px;
                background: #000;
                -moz-border-radius: 50% ;
                -webkit-border-radius: 50% ;
                border-radius: 50% ;
                content: '';
        }
        .table-basic ul li:first-child{
                margin-top: 0;
        }
        .table-basic ul li > *{
                flex: calc(100% - 250px);
        }
        .table-basic ul li > *:first-child{
                flex: 250px;
        }
        .table-basic tfoot td:first-child + td,
        .table-basic .td-rate{
                width: 115px;
        }
        .rate{
                text-align: left;
        }
        .rate > *{
                display: inline-block;
                vertical-align: middle;
                color: #efce4a;
                margin: 0 1px;
        }
        .rate.min4 > *:nth-child(5),
        .rate.min4 > *:nth-child(4),
        .rate.min4 > *:nth-child(3),
        .rate.min4 > *:nth-child(2),
        .rate.min3 > *:nth-child(5),
        .rate.min3 > *:nth-child(4),
        .rate.min3 > *:nth-child(3),
        .rate.min2 > *:nth-child(5),
        .rate.min2 > *:nth-child(4),
        .rate.min1 > *:last-child{
                color: #dddddd;
        }
        .user-verified{
                position: relative;
                text-decoration: none;
        }
        .user-verified:hover > *,
        .user-verified:hover{
                color: #0073aa !important;
        }
        .user-verified:after{
                display: inline-block;
                margin-left: 5px;
                color: #0073aa;
                content: '\f12a';
                font-family: 'dashicons';
                font-size: 15px;
                position: relative;
                top: 3px;
        }
        .filter-review {
                display: none;
        }
        .filter-review.showup{
                display: table-row;
        }
        .filter-review .cols{
                display: flex;
                align-items: center;
                margin: 0 -15px 15px;
        }
        .filter-review .cols > *{
                flex: 1;
                padding: 0 15px;
        }
        .filter-review .item label{
                font-weight: 600;
                display: block;
                margin: 0 0 8px;
        }
        .filter-review .item select{
                max-width: none;
                width: 100%;
        }
</style>
<?php if(isset($_GET['detail_review']) && $_GET['detail_review'] != ''):
        $post_id = (int)$_GET['detail_review'];
        $param = [
                'status' => 'approve',
                'post_id' => $post_id,
                'number' => 0
        ];
        $comments = get_comments($param );
        $fields = [
                [
                        'title' => __('What is your skin type?', 'avoskin'),
                        'key' => 'skin_type',
                        'items' => [
                                __('Oily', 'avoskin') => 0,
                                __('Normal', 'avoskin') => 0,
                                __('Dry', 'avoskin') => 0,
                                __('Combo', 'avoskin') => 0,
                                __('Acne-Prone', 'avoskin') => 0,
                                __('Sensitive', 'avoskin') => 0
                        ]
                ],
                [
                        'title' => __('How long you wear it?', 'avoskin'),
                        'key' => 'use_duration',
                        'items' => [
                                __('Less than A Week', 'avoskin') => 0,
                                __('A Week', 'avoskin') => 0,
                                __('A Month', 'avoskin') => 0,
                                __('3-6 Months', 'avoskin') => 0,
                                __('6 Month-1 year', 'avoskin') => 0,
                                __('More Than A Year', 'avoskin') => 0
                        ]
                ],
                [
                        'title' => __('Package Quality?', 'avoskin'),
                        'key' => 'package_quality',
                        'items' => [
                                __('Poor', 'avoskin') => 0,
                                __('Can Be Improved', 'avoskin') => 0,
                                __('Okay', 'avoskin') => 0,
                                __('Good', 'avoskin') => 0,
                                __('Perfect', 'avoskin') => 0,
                        ]
                ],
                [
                        'title' => __('How Is The Product Price?', 'avoskin'),
                        'key' => 'product_price',
                        'items' => [
                                __('Expensive', 'avoskin') => 0,
                                __('Just Right', 'avoskin') => 0,
                                __('Value For Money', 'avoskin') => 0
                        ]
                ],
                [
                        'title' => __('Are You Recommend This Product?', 'avoskin'),
                        'key' => 'recommend',
                        'items' => [
                                __('Yes', 'avoskin') => 0,
                                __('No', 'avoskin') => 0
                        ]
                ],
                [
                        'title' => __('Will You Repurchase This Product?', 'avoskin'),
                        'key' => 'repurchase',
                        'items' => [
                                __('Yes', 'avoskin') => 0,
                                __('No', 'avoskin') => 0,
                        ]
                ]
        ];
        
?>
        <div class="wrap review-wrap detail">
                <h2><?php _e('Review summary for ','avoskin');?> <?php echo get_the_title($post_id) ;?>
                        <a href="<?php echo admin_url('edit-comments.php?page=avoskin_review');?>"><?php _e('back to product review list','avoskin');?></a>
                </h2>
                <div class="summary table-basic">
                        <table>
                                <thead>
                                        <tr>
                                                <th colspan="7"><?php _e('Review summary from ','avoskin');?> <?php echo count($comments) ;?> <?php _e('customer','avoskin');?></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        <?php foreach($fields as $f):?>
                                        <tr class="field-title">
                                                <td><?php echo $f['title'];?></td>
                                                <?php foreach($f['items'] as $item => $v):?>
                                                        <td><?php echo $item ;?></td>
                                                <?php endforeach;?>
                                                <?php if(count($f['items']) < 6):?>
                                                        <?php for($i=1;$i <= (6 - count($f['items'])); $i++):?>
                                                                <td>&nbsp;</td>
                                                        <?php endfor;?>
                                                <?php endif;?>
                                        </tr>
                                        <tr>
                                                <td class="the-title">&nbsp;</td>
                                                <?php foreach($f['items'] as $item => $v):?>
                                                        <td>
                                                                <?php
                                                                        foreach($comments as $c) $v += $controller->count_review_field($c->comment_ID, $f['key'], $item);
                                                                        echo $v;
                                                                ?>
                                                        </td>
                                                <?php endforeach;?>
                                                <?php if(count($f['items']) < 6):?>
                                                        <?php for($i=1;$i <= (6 - count($f['items'])); $i++):?>
                                                                <td>&nbsp;</td>
                                                        <?php endfor;?>
                                                <?php endif;?>
                                        </tr>
                                <?php endforeach;?>
                                </tbody>
                                <tfoot>
                                        <tr>
                                                <th colspan="6"><?php _e('Review Detail','avoskin');?></th>
                                                <th style="text-align: right"><a href="#" class="button pop-filter"><?php _e('Filter Review','avoskin');?></a></th>
                                        </tr>
                                        <tr class="filter-review <?php echo (isset($_POST['skin_type']) && $_POST['skin_type'] != '') ? 'showup' : '';?>">
                                                <td><strong><?php _e('Filter:','avoskin');?></strong></td>
                                                <td colspan="6">
                                                        <form method="POST" action="">
                                                                <div class="cols">
                                                                        <?php $s=1;foreach($fields as $f):?>
                                                                                <div class="item">
                                                                                        <label><?php echo $f['title'] ;?></label>       
                                                                                        <select name="<?php echo $f['key'] ;?>" class="widefat">
                                                                                                <option value="all"
                                                                                                        <?php echo (!isset($_POST[$f['key']]) || isset($_POST[$f['key']]) && $_POST[$f['key']] == 'all') ? 'selected="selected"' : '';?>>
                                                                                                        <?php _e('All','avoskin');?>
                                                                                                </option>
                                                                                                <?php foreach($f['items'] as $v => $n):?>
                                                                                                        <option value="<?php echo $v ;?>" <?php echo (isset($_POST[$f['key']]) && $_POST[$f['key']] == $v) ? 'selected="selected"' : '';?>><?php echo $v ;?></option>
                                                                                                <?php ;endforeach;?>
                                                                                        </select>
                                                                                </div>
                                                                        <?php if(($s % 3) == 0 && $s < count($fields)):?>
                                                                                </div>
                                                                                <div class="cols">
                                                                        <?php endif;?>
                                                                        <?php $s++;endforeach;?>
                                                                </div>
                                                                <div class="item" style="padding: 10px 0 15px;">
                                                                        <button type="submit" class="button button-primary"><?php _e('Apply Filter','avoskin');?></button>
                                                                </div>
                                                        </form>
                                                </td>
                                        </tr>
                                        <tr class="field-title">
                                                <td><?php _e('Author','avoskin');?></td>
                                                <td><?php _e('Date','avoskin');?></td>
                                                <td colspan="3"><?php _e('Comment','avoskin');?></td>
                                                <td><?php _e('Rating','avoskin');?></td>
                                                <td><?php _e('Action','avoskin');?></td>
                                        </tr>
                                        <?php
                                                
                                                if(isset($_POST['skin_type']) && $_POST['skin_type'] != ''){
                                                        $param['meta_query'] = [];
                                                        $param['meta_query']['relation'] = 'AND';
                                                        foreach($fields as $f){
                                                                if($_POST[$f['key']] != 'all'){
                                                                        $param['meta_query'][] =[
                                                                                'key' => $f['key'],
                                                                                'value' =>$_POST[$f['key']],
                                                                                'compare' => ($f['key'] != 'skin_type') ? '=' : 'LIKE'
                                                                        ];
                                                                }
                                                        }
                                                        $comments = get_comments($param );
                                                }
                                                if(is_array($comments) && !empty($comments)):
                                                        foreach($comments as $c):
                                                ?>
                                                        <tr>
                                                                <td><a href="<?php echo admin_url('user-edit.php?user_id='.$c->user_id)  ;?>" class="<?php echo avoskin_is_verified($c->user_id);?>"><strong><?php echo $c->comment_author ;?></strong></a></td>
                                                                <td><?php echo get_comment_date( 'F d, Y', $c->comment_ID )?></td>
                                                                <td colspan="3">
                                                                        <p><?php echo $c->comment_content ;?></p>
                                                                        <ul>
                                                                                <?php foreach($fields as $f):?>
                                                                                        <li>
                                                                                                <b><?php echo $f['title'] ;?></b>
                                                                                                <span>:&nbsp;
                                                                                                        <?php if($f['key'] != 'skin_type'){
                                                                                                                echo get_comment_meta($c->comment_ID, $f['key'], true) ;
                                                                                                        }else{
                                                                                                              $skin_type =   get_comment_meta($c->comment_ID, $f['key'], true);
                                                                                                              $sk=1;foreach($skin_type as $s) {
                                                                                                                        echo $s;
                                                                                                                        echo  ($sk < count($skin_type)) ? ', ' : '';
                                                                                                                        $sk++;
                                                                                                              }
                                                                                                        }?>
                                                                                                </span>
                                                                                        </li>
                                                                                <?php endforeach;?>
                                                                        </ul>
                                                                </td>
                                                                <td class="td-rate"><?php echo avoskin_rating_admin(get_comment_meta($c->comment_ID, 'rating', true));?></td>
                                                                <td><a href="<?php echo admin_url('comment.php?action=editcomment&c='.$c->comment_ID)  ;?>" class="button expand-field">
                                                                        <?php _e('More Info','avoskin');?>
                                                                </a></td>
                                                        </tr>
                                                <?php endforeach;?>
                                        <?php else:?>
                                                <tr>
                                                        <td colspan="7" style="text-align: center"><?php _e('Review not found!','avoskin');?></td>
                                                </tr>
                                        <?php endif;?>
                                </tfoot>
                        </table>
                </div><!-- end of summary -->
        </div><!-- end of review wrap -->
        <script type="text/javascript">
                ;(function($){
                        $(document).ready(function(){
                                $('.expand-field').on('click', function(e){
                                        e.preventDefault();
                                        $('.table-basic').find('.expand-field').not($(this)).closest('tr').find('ul').slideUp('fast');
                                        $(this).closest('tr').find('ul').stop().slideToggle('fast');
                                });
                                
                                $('.pop-filter').on('click', function(e){
                                        e.preventDefault();
                                        $('.filter-review').toggle();
                                });
                        });
                }(jQuery));
        </script>
<?php else:?>
        <div class="wrap review-wrap">
                <h2><?php _e('Reviews','avoskin');?></h2>
                <div class="table-basic">
                        <table>
                                <thead>
                                        <tr>
                                                <th><?php _e('Product','avoskin');?></th>
                                                <th><?php _e('Rating','avoskin');?></th>
                                                <th><?php _e('Total Review','avoskin');?></th>
                                                <th><?php _e('Action','avoskin');?></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        <?php
                                                $got_review = 0;
                                                $loop = new WP_Query([
                                                        'post_type' => 'product',
                                                        'orderby' => 'date',
                                                        'order' => 'DESC',
                                                        'posts_per_page' => -1,
                                        ]);?>
                                        
                                        <?php if($loop->have_posts()) : while($loop->have_posts()):$loop->the_post();?>
                                                <?php
                                                        global $post;
                                                        if($post->comment_count > 0):
                                                        $got_review++;
                                                        $product = wc_get_product(get_the_ID());
                                                ?>
                                                        <tr>
                                                                <td><strong><?php the_title();?></strong></td>
                                                                <td><?php echo avoskin_rating_admin($product->get_average_rating());?></td>
                                                                <td><strong><?php echo $post->comment_count ;?></strong></td>
                                                                <td>
                                                                        <a href="<?php echo admin_url('edit-comments.php?page=avoskin_review&detail_review='.get_the_ID())  ;?>" class="button"><?php _e('Review Summary','avoskin');?></a>
                                                                </td>
                                                        </tr>
                                               <?php endif;?>
                                        <?php endwhile; ?>
                                        <?php endif;?>
                                        <?php wp_reset_query(); ?>
                                        
                                        <?php if($got_review === 0):?>
                                                <tr>
                                                        <td colspan="5" style="text-align: center">
                                                                <?php _e('Product with review not found.','avoskin');?>
                                                        </td>
                                                </tr>
                                        <?php endif;?>
                                </tbody>
                        </table>
                </div>
        </div><!-- end of wrap -->
<?php endif;?>