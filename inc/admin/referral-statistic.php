<style type="text/css">
        .referral-wrap{
                max-width: 1200px;
        }
        .referral-wrap h2{
                margin-bottom: 20px !important;
        }
        .referral-wrap h2 a{
                font-size: 14px;
                font-style: italic;
                float: right;
                position: relative;
                top: 12px;
        }
        .table-basic table{
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border-radius: 6px;
                box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
                overflow: hidden;
                font-family: "Open Sans";
        }
        .table-basic th{
                background: #0073aa;
                color: #fff;
                padding: 18px 24px;
                font-weight: 600;
                text-align: left;
        }
        .table-basic tfoot td,
        .table-basic tbody td{
                padding: 15px 24px;
                vertical-align: middle;
                border-top: 1px solid #eee;
                font-size: 13px;
                color: #22272b;
        }
        .table-basic tfoot .topped td
        .table-basic tbody .topped td{
                vertical-align: top;
        }
        .table-basic tbody tr:first-child td{
                border: none;
        }
        .table-basic tfoot strong{
                font-weight: 700;
                color: #000;
        }
</style>
<?php if(isset($_GET['referral']) && $_GET['referral'] != ''):
        $referral = (int)$_GET['referral'];
        $order = $controller->get_referral_order($referral);
?>
        <div class="wrap referral-wrap">
                <h2><?php _e('Conversion detail for ','avoskin');?> <?php echo get_the_title($referral) ;?>
                        <a href="<?php echo admin_url('edit.php?post_type=avoskin-referral&page=avoskin_referral');?>"><?php _e('back to referral list','avoskin');?></a>
                </h2>
                <div class="table-basic">
                        <table>
                                <thead>
                                        <tr>
                                                <th><?php _e('Order','avoskin');?></th>
                                                <th><?php _e('Date','avoskin');?></th>
                                                <th><?php _e('Amount','avoskin');?></th>
                                                <th><?php _e('Action','avoskin');?></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        <?php if(is_array($order) && !empty($order)):
                                                $total = 0;
                                                $quantity = 0;
                                        ?>
                                                <?php foreach($order as $o):
                                                        $item = wc_get_order($o);
                                                        $item_count = $item->get_item_count() - $item->get_item_count_refunded();
                                                        $quantity += $item_count;
                                                        $total += $item->get_total();
                                                ?>
                                                        <tr>
                                                                <td><a href="<?php echo get_edit_post_link($item->get_id());?>"><?php echo $item->get_id() ;?></a></td>
                                                                <td><?php echo wc_format_datetime($item->get_date_completed() );?></td>
                                                                <td><?php echo $item->get_formatted_order_total()  ;?> <?php _e('for','avoskin');?> <?php echo $item_count ;?>  <?php _e('item','avoskin');?></td>
                                                                <td><a href="<?php echo get_edit_post_link($item->get_id());?>" class="button"><?php _e('Order Detail','avoskin');?></a></td>
                                                        </tr>
                                                <?php endforeach;?>
                                        <?php else:?>
                                                <tr>
                                                        <td colspan="4"><?php _e('No order detail available','avoskin');?></td>
                                                </tr>
                                        <?php endif;?>
                                </tbody>
                                <tfoot>
                                        <tr>
                                                <td><strong><?php _e('Total','avoskin');?></strong></td>
                                                <td>&nbsp;</td>
                                                <td><strong><?php echo wc_price($total) ;?> <?php _e('for','avoskin');?> <?php echo $quantity ;?> <?php _e('item','avoskin');?></strong></td>
                                                <td>&nbsp;</td>
                                        </tr>
                                </tfoot>
                        </table>
                </div>
        </div><!-- end of wrap -->
<?php else:?>
        <div class="wrap referral-wrap">
                <h2><?php _e('Statistic','avoskin');?></h2>
                <div class="table-basic">
                        <table>
                                <thead>
                                        <tr>
                                                <th><?php _e('Referral','avoskin');?></th>
                                                <th><?php _e('Page Visit','avoskin');?></th>
                                                <th><?php _e('Unique Visit','avoskin');?></th>
                                                <th><?php _e('Conversion','avoskin');?></th>
                                                <th><?php _e('Campaign Source','avoskin');?></th>
                                                <th><?php _e('Campaign Medium','avoskin');?></th>
                                                <th><?php _e('Campaign Name','avoskin');?></th>
                                                <th><?php _e('Action','avoskin');?></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        <?php
                                                $loop = new WP_Query([
                                                        'post_type' => 'avoskin-referral',
                                                        'orderby' => 'date',
                                                        'order' => 'DESC',
                                                        'posts_per_page' => -1,
                                        ]);?>
                                        
                                        <?php if($loop->have_posts()) : while($loop->have_posts()):$loop->the_post();?>
                                               <tr>
                                                        <td><a href="<?php echo get_edit_post_link(get_the_ID()) ;?>" target="_blank"><?php the_title();?></a></td>
                                                        <td><?php echo $controller->count_visit(get_the_ID()) ;?></td>
                                                        <td><?php echo $controller->count_unique_visit(get_the_ID()) ;?></td>
                                                        <td><?php echo $controller->count_conversion(get_the_ID()) ;?></td>
                                                        <td><?php echo get_post_meta(get_the_ID(), 'campaign_source', true) ;?></td>
                                                        <td><?php echo get_post_meta(get_the_ID(), 'campaign_medium', true) ;?></td>
                                                        <td><?php echo get_post_meta(get_the_ID(), 'campaign_name', true) ;?></td>
                                                        <td>
                                                                <?php if($controller->count_conversion(get_the_ID()) != 0):?>
                                                                        <a href="<?php echo admin_url('edit.php?post_type=avoskin-referral&page=avoskin_referral&referral='.get_the_ID())  ;?>" class="button"><?php _e('Conversion Detail','avoskin');?></a>
                                                                <?php else:?>
                                                                        <?php _e('Empty Conversion','avoskin');?>
                                                                <?php endif;?>
                                                        </td>
                                               </tr>
                                        <?php endwhile; ?>
                                        <?php else:?>
                                                <tr>
                                                        <td colspan="5" style="text-align: center">
                                                                <?php _e('Referral not found.','avoskin');?>
                                                        </td>
                                                </tr>
                                        <?php endif;?>
                                        <?php wp_reset_query(); ?>
                                </tbody>
                        </table>
                </div>
        </div><!-- end of wrap -->
<?php endif;?>