<style type="text/css">
        .review-wrap{
                max-width: 500px;
        }
        .review-wrap h2{
                margin-bottom: 20px !important;
        }
        .review-wrap table{
                width: 100%;
        }
        .review-wrap  td{
                width: 50%;
        }
        .review-wrap label{
                display: block;
                margin-top: 10px;
        }
        .review-wrap label:first-child{
                margin: 0;
        }
        .review-wrap td{
                padding: 8px 0;
                vertical-align: top;
        }
        .review-wrap input[type="submit"]{
                min-width: 100px;
        }
</style>
 <div class="wrap review-wrap">
        <h2><?php _e('Review Settings','avoskin');?></h2>
        <?php
                if(!empty($_POST)){
                        update_option('avoskin_review_settings', $_POST);
                }
                $opt = get_option('avoskin_review_settings', []);
                $elem = (isset($opt['element_review'])) ? $opt['element_review'] : [];
        ?>
        <form method="post" action="">
                <table>
                        <tr>
                                <td><?php _e('Review element to be displayed','avoskin');?></td>
                                <td>
                                        <label><input type="checkbox" name="element_review[]" value="pict" <?php echo (in_array('pict', $elem)) ? 'checked' : '';?> /><?php _e('Profile Picture','avoskin');?></label>
                                        <label><input type="checkbox" name="element_review[]" value="name" <?php echo (in_array('name', $elem)) ? 'checked' : '';?> /><?php _e('Name','avoskin');?></label>
                                        <label><input type="checkbox" name="element_review[]" value="date" <?php echo (in_array('date', $elem)) ? 'checked' : '';?> /><?php _e('Date','avoskin');?></label>
                                        <label><input type="checkbox" name="element_review[]" value="rate" <?php echo (in_array('rate', $elem)) ? 'checked' : '';?> /><?php _e('Rate','avoskin');?></label>
                                        <label><input type="checkbox" name="element_review[]" value="skin_type" <?php echo (in_array('skin_type', $elem)) ? 'checked' : '';?> /><?php _e('Skin Type','avoskin');?></label>
                                        <label><input type="checkbox" name="element_review[]" value="comment" <?php echo (in_array('comment', $elem)) ? 'checked' : '';?> /><?php _e('Comment','avoskin');?></label>
                                </td>
                        </tr>
                        <tr>
                                <td>&nbsp;</td>
                                <td><input type="submit" class="button button-primary" value="<?php _e('Save','avoskin');?>" /></td>
                        </tr>
                </table>
        </form>
</div><!-- end of wrap -->