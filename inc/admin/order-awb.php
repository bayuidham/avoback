<?php
        $tab = (isset($_GET['page']) && $_GET['page'] == 'avoskin_awb' && isset($_GET['tab']) && $_GET['tab'] != '') ? $_GET['tab'] : 'awb-request';
?>
<style type="text/css">
        .awb-wrap{
                max-width: 1000px;
        }
        .awb-wrap h2{
                margin-bottom: 20px !important;
        }
        .awb-wrap h2 a{
                font-size: 14px;
                font-style: italic;
                float: right;
                position: relative;
                top: 12px;
        }
        .table-basic{
                position: relative;
        }
        .popup-awb .holder.fetching:after,
        .table-basic.fetching:after{
                display: block;
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 999;
                background: rgba(255,255,255,.6);
                content: '';
        }
        .table-basic table{
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border-radius: 6px;
                box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
                overflow: hidden;
                font-family: "Open Sans";
        }
        .table-basic th{
                background: #0073aa;
                color: #fff;
                padding: 18px 24px;
                font-weight: 600;
                text-align: left;
        }
        .table-basic tfoot td,
        .table-basic tbody td{
                padding: 15px 24px;
                vertical-align: middle;
                border-top: 1px solid #eee;
                font-size: 13px;
                color: #22272b;
        }
        .table-basic tbody .topped td{
                vertical-align: top;
        }
        .table-basic tbody tr:first-child td{
                border: none;
        }
        .table-basic tbody td:first-child,
        .table-basic thead th:first-child{
                width: 20px;
        }
        .generated-awb{
                background: white                
        }
        .popup-awb .holder{
                max-width: 1250px;
                margin: 0 auto;
                position: relative;
        }
        .popup-awb.popup-sorting .holder{
                width: 500px;
        }
        .popup-awb.popup-sorting .holder .field{
                padding: 20px;
                background: #fff;
        }
        .generated-awb table{
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
        }
        .generated-awb td{
                vertical-align: top;
                padding: 15px 30px;
                border-top: 2px solid #eee;
                font-size: 16px;
                line-height: 160%;
                width: 50%;
        }
        .generated-awb td:first-child{
                padding-right: 50px;
        }
        .generated-awb td b{
                font-weight: 600;
                display: block;
                line-height: 100%;
                margin-bottom: 5px;
        }
        .popup-awb{
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                z-index: 9999999;
                padding: 50px;
                background: rgba(0,0,0,.6);
                overflow-y: scroll;
                display: none;
        }
        body.open-popup-awb .popup-awb{
                display: block;
        }
        body.open-popup-awb .popup-awb.popup-sorting{
                display: flex;
                align-items: center;
        }
        body.open-popup-awb{
                height: 100vh;
                overflow: hidden;
        }
        .popup-awb .head{
                background: #0073aa;
                padding: 15px 30px;
                position: relative;
        }
        .popup-awb .head h2{
                color: #fff;
                font-weight:600;
                font-size: 20px;
        }
        .popup-awb .head a.cls{
                color: #fff;
                text-decoration: none;
                position: absolute;
                right: 20px;
                top: 50%;
                -ms-transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
        }
        .generated-awb td svg{
                display: inline-block;
                max-width: 47%;
                margin: 0 1%;
        }
        .tab-awb .tab-head{
                padding-left: 20px;
                position: relative;
                bottom: -1px;
        }
        .tab-awb .tab-head a{
                display: inline-block;
                border: 1px solid rgba(0,0,0,0);
                color: #444;
                text-decoration: none;
                padding: 11px 20px 13px;
                font-weight: 600;
                line-height: 100%;
                border-bottom: none;
                border-radius: 5px 5px 0 0;
                box-shadow: 0 0 0 0 rgba(0,0,0,0) !important;
                outline: none !important;
        }
        .tab-awb .tab-head a:hover{
                color: #0073aa;
        }
        .tab-awb .tab-head a.active{
                background: #fff;
                border: 1px solid #ccd0d4;
                border-bottom: none;
        }
        .tab-awb .tab-item{
                background: #fff;
                border: 1px solid #ccd0d4;
                padding: 25px 20px;
                border-radius: 5px;
        }
        .sort-courier{
                margin-bottom: 20px;
                float: right;
        }
        .sort-courier form > *{
                display: inline-block;
                vertical-align: middle;
                margin-right: 10px;
        }
        .popup-awb:after{
                display: none;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9;
                background: rgba(255,255,255,.9);
                content: '';
        }
        .popup-awb.printed:after{
                display: block;
        }
        .popup-awb.popup-need-print .holder{
                width: 1000px;
        }
        .print-awb-jalan{
                background: #fff;
                padding: 30px;
                font-family: 'Times New Roman';
                color: #000;
                font-size: 16px;
        }
        .print-awb-jalan h2{
                text-align: center;
                font-size: 18px;
                background: #dbe5f1;
                padding: 15px 10px;
                margin: 0 0 25px;
                font-weight: 400;
        }
        .print-awb-jalan .meta{
                margin-bottom: 25px;
        }
        .print-awb-jalan .meta span{
                display: block;
                margin-top: 12px;
                line-height: 100%;
        }
        .print-awb-jalan .meta span b{
                font-weight: 400;
        }
        .print-awb-jalan .meta span:first-child{
                margin: 0;
        }
        .print-awb-jalan table{
                width: 100%;
                text-align: left;
                border: 1px solid #000;
                border-collapse: collapse;
                border-spacing: 0;
        }
        .print-awb-jalan table tbody td:first-child,
        .print-awb-jalan table th:first-child{
                text-align: center;
                border-left: none;
        }
        .print-awb-jalan table th{
                font-weight: 400;
                padding: 8px 15px;
                border-left: 1px solid #000;
        }
        .print-awb-jalan table tbody td{
                padding: 8px 15px;
                border-left: 1px solid #000;
                border-top: 1px solid #000;
        }
        .print-awb-jalan .sign{
                text-align: center;
                display: flex;
                justify-content: space-around;
                padding: 50px 0 30px;
        }
        .print-awb-jalan .sign > *{
                max-width: 200px;
                flex: 200px;
                padding-bottom: 60px;
                border-bottom: 1px dashed #000;
        }
        .request-result span{
                display: block;
                margin-bottom: 5px;
                line-height: 100%;
        }
        .request-result span:last-child{
                margin-bottom: 15px;
        }
        .awb-wrap pre{
                max-width: 100%;
                white-space: pre-wrap;
                 overflow-x: auto;
        }
        .awb-wrap .table-basic tr h4{
                margin: 0 0 5px;
                padding: 0 0 10px;
                border-bottom: 1px solid #ccc;
                line-height: 100%;
                position: relative;
                cursor: pointer;
                -webkit-user-select: none; /* Safari */        
                -moz-user-select: none; /* Firefox */
                -ms-user-select: none; /* IE10+/Edge */
                user-select: none; /* Standard */
        }
        .awb-wrap .table-basic tr h4.expanded{
                margin: 0 0 2px;
                padding: 0;
                border: none;
        }
        .awb-wrap .table-basic tr h4:after{
                position: absolute;
                right: 0;
                display: inline-block;
                vertical-align: middle;
                content: '';
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 5px 4px 0 4px;
                border-color: #000333 transparent transparent transparent;
                top: 3px;
        }
        .awb-wrap .table-basic tr h4.expanded:after{
                border-width: 0 4px 5px 4px;
                border-color: transparent transparent #000333 transparent;
        }
        .awb-wrap .table-basic tr h4 + *,
        .awb-wrap .table-basic tr.detail{
                display: none;
        }
        .awb-wrap .table-basic tr h4.expanded + *{
                display: block;
        }
        .awb-wrap .table-basic tr.detail form{
                padding-top: 10px;
                display: none;
                flex-wrap: wrap;
                margin: 0 -10px;
        }
        .awb-wrap .table-basic tr.detail h4.expanded + form{
                display: flex;
        }
        .awb-wrap .table-basic tr.detail form > *{
                flex: calc(50% - 20px);
                max-width: calc(50% - 20px);
                margin: 0 10px 15px;
        }
        .awb-wrap .table-basic tr.detail form label{
                display: block;
                margin-bottom: 5px;
                font-weight: 600;
                color: #333;
        }
</style>
<div class="wrap awb-wrap">
        <h2>Airwaybill</h2>
        <div class="tab-awb">
                <div class="tab-head">
                        <a href="<?php echo admin_url('admin.php?page=avoskin_awb&tab=awb-request') ;?>" <?php echo ($tab == 'awb-request') ? 'class="active"' : '' ;?>>Request AWB</a>
                        <a href="<?php echo admin_url('admin.php?page=avoskin_awb&tab=awb-label') ;?>" <?php echo ($tab == 'awb-label') ? 'class="active"' : '' ;?>>Label</a>
                        <a href="<?php echo admin_url('admin.php?page=avoskin_awb&tab=awb-error') ;?>" <?php echo ($tab == 'awb-error') ? 'class="active"' : '' ;?>>Error</a>
                </div><!-- end of tab head -->
                <?php if($tab == 'awb-request'):?>
                        <div id="awb-request" class="tab-item">
                                <div class="request-result"></div>
                                <?php get_template_part('inc/admin/awb/request');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                <?php if($tab == 'awb-label'):?>
                        <div id="awb-label" class="tab-item">
                                <?php get_template_part('inc/admin/awb/label');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                <?php if($tab == 'awb-error'):?>
                        <div id="awb-error" class="tab-item">
                                <div class="request-result"></div>
                                <?php get_template_part('inc/admin/awb/error');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                
        </div><!-- end of tab awb -->
</div>

<script type="text/javascript" src="<?php avoskin_dir();?>/assets/js/admin.js" ></script>
<script type="text/javascript" src="<?php avoskin_dir();?>/assets/js/pdf.js" ></script>
<script type="text/javascript">
        ;(function($){
                var selectedOrder = [],
                        html = '',
                        ajaxQueue = $({});
                
                function exportToCsv(filename, rows) {
                        var processRow = function (row) {
                                var finalVal = '';
                                for (var j = 0; j < row.length; j++) {
                                        var innerValue = row[j] === null ? '' : row[j].toString();
                                        if (row[j] instanceof Date) {
                                                innerValue = row[j].toLocaleString();
                                        };
                                        var result = innerValue.replace(/"/g, '""');
                                        if (result.search(/("|,|\n)/g) >= 0)
                                                result = '"' + result + '"';
                                                if (j > 0)
                                                        finalVal += ',';
                                                        finalVal += result;
                                }
                                return finalVal + '\n';
                        };
                
                        var csvFile = '';
                        for (var i = 0; i < rows.length; i++) {
                                csvFile += processRow(rows[i]);
                        }
                
                        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
                        if (navigator.msSaveBlob) { // IE 10+
                                navigator.msSaveBlob(blob, filename);
                        } else {
                                var link = document.createElement("a");
                                if (link.download !== undefined) { // feature detection
                                        // Browsers that support HTML5 download attribute
                                        var url = URL.createObjectURL(blob);
                                        link.setAttribute("href", url);
                                        link.setAttribute("download", filename);
                                        link.style.visibility = 'hidden';
                                        document.body.appendChild(link);
                                        link.click();
                                        document.body.removeChild(link);
                                }
                        }
                        window.location.reload();
                }
                
                $(document).ready(function(){
                        
                        $.ajaxQueue = function(ajaxOpts) {
                                // hold the original complete function
                                var oldComplete = ajaxOpts.complete;
                              
                                // queue our ajax request
                                ajaxQueue.queue(function(next) {
                              
                                        // create a complete callback to fire the next event in the queue
                                        ajaxOpts.complete = function() {
                                                // fire the original complete if it was there
                                                if (oldComplete) oldComplete.apply(this, arguments);
                                                next(); // run the next query in the queue
                                        };
                                      
                                        // run the query
                                        $.ajax(ajaxOpts);
                                });
                        };
                        
                        $('.table-basic thead input').on('click', function(){
                                if($(this).is(":checked")){
                                        $('.table-basic tbody input').each(function(){
                                                $(this).prop( "checked", true );        
                                        });
                                }
                                else if($(this).is(":not(:checked)")){
                                        $('.table-basic tbody input').each(function(){
                                                $(this).prop( "checked", false );        
                                        });
                                }
                        });
                        
                        $('#btn-request-awb').on('click', function(e){
                                e.preventDefault();
                                var data = [],
                                        table = $(this).closest('.table-basic'),
                                        status = $('.request-result');
                                
                                table.find('tbody input:checked').each(function(){
                                        data.push($(this).val());
                                });
                                
                                if (data.length > 0) {
                                        if (!table.hasClass('fetching')) {
                                                table.addClass('fetching');
                                                var length = data.length;
                                                $.each(data, function(k, v){
                                                         $.ajaxQueue({
                                                                url: '<?php echo site_url('/wp-json/avoskin/v1/request_awb/');?>',
                                                                data: {
                                                                        order:v
                                                                },
                                                                type: 'POST',
                                                                success: function(result) {
                                                                        //$('#row-'+result).remove();
                                                                        $(result).appendTo(status);
                                                                        length--;
                                                                        if (length == 0) {
                                                                                window.location.reload();
                                                                        }
                                                                }
                                                        });
                                                });
                                        }
                                }else{
                                        alert('Pilih order terlebih dahulu!');
                                }
                        });
                        
                          $('#print-label').click(function (e) {
                                e.preventDefault();
                                 var opt = {
                                                margin:       .2,
                                                filename:      'shipping-label-<?php echo current_time('Y-m-d-H-i-s');?>.pdf',
                                                image:        { type: 'jpeg', quality: 1 },
                                                html2canvas:  { scale: 1 },
                                                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' },
                                                 pagebreak: { mode: ['avoid-all', 'css', 'legacy'] }
                                         },
                                        worker = html2pdf().set(opt).from($('#content .item:first-child').html()).toPdf(),
                                        n = 1,
                                        total = $('#content .item').length;
                                $('.popup-label').addClass('printed');
                                $('#content .item').each(function(){
                                     if (n != 1) {
                                        worker = worker.get('pdf').then(function (pdf) {
                                                pdf.addPage();
                                        }).from($(this).html()).toContainer().toCanvas().toPdf().then(function(a,b,c){
                                             $('.popup-label .progress b').text(Math.round((n/total) * 100));
                                             n++;
                                        });
                                     }else{
                                        n++;   
                                     }
                                     
                                     
                                });
                                worker = worker.save().then(function(pdf){
                                        $('.popup-label').removeClass('printed');
                                        $.post( "<?php echo site_url('/wp-json/avoskin/v1/add_has_print_order/');?>", {data: selectedOrder}, function( result ) {
                                                window.location.reload();
                                        });
                                });
                        });
                          
                        $('#preview-label').on('click', function(e){
                                e.preventDefault();
                                
                                $('.table-basic tbody input:checked').each(function(){
                                        var self = $(this),
                                                parent = self.closest('tr'),
                                                data = parent.data('order');
                                                
                                        html += '<div class="item"><table><tbody><tr class="logo-row"><td><img src="'+data.logo+'" /></td>';
                                        html += '<td><svg class="barcode" jsbarcode-value="'+data.resi+'"></svg></td>';
                                        html += '<td><img src="'+data.courier_logo+'"/></td></tr>';
                                        html += '<tr class="service-row"><td>Jenis Layanan: '+data.courier + ' ' + data.courier_service+'</td><td colspan="2">Tanggal Order: '+data.date+'</td></tr>';
                                        html += '<tr class="detail-row"><th rowspan="3"><svg class="barcode" jsbarcode-value="#'+data.id+'"></svg></th><td colspan="2">Asuransi: Rp.0,-</td></tr>';
                                        html += '<tr class="detail-row"><td colspan="2">Berat: '+data.weight+'</td></tr>';
                                        html += '<tr class="detail-row"><td colspan="2">Quantity: '+data.qty+' PCS</td></tr></tbody>';
                                        html += '<tfoot><tr class="address-row"><td colspan="3"><div class="holder">';
                                        html += ' <div class="aitem"><p>Penerima:<br/>Nama: '+data.to.name+'</p><p>Alamat Lengkap: <br/>'+data.to.address+'</p>';
                                        html += '<p>Nomor Telefon: '+data.to.phone +'</p></div>';
                                        html += ' <div class="aitem"><p>Pengirim:<br/>Nama: '+data.from.name+'</p><p>Alamat Lengkap: <br/>'+data.from.address+'</p>';
                                        html += '<p>Nomor Telefon: '+data.from.phone +'</p></div></div></td></tr>';
                                        html += '<tr class="note-row"><td colspan="3">Catatan: '+data.note+'</td></tr>';
                                        html += '<tr class="product-row"><td colspan="3">Detail Order:</td></tr>';
                                        $.each(data.items, function(k, v){
                                                html += '<tr class="product-row"><td colspan="2">'+v['name']+'</td><td>'+v['qty']+'</td></tr>';
                                        });
                                        html += '</tfoot></table></div>';
                                        
                                        selectedOrder.push(data.id);
                                });
                                
                                if (selectedOrder.length > 0) {
                                        $('#content').html(html);
                                        JsBarcode(".barcode").init();
                                        $('body').addClass('open-popup-label');
                                }else{
                                        alert('Please select order first!');
                                }
                        });
                        
                        $('.popup-label .head a.cls').on('click', function(e){
                                e.preventDefault();
                                selectedOrder = [];
                                html = '';
                                $('body').removeClass('open-popup-label');
                        });
                        $('.awb-wrap .table-basic tr .toggle-error').on('click', function(e){
                             e.preventDefault();
                             $($(this).attr('data-target')).toggle();
                        });
                        
                        $('#btn-move-to-label').on('click', function(e){
                                 e.preventDefault();
                                var data = [],
                                        table = $(this).closest('.table-basic'),
                                        status = $('.request-result');
                                
                                table.find('tbody input:checked').each(function(){
                                        data.push($(this).val());
                                });
                                
                                if (data.length > 0) {
                                        if (!table.hasClass('fetching')) {
                                                table.addClass('fetching');
                                                var length = data.length;
                                                $.each(data, function(k, v){
                                                         $.ajaxQueue({
                                                                url: '<?php echo site_url('/wp-json/avoskin/v1/move_awb/');?>',
                                                                data: {
                                                                        order:v
                                                                },
                                                                type: 'POST',
                                                                success: function(result) {
                                                                        //$('#row-'+result).remove();
                                                                        $(result).appendTo(status);
                                                                        length--;
                                                                        if (length == 0) {
                                                                                window.location.reload();
                                                                        }
                                                                }
                                                        });
                                                });
                                        }
                                }else{
                                        alert('Pilih order terlebih dahulu!');
                                }
                        });
                        $('.awb-wrap .table-basic tr h4').on('click', function(){
                                $(this).toggleClass('expanded');
                        });
                        
                        $('.awb-wrap .table-basic tr.detail form').on('submit', function(e){
                                e.preventDefault();
                                var self = $(this),
                                        table = self.closest('.table-basic'),
                                        data = self.serialize();
                                        
                                  if (!table.hasClass('fetching')) {
                                        table.addClass('fetching');
                                        $.post( "<?php echo site_url('/wp-json/avoskin/v1/update_awb/');?>", {data: data}, function( result ) {
                                                alert('Request paramater berhasil diupdate!');
                                                table.removeClass('fetching');
                                        });
                                }
                        });
                        
                        $('#btn-export-awb').on('click', function(e){
                                 e.preventDefault();
                                var data = [],
                                        table = $(this).closest('.table-basic'),
                                        status = $('.request-result');
                                
                                table.find('tbody input:checked').each(function(){
                                        data.push($(this).val());
                                });
                                
                                if (data.length > 0) {
                                        if (!table.hasClass('fetching')) {
                                                table.addClass('fetching');
                                                $('.table-export tbody tr').each(function(){
                                                        var self = $(this);
                                                        if($.inArray(self.attr('data-id'), data) != -1) {} else {
                                                                self.remove();
                                                        } 
                                                });
                                                var rows = [];
                                                $('.table-export table tr').each(function() {
                                                        var row = [];
                                                        $(this).find('td').each(function(){
                                                                row.push($(this).text());   
                                                        });
                                                        rows.push(row);
                                                });
                                                
                                                exportToCsv('data-error.csv', rows);
                                        }
                                }else{
                                        alert('Pilih order terlebih dahulu!');
                                }
                        });
                });
        }(jQuery));
</script>