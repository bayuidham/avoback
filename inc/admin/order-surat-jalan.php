<style type="text/css">
        .surat-wrap{
                max-width: 1000px;
        }
        .surat-wrap h2{
                margin-bottom: 20px !important;
        }
        .surat-wrap h2 a{
                font-size: 14px;
                font-style: italic;
                float: right;
                position: relative;
                top: 12px;
        }
        .table-basic{
                position: relative;
        }
        .popup-surat .holder.fetching:after,
        .table-basic.fetching:after{
                display: block;
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 999;
                background: rgba(255,255,255,.6);
                content: '';
        }
        .table-basic table{
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border-radius: 6px;
                box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
                background-color: #ffffff;
                overflow: hidden;
                font-family: "Open Sans";
        }
        .table-basic th{
                background: #0073aa;
                color: #fff;
                padding: 18px 24px;
                font-weight: 600;
                text-align: left;
        }
        .table-basic tfoot td,
        .table-basic tbody td{
                padding: 15px 24px;
                vertical-align: middle;
                border-top: 1px solid #eee;
                font-size: 13px;
                color: #22272b;
        }
        .table-basic tbody .topped td{
                vertical-align: top;
        }
        .table-basic tbody tr:first-child td{
                border: none;
        }
        .table-basic tbody td:first-child,
        .table-basic thead th:first-child{
                width: 20px;
        }
        .generated-surat{
                background: white                
        }
        .popup-surat .holder{
                max-width: 1250px;
                margin: 0 auto;
                position: relative;
        }
        .popup-surat.popup-sorting .holder{
                width: 500px;
        }
        .popup-surat.popup-sorting .holder .field{
                padding: 20px;
                background: #fff;
        }
        .generated-surat table{
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
        }
        .generated-surat td{
                vertical-align: top;
                padding: 15px 30px;
                border-top: 2px solid #eee;
                font-size: 16px;
                line-height: 160%;
                width: 50%;
        }
        .generated-surat td:first-child{
                padding-right: 50px;
        }
        .generated-surat td b{
                font-weight: 600;
                display: block;
                line-height: 100%;
                margin-bottom: 5px;
        }
        .popup-surat{
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                z-index: 9999999;
                padding: 50px;
                background: rgba(0,0,0,.6);
                overflow-y: scroll;
                display: none;
        }
        body.open-popup-surat .popup-surat{
                display: block;
        }
        body.open-popup-surat .popup-surat.popup-sorting{
                display: flex;
                align-items: center;
        }
        body.open-popup-surat{
                height: 100vh;
                overflow: hidden;
        }
        .popup-surat .head{
                background: #0073aa;
                padding: 15px 30px;
                position: relative;
        }
        .popup-surat .head h2{
                color: #fff;
                font-weight:600;
                font-size: 20px;
        }
        .popup-surat .head a.cls{
                color: #fff;
                text-decoration: none;
                position: absolute;
                right: 20px;
                top: 50%;
                -ms-transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
        }
        .generated-surat td svg{
                display: inline-block;
                max-width: 47%;
                margin: 0 1%;
        }
        .tab-surat .tab-head{
                padding-left: 20px;
                position: relative;
                bottom: -1px;
        }
        .tab-surat .tab-head a{
                display: inline-block;
                border: 1px solid rgba(0,0,0,0);
                color: #444;
                text-decoration: none;
                padding: 11px 20px 13px;
                font-weight: 600;
                line-height: 100%;
                border-bottom: none;
                border-radius: 5px 5px 0 0;
                box-shadow: 0 0 0 0 rgba(0,0,0,0) !important;
                outline: none !important;
        }
        .tab-surat .tab-head a:hover{
                color: #0073aa;
        }
        .tab-surat .tab-head a.active{
                background: #fff;
                border: 1px solid #ccd0d4;
                border-bottom: none;
        }
        .tab-surat .tab-item{
                background: #fff;
                border: 1px solid #ccd0d4;
                padding: 25px 20px;
                border-radius: 5px;
        }
        .sort-courier{
                margin-bottom: 20px;
                float: right;
        }
        .sort-courier form > *{
                display: inline-block;
                vertical-align: middle;
                margin-right: 10px;
        }
        .popup-surat:after{
                display: none;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9;
                background: rgba(255,255,255,.9);
                content: '';
        }
        .popup-surat.printed:after{
                display: block;
        }
        .popup-surat.popup-need-print .holder{
                width: 1000px;
        }
        .print-surat-jalan{
                background: #fff;
                padding: 30px;
                font-family: 'Times New Roman';
                color: #000;
                font-size: 16px;
        }
        .print-surat-jalan h2{
                text-align: center;
                font-size: 18px;
                background: #dbe5f1;
                padding: 15px 10px;
                margin: 0 0 25px;
                font-weight: 400;
        }
        .print-surat-jalan .meta{
                margin-bottom: 25px;
        }
        .print-surat-jalan .meta span{
                display: block;
                margin-top: 12px;
                line-height: 100%;
        }
        .print-surat-jalan .meta span b{
                font-weight: 400;
        }
        .print-surat-jalan .meta span:first-child{
                margin: 0;
        }
        .print-surat-jalan table{
                width: 100%;
                text-align: left;
                border: 1px solid #000;
                border-collapse: collapse;
                border-spacing: 0;
        }
        .print-surat-jalan table tbody td:first-child,
        .print-surat-jalan table th:first-child{
                text-align: center;
                border-left: none;
        }
        .print-surat-jalan table th{
                font-weight: 400;
                padding: 8px 15px;
                border-left: 1px solid #000;
        }
        .print-surat-jalan table tbody td{
                padding: 8px 15px;
                border-left: 1px solid #000;
                border-top: 1px solid #000;
        }
        .print-surat-jalan .sign{
                text-align: center;
                display: flex;
                justify-content: space-around;
                padding: 50px 0 30px;
        }
        .print-surat-jalan .sign > *{
                max-width: 200px;
                flex: 200px;
                padding-bottom: 60px;
                border-bottom: 1px dashed #000;
        }
</style>
<div class="wrap surat-wrap">
        <h2>Surat Jalan</h2>
        <div class="tab-surat">
                <?php
                        $tab = (isset($_GET['page']) && $_GET['page'] == 'avoskin_surat_jalan' && isset($_GET['tab']) && $_GET['tab'] != '') ? $_GET['tab'] : 'sj-sorting';
                ?>
                <div class="tab-head">
                        <a href="<?php echo admin_url('admin.php?page=avoskin_surat_jalan&tab=sj-sorting') ;?>" <?php echo ($tab == 'sj-sorting') ? 'class="active"' : '' ;?>>Sorting</a>
                        <a href="<?php echo admin_url('admin.php?page=avoskin_surat_jalan&tab=sj-need-print') ;?>" <?php echo ($tab == 'sj-need-print') ? 'class="active"' : '' ;?>>Perlu diprint</a>
                        <a href="<?php echo admin_url('admin.php?page=avoskin_surat_jalan&tab=sj-has-print') ;?>" <?php echo ($tab == 'sj-has-print') ? 'class="active"' : '' ;?>>Sudah diprint</a>
                </div><!-- end of tab head -->
                <?php if($tab == 'sj-sorting'):?>
                        <div id="sj-sorting" class="tab-item">
                                <?php get_template_part('inc/admin/surat-jalan/sorting');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                <?php if($tab == 'sj-need-print'):?>
                        <div id="sj-need-print" class="tab-item">
                                <?php get_template_part('inc/admin/surat-jalan/need-print');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                <?php if($tab == 'sj-has-print'):?>
                        <div id="sj-has-print" class="tab-item">
                                <?php get_template_part('inc/admin/surat-jalan/has-print');?>
                        </div><!-- end of tab item -->
                <?php endif;?>
                
        </div><!-- end of tab surat -->
</div>
<script type="text/javascript" src="<?php avoskin_dir();?>/assets/js/admin.js" ></script>
<script type="text/javascript" src="<?php avoskin_dir();?>/assets/js/pdf.js" ></script>
<script type="text/javascript">
        ;(function($){
                var selectedOrder = [],
                        html = '';
                        
                $(document).ready(function(){
                        
                        $('.table-basic thead input').on('click', function(){
                                if($(this).is(":checked")){
                                        $('.table-basic tbody input').each(function(){
                                                $(this).prop( "checked", true );        
                                        });
                                }
                                else if($(this).is(":not(:checked)")){
                                        $('.table-basic tbody input').each(function(){
                                                $(this).prop( "checked", false );        
                                        });
                                }
                        });
                        
                        
                        $('#scan-order-sorting').on('click', function(e){
                                e.preventDefault()
                                $('body').addClass('open-popup-surat');
                        });
                        
                        $('.popup-surat .head a.cls').on('click', function(e){
                                e.preventDefault();
                                selectedOrder = [];
                                html = '';
                                $('body').removeClass('open-popup-surat');
                        });
                        
                        //Sorting tab
                        $('#need-print').on('click', function(e){
                                e.preventDefault();
                                var data = [],
                                        table = $(this).closest('.table-basic');
                                
                                table.find('tbody input:checked').each(function(){
                                        var self = $(this),
                                                parent = self.closest('tr');
                                                
                                        data.push(self.val());
                                });
                                
                                if (data.length > 0) {
                                        if (!table.hasClass('fetching')) {
                                                table.addClass('fetching');
                                                $.post( "<?php echo site_url('/wp-json/avoskin/v1/order_need_print/');?>", {data: data}, function( result ) {
                                                        if (result.status == 'ok') {
                                                                $.each(result.success, function(k,v){
                                                                        $('#row-'+v).remove();
                                                                });
                                                        }
                                                        data = [];
                                                        if (table.find('tbody tr').length < 1) {
                                                                window.location.reload();
                                                        }else{
                                                                table.removeClass('fetching');        
                                                        }
                                                        
                                                        
                                                });
                                        }
                                }else{
                                        alert('Pilih order terlebih dahulu!');
                                }
                        });
                        
                        
                        $('#scan-barcode').bind("input", function(event){
                                var self = $(this),
                                        holder = self.closest('.holder');
                                
                                fetch_surat_jalan(self, holder);
                        });
                        
                        var tried = 0;
                        
                        function fetch_surat_jalan(self, holder){
                                 if (self.val() != '' && self.val().length > 9 && !holder.hasClass('fetching')) {
                                        holder.addClass('fetching');
                                        $.post( "<?php echo site_url('/wp-json/avoskin/v1/order_need_print/');?>", {data: [self.val()]}, function( result ) {
                                                if (result.status == 'empty') {
                                                        holder.removeClass('fetching');
                                                        if (tried<=5) {
                                                                fetch_surat_jalan(self, holder);
                                                                tried++;
                                                        }else{
                                                                alert('Something went wrong, please reload the page');
                                                                window.location.reload();
                                                        }
                                                }else{
                                                        tried = 0;
                                                        if (result.status == 'ok') {
                                                                $.each(result.success, function(k,v){
                                                                        $('#row-'+v).remove();
                                                                });
                                                        }
                                                        self.val('');
                                                        if ($('.table-basic tbody tr').length < 1) {
                                                                window.location.reload();
                                                        }else{
                                                                holder.removeClass('fetching');        
                                                        }      
                                                }
                                        });
                                }
                        }
                        
                        $('#print-surat').click(function (e) {
                                //e.preventDefault();
                                //var content = $('#content'),
                                //        width = 2120,
                                //        height = content.outerHeight() * 2;
                                //
                                //$('.popup-surat').addClass('printed');
                                //content.css({
                                //        width: 1000,
                                //        transform: 'scale(2,2)',
                                //        'transform-origin': 'top left'
                                //});
                                //
                                //domtoimage.toBlob(document.getElementById('content'),{width:width,height:height}).then(function (blob) {
                                //        invokeSaveAsDialog(blob, 'surat-jalan-<?php echo current_time('Y-m-d-H-i-s');?>.png');
                                //        content.css({
                                //                width: 'auto',
                                //                transform: 'scale(1,1)'
                                //        });
                                //        $.post( "<?php echo site_url('/wp-json/avoskin/v1/order_print_surat_jalan/');?>", {data: selectedOrder}, function( result ) {});
                                //        window.location.reload();
                                //});
                                
                                    e.preventDefault();
                                var element = document.getElementById('content');
                                 var opt = {
                                   margin:       .2,
                                   filename:      'surat-jalan-<?php echo current_time('Y-m-d-H-i-s');?>.pdf',
                                   image:        { type: 'jpeg', quality: 1 },
                                   html2canvas:  { scale: 1 },
                                   jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' },
                                    pagebreak: { mode: ['avoid-all', 'css', 'legacy'] }
                                 };
                                html2pdf().set(opt).from(element).save();
                                $.post( "<?php echo site_url('/wp-json/avoskin/v1/order_print_surat_jalan/');?>", {data: selectedOrder}, function( result ) {
                                        window.location.reload();        
                                });
                                
                        });
                        
                         //Sorting tab
                        $('#preview-print').on('click', function(e){
                                e.preventDefault();
                                var data = [],
                                        table = $(this).closest('.table-basic'),
                                        manifest = $(this).data('manifest');
                                        n = 1;
                                
                                table.find('tbody input:checked').each(function(){
                                        var self = $(this),
                                                parent = self.closest('tr'),
                                                data = parent.data('order');
                                                
                                        html += '<tr><td>'+n+'</td><td>'+data.order_id+'</td><td>'+data.order_date+'</td>'+
                                                '<td>'+data.name+'</td><td>'+data.courier+'</td><td>'+data.resi+'</td></tr>';
                                        
                                        selectedOrder.push(data.id);
                                        n++;
                                });
                                        
                                if (selectedOrder.length > 0) {
                                        $('#content table tbody').html(html);
                                        $('#content .mf-id b').html(manifest.id);
                                        $('#content .mf-date b').html(manifest.date);
                                        $('body').addClass('open-popup-surat');
                                }else{
                                        alert('Please select order first!');
                                }
                                        
                        });
                        
                        $('#to-ondelivery').on('click', function(e){
                                   e.preventDefault();
                                        var data = [],
                                                table = $(this).closest('.table-basic');
                                        
                                        table.find('tbody input:checked').each(function(){
                                                var self = $(this),
                                                        parent = self.closest('tr');
                                                        
                                                data.push(self.val());
                                        });
                                        
                                        if (data.length > 0) {
                                                if (!table.hasClass('fetching')) {
                                                        table.addClass('fetching');
                                                        $.post( "<?php echo site_url('/wp-json/avoskin/v1/order_to_ondelivery/');?>", {data: data}, function( result ) {
                                                                if (result.status == 'ok') {
                                                                        $.each(result.success, function(k,v){
                                                                                $('#row-'+v).remove();
                                                                        });
                                                                }
                                                                data = [];
                                                                if (table.find('tbody tr').length < 1) {
                                                                        window.location.reload();
                                                                }else{
                                                                        table.removeClass('fetching');        
                                                                }
                                                        });
                                                }
                                        }else{
                                                alert('Pilih order terlebih dahulu!');
                                        }
                                });
                        
                });
        }(jQuery));
</script>