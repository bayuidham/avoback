<div id="table-need-print" class="table-basic">
        <?php $fcourier = (isset($_GET['shipping_courier']) && $_GET['shipping_courier'] != '') ? $_GET['shipping_courier'] : 'all';?>
        <div class="sort-courier">
                <form method="GET" action="<?php echo admin_url('admin.php') ;?>">
                        <label>Filter Ekspedisi</label>
                        <select name="shipping_courier">
                                <option value="all" <?php selected('all', $fcourier);?>>Semua</option>
                                <option value="jne" <?php selected('jne', $fcourier);?>>JNE</option>
                                <option value="pos" <?php selected('pos', $fcourier);?>>Pos Indonesia</option>
                                <option value="rpx" <?php selected('rpx', $fcourier);?>>RPX</option>
                                <option value="jnt" <?php selected('jnt', $fcourier);?>>JNT</option>
                                <option value="sicepat" <?php selected('sicepat', $fcourier);?>>SiCepat</option>
                        </select>
                        <div class="act">
                                <input type="hidden" name="page" value="avoskin_surat_jalan"/>
                                <input type="hidden" name="tab" value="sj-need-print"/>
                                <button type="submit" class="button button-primary">Submit</button>
                        </div>
                </form>
        </div><!-- end of sort courier -->
        <div class="clear"></div>
        <table>
                <thead>
                        <tr>
                                <th><input type="checkbox" name="check_all" value="all"/></th>
                                <th><?php _e('Order','avoskin');?></th>
                                <th><?php _e('Tanggal Order','avoskin');?></th>
                                <th><?php _e('Nama','avoskin');?></th>
                                <th><?php _e('Ekspedisi','avoskin');?></th>
                                <th><?php _e('Resi','avoskin');?></th>
                        </tr>
                </thead>
                <tbody>
                        <?php
                                $orders = $controller->get_admin_order(['wc-packing']);
                                $has_order = false;
                                if(is_array($orders) && !empty($orders)):
                                $n=1;
                                foreach($orders as $o):
                                        if(
                                                get_post_meta((int)$o['ID'], 'jalan_status', true) == 'sj-need-print' && $fcourier == 'all'
                                                || get_post_meta((int)$o['ID'], 'jalan_status', true) == 'sj-need-print' && $fcourier != 'all' && $fcourier == get_post_meta((int)$o['ID'], 'shipping_courier', true)
                                        ):
                                                //update_post_meta((int)$o['ID'], 'jalan_status', 'sj-sorting');
                                                $order = wc_get_order($o['ID']);
                                                $courier = get_post_meta($order->get_id(), 'shipping_courier', true);
                                                $courier_service = get_post_meta($order->get_id(), 'shipping_service', true);
                                                $data = [
                                                        'id' => $order->get_id(),
                                                        'order_id' => '#' . $order->get_order_number(),
                                                        'order_date' => esc_html( wc_format_datetime( $order->get_date_created() ) ) ,
                                                        'name' => wp_strip_all_tags(esc_attr(str_replace(['"', "'"], '', $order->get_shipping_first_name()  . ' ' .  $order->get_shipping_last_name()))) ,
                                                        'courier' => strtoupper($courier) . ' ' . strtoupper($courier_service),
                                                        'resi' => get_post_meta($order->get_id(), 'resi_order', true)
                                                ];
                        ?>
                                                <tr id="row-<?php echo $order->get_id() ;?>" data-order='<?php echo wp_json_encode($data) ;?>'>
                                                        <td><input type="checkbox" name="check"  value="<?php echo $order->get_id();?>"/></td>
                                                        <td>#<?php echo $order->get_order_number() ;?></td>
                                                        <td><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ) ;?></td>
                                                        <td><?php echo $order->get_shipping_first_name() ;?> <?php echo $order->get_shipping_last_name() ;?></td>
                                                        <td><?php echo strtoupper($courier);?> <?php echo strtoupper($courier_service) ;?> </td>
                                                        <td><?php echo get_post_meta($order->get_id(), 'resi_order', true);?></td>
                                                </tr>
                                        <?php $has_order = true; $n++;endif;?>
                                <?php endforeach;?>
                        <?php endif; ?>
                        <?php if(!$has_order):?>
                                <tr>
                                        <td colspan="6" style="text-align: center">
                                                <?php _e('Order not found.','avoskin');?>
                                        </td>
                                </tr>
                        <?php endif;?>
                </tbody>
                <?php if($has_order):?>
                        <tfoot>
                                <tr>
                                        <td colspan="6">
                                                <?php
                                                        $manifest = [
                                                                'id' => 'MNF-'.time(),
                                                                'date' => current_time('d F Y')
                                                        ];
                                                ?>
                                                <a href="#" id="preview-print" class="button button-primary"  data-manifest='<?php echo wp_json_encode($manifest) ;?>'><?php _e('Preview Surat jalan','avoskin');?></a>
                                        </td>
                                </tr>
                        </tfoot>
                <?php endif;?>
        </table>
</div>
<div class="popup-surat popup-need-print">
        <div class="holder">
                <div class="head">
                        <a href="#" id="print-surat" class="button"><?php _e('Print Surat Jalan','avoskin');?></a>
                        <a href="#" class="cls"><i class="dashicons dashicons-dismiss"></i></a>
                </div>
                <div id="content" class="print-surat-jalan">
                        <h2>AVOSKIN Manifest</h2>
                        <div class="meta">
                                <span class="mf-id">Manifest ID : <b>291993992</b></span>
                                <span class="mf-date">Tanggal Manifest: <b>29 August 2020</b></span>
                        </div><!-- end of meta -->
                        <table>
                                <thead>
                                        <tr>
                                                <th>No</th>
                                                <th>Order ID</th>
                                                <th>Tanggal Order</th>
                                                <th>Nama</th>
                                                <th>Ekspedisi</th>
                                                <th>Resi</th>
                                        </tr>
                                </thead>
                                <tbody></tbody>
                        </table>
                        <div class="sign">
                                <div>
                                        <span>Kurir</span>
                                </div>
                                <div>
                                        <span>Admin</span>
                                </div>
                        </div><!-- end of sign -->
                </div><!-- endof generated surat jalan -->
        </div>
</div>