<style type="text/css">
        .resi-wrap .layer{
                padding: 20px;
                background: #fff;
                box-shadow: 0 1px 1px rgba(0,0,0,.04);
                border: 1px solid #ccd0d4;
                margin-top: 20px;
                max-width: 650px;
                position: relative;
        }
        .resi-wrap .layer.fetching:after{
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                content: '';
                display: block;
                background: rgba(255,255,255,.5);
                z-index: 2;
        }
        .resi-wrap .layer form > *{
                display: inline-block;
                vertical-align: middle;
                margin-left: 10px;
        }
        .resi-wrap .layer input{
                width: 200px;
        }
        .resi-wrap .layer form > *:first-child{
                margin: 0;
        }
        .resi-wrap .layer .msg  ol{
                margin: 0;
                padding: 0 0 0 15px;
        }
        .resi-wrap .layer .msg li{
                margin: 15px 0 0;
        }
</style>
<div class="wrap resi-wrap">
        <h2><?php _e('Import Resi Number','avoskin');?></h2>
        
        <div class="layer">
                <form id="form-import-resi">
                        <label>Import Resi From this courier</label>
                        <select name="courier">
                                <option value="jne" selected="selected">JNE</option>
                                <option value="jnt">J&T</option>
                                <option value="rpx">RPX</option>
                                <option value="pos">Pos Indonesia</option>
				<option value="sicepat">SiCepat</option>
                        </select>
                        <input type="file" id="resi_file" required="required" accept=".csv,.xlsx" />
                        <button type="submit" class="button button-primary">Import</button>
                </form>
                
                <div class="msg"><ol></ol></div>
        </div><!-- end of layer -->
</div><!-- end wrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>
<script type="text/javascript">
        ;(function($){
                $(document).ready(function(){
                        var dataResi = [],
                                ajaxQueue = $({});
	      
                        $.ajaxQueue = function(ajaxOpts) {
                                // hold the original complete function
                                var oldComplete = ajaxOpts.complete;
                              
                                // queue our ajax request
                                ajaxQueue.queue(function(next) {
                              
                                        // create a complete callback to fire the next event in the queue
                                        ajaxOpts.complete = function() {
                                                // fire the original complete if it was there
                                                if (oldComplete) oldComplete.apply(this, arguments);
                                                next(); // run the next query in the queue
                                        };
                                      
                                        // run the query
                                        $.ajax(ajaxOpts);
                                });
                        };
			var ExcelToJSON = function() {
				this.parseExcel = function(file) {
					var reader = new FileReader();
			  
					reader.onload = function(e) {
						var data = e.target.result;
						var workbook = XLSX.read(data, {
							type: 'binary'
						});
						workbook.SheetNames.forEach(function(sheetName) {
						// Here is your object
							var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
							var json_object = JSON.parse(JSON.stringify(XL_row_object));
							var head = '';

							$.each(json_object[0], function(kk,vv){
								head += kk+',';
							});
							head = head.replace(/,\s*$/, "");
							dataResi.push(head);
							$.each(json_object,function(k,v){
								var text = '';
								$.each(v, function(kk,vv){
									text += vv+',';
								});
								text = text.replace(/,\s*$/, "");
								dataResi.push(text);
							});
						});
					};
			  
					reader.onerror = function(ex) {
						console.log(ex);
					};
			  
					reader.readAsBinaryString(file);
				};
			};
			function parseCSV(e){
				var reader = new FileReader();
				reader.onload = function(e) {
					var lines = e.target.result.split('\r\n');
					for (i = 0; i < lines.length; ++i){
						dataResi.push(lines[i].replace(new RegExp('"', 'g'),""));
					}
				};
				reader.readAsText(e.target.files.item(0));
			}
                        $('#resi_file').on('change', function(e){
                               var ext = $("input#resi_file").val().split(".").pop().toLowerCase();
			       if (e.target.files != undefined) {
					if($.inArray(ext, ["csv"]) != -1) {
						parseCSV(e);
					}else if($.inArray(ext, ["xlsx"]) != -1) {
						var files = e.target.files; // FileList object
						var xl2json = new ExcelToJSON();
						xl2json.parseExcel(files[0]);
					}else{
						alert('Upload CSV or XLSX file.');
					}
                                }
                                return false;
                        });
                        $('#form-import-resi').on('submit', function(e){
                                e.preventDefault();
                                var self = $(this),
                                        layer = self.closest('.layer'),
                                        msg = layer.find('.msg ol');
                                if (dataResi.length > 0) {
                                        if (!layer.hasClass('fetching')) {
                                                msg.html('');
                                                var total = dataResi.length - 1,
                                                        counter = 1;
                                                layer.addClass('fetching');
                                                for(i=1;i<=total;i++){
                                                        if (dataResi[i].length > 5) {
                                                                $.ajaxQueue({
                                                                        url: '<?php echo site_url('/wp-json/avoskin/v1/import_resi_order/');?>',
                                                                        data: {
                                                                                head: dataResi[0],
                                                                                resi: dataResi[i],
                                                                                courier: self.find('select').val()
                                                                        },
                                                                        type: 'POST',
                                                                        success: function(data) {
                                                                                $(data).appendTo(msg);
                                                                                if (counter == total) {
                                                                                        layer.removeClass('fetching');
                                                                                        $('#resi_file').val('');
                                                                                        dataResi = [];
                                                                                }
                                                                                counter++;
                                                                        }
                                                                });
                                                        }else{
                                                                total--;
                                                        }      
                                                }
                                        }
                                }
                        });
                });
        }(jQuery));
</script>