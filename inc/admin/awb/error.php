
<!-- ::CRWFC:: -->
<div id="table-request" class="table-basic">
        <table>
                <thead>
                        <tr>
                                <th><input type="checkbox" name="check_all" value="all"/></th>
                                <th><?php _e('Order','avoskin');?></th>
                                <th><?php _e('Date','avoskin');?></th>
                                <th><?php _e('Nama','avoskin');?></th>
                                <th><?php _e('Ekspedisi','avoskin');?></th>
                                <th><?php _e('Total','avoskin');?></th>
                                <th><?php _e('Info','avoskin');?></th>
                        </tr>
                </thead>
                <tbody>
                        <?php
                                $orders = $controller->get_admin_order(['wc-processing']);
                                $has_order = false;
                                if(is_array($orders) && !empty($orders)):
                                foreach($orders as $o):
                                        if(get_post_meta((int)$o['ID'], 'awb_status', true) == 'awb-error'):
                                                $order = wc_get_order($o['ID']);
                                                $courier = get_post_meta($order->get_id(), 'shipping_courier', true);
                                                $courier_service = get_post_meta($order->get_id(), 'shipping_service', true);
                                                $err_body = get_post_meta($order->get_id(), $courier.'_response_awb_body', true);
                                                $err_all = get_post_meta($order->get_id(), $courier.'_response_awb_all', true);
                                                $request_param = get_post_meta($order->get_id(), $courier.'_temp_request', true);
                        ?>
                                                <tr id="row-<?php echo $order->get_id() ;?>">
                                                        <td><input type="checkbox" name="check"  value="<?php echo $order->get_id() ;?>"/></td>
                                                        <td><a href="<?php echo get_edit_post_link($order->get_id()) ;?>">#<?php echo $order->get_order_number() ;?></a></td>
                                                        <td><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ) ;?></td>
                                                        <td><?php echo $order->get_shipping_first_name() ;?> <?php echo $order->get_shipping_last_name() ;?></td>
                                                        <td><?php echo strtoupper($courier);?> <?php echo strtoupper($courier_service) ;?> </td>
                                                        <td><?php echo $order->get_formatted_order_total() ;?></td>
                                                        <td><a href="#" class="button toggle-error" data-target="#errdetail-<?php echo $order->get_id() ;?>">Detail</a></td>
                                                </tr>
                                                <tr id="errdetail-<?php echo $order->get_id() ;?>" class="detail">
                                                        <td colspan="7">
                                                                <?php if($err_body != ''):
                                                                        $err_body = json_encode(json_decode($err_body), JSON_PRETTY_PRINT);
                                                                ?>
                                                                        <h4>Error Message Body</h4>
                                                                        <pre><?php echo $err_body ;?></pre><br/>
                                                                <?php endif;?>
                                                                <?php if($err_all != ''):?>
                                                                        <h4>Error Message All</h4>
                                                                        <pre><?php echo $err_all ;?></pre><br/>
                                                                <?php endif;?>
                                                                <?php if($request_param != ''):
                                                                        $pretty_param = json_encode(json_decode($request_param), JSON_PRETTY_PRINT);
                                                                        $request_param = json_decode($request_param, true);
                                                                ?>
                                                                <h4>Request Paramater</h4>
                                                                <pre><?php echo $pretty_param ;?></pre><br/>
                                                                <?php if(is_array($request_param) && !empty($request_param)):?>
                                                                        <h4>Update Paramater</h4>
                                                                        <form>
                                                                                <?php foreach($request_param as $key => $rp):
                                                                                        if($key != 'PackageList'):
                                                                                ?>
                                                                                        <div>
                                                                                                <label><?php echo $key ;?></label>
                                                                                                <input type="text" name="param[<?php echo $key ;?>]" value="<?php echo $rp ;?>" class="widefat" />
                                                                                        </div>
                                                                                        <?php else:
                                                                                                foreach($rp[0] as $plk => $plv):
                                                                                        ?>
                                                                                                        <div>
                                                                                                                <label><?php echo $plk ;?></label>
                                                                                                                <input type="text" name="param[<?php echo $key ;?>][0][<?php echo $plk ;?>]" value="<?php echo $plv ;?>" class="widefat" />
                                                                                                        </div>
                                                                                                <?php endforeach;?>
                                                                                        <?php endif;?>
                                                                                <?php endforeach;?>
                                                                                <div class="last">
                                                                                        <label>&nbsp;</label>
                                                                                        <input type="hidden" name="order_id" value="<?php echo $o['ID'] ;?>" />
                                                                                        <input type="hidden" name="courier" value="<?php echo $courier ;?>" />
                                                                                        <button type="submit" class="button button-primary" style="width:180px;"><?php _e('Update','avoskin');?></button>
                                                                                </div>
                                                                        </form>
                                                                        <?php endif;?>
                                                                <?php endif;?>
                                                        </td>
                                                </tr>
                                        <?php $has_order = true; endif;?>
                                <?php endforeach;?>
                        <?php endif; ?>
                        <?php if(!$has_order):?>
                                <tr>
                                        <td colspan="7" style="text-align: center">
                                                <?php _e('Order not found.','avoskin');?>
                                        </td>
                                </tr>
                        <?php endif;?>
                </tbody>
                <?php if($has_order):?>
                        <tfoot>
                                <tr>
                                        <td colspan="7">
                                                <a href="#" id="btn-request-awb" class="button button-primary"><?php _e('Request Airwaybill','avoskin');?></a>
                                                &nbsp;<a href="#" id="btn-move-to-label" class="button"><?php _e('Pindahkan ke Label','avoskin');?></a>
                                                &nbsp;<a href="#" id="btn-export-awb" class="button"><?php _e('Export Data','avoskin');?></a>
                                        </td>
                                </tr>
                        </tfoot>
                <?php endif;?>
        </table>
</div>

<div class="table-export" style="display: none;">
        <table>
                <thead>
                        <tr>
                                <td>Order</td>
                                <td>Alamat</td>
                                <td>Kabupaten/Kota</td>
                                <td>Provinsi</td>
                                <td>Kode POS</td>
                                <td>Response</td>
                        </tr>
                </thead>
                <tbody>
                        <?php
                        if(is_array($orders) && !empty($orders)):
                                foreach($orders as $o):
                                        if(get_post_meta((int)$o['ID'], 'awb_status', true) == 'awb-error'):
                                                $order = wc_get_order($o['ID']);
                                                $split_country = explode( ":", $store_raw_country );
                                                $country = $order->get_billing_country();
                                                $state = $order->get_shipping_state();
                                                $courier = get_post_meta($order->get_id(), 'shipping_courier', true);
                                                if($courier != 'sicepat'){
                                                        $address = wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_shipping_address_1())));
                                                        $city = wp_strip_all_tags(esc_attr($order->get_shipping_city()));
                                                        $prov = WC()->countries->get_states( $country )[$state];
                                                        $zip = $order->get_shipping_postcode();
                                                }else{
                                                        $sicepat_req = json_decode(get_post_meta($order->get_id(), 'sicepat_temp_request', true), true);
                                                        $sicepat_req = $sicepat_req['PackageList'][0];
                                                        
                                                        $address = ($sicepat_req['recipient_address'] != '' ) ? $sicepat_req['recipient_address'] : wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_shipping_address_1())));
                                                        $district =  ($sicepat_req['recipient_district']  != '' ) ? $sicepat_req['recipient_district'] .' ' : '';
                                                        $city = ($sicepat_req['recipient_city'] != '' ) ? $sicepat_req['recipient_city'] : wp_strip_all_tags(esc_attr($order->get_shipping_city())) ;
                                                        $city = $district . $city;
                                                        $prov = ($sicepat_req['recipient_province'] != '') ? $sicepat_req['recipient_province'] : WC()->countries->get_states( $country )[$state] ;
                                                        $zip = ($sicepat_req['recipient_zip']  != '' ) ? $sicepat_req['recipient_zip'] : $order->get_shipping_postcode();
                                                }
                                                $err_all = get_post_meta($order->get_id(), $courier.'_response_awb_all', true);
                                        ?>
                                        <tr data-id="<?php echo $order->get_id() ;?>">
                                                <td>#<?php echo $order->get_order_number() ;?></td>
                                                <td><?php echo str_replace(',', ' ', $address) ;?></td>
                                                <td><?php echo str_replace(',', ' ', $city) ;?></td>
                                                <td><?php echo str_replace(',', ' ', $prov) ;?></td>
                                                <td><?php echo $zip ;?></td>
                                                <td><?php echo $err_all ;?></td>
                                        </tr>
                                <?php endif;?>
                        <?php endforeach;?>
                        <?php endif;?>
                        <?php wp_reset_query(); ?>
                </tbody>
        </table>
</div><!-- end of table export -->