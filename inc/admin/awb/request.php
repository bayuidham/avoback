<!-- ::CRWFC:: -->
<div id="table-request" class="table-basic">
        <table>
                <thead>
                        <tr>
                                <th><input type="checkbox" name="check_all" value="all"/></th>
                                <th><?php _e('Order','avoskin');?></th>
                                <th><?php _e('Date','avoskin');?></th>
                                <th><?php _e('Nama','avoskin');?></th>
                                <th><?php _e('Ekspedisi','avoskin');?></th>
                                <th><?php _e('Total','avoskin');?></th>
                        </tr>
                </thead>
                <tbody>
                        <?php
                                $orders = $controller->get_admin_order(['wc-processing']);
                                $has_order = false;
                                if(is_array($orders) && !empty($orders)):
                                foreach($orders as $o):
                                        if(
                                                get_post_meta((int)$o['ID'], 'awb_status', true) != 'awb-success' &&
                                                get_post_meta((int)$o['ID'], 'awb_status', true) != 'awb-error' &&
                                                get_post_meta((int)$o['ID'], 'resi_order', true) == ''
                                                
                                        ):
                                                $order = wc_get_order($o['ID']);
                                                $courier = get_post_meta($order->get_id(), 'shipping_courier', true);
                                                $courier_service = get_post_meta($order->get_id(), 'shipping_service', true);
                        ?>
                                                <tr id="row-<?php echo $order->get_id() ;?>">
                                                        <td><input type="checkbox" name="check"  value="<?php echo $order->get_id() ;?>"/></td>
                                                        <td>#<?php echo $order->get_order_number() ;?></td>
                                                        <td><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ) ;?></td>
                                                        <td><?php echo $order->get_shipping_first_name() ;?> <?php echo $order->get_shipping_last_name() ;?></td>
                                                        <td><?php echo strtoupper($courier);?> <?php echo strtoupper($courier_service) ;?> </td>
                                                        <td><?php echo $order->get_formatted_order_total() ;?></td>
                                                </tr>
                                        <?php $has_order = true; endif;?>
                                <?php endforeach;?>
                        <?php endif; ?>
                        <?php if(!$has_order):?>
                                <tr>
                                        <td colspan="6" style="text-align: center">
                                                <?php _e('Order not found.','avoskin');?>
                                        </td>
                                </tr>
                        <?php endif;?>
                </tbody>
                <?php if($has_order):?>
                        <tfoot>
                                <tr>
                                        <td colspan="6">
                                                <a href="#" id="btn-request-awb" class="button button-primary"><?php _e('Request Airwaybill','avoskin');?></a>
                                        </td>
                                </tr>
                        </tfoot>
                <?php endif;?>
        </table>
</div>