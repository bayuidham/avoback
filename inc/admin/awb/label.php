<!-- ::CRWFC:: --->
<style type="text/css">
        .generated-label table{
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                border: 1px solid #000;
                color: #000;
        }
        .popup-label{
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                z-index: 9999999;
                padding: 50px;
                background: rgba(0,0,0,.6);
                overflow-y: scroll;
                display: none;
        }
        .popup-label:after{
                display: none;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9;
                background: rgba(255,255,255,.9);
                content: '';
        }
        .popup-label.printed:after{
                display: block;
        }
        body.open-popup-label .popup-label{
                display: block;
        }
        body.open-popup-label{
                height: 100vh;
                overflow: hidden;
        }
        .popup-label .head{
                background: #0073aa;
                padding: 15px 30px;
                position: relative;
        }
        .popup-label .head a.cls{
                color: #fff;
                text-decoration: none;
                position: absolute;
                right: 20px;
                top: 50%;
                -ms-transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
        }
        /*.generated-label td svg{
                display: inline-block;
                max-width: 47%;
                margin: 0 1%;
        }*/
     /*   .generated-label td  .hdr span{
                display: block;
                position: relative;
                left: -9px;
        }*/
        .address-row .holder{
                display: flex;
                flex-wrap:wrap;
                justify-content: space-between;
                max-width: none;
                margin: 0;
        }
        .address-row .holder .aitem{
                flex: calc(50% - 5px);
                max-width:calc(50% - 10px);
                margin: 0 0 30px;
        }
        img{
                max-width:100%;
        }
        .logo-row img{
                max-height:50px;
                width: auto;
        }
        .logo-row td{
                padding: 20px;
                border-bottom: 1px solid #000;
                width: 33.3333333333333333333333%;
        }
        .logo-row td svg{
                position: relative;
                left: -50px;
        }
        .logo-row td:last-child{
                text-align: right;
        }
        .service-row td,
        .resi-row td{
                text-align: center;
                padding: 5px  10px;
                border-bottom: 1px solid #000;
        }
        .service-row td{
                text-align: left;
                font-size: 16px;
                font-weight:700;
                padding: 12px 15px;
        }
        .detail-row  th{
                text-align: center;
                border-bottom: 1px solid #000;
                padding: 5px 10px;
                width: 42%;
        }
        .detail-row  th > *{
                display: inline-block;
        }
        .detail-row td{
                border-left: 1px solid #000;
                padding: 5px 15px;
                font-weight:700;
                font-size: 20px;
                border-bottom: 1px solid #000;
                width: 58%;
        }
        .address-row td{
                padding: 15px;
                border-bottom: 1px solid #000;
                font-weight:600;
                font-size: 18px;
        }
        .address-row td p{
                font-size: 18px;
                margin: 10px 0 0;
                padding: 0;
                line-height: 160%;
        }
        .address-row .holder .aitem,
        .address-row td p:first-child{
                margin: 0;
        }
        .product-row td,
        .date-row td,
        .note-row td{
                padding: 10px 15px;
                font-size: 18px;
                font-weight: 600;
        }
        .date-row td{
                border-top: 1px solid #000;
                border-bottom: 1px solid #000;
        }
        .note-row td{
                border-bottom: 1px solid #000;
        }
        .product-row td{
                padding: 5px 15px;
        }
        .date-row + .product-row td{
                padding: 10px 15px;
        }
        .product-row:last-child td{
                padding-bottom: 15px;
        }
        .popup-label .progress{
                display: none;
                position: absolute;
                left: 0;
                top: 50%;
                -ms-transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
                text-align: center;
                width: 100%;
                color: #000;
                font-size: 30px;
                font-weight: 700;
                z-index: 9999;
        }
        .popup-label.printed .progress{
                display: block;
        }
        .generated-label #content{
                background: #fff;
        }
        .popup-label .holder{
                max-width: 874px;
                margin: 0 auto;
        }
</style>
<div id="table-label" class="table-basic">
        <table>
                <thead>
                        <tr>
                                <th><input type="checkbox" name="check_all" value="all"/></th>
                                <th><?php _e('Order','avoskin');?></th>
                                <th><?php _e('Date','avoskin');?></th>
                                <th><?php _e('No. Resi','avoskin');?></th>
                                <th><?php _e('Total','avoskin');?></th>
                        </tr>
                </thead>
                <tbody>
                        <?php
                                $orders = $controller->get_admin_order();
                                $has_order = false;
                                if(is_array($orders) && !empty($orders)):
                                foreach($orders as $o):
                                        //update_post_meta((int)$o['ID'], 'has_print', 'nope');
                                        if(get_post_meta((int)$o['ID'], 'awb_status', true) == 'awb-success'):
                                        //if(true):
                                                $have_product = false;
                                                $order = wc_get_order($o['ID']);
                                                $store_raw_country = get_option( 'woocommerce_default_country' );
                                                $split_country = explode( ":", $store_raw_country );
                                                $store_country = $split_country[0];
                                                $store_state   = $split_country[1];
                                                $country = $order->get_billing_country();
                                                $state = $order->get_shipping_state();
                                                $courier = get_post_meta($order->get_id(), 'shipping_courier', true);
                                                $courier_service = get_post_meta($order->get_id(), 'shipping_service', true);
                                                $weight = 0;
                                                $qty = 0;
                                                foreach ( $order->get_items() as $item_id => $item ) {
                                                        $qty += $item->get_quantity();
                                                        $product = $item->get_product();
                                                        if($product){
                                                                $weight += floatval( $product->get_weight() * $item->get_quantity() );        
                                                        }else{
                                                                $weight += 0;
                                                        }
                                                        
                                                }
                                                
                                                $to = [
                                                        'name' => wp_strip_all_tags(esc_attr($order->get_shipping_first_name()) . ' ' . esc_attr($order->get_shipping_last_name())),
                                                        'phone' => $order->get_billing_phone(),
                                                ];
                                                if($courier != 'sicepat'){
                                                     $to['address'] =  wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_shipping_address_1()))) . ' '
                                                                                        . wp_strip_all_tags(esc_attr($order->get_shipping_city())) . ' '
                                                                                        . WC()->countries->get_states( $country )[$state] . ' '
                                                                                        . $order->get_shipping_postcode();
                                                }else{
                                                        $sicepat_req = json_decode(get_post_meta($order->get_id(), 'sicepat_temp_request', true), true);
                                                        $sicepat_req = $sicepat_req['PackageList'][0];
                                                        $address = ($sicepat_req['recipient_address'] != '' ) ? wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $sicepat_req['recipient_address']))) :
                                                                                                                                                                        wp_strip_all_tags(esc_attr( str_replace(['"', "'"], '', $order->get_shipping_address_1())));
                                                        $district =  ($sicepat_req['recipient_district']  != '' ) ? $sicepat_req['recipient_district']  : '';
                                                        $city = ($sicepat_req['recipient_city'] != '' ) ? $sicepat_req['recipient_city'] : wp_strip_all_tags(esc_attr($order->get_shipping_city())) ;
                                                        $prov = ($sicepat_req['recipient_province'] != '') ? $sicepat_req['recipient_province'] : WC()->countries->get_states( $country )[$state] ;
                                                        $zip = ($sicepat_req['recipient_zip']  != '' ) ? $sicepat_req['recipient_zip'] : $order->get_shipping_postcode();
                                                        $to['address'] = $address. ' ' . $district . ' ' . $city  . ' ' . $prov. ' ' .  $zip ;
                                                }
                                                
                                                $weight = (get_option('woocommerce_weight_unit') != 'kg') ? avoskin_markup($weight) / 1000 : avoskin_markup($weight * 1000) / 1000;
                                                $data = [
                                                        'id' => $order->get_order_number(),
                                                        'date'=>wc_format_datetime( $order->get_date_created(), 'd F Y' ),
                                                        //'logo' =>$data_logo['logo'],
                                                        //'courier_logo' => $data_logo[$courier],
                                                        'logo' =>esc_url( get_template_directory_uri() ) . '/assets/img/shipping/logo-black.png',
                                                        'courier_logo' => esc_url( get_template_directory_uri() ) . '/assets/img/shipping/'.$courier.'-black.png',
                                                        'weight' => $weight .' KG',
                                                        'qty' => $qty,
                                                        'resi' => get_post_meta($order->get_id(), 'resi_order', true),
                                                        'courier' => strtoupper($courier),
                                                        'courier_service' => strtoupper($courier_service),
                                                        'from' => [
                                                                'name' => esc_attr(get_option($courier.'_awb_shipper_name')),
                                                                'phone' => get_option($courier.'_awb_shipper_phone'),
                                                                'address' => wp_strip_all_tags(esc_attr(str_replace(['"', "'"], '', get_option( 'woocommerce_store_address' )))) . ' '
                                                                                        . wp_strip_all_tags(esc_attr(get_option( 'woocommerce_store_city' ))) . ' '
                                                                                        . WC()->countries->get_states( $store_country )[$store_state] . ' '
                                                                                        . get_option( 'woocommerce_store_postcode' )
                                                        ],
                                                        'to' => $to,
                                                        'note' =>  ($order->get_customer_note() != '')  ? wp_strip_all_tags(esc_attr(str_replace(['"', "'"], '', $order->get_customer_note()))) : '-',
                                                        'items' => []
                                                ];
                                                foreach ($order->get_items() as $item_id => $item_data){
                                                        $product = $item_data->get_product();
                                                        if(is_object($product) ){
                                                                $have_product = true;
                                                                $data['items'][] = [
                                                                        'name' => esc_attr($product->get_name()),
                                                                        'qty' => $item_data->get_quantity()
                                                                ];        
                                                        }
                                                }
                                                $has_order = true;
                                        ?>
                                        <tr data-order='<?php echo wp_json_encode($data);?>'>
                                                <td><input type="checkbox" name="check" value="<?php echo $order->get_id();?>"/></td>
                                                <td><a href="<?php echo get_edit_post_link($order->get_id()) ;?>">#<?php echo $order->get_order_number() ;?> <?php echo $order->get_shipping_first_name() ;?> <?php echo $order->get_shipping_last_name() ;?></a></td>
                                                <td><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ) ;?></td>
                                                <td><?php echo strtoupper($courier);?> [<?php echo strtoupper($courier_service) ;?>] - <?php echo get_post_meta($order->get_id(), 'resi_order', true);?></td>
                                                <td><?php echo $order->get_formatted_order_total() ;?></td>
                                        </tr>
                                <?php endif;?>
                        <?php endforeach;?>
                        <?php else:?>
                        <?php endif;?>
                        <?php wp_reset_query(); ?>
                        <?php if($has_order == false):?>
                                <tr>
                                        <td colspan="5" style="text-align: center">
                                                <?php _e('Order not found.','avoskin');?>
                                        </td>
                                </tr>
                        <?php endif;?>
                </tbody>
                <?php if($has_order):?>
                        <tfoot>
                                <tr>
                                        <td colspan="5">
                                                <a href="#" id="preview-label" class="button button-primary"><?php _e('Preview Label','avoskin');?></a>
                                        </td>
                                </tr>
                        </tfoot>
                <?php endif;?>
        </table>
</div>
<div class="popup-label">
        <span class="progress"><b>0</b>% processed</span>
        <div class="holder">
                <div class="head">
                        <a href="#" id="print-label" class="button"><?php _e('Print Label','avoskin');?></a>
                        <a href="#" class="cls"><i class="dashicons dashicons-dismiss"></i></a>
                </div>
                <div class="generated-label"><div id="content"></div></div>
        </div>
</div>