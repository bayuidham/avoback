<?php

// HEADER
add_action( 'avoskin_header', 'avoskin_before_head', 5 );
add_action( 'avoskin_header', 'avoskin_topbar_head', 10 );
add_action( 'avoskin_header', 'avoskin_botbar_head', 15 );
add_action( 'avoskin_header', 'avoskin_after_head', 20 );

// FOOTER
add_action( 'avoskin_footer', 'avoskin_before_foot', 5 );
add_action( 'avoskin_footer', 'avoskin_optin_foot', 10 );
add_action( 'avoskin_footer', 'avoskin_widget_foot', 15 );
add_action( 'avoskin_footer', 'avoskin_tribute_foot', 20 );
add_action( 'avoskin_footer', 'avoskin_after_foot', 25 );

//ABOUT
add_action( 'avoskin_about', 'avoskin_intro_about', 5 );
add_action( 'avoskin_about', 'avoskin_discover_about', 10 );
add_action( 'avoskin_about', 'avoskin_cruelty_about', 15 );
add_action( 'avoskin_about', 'avoskin_love_about', 20 );

//REFINING SERIES
add_action( 'avoskin_refining', 'avoskin_banner_refining', 5 );
add_action( 'avoskin_refining', 'avoskin_copy_refining', 10 );
add_action( 'avoskin_refining', 'avoskin_benefit_refining', 15 );
add_action( 'avoskin_refining', 'avoskin_action_refining', 20 );

//RETINOL SERIES
add_action( 'avoskin_retinol', 'avoskin_banner_retinol', 5 );
add_action( 'avoskin_retinol', 'avoskin_video_retinol', 10 );
add_action( 'avoskin_retinol', 'avoskin_copy_retinol', 15 );
add_action( 'avoskin_retinol', 'avoskin_benefit_retinol', 20 );
add_action( 'avoskin_retinol', 'avoskin_action_retinol', 25 );

//HOMEPAGE
add_action( 'avoskin_homepage', 'avoskin_slider_homepage', 5 );
add_action( 'avoskin_homepage', 'avoskin_feature_homepage', 10 );
add_action( 'avoskin_homepage', 'avoskin_avostore_homepage', 15 );
add_action( 'avoskin_homepage', 'avoskin_testimonial_homepage', 20 );
add_action( 'avoskin_homepage', 'avoskin_flash_sale_homepage', 25 );
add_action( 'avoskin_homepage', 'avoskin_best_seller_homepage', 30 );
add_action( 'avoskin_homepage', 'avoskin_instagram_homepage', 35 );
add_action( 'avoskin_homepage', 'avoskin_blog_homepage', 40 );

//HOMEPAGE2
//$order = get_post_meta(get_option('page_on_front'), 'section_order', true);
$order = get_post_meta(550572, 'section_order', true); //HARDCODE ::TEMP::
if(is_array($order) && !empty($order)){
        foreach($order as $o){
                add_action( 'avoskin_homepage2', 'avoskin_'.$o.'_homepage2' );        
        }      
}

//add_action( 'avoskin_homepage2', 'avoskin_slider_homepage2', 5 );
//add_action( 'avoskin_homepage2', 'avoskin_series_homepage2', 10 );
//add_action( 'avoskin_homepage2', 'avoskin_flash_homepage2', 15 );
//add_action( 'avoskin_homepage2', 'avoskin_best_homepage2', 20 );
//add_action( 'avoskin_homepage2', 'avoskin_recom_homepage2', 25 );
//add_action( 'avoskin_homepage2', 'avoskin_testi_homepage2', 30 );
//add_action( 'avoskin_homepage2', 'avoskin_caro_homepage2', 35 );
add_action( 'avoskin_homepage2', 'avoskin_subs_homepage2', 40 );

add_action( 'sbi_before_feed', 'avoskin_instagram_feed', 10,2 );

//NAV SIDEBAR
add_action('avoskin_navside', 'avoskin_navside_display');

//SKINBAE
add_action( 'avoskin_skinbae', 'avoskin_navi_skinbae', 2 );
add_action( 'avoskin_skinbae', 'avoskin_intro_skinbae', 5 );
add_action( 'avoskin_skinbae', 'avoskin_variant_skinbae', 10 );
add_action( 'avoskin_skinbae', 'avoskin_hero_skinbae', 15 );
add_action( 'avoskin_skinbae', 'avoskin_product_skinbae', 20 );
add_action( 'avoskin_skinbae', 'avoskin_ingre_skinbae', 25 );
add_action( 'avoskin_skinbae', 'avoskin_benefit_skinbae', 30 );
add_action( 'avoskin_skinbae', 'avoskin_function_skinbae', 35 );
add_action( 'avoskin_skinbae', 'avoskin_proof_skinbae', 40 );
add_action( 'avoskin_skinbae', 'avoskin_person_skinbae', 45 );

//BLOG
add_action( 'avoskin_blog_header', 'avoskin_blog_header_view' );
add_action( 'avoskin_blog_footer', 'avoskin_blog_footer_view' );