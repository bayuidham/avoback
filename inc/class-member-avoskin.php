<?php
/**
 * Avoskin Member Class
 *
 * @since    1.0.0
 * @package  avoskin
 */

namespace Avoskin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Member' ) ) {
        
        class Member{
                private static $instance;
                
                public function __construct(){
                        
                }
                
                public static function get_instance(){
                        if(!isset(self::$instance)) {
                                self::$instance = new Member();
                        }
                        return self::$instance;
                }
                
                public function user_login($data, $results){
                        $user_signon = wp_signon($data, true);
                        if (is_wp_error($user_signon)) {
                                $results['msg'] = __('Wrong username or password.', 'avoskin');
                        } else {
                                $user_roles = $user_signon->roles;
                                $user_id = $user_signon->ID;
                                wp_set_auth_cookie( $user_id, true );
                                wp_set_current_user( $user_id, $data['user_login'] );
                                do_action( 'wp_login', $data['user_login'], get_userdata( $user_id ) );
				
				$results = [
					'status' => 'ok',
					'title' => __('Success', 'avoskin'),
					'msg' => __('Login successful, redirecting...', 'avoskin'),
					'redirect' =>  (in_array("administrator", $user_roles)) ? admin_url() :  get_permalink( wc_get_page_id( 'myaccount' ) ),
					'user_id' => $user_id // ::CRWFC::
				];
                        }
                        return $results;
                }
		
		public function user_register($data, $results){
			//Check if email or username already taken
			if( null == username_exists( $data['user_email'] ) && null == email_exists( $data['user_email'] ) ) {
				if(strlen($data['user_password']) < 6){
					$results['msg'] = __('Password minimum 6 character.','avoskin');
				}else{
					$username = $this->generate_unique_username($data['first_name'] . $data['last_name']);
					$user_id = wp_create_user( $username, $data['user_password'], $data['user_email'] );
					if(!is_wp_error($user_id)){
						//wp_new_user_notification($user_id, NULL, 'admin');
						update_user_meta($user_id, 'first_name', $data['first_name']);
						update_user_meta($user_id, 'last_name', $data['last_name']);
						// ::CRLOYALTY::
						update_user_meta($user_id, 'loyalty_phone', $data['loyalty_phone']);
						$results = [
							'status' => 'ok',
							'title' => __('Success', 'avoskin'),
							'msg' => __('Register successful, this page will be reloaded...', 'avoskin'),
							'data_user' => [
								'id' => $user_id,
								'email' => $data['user_email'],
								'password' => $data['user_password']
							],
							'data' => [
								'phone' => $data['loyalty_phone'],
								'email' => $data['user_email'],
								'name' => $data['first_name'] . ' ' . $data['last_name'],
								'gender' => ''
							]
						];
					}	
				}
			}else{
				$results['msg'] = __('Email already taken.','avoskin');
			}

			return $results;
		}
		
		private function generate_unique_username( $username ) {
			if(strlen($username) > 5){
				$username = sanitize_user( $username );
				static $i;
				if ( null === $i ) {
					$i = 1;
				} else {
					$i ++;
				}
				if ( ! username_exists( $username ) ) {
					return $username;
				}
				$new_username = sprintf( '%s-%s', $username, $i );
				if ( ! username_exists( $new_username ) ) {
					return $new_username;
				} else {
					return call_user_func( __FUNCTION__, $username );
				}
			}else{
				return sanitize_user( $username ).current_time('timestamp');
			}
		}
		
		public function user_forgot_password($data, $results){
			$email = $data['email'];
			
			if (!is_email($email)) {
				$results['msg'] = __('Invalid email address', 'avoskin');
			} elseif (!email_exists($email)) {
				$results['msg'] = __('There is no user registered with that email address', 'avoskin');
			} else {
				$user = get_user_by('email', $email);
				$user_login = $user->user_login;
				$reset_url = add_query_arg([
					'key' => get_password_reset_key( $user ),
					'user' => $user_login
				], avoskin_get_page('new_password', true));
				
				$results = [
					'status' => 'ok',
					'title' => __('Success', 'avoskin'),
					'msg' => __('An email with reset password link has been delivered to your email address.', 'avoskin'),
					'reset_key' => get_password_reset_key( $user ),
					'login' => $user_login
				];
			}
			
			return $results;
		}
		
		public function update_user($user_id, $data, $results){
			$user_id = wp_update_user([
				'ID' => $user_id,
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'user_email' => $data['email'],
			]);
			
			update_user_meta($user_id, 'dob_d', $data['dob_day']);
			update_user_meta($user_id, 'dob_m', $data['dob_month']);
			update_user_meta($user_id, 'dob_y', $data['dob_year']);
			update_user_meta($user_id, 'gender', $data['gender']);
			
			// ::CRLOYALTY::
			$reload = 'yep';
			$old_phone = get_user_meta($user_id, 'loyalty_phone', true);
			if($old_phone != $data['loyalty_phone']){
				update_user_meta($user_id, 'loyalty_phone_old', $old_phone); 
				update_user_meta($user_id, 'loyalty_phone', $data['loyalty_phone']); 
				update_user_meta($user_id, 'loyalty_phone_verified', 'nope');
				
				$reload = 'yep';
			}
			
			$results = [
				'status' => 'ok',
				'title' => __('Success', 'avoskin'),
				'msg' => __('Your profile successfully updated!', 'avoskin'),
				'reload' => $reload,
				'data' => [
					'email' => $data['email'],
					'name' => $data['first_name'] . ' ' . $data['last_name'],
					'gender' => $data['gender']
				]
			];
			
			if(isset($data['profile_picture']) && $data['profile_picture'] != ''){
				$attachment_id = avoskin_upload_img(json_decode($data['profile_picture'], true));
				update_user_meta($user_id, 'user_dp', $attachment_id);
			}
			
			return $results;
		}
		
		public function update_user_password($user_id, $data, $results){
			$user = get_user_by( 'id', $user_id );
			if ( $user && wp_check_password( $data['old_password'], $user->data->user_pass, $user->ID ) ) {
				wp_set_password($data['new_password'], $user->ID);
				
				$user_signon = wp_signon([
					'user_login' => $user->data->user_login,
					'user_password' => $data['new_password']
				], true);
				
				wp_set_auth_cookie( $user_id, true );
				wp_set_current_user( $user_id, $user->data->user_login);
				do_action( 'wp_login', $user->data->user_login, get_userdata( $user_id ) );
				
				$results = [
					'status' => 'ok',
					'title' => __('Success', 'avoskin'),
					'msg' => __('Your password successfully updated!', 'avoskin')
				];
			}else{
				$results['msg'] = __('Invalid old password!', 'avoskin');
			}
			
			return $results;
		}
		
		public function update_user_address($user_id, $data){
			foreach($data as $d => $v){
				update_user_meta($user_id, $d, $v);
			}
			
			return [
				'status' => 'ok',
				'title' => __('Success', 'avoskin'),
				'msg' => __('Your address successfully updated!', 'avoskin'),
				'redirect' => wc_get_endpoint_url('edit-address', '', get_permalink(get_option('woocommerce_myaccount_page_id')))
			];
		}
		
		
        }
}