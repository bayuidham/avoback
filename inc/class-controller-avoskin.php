<?php
/**
 * Avoskin Controller Class
 *
 * @since    1.0.0
 * @package  avoskin
 */

namespace Avoskin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Controller' ) ) {
	
        class Controller{
                private $member, $results, $model, $loyalty; // ::CRLOYALTY::
                private static $instance;
                
                public function __construct(){
                        $this->member = \Avoskin\Member::get_instance();
			$this->model = \Avoskin\Model::get_instance();
			$this->loyalty = \Avoskin\Loyalty::get_instance(); // ::CRLOYALTY::
			
			$this->results = [
                                'status' => 'fail',
                                'title' => __('Error', 'avoskin'),
                                'msg' => __('Something went wrong, please try again later', 'avoskin')
                        ];
			
                        //CREATE REST ROUTE
                        add_action('rest_api_init', [$this, 'register_endpoint']);
			
			//SEND EMAIL TRACKING INFO TO USER IF THE RESI INFO UPDATED
			//add_action('save_post', [$this,'order_update_resi']); Turn out, we are using auto generate way bill
			
			//SAVE REFERAL CONVERSION WHEN ORDER STATUS CHANGED TO COMPLETE
			add_action( 'woocommerce_order_status_completed', [$this,'add_referral_conversion']);
			
			//GENERATE AIR WAYBILL WHEN ORDER STATUS CHANGED TO PROCESSING
			//add_action( 'woocommerce_order_status_pending_to_processing', [$this,'generate_airwaybill']);
			//add_action( 'woocommerce_order_status_on-hold_to_processing', [$this,'generate_airwaybill']);
			
			//CHANGE USER STATUS TO VERIFIED IF ORDER IS COMPLETE
			add_action( 'woocommerce_order_status_completed', [$this,'add_verified_info']);
			
			//SEND AWB DATA WHEN ORDER STATUS CHANGED FROM PROCESSING TO ON DELIVERY
			add_action( 'woocommerce_order_status_changed', [$this,'send_awb_to_customer'], 10, 4);
			
			//DECREASE COUPON USAGE WHEN STATUS CHANGED FROM PENDING TO ON HOLD
			add_action( 'woocommerce_order_status_changed', [$this,'decrease_coupon_usage'], 10, 4);
			
			//COMPLETING SCHEDULED ORDER
			add_action('do_complete_scheduled_order', [$this, 'process_complete_scheduled_order']);
			
			add_action( 'woocommerce_order_status_changed', [$this,'send_whatsapp_message'], 10, 4);
			
			add_action( 'init', [$this, 'add_order_email_schedule'] );
			
			add_action( 'send_email_order_schedule', [$this, 'send_email_order_schedule_execute'] );
			
			// ::CRLOYALTY:: Trigger order expired
			add_action('avoskin_loyalty_order_cancelled', [$this, 'avoskin_loyalty_do_order_cancelled']);
                }
		
                        
                public static function get_instance(){
                        if(!isset(self::$instance)) {
                                self::$instance = new Controller();
                        }
                        return self::$instance;
                }
		
		private function wc_request($endpoint = 'products', $args = [], $method = 'GET'){
                        add_filter( 'woocommerce_rest_check_permissions', '__return_true' );
                        $request = new \WP_REST_Request( $method, "/wc/v3/$endpoint" );
                        if(!empty($args)){
                                $request->set_query_params( $args );
                        }
                        $response = rest_do_request( $request );
                        $server = rest_get_server();
                        $data = $server->response_to_data( $response, false );
                        
                        return $data;
                }
                
                public function register_endpoint(){
                        $namespace = 'avoskin/v1';
                        $route = [
                                //User Login
                                'user_login' => [
                                        'method' => 'POST',
                                        'args' => [
                                                'data' => ['required' => true]
                                        ]
                                ],
				//Register
				'user_register' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Forgot password
				'user_forgot_password' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Add to cart
				'add_to_cart' => [
					'method' => 'POST',
					'args' => [
						'id' => ['required' => true],
						'quantity' => ['required' => true]
					]
				],
				'add_to_cart_wfc' => [ // ::CRWFC::
					'method' => 'POST',
					'args' => [
						'id' => ['required' => true]
					]
				],
				//Add to cart
				'advisor_add_to_cart' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Remove product from cart
				'remove_cart_item' => [
					'method' => 'POST',
					'args' => [
						'id' => ['required' => true]
					]
				],
				//Fetch cart cart items sidebar
				'fetch_cart_items' => [
					'method' => 'GET',
					'args' => []
				],
				'update_cart_items' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Submit review
				'submit_review' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true],
						'user' => ['required' => true]
					]
				],
				//Get Rajaongkir States
				'get_states' => [
					'method' => 'POST',
					'args' => [
						'country' => ['required' => true]
					]
				],
				//Get Rajaongkir Cities
				'get_cities' => [
					'method' => 'POST',
					'args' => [
						'state' => ['required' => true]
					]
				],
				//Get Couriers Cities
				'get_couriers' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Generate checkout summary
				'generate_checkout_summary' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Check if user exists when visitor input email address
				'check_user_exists' => [
					'method' => 'POST',
					'args' => [
						'email' => ['required' => true]
					]
				],
				//Submit checkout
				'submit_checkout' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Tracking order
				'track_order' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Update user info
				'update_user_info' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Update user password
				'update_user_password' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Update user address
				'update_user_address' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Add product to wishlist
				'add_to_wish' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Emptying cart
				'emptying_cart' => [
					'method' => 'POST',
					'args' => []
				],
				//Record visit from referral
				'record_referral' => [
					'method' => 'POST',
					'args' => [
						'referral' => ['required' => true],
						'visitor' => ['required' => true]
					]
				],
				'send_resi_info' => [
					'method' => 'POST',
					'args' => [
						'order' => ['required' => true]
					]
				],
				//Fetch all city name in indonesia from JSON file
				'get_all_city' => [
					'method' => 'GET',
					'args' => []
				],
				
				//Join avostore form
				'join_avostore' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Sendgrid Newsletter
				'subscribe_optin' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//Add printed order meta
				'add_has_print_order' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				'order_need_print' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				'order_to_ondelivery' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				'order_print_surat_jalan' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				//When user clock button "order received" at order detail
				'user_order_received' => [
					'method' => 'POST',
					'args' => [
						'order' => ['required' => true]
					]
				],
				//Add tracking info detail at admin order
				'admin_tracking_detail' => [
					'method' => 'POST',
					'args' => [
						'resi' => ['required' => true],
						'courier' => ['required' => true]
					]
				],
				//Import resi order
				'import_resi_order' => [
					'method' => 'POST',
					'args' => [
						'resi' => ['required' => true],
						'head' => ['required' => true],
						'courier' => ['required' => true]
					]
				],
				//Add tracking info detail at admin order
				'sicepat_reset_counter' => [
					'method' => 'POST',
					'args' => []
				],
				//Add tracking info detail at admin order
				'sicepat_reset_counter' => [
					'method' => 'POST',
					'args' => []
				],
				'get_product_list' => [
					'method' => 'GET',
					'args' => [
						'total' => ['required' => false],
						'page' => ['required' => false],
						'search' => ['required' => false]
					]
				],
				'skinbae_product' => [
					'method' => 'POST',
					'args' => [
						'cat' => ['required' => false],
						'product' => ['required' => false]
					]
				],
				//Get postcode
				'get_postcode' => [
					'method' => 'POST',
					'args' => [
						'state' => ['required' => true],
						'current' => ['required' => false]
					]
				],
				//Request generate AWB
				'request_awb' => [
					'method' => 'POST',
					'args' => [
						'order' => ['required' => true]
					]
				],
				
				//Move AWB to Label
				'move_awb' => [
					'method' => 'POST',
					'args' => [
						'order' => ['required' => true]
					]
				],
				//Update AWB
				'update_awb' => [
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				'avodash_customer_info_stats' => [
					'method' => 'POST',
					'args' => [
						'start_date' => ['required' => true],
						'end_date' => ['required' => true]
					]
				],
				'avodash_customer_info_city_stats' => [
					'method' => 'POST',
					'args' => [
						'start_date' => ['required' => true],
						'end_date' => ['required' => true],
						'search' => ['required' => false],
					]
				],
				'generate_loyalty_otp' => [ // ::CRLOYALTY::
					'method' => 'POST',
					'args' => [
						'new_phone' => ['required' => true],
						'old_phone' => ['required' => true],
						'user' => ['required' => true],
					]
				],
				'validate_loyalty_otp' => [ // ::CRLOYALTY::
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true]
					]
				],
				'get_best_product' => [ // ::CRFRIDAY::
					'method' => 'POST',
					'args' => [
						'product' => ['required' => true],
						'page' => ['required' => true],
						'total' => ['required' => true],
					]
				],
				// ::AVOMOB::
				'get_user_point' => [
					'method' => 'POST',
					'args' => [
						'user_id' => ['required' => true]
					]
				],
				'get_user_point_history' => [
					'method' => 'POST',
					'args' => [
						'user_id' => ['required' => true]
					]
				],
				
				
				
				'wfc_redeem' => [ // ::CRWFC::
					'method' => 'POST',
					'args' => [
						'data' => ['required' => true],
						'user' => ['required' => true]
					]
				]
				
                        ];
                        
                        foreach ($route as $r => $v) {
                                register_rest_route($namespace, '/' . $r . '/', [
                                        'methods' => $v['method'],
                                        'callback' => [$this, $r],
                                        'args' => $v['args']
                                ]);
                        }
                }
                
                public function user_login($request){
                        $params = $request->get_params();
                        parse_str(wp_strip_all_tags($params['data']), $data);
			// ::PENTEST
			$validate = wp_remote_post('https://www.google.com/recaptcha/api/siteverify', [
				'body' => [
					'secret' => '6Le9A80bAAAAAGTgKhcYxby8HofM1XopA5BjbMjU',
					'response' => $data['g-recaptcha-response']
				]
			]);
			if($validate['response']['code'] == 200){
				$validate = json_decode(wp_remote_retrieve_body($validate), true);
				if($validate['success'] == true){
					$has_login = [
						'attemp' => 0,
						'expired' => 0
					];
					$minutes = 0;
					if ( false === ( $has_login = get_transient( 'has_login'. $data['token']) )) {
						$has_login = [
							'attemp' => 1,
							'expired' => current_time('timestamp') + (10 * 60)
						];	
						set_transient( 'has_login'.$data['token'], $has_login, 10 * MINUTE_IN_SECONDS );
					}else{
						delete_transient( 'has_login'.base64_encode($data['token']));
						$has_login['attemp'] = (int)$has_login['attemp'] + 1;
						$minutes = ((int)$has_login['expired'] - (int)current_time('timestamp')) / 60 ;
						set_transient( 'has_login'.$data['token'], $has_login, absint($minutes) * MINUTE_IN_SECONDS );
					}
					
					if((int)$has_login['attemp'] <= 3){
						$login = $this->member->user_login($data, $this->results);
						if($login['status'] == 'ok'){
							delete_transient( 'has_login'.$data['token']);
							$validate = avoskin_validate_wfc_after_login_register($login['user_id']); // ::CRWFC::
							if($validate == 'checkout'){
								$login['redirect'] = esc_url( wc_get_checkout_url() );
							}
						}
						return $login;
					}else{
						return [
							'title' => __('Info', 'avoskin'),
							'msg' => __('Please wait after ', 'avoskin') . absint($minutes) . __(' minutes to make another login request.', 'avoskin')
						];
					}
				}else{
					return [
						'title' => __('Error', 'avoskin'),
						'msg' => __('Invalid reCaptcha validation.', 'avoskin'),
					];
				}
			}else{
				return [
					'title' => __('Error', 'avoskin'),
					'msg' => __('Something went wrong when validate reCaptcha, please try again later.', 'avoskin'),
				];
			}
                }
		
		
		private function validate_register_data($data){// ::PENTEST::
			$result = [
				'valid' => true,	  
			];
			foreach($data as $k => $v){
				if($k == 'first_name'){
					if(!preg_match("#^[a-zA-Z ]+$#", $v)){
						return [
							'valid' => false,
							'msg' =>'Invalid full name character!'
						];
					}
				}elseif($k == 'loyalty_phone'){
					if(strlen($v) >= 8 && strlen($v) <= 14){
						if(!preg_match("#^[0-9]+$#", $v)){
							return [
								'valid' => false,
								'msg' =>'Invalid phone number character!'
							];
						}	
					}else{
						return [
							'valid' => false,
							'msg' =>'Phone number minimum 8 digit and maximum 14 digit!'
						];
					}
					
				}elseif($k == 'user_email'){
					if(!is_email($v)){
						return [
							'valid' => false,
							'msg' =>'Invalid email address!'
						];
					}
				}elseif($k == 'user_password'){
					if(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $v)){
						return [
							'valid' => false,
							'msg' =>'Password minimum 8 characters, at least one uppercase letter, one lowercase letter and one number!'
						];
					}
				}
			}
			
			return $result;
		}
		
		public function user_register($request){
                        $params = $request->get_params();
                        parse_str(wp_strip_all_tags($params['data']), $data);
			
			// ::PENTEST::
			$data['user_email'] = strtolower($data['user_email']);
			$validate = $this->validate_register_data($data);
			if(!$validate['valid']){
				return [
					'status' => 'fail',
					'title' => 'Error',
					'msg' => $validate['msg']
				];
			}
			
			// ::CRLOYALTY::
			if($this->validate_phone($data['loyalty_phone'])){
				if($this->loyalty->check_phone_avail($data['loyalty_phone'])){
					$results = $this->member->user_register($data, $this->results);
					if($results['status'] == 'ok'){
						$this->user_register_email($results);
						$this->loyalty->update_user_info($results['data']);
						unset($results['data_user']);
						unset($results['data']);
					}
				}else{
					$results['title'] = 'Error';
					$results['msg'] = __('Phone number already used!', 'avoskin');
				}	
			}else{
				$results['title'] = 'Error';
				$results['msg'] = __('Invalid phone number!', 'avoskin');
			}
			
                        return $results;
                }
		
		private function user_register_email($data){
			WC()->mailer()->emails['WC_Customer_Register_New_Account']->trigger((int)$data['data_user']['id'], $data['data_user']['password']);
		}
		
		public function user_forgot_password($request){ // ::PENTEST::
			$params = $request->get_params();
                        parse_str(wp_strip_all_tags($params['data']), $data);
			//delete_transient('reset_password_'.base64_encode($data['email']));
			$has_reset = 0;
			if ( false === ( $has_reset = get_transient( 'reset_password_'.base64_encode($data['email'])) )) {
				$results = $this->member->user_forgot_password($data, $this->results);
				if($results['status'] == 'ok'){
					WC()->mailer()->emails['WC_Customer_Reset_Password']->trigger($results['login'], $results['reset_key']);
					unset($results['reset_key']);
					unset($results['login']);
					set_transient( 'reset_password_'.base64_encode($data['email']), current_time('timestamp') + (10 * 60), 10 * MINUTE_IN_SECONDS );
				}	
			}else{
				$limit = ((int)$has_reset - (int)current_time('timestamp')) / 60 ;
				$results['title'] = __('Info', 'avoskin');
				$results['msg'] = __('Please wait after ', 'avoskin') . absint($limit) . __(' minutes to make another reset password request.', 'avoskin');
			}
			
			return $results;
		}
		
		
		public function add_to_cart($request){
			$params = $request->get_params();
			$id = $params['id'];
			$quantity = intval($params['quantity']);
			$cart = WC()->cart;
			$limit = avoskin_check_product_limit($cart, $id, $quantity);
			if($limit == 'nope'){
				if($cart->add_to_cart( $id, $quantity )){
					return [
						'status' => 'ok',
						'count' => $cart->get_cart_contents_count(),
						'cart' => avoskin_request('fetch_cart_items'),
						'title' => __('Info', 'avoskin'),
						'msg' => __('Product added to bag', 'avoskin')
					];	
				}else{
					return ['status' => 'fail'];
				}	
			}else{
				return [
					'status' => 'ok',
					'count' => $cart->get_cart_contents_count(),
					'cart' => avoskin_request('fetch_cart_items'),
					'title' => __('Info', 'avoskin'),
					'msg' => __('Maximum ','avoskin') .$limit. __(' product', 'avoskin') 
				];	
			}
		}
		
		public function add_to_cart_wfc($request){ // ::CRWFC::
			$params = $request->get_params();
			$id = $params['id'];
			$code = $params['code'];
			$cart = WC()->cart;			
			$content = $cart->get_cart();
			$coupon = WC()->cart->get_applied_coupons();
			$coupon = (is_array($coupon)) ? $coupon : [];
			
			if(in_array($code, $coupon)){
				if(is_array($content) && !empty($content)){
					foreach($content as $key => $value){
						if(isset($value['wfc_code'])){
							if($code == $value['wfc_code'] ){
								if($value['product_id'] == $id){
									return [
										'status' => 'ok',
										'count' => $cart->get_cart_contents_count(),
										'cart' => avoskin_request('fetch_cart_items'),
										'title' => __('Info', 'avoskin'),
										'msg' => __('Product already in bag!', 'avoskin') 
									];
								}else{
									$cart->remove_cart_item($key);
								}	
							}else{
								$cart->remove_cart_item($key);
							}
						}
					}
				}	
			}else{
				return [
					'status' => 'ok',
					'count' => $cart->get_cart_contents_count(),
					'cart' => avoskin_request('fetch_cart_items'),
					'title' => __('Info', 'avoskin'),
					'msg' => __('Invalid coupon code applied. Please reload the page!', 'avoskin') 
				];
			}
			
			
			if($cart->add_to_cart( $id, 1, 0, [] , ['wfc_code' => $params['code']] )){
				avoskin_remove_wfc_product();
				return [
					'status' => 'ok',
					'count' => $cart->get_cart_contents_count(),
					'cart' => avoskin_request('fetch_cart_items'),
					'title' => __('Info', 'avoskin'),
					'msg' => __('Product added to bag', 'avoskin')
				];	
			}else{
				return ['status' => 'fail'];
			}
		}
		
		public function advisor_add_to_cart($request){
			$params = $request->get_params();
			$data = $params['data'];
			$data = json_decode(base64_decode($data), true);
			$valid = false;
			if(isset($data['product']) && !empty($data['product'])){
				foreach($data['product'] as $p){
					if(WC()->cart->add_to_cart( (int)$p['id'], (int)$p['qty'] )){
						$valid = true;
					}
				}
			}
			
			if($valid){
				$name = explode(' ', $data['name']);
				return add_query_arg([
					'fname' => (isset($name[0])) ? $name[0] : '',
					'lname' => (isset($name[1])) ? $name[1] : '',
					'avmail' => $data['email'],
					'advisor' => $this->record_advisor_statistic($data)
				], wc_get_checkout_url());
			}
			return home_url('/');
		}
		
		private function record_advisor_statistic($data){
			$query = [
				'post_title' => $data['name'] . ' - ' . date('Y-m-d H:i:s', current_time('timestamp')),
				'post_type' => 'advisor-statistic',
				'post_status' => 'publish'
			];
			
			$post_id = wp_insert_post($query);
			if($post_id) {
				return $post_id;
			}
			return 0;
		}
		
		public function remove_cart_item($request){
			$params = $request->get_params();
			$product_id = $params['id'];
			$product_cart_id = WC()->cart->generate_cart_id( $product_id );
			$cart_item_key = WC()->cart->find_product_in_cart( $product_cart_id );
			if ( $cart_item_key != '' ){
				WC()->cart->remove_cart_item( $cart_item_key );
			}else{
				$cart = WC()->cart->get_cart();
				$coupon = WC()->cart->get_applied_coupons();
				$coupon = (is_array($coupon)) ? $coupon : [];
				if(is_array($cart) && !empty($cart)){
					foreach($cart as $key => $value){
						$cart_item_key = 'test';
						if(isset($value['wfc_code']) && $value['wfc_code'] != '' && in_array($value['wfc_code'], $coupon)){
							WC()->cart->remove_cart_item($key);
						}
					}
				}
			}
			
			return [
				'subtotal' => WC()->cart->get_cart_total(),
				'count' => WC()->cart->get_cart_contents_count()
			];
		}
		
		public function fetch_cart_items(){ // ::CRWFC::
			$items = WC()->cart->get_cart();
			ob_start();?>
			<?php if(is_array($items) && !empty($items)):?>
				<h3><?php _e('Product is Added to Your Bag','avoskin');?> <a href="#"></a></h3>
				<div class="wrap">
					<?php foreach($items as $item => $values):
						$product =  wc_get_product( $values['data']->get_id() );
						$price = $product->get_regular_price();
						$sale = $product->get_sale_price();
						if(!$product->is_on_sale()){
							$sale = 0;
						}
					?>
						<?php if(isset($values['wfc_code']) && $values['wfc_code'] != ''):?>
							<div class="item">
								<a href="#" class="rmv" data-id="<?php echo $product->get_id();?>"></a>
								<figure>
									<?php echo $product->get_image([83,83]) ;?>
								</figure><!--
								--><div class="caption">
									<h4><?php echo $product->get_name() ;?></h4>
									<span><?php echo $values['quantity'] ;?> x <b><?php echo ($sale > 0) ? wc_price($sale) : wc_price($price) ;?></b></span>
									<a href="<?php echo  get_permalink(get_theme_mod('avoskin_wfc_redeem_page'));?>" style="display: block;margin: 5px 0 0;color: #89a284;text-decoration: underline;"><?php _e('Pilih Gift Lain','avoskin');?></a>
								</div><!-- end of caption -->
							</div><!-- end of item -->
						<?php else:?>
							<div class="item">
								<a href="#" class="rmv" data-id="<?php echo $product->get_id();?>"></a>
								<figure>
									<a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_image([83,83]) ;?></a>
								</figure><!--
								--><div class="caption">
									<h4><a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a></h4>
									<span><?php echo $values['quantity'] ;?> x <b><?php echo ($sale > 0) ? wc_price($sale) : wc_price($price) ;?></b></span>
								</div><!-- end of caption -->
							</div><!-- end of item -->
						<?php endif;?>
						
					<?php endforeach;?>
				</div><!-- end of wrap -->
				<div class="info">
					<div class="subtotal">
						<span><?php _e('Subtotal','avoskin');?></span>
						<strong><?php echo WC()->cart->get_cart_total() ;?></strong>
						<div class="clear"></div>
					</div><!-- end of subtotal -->
					<div class="action">
						<a href="<?php echo wc_get_cart_url();?>" class="button btn-fullwidth btn-hollow"><?php _e('View Cart','avoskin');?></a>
						<a href="<?php echo wc_get_checkout_url();?>" class="button btn-fullwidth"><?php _e('Checkout','avoskin');?></a>
					</div><!-- end of action -->
				</div><!-- end of action -->
				<?php else:?>
					<h3><?php _e('Your Bag is Empty!','avoskin');?> <a href="#"></a></h3>
				<?php endif;?>
			<?php
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		
		public function submit_review($request){
			$params = $request->get_params();
                        parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			$comment_id = wp_insert_comment([
				'comment_post_ID'      => (int)$data['product_id'], 
				'comment_author'       => $data['name'],
				'comment_author_email' => $data['email'],
				'comment_content'      => $data['review_text'],
				'user_id'              => (int)$params['user'],
				'comment_approved'     => 0,
				'comment_meta' => [
					'rating' => (int)$data['rating'],
					'skin_type' => $data['skin_type'],
					'use_duration' => $data['use_duration'],
					'package_quality' => $data['package_quality'],
					'product_price' => $data['product_price'],
					'recommend' => $data['recommend'],
					'repurchase' => $data['repurchase']
				]
			]);
			
			if($comment_id){				
				$results = [
					'status' => 'ok',
					'title' => __('Success', 'avoskin'),
					'msg' => __('Your review successfully submitted.', 'avoskin')
				];
				
				//$commentarr = [
				//	'comment_ID' => $comment_id,
				//	'comment_approved' => 1
				//];
				//if(wp_update_comment( $commentarr ) == 1){
				//	$results = [
				//		'status' => 'ok',
				//		'title' => __('Success', 'avoskin'),
				//		'msg' => __('Your review successfully submitted.', 'avoskin')
				//	];	
				//}
			}
			
			return $results;
		}
		
		public function update_cart_items($request){
			$params = $request->get_params();
			$results = $this->results;
			$data = $params['data'];
			$current_cart =  WC()->cart->get_cart_item_quantities();
			$msg = [];
			if(is_array($data) && !empty($data)){
				WC()->cart->empty_cart();
				$fail = false;
				foreach($data as $d){
					$limit = avoskin_check_cart_limit($d);
					if($limit == 'nope'){
						if(WC()->cart->add_to_cart( (int)$d['product'], (int)$d['quantity'] )){}else{
							WC()->cart->add_to_cart( $d['product'], $current_cart[$d['product']] );
							$fail = true;
						}	
					}else{
						WC()->cart->add_to_cart( $d['product'], $current_cart[$d['product']] );
						$msg[] = $limit;
					}
				}

				if(!empty($msg)){
					$limit_msg = '';
					foreach($msg as $m){
						$limit_msg .= $m['title'] . ' maximum ' . $m['limit'] . ' item <br/>';
					}
					$results = [
						'status' => 'ok',
						'title' =>__('Notice', 'avoskin') ,
						'msg' =>  $limit_msg
					];	
				}else{
					$results = [
						'status' => 'ok',
						'title' => ($fail) ? __('Notice', 'avoskin') : __('Info', 'avoskin'),
						'msg' =>  ($fail) ? __('Some product are not updated because the stock is not available.', 'avoskin') : __('Cart item successfully updated!', 'avoskin')
					];	
				}
				
			}
			return $results;
		}
		
		public function get_states($request){
			$params = $request->get_params();
			$states =  avoskin_get_country_states($params['country']);
			$current = (isset($params['current'])) ? $params['current'] : '';
			ob_start();?>
				<option value="" <?php echo ($current == '') ? 'selected="selected"': '';?>><?php _e('Choose Province','avoskin');?></option>
				<?php if(is_array($states) && !empty($states)) :
				foreach($states as $s => $v):?>
					<option value="<?php echo $s;?>" <?php selected($s, $current);?>><?php echo $v;?></option>
				<?php endforeach;
				endif;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		
		public function get_cities($request){
			$params = $request->get_params();
			$cities =  avoskin_get_states_cities($params['state']);
			$current = (isset($params['current'])) ? $params['current'] : '';
			ob_start();?>
				<option value="" <?php echo ($current == '') ? 'selected="selected"': '';?>><?php _e('Choose City','avoskin');?></option>
				<?php if(is_array($cities) && !empty($cities)) :
				foreach($cities as $c => $v):?>
					<option value="<?php echo $c;?>" <?php selected($c, $current);?>><?php echo $v;?></option>
				<?php endforeach;
				endif;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		
		public function get_couriers($request){
			$params = $request->get_params();
			$data = $this->validate_payload($params['data']);
			if($data['status']){
				$args = [
					'destination' => $data['data']['destination'],
					'weight' => $data['data']['weight']
				];
				return avoskin_display_couriers($args);	
			}else{
				return false;
			}
		}
		
		
		private function validate_combine_coupon($id,$data){
			$results = [
				'coupon' => 'ok',
				'msg' =>''
			];
			
			if(get_post_meta($id, 'combine_validation', true) == 'yes'){
				$raw_courier =  get_post_meta($id, 'combine_courier', true);
				if(is_array($raw_courier) && !empty($raw_courier)){
					$results['coupon'] = 'fail';
					$courier = [];
					
					$choosed_courier = strtolower(substr(wp_strip_all_tags($data['shipping_name']) , 0, 3));
					$choosed_courier = ($choosed_courier != 'sic') ? $choosed_courier : 'sicepat';
					$courier_service = avoskin_get_string_between( wp_strip_all_tags($data['shipping_name']));
					
					foreach($raw_courier as $rw){
						$parsed = base64_decode($rw);
						if($parsed == strtolower($choosed_courier.'_'.$courier_service)){
							$results['coupon'] = 'ok';
						}
						$courier[] = $parsed;
					}
					
					if($results['coupon'] != 'ok' && !empty($courier)){
						$results['msg'] = 'Coupon only eligible for this courier: ';
						$n=1;
						foreach($courier as $c){
							$results['msg'] .= ($n < count($courier)) ? strtoupper(str_replace('_', ' ', $c)) .', ' : strtoupper(str_replace('_', ' ', $c));
							$n++;	
						}
					}
				}
				
				if($results['coupon'] == 'ok'){
					$payment = get_post_meta($id, 'combine_payment', true);
					if(is_array($payment) && !empty($payment)){
						$results['coupon'] = 'fail';
						$choosed_payment = json_decode(wp_unslash($data['payment']),true);
						foreach($payment as $p){
							if($p == $choosed_payment['name']){
								$results['coupon'] = 'ok';
							}
						}
						if($results['coupon'] != 'ok' ){
							$results['msg'] = ($results['msg'] != '') ? $results['msg'] . ' and this payment method: ' : 'Coupon only eligible for this payment method: ';
							$n=1;
							$template_payment = [
								'bca_va' =>'Bank BCA',
								'echannel' => 'Bank Mandiri',
								'bni_va' => 'Bank BNI',
								'bri_va' => 'Bank BRI',
								'permata_va' =>'Bank Permata',
								'credit_card' => 'Credit Card',
								'gopay' => 'GoPay/QRIS',
								'alfamart' => 'Alfamart/Alfamidi/Dan+Dan',
								'akulaku' => 'Akulaku',
								'shopeepay' => 'ShopeePay'
							];
							foreach($payment as $p){
								$results['msg'] .= ($n < count($payment)) ? $template_payment[$p] .', ' : $template_payment[$p];
								$n++;
							}
						}
					}	
				}
			}
			
			return $results;
		}
		
		private function product_cart_summary(){
			$items = WC()->cart->get_cart();
			$weight = 0;
			ob_start();
			if(is_array($items) && !empty($items)):?>
				<h2><?php _e('Your Order','avoskin');?></h2>
				<table>
					<?php foreach($items as $item => $values):
						$product =  wc_get_product( $values['data']->get_id() );
						$is_gift = (isset($values['free_gift'])) ? true : false;
						$price = $product->get_regular_price();
						$sale = $product->get_sale_price();
						if(!$product->is_on_sale()){
							$sale = 0;
						}
						$quantity = $values['quantity'];
						
						// ::CRWFC::
						if(
							count($items) > 1
							&& isset($values['wfc_code'])
							&& $values['wfc_code'] != ''
							&& get_post_meta(wc_get_coupon_id_by_code($values['wfc_code']), 'wfc_freeship', true) == 'yes'
						){
							$weight += 0;
						}else{
							$weight += (int)$product->get_weight() * $quantity;	
						}
						
						$total =  ($sale > 0) ? (float)$sale * $quantity : (float)$price * $quantity;
						$product_data = [
							'id' => $product->get_id(),
							'quantity' => $quantity,
							'stupid' => ($is_gift) ? 'not_really' : 'nope' //It should be 'gift' but just a decoy so we call it 'stupid'
						];
					?>
						<tr>
							<td><?php echo $product->get_name() ;?> <strong>x<?php echo $quantity ;?></strong></td>
							<td>
								<?php echo ($is_gift) ? __('Free!','avoskin') : wc_price($total);?>
								<input type="hidden" name="product_data[]" value='<?php echo base64_encode(wp_json_encode($product_data));?>' />
							</td>
						</tr>
					<?php endforeach;?>
				</table>
				<input id="product-weight" type="hidden" name="product_weight" value="<?php echo $weight ;?>" />
			<?php endif;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		
		private function checkout_payment_method($total){ // ::CRWFC::
			ob_start();
			$type = 'free';
			if((int)$total > 0){
				echo avoskin_payment_method_html();
				$type = 'pay';
			}else{
				$type = 'free';
				$method = ['name' => 'wfc_payment','title' => 'Payment Free'];?>
					<input id="free-wfc" type="hidden" name="payment" value='<?php echo wp_json_encode($method);?>' />
				<?php
			}
			$content = ob_get_contents();
			ob_end_clean();
			return [
				'type' => $type,
				'content' => $content
			];
		}
		
		public function generate_checkout_summary($request){ 
			$params = $request->get_params();
			$data = $this->validate_payload($params['data']); // ::PENTEST::
			if($data['status']){
				$data = $data['data'];
			}else{
				return false;
			}
			
			$results = ['coupon' => ''];			
			$is_freeshipping = false;
			$is_discount_shipping = false;
			if($data['hasCoupon'] != ''){
				$hasCoupon = json_decode($data['hasCoupon'], true);
				foreach($hasCoupon as $hc){
					$usedCoupon = new \WC_Coupon( $hc );
					$validate = $this->validate_combine_coupon($usedCoupon->get_id(),$data);
					if($validate['coupon'] == 'ok'){
						if($usedCoupon->get_discount_type() == 'avo_shipping'){
							$is_discount_shipping = $usedCoupon->get_amount();
						}else{
							$is_freeshipping = $usedCoupon->get_free_shipping();	
						}	
					}else{
						WC()->cart->remove_coupon( $hc );
						WC()->cart->calculate_totals();
						$results['coupon'] = 'removed';
					}	
				}
			}
			
			// ::CRWFC::
			$is_wfc = false;
			if(isset($data['coupon']) && $data['coupon'] != ''){
				if(!isset($data['removeCoupon']) || isset($data['removeCoupon']) && $data['removeCoupon'] != 'yes'){
					$coupon = new \WC_Coupon( $data['coupon'] );
					if($coupon->get_discount_type() == 'avo_wfc'){
						$is_wfc = true;
					}	
				}
			}
			
			
			if(isset($data['removeCoupon']) && $data['removeCoupon'] == 'yes'){
				WC()->cart->remove_coupon( $data['coupon'] );
				WC()->cart->calculate_totals();
				$results['coupon'] = 'ok';
				$results['msg'] = __('Coupon successfully removed','avoskin');
				$results['title'] = __('Info','avoskin');
			}elseif(isset($data['coupon']) && $data['coupon'] != '' && !$is_freeshipping && !$is_wfc ){ // ::CRWFC::
				$current_cart_total = WC()->cart->get_cart_contents_count();
				$coupon = new \WC_Coupon( $data['coupon'] );
				$results = $this->validate_combine_coupon($coupon->get_id(),$data);
				$is_eligible = ($results['coupon'] == 'ok') ? true : false;
				$user = $data['user'];
				
				$limit = $coupon->get_usage_limit_per_user();
				if( absint($limit) > 0){
					$is_eligible = false;
					$used = $coupon->get_used_by();
					if(is_array($used) && !empty($used)){
						$user_has = 0;
						foreach($used as $u){
							if($u == $user){
								$user_has++;
							}
						}
						if($user_has < $limit){
							$is_eligible = true;	
						}
					}else{
						$is_eligible = true;
					}
				}
				
				$emails = $coupon->get_email_restrictions();
				if(!empty($emails)){
					$is_eligible = false;
					if($user != 'nope'){
						$user = get_user_by('id', (int)$user);
						if(in_array($user->data->user_email, $emails)){
							$is_eligible = true;
						}
					}
				}
				
				if($is_eligible){
					if(WC()->cart->apply_coupon($data['coupon'])){
						$after_cart_total = WC()->cart->get_cart_contents_count();
						$results['coupon'] = 'ok';
						$results['msg'] = __('Coupon successfully applied','avoskin');
						$results['title'] = __('Info','avoskin');
						$results['is_reload'] = ((int)$after_cart_total > (int)$current_cart_total) ? 'yep' : 'nope';
						$is_freeshipping = $coupon->get_free_shipping();
						if($coupon->get_discount_type() == 'avo_shipping'){
							$is_discount_shipping = $coupon->get_amount();
						}
					}else{
						$has_individual = '';
						$applied_coupons = WC()->cart->get_applied_coupons();
						if(is_array($applied_coupons) && !empty($applied_coupons)){
							foreach($applied_coupons as $ac){
								$ac_coupon = new \WC_Coupon( $ac);
								if($ac_coupon->get_individual_use()){
									$has_individual = $ac;
								}
							}
						}
						
						if($has_individual != ''){
							$results['msg'] = "remove voucher ".strtoupper($has_individual)." for use this voucher.";
						}else{
							$results['msg'] = ($results['msg'] != '') ? $results['msg'] :  __('Invalid coupon code','avoskin');
						}
						$results['coupon'] = 'fail';
						$results['title'] = __('Info','avoskin');	
						
					}	
				}else{
					$results['coupon'] = 'fail';
					$results['msg'] = ($results['msg'] != '') ? $results['msg'] : __('You\'re not eligible for this coupon code','avoskin');
					$results['title'] = __('Info','avoskin');
				}
			}
			
			// ::CRWFC::
			if($is_wfc){
				$results['coupon'] = 'fail';
				$results['msg'] = __('This coupon only applicable at Waste for Changes page. <a href="'. get_permalink(get_theme_mod('avoskin_wfc_page')).'" style="color:#8aa385;text-decoration:underline">Click here</a> to go to that page.', 'avoskin');
				$results['title'] = __('Info','avoskin');
			}
			if(avoskin_is_wfc_freeship()){
				WC()->cart->set_shipping_total(0);
			}else{
				$auto_coupon = $this->get_auto_coupon($data);
				if($auto_coupon['shipping'] != '' && $is_freeshipping == false){
					$coupon = new \WC_Coupon( $auto_coupon['shipping'] );
					if(WC()->cart->apply_coupon($auto_coupon['shipping'])){
						$is_discount_shipping = ($is_discount_shipping != false) ?  $coupon->get_amount() + $is_discount_shipping : $coupon->get_amount() ;
					}
				}
				
				if(isset($data['shipping']) && $data['shipping'] != '' && $is_freeshipping == false){
					if($is_discount_shipping != false){
						$current_shipping = ((int)$is_discount_shipping >= (int)$data['shipping']) ? 0 : (int)$data['shipping'] - (int)$is_discount_shipping;
						WC()->cart->set_shipping_total($current_shipping);
					}else{
						WC()->cart->set_shipping_total($data['shipping']);	
					}
					
				}elseif($is_freeshipping){
					WC()->cart->set_shipping_total(0);
				}
			}
			
			
			// ::CRLOYALTY::
			$point = 0;
			$user_point = 0;
			$point_overlap = 'no';
			if(isset($data['point']) && $data['point'] != '' && $data['removePoint'] == 'no' && isset($data['grandTotal']) && $data['grandTotal'] != '' ){
				$point = (int)$data['point'];
				if($point <= ((float)$data['grandTotal'] / 2)){
					WC()->cart->add_fee( 'Loyalty Point', -$point );	
				}else{
					$point = 0;
				}
			}
			if($data['user'] != 'nope'){
				$user_point = $this->loyalty->get_user_point((int)$data['user']);
				if((int)$point > (int)$user_point){
					$point = 0;
					$point_overlap = 'yes';
				}
			}
			
			WC()->cart->add_fee( 'Loyalty Point', $point );
			
			
			$tables = WC()->cart->get_totals();
			$total = $tables['subtotal'];
			
			ob_start();?>
				<tr>
					<td><?php _e('Subtotal','avoskin');?></td>
					<td>
						<?php wc_cart_totals_subtotal_html(); ?>
						<input type="hidden" name="subtotal" value="<?php echo $total ;?>"/>
					</td>
				</tr>
				<?php if(isset($tables['discount_total']) && $tables['discount_total'] != 0):
					$total = (int)$total - (int)$tables['discount_total'];
				?>
					<tr>
						<td><?php _e('Voucher/Coupon','avoskin');?></td>
						<td>
							<i>- <?php echo strip_tags(wc_price($tables['discount_total'])) ;?></i>
							<input type="hidden" name="discount" value="<?php echo $tables['discount_total'] ;?>"/>
						</td>
					</tr>
				<?php endif;?>
				<?php if($point != 0): // ::CRLOYALTY::
					$total = (int)$total - (int)$point;
				?>
					<tr>
						<td><?php _e('Loyalty Point','avoskin');?></td>
						<td>
							<i>- <?php echo strip_tags(wc_price($point)) ;?></i>
							<input type="hidden" name="loyalty_point_final" value="<?php echo $point ;?>"/>
						</td>
					</tr>
				<?php endif;?>
				<?php if(isset($tables['shipping_total'])):
					$total = (int)$total + (int)$tables['shipping_total'];
					$courier = strtolower(substr(wp_strip_all_tags($data['shipping_name']) , 0, 3));
					$courier = ($courier != 'sic') ? $courier : 'sicepat';
				?>
					<tr>
						<td><?php _e('Shipping','avoskin');?></td>
						<td>
							<?php echo strip_tags(wc_price($tables['shipping_total'])) ;?>
							<?php if($is_discount_shipping != false):?>
								<small class="ship-disc">voucher: -<?php echo strip_tags(wc_price($is_discount_shipping)) ?></small>
							<?php endif;?>
							<input type="hidden" name="shipping" value="<?php echo $tables['shipping_total'] ;?>" />
							<input type="hidden" name="shipping_name" value="<?php echo wp_strip_all_tags($data['shipping_name']) ;?>" />
							<input type="hidden" name="shipping_courier" value="<?php echo $courier;?>" />
							<input type="hidden" name="shipping_service" value="<?php echo avoskin_get_string_between( wp_strip_all_tags($data['shipping_name']));?>" />
						</td>
					</tr>
				<?php endif;?>
				<?php
					if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() && WC()->cart->get_total_tax() > 0 ) {
						$tax_classes = array_merge([''], \WC_Tax::get_tax_classes());
						$tax_rates = \WC_Tax::get_rates($tax_class);
						$tax_rates = array_values($tax_rates);
						$shipping_tax = (isset($tables['shipping_total']) && $tables['shipping_total'] != 0) ? ((float)$tax_rates[0]['rate'] / 100 ) * (int)$tables['shipping_total'] : 0;
						$tax = WC()->cart->get_total_tax() + $shipping_tax;
						$total += $tax;
						?>
						<tr class="tax-total">
							<td><?php echo esc_html( WC()->countries->tax_or_vat() );?></td>
							<td data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>">
								<?php echo wc_price($tax) ;?>
								<input type="hidden" name="tax" value="<?php echo $tax ;?>" />
							</td>
						</tr>
						<?php
					}/*else{ ?>
						<tr class="tax-total">
							<td><?php echo esc_html( WC()->countries->tax_or_vat() );?></td>
							<td data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>">
								<?php echo wc_price('0') ;?>
								<input type="hidden" name="tax" value="0" />
							</td>
						</tr>
					<?php // }
				*/?>
				<tr>
					<td><?php _e('Grand Total','avoskin');?></td>
					<td>
						<strong><?php echo wc_price($total) ;?></strong>
						<input type="hidden" name="grand_total" value="<?php echo $total ;?>" />
					</td>
				</tr>
			<?php
			$results['content'] = ob_get_contents();
			ob_end_clean();
			
			// ::CRWFC::
			avoskin_remove_wfc_product();
			
			$results['products'] = $this->product_cart_summary();
			$results['paymentMethod'] = $this->checkout_payment_method($total); // ::CRWFC::
			$results['paymentMethod']['cart'] = avoskin_request('fetch_cart_items');
			$results['paymentMethod']['count'] = WC()->cart->get_cart_contents_count();
			$results['autoCoupon'] = $auto_coupon['content'];
			$results['appliedCoupon'] = $this->get_applied_coupon();
			$results['hasPoint'] = ($point != 0) ? 'yes' : 'no';
			$results['userPoint'] = $user_point;
			$results['pointOverlap'] = $point_overlap;
			
			return $results;
		}
		
		private function get_applied_coupon(){ 
			$coupons = WC()->cart->get_applied_coupons();
			ob_start();
			if(is_array($coupons) && !empty($coupons)):?>
				<h3><?php _e('Applied Coupon','avoskin');?></h3>
				<ul>
					<?php foreach($coupons as $c):
						$coupon = new \WC_Coupon( $c);
					?>
						<li class="type-<?php echo $coupon->get_discount_type() ;?>">
							<b><?php echo $c ;?></b>
							<?php if($coupon->get_discount_type() != 'avo_shipping'):?>
								<a href="#" data-coupon="<?php echo $c ;?>"><?php _e('Remove','avoskin');?></a>
							<?php endif;?>
						</li>
					<?php endforeach;?>
				</ul>
				<input type="hidden" name="coupon_code" value='<?php echo wp_json_encode($coupons) ;?>' />
				<br/>
			<?php endif;
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
		
		private function get_auto_coupon($data){ 
			$coupons = get_posts([
				'fields' => 'ids',
				'post_type' => 'shop_coupon',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'meta_query' => [
					[
						'key' => 'auto_apply',
						'value' => 'yes',
						'compare' => '='
					]
				]
			]);
			ob_start();
			if(is_array($coupons) && !empty($coupons)):
				$eligible = [];
				$shipping = '';
				$applied_coupons = WC()->cart->get_applied_coupons();
				$applied_coupons = (is_array($applied_coupons)) ? $applied_coupons : [];
				foreach($coupons as $c){
					if(!in_array(strtolower(get_the_title($c)), $applied_coupons)){
						$coupon = new \WC_Coupon( strtolower(get_the_title($c)) );
						$valid = $this->validate_combine_coupon($c,$data);
						$is_eligible = ($valid['coupon'] == 'ok') ? true : false;
						$user = $data['user'];
						
						$limit = $coupon->get_usage_limit_per_user();
						if( absint($limit) > 0){
							$is_eligible = false;
							$used = $coupon->get_used_by();
							if(is_array($used) && !empty($used)){
								$user_has = 0;
								foreach($used as $u){
									if($u == $user){
										$user_has++;
									}
								}
								if($user_has < $limit){
									$is_eligible = true;	
								}
							}else{
								$is_eligible = true;
							}
						}
						
						$emails = $coupon->get_email_restrictions();
						if(!empty($emails)){
							$is_eligible = false;
							if($user != 'nope'){
								$user = get_user_by('id', (int)$user);
								if(in_array($user->data->user_email, $emails)){
									$is_eligible = true;
								}
							}
						}
						
						if($is_eligible && $coupon->is_valid()){
							$eligible[] = $c;
							if($coupon->get_discount_type() == 'avo_shipping'){
								$shipping = get_the_title($c);
							}
						}	
					}
				}
				if(!empty($eligible)):
			?>
				<div class="auto-coupon">
					<h3><?php _e('You\'re eligible for this coupon:','Avoskin');?></h3>
					<ul>
						
						<?php foreach($eligible as $e):?>
							<?php if(get_the_title($e) != $shipping):?>
								<li><a href="#"><strong><?php echo get_the_title($e) ;?></strong><small>Use Coupon</small></a></li>
							<?php endif;?>
						<?php endforeach;?>
					</ul>
				</div><!-- end of autocoupon -->
			<?php
				else: echo '';
				endif;
			else: echo '';
			endif;
			$content = ob_get_contents();
			ob_end_clean();
			return [
				'content' => $content,
				'shipping' => $shipping
			];
		}
		
		public function check_user_exists($request){
			$params = $request->get_params();
			$results = $this->results;
			$email = $params['email'];
			
			// ::CRWFC::
			$code = avoskin_has_wfc_coupon();
			if(is_email($email) && $code != 'nope'){
				$coupon = new \WC_Coupon( $code );
				if ( $coupon->is_valid() ) {
					$restrictions = $coupon->get_email_restrictions();
					if ( is_array( $restrictions ) && !empty($restrictions) && !in_array($email, $restrictions)) {
						WC()->cart->remove_coupon( $code);
						$results['status'] = 'removed_coupon';
						$results['title'] = __('Error', 'avoskin');
						$results['msg'] = __('You\'re not eligible for '.strtoupper($code).' coupon code.', 'avoskin');
						return $results;
					}
				}
			}
			
			if( is_email($email) && null != email_exists( $email )  ) { 
				$results['status'] = 'ok';
				$results['title'] = __('Info', 'avoskin');
				$results['msg'] = __('You already have an account, please login first.', 'avoskin');	
			}
			
			return $results;
		}
		
		private function validate_postcode($state, $postcode){
			$fileurl =  get_parent_theme_file_path("inc/libs/postcode/".strtolower($state).".json");
			$args = [
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false,
				]
			];
			   
			$zip_list = json_decode(file_get_contents( $fileurl, false, stream_context_create($args) ), true);
			return (in_array($postcode, $zip_list)) ? true : false;
		}
		
		// ::PENTEST::
		private function validate_payload($data){
			$pk = get_option('avo_enc_private', '');
			$final = [];
			$valid = true;
			foreach($data as $d){
				if(openssl_private_decrypt(base64_decode($d['key']), $key, $pk)){
					if($key != 'product_data'){
						if(openssl_private_decrypt(base64_decode($d['val']), $val, $pk)){
							$final[$key] = $val;		
						}else{
							$valid = false;
						}
					}else{
						$product = json_decode(wp_unslash($d['val']), true)	;
						$final[$key] = $product;
					}
				}else{
					$valid = false;
				}	
			}
			return [
				'status' => $valid,
				'data' => $final
			];
		}
		
		
		public function submit_checkout($request){
			$params = $request->get_params();
			//parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			$user_id = '';
			
			// ::PENTEST::
			$data = $this->validate_payload($params['data']);
			if(!$data['status']){
				return  [
					'status' => 'fail',
					'result' => 'invalid_postcode',
					'title' => __('Error', 'avoskin'),
					'msg' => __('Invalid checkout data!', 'avoskin'),
				];
			}else{
				$data = $data['data'];
			}
			
			if(!$this->validate_address($data, [ 'channel', 'coupon_code', 'user', 'shipping_name', 'advisor', 'shipping_service', 'payment', 'email', 'product_data', 'dummy_ga'])){
				return  [
					'status' => 'fail',
					'result' => 'invalid_postcode',
					'title' => __('Error', 'avoskin'),
					'msg' => __('Invalid character inputted!', 'avoskin'),
					
				];
			};
			
			//::CRLOYALTY:: 
			if(!$this->validate_phone($data['phone'])){
				return  [
					'status' => 'fail',
					'result' => 'invalid_postcode',
					'title' => __('Error', 'avoskin'),
					'msg' => __('Invalid phone number.', 'avoskin')
				];
			}
			
			if($data['user'] == 'not_login' && !$this->loyalty->check_phone_avail($data['phone'])){
				return  [
					'status' => 'fail',
					'result' => 'invalid_postcode',
					'title' => __('Error', 'avoskin'),
					'msg' => __('Phone number already used!', 'avoskin')
				];
			}
			
			if($this->validate_order_data($data)){
				if(is_email($data['email']) && $this->validate_email_tld($data['email'])){ // ::CRLOYALTY::
					if($this->validate_postcode($data['state'], $data['postcode'])){
						//Check if user is logged in
						if($data['user'] != 'not_login'){
							$user_id = $this->verify_user($data['user']);
							if( $user_id !== false){
								// ::CRLOYALTY::
								if(isset($data['loyalty_point_final']) && $data['loyalty_point_final'] != ''){
									$applied_point = (int)$data['loyalty_point_final'];
									$user_point = $this->loyalty->get_user_point($user_id);
									if($applied_point > $user_point){
										return  [
											'status' => 'fail',
											'result' => 'invalid_postcode',
											'title' => __('Error', 'avoskin'),
											'msg' => __('You don\'t have enough point.', 'avoskin')
										];
									}
								}
								$results = $this->process_checkout($user_id, $data);
							}
						}else{
							//If not let's find out if user already registered
							if( null == username_exists( $data['email'] ) && null == email_exists( $data['email'] ) ) {
								$password = wp_generate_password( 12, false );
								$register = $this->member->user_register([
									'user_email' => $data['email'],
									'user_password' => $password,
									'first_name' => $data['first_name'],
									'last_name' => $data['last_name']
								], $this->results);
								
								if($register['status'] == 'ok'){
									//::CRLOYALTY:: 
									$this->loyalty->update_user_info($register['data']);
									unset($register['data']);
									
									$user_id = $register['data_user']['id'];
									$this->user_register_email($register);
									$login_user = $this->member->user_login([
										'user_login' => $register['data_user']['email'],
										'user_password' => $register['data_user']['password']
									], $this->results);
									
									if($login_user['status'] == 'ok'){
										$results = $this->process_checkout($user_id, $data);
										if($results['result'] == 'success'){
											$unset = ['notes','do_register','product_data','shipping','payment','coupon_code','subtotal','discount','tax','shipping_name','grand_total','do_agree','user', 'referral', 'visitor', 'shipping_courier', 'product_weight', 'shipping_service', 'channel'];
											foreach($unset as $u){
												if(isset($data[$u])) unset($data[$u]);
											}
											
											$address = ['billing_', 'shipping_'];
											foreach($address as $a){
												foreach($data as $d => $v){
													update_user_meta($user_id, $a.$d, $v);
												}
											}
										}
									}
								}
							}
						}
					}else{
						$results = [
							'result' => 'invalid_postcode',
							'title' => __('Error', 'avoskin'),
							'msg' => __('Invalid postal code number.', 'avoskin')
						];
					}	
				}else{
					$results = [
						'result' => 'invalid_postcode',
						'title' => __('Error', 'avoskin'),
						'msg' => __('Invalid email address.', 'avoskin')
					];
				}	
			}else{
				$results = [
					'result' => 'invalid_postcode',
					'title' => __('Error', 'avoskin'),
					'msg' => __('Invalid order data.', 'avoskin')
				];
			}
			
			return $results;
		}
		
		private function validate_order_data($data){
			$valid = true;
			if(is_array($data) && !empty($data)){
				foreach($data as $key => $d){
					if ($key != 'notes' && $key != 'tax' && $key != 'last_name' && $key != 'loyalty_point') { // ::CRLOYALTY::
						if ($d != '') {
							if ($key == 'shipping' || $key == 'grand_total' || $key == 'subtotal') {
								if($key != 'shipping'){
									if ((float)$d <= 0 && !avoskin_is_wfc_freeship() && !avoskin_has_wfc_coupon_applied()) { // ::CRWFC::
										$valid = false;
									}
								}else{
									if (
										(float)$d <= 0 && !isset($data['coupon_code'])
										|| (float)$d <= 0 && ($data['coupon_code'] == '') 
										|| (float)$d <= 0 && !avoskin_is_wfc_freeship() && !avoskin_has_wfc_coupon_applied()) // ::CRWFC::
									{
										$valid = false;
									}
								}
							}
						}else{
							$valid = false;
						}
					}
				}
			}else{
				$valid = false;
			}
			return $valid;
		}
		
		private function checkout_use_billing($address, $user_id){ // ::PENTEST:: ::CRWFC::
			foreach($address as $k => $v){
				if($k == 'email'){
					$user = get_user_by('id', $user_id);
					$address[$k] = $user->data->user_email;
					update_user_meta($user_id, 'billing_email', $user->data->user_email);
				}else{
					$current = get_user_meta($user_id, 'billing_'. $k, true);
					if($current != '') {
						$address[$k] = $current;
					}else{
						update_user_meta($user_id, $k, $address[$k]);
						update_user_meta($user_id, 'billing_'.$k, $address[$k]);
					}
					
					$current_shipping = get_user_meta($user_id, 'shipping_'. $k, true);
					if($current != '') {}else{
						update_user_meta($user_id, 'shipping_'.$k, $address[$k]);
					}
					
				}
			}
			
			return $address;
		}
		
		private function process_checkout($user_id,$data){
			$result = $this->results;
			$address = [
				'first_name' => ( isset($data['first_name'])) ? wp_strip_all_tags($data['first_name']) : '',
				'last_name' => ( isset($data['last_name'])) ? wp_strip_all_tags($data['last_name']) : '',
				'company'    => '',
				'email' => ( isset($data['email'])) ? strtolower(wp_strip_all_tags($data['email'])) : '', // ::PENTEST
				'phone' => ($data['phone'][0] == '0') ? preg_replace('/0/', '62', wp_strip_all_tags($data['phone']), 1) : wp_strip_all_tags($data['phone']), // ::CRLOYALTY::
				'address_1' => ( isset($data['address_1'])) ? wp_strip_all_tags($data['address_1'] ): '',
				'address_2'  => '',
				'city' => ( isset($data['city'])) ? wp_strip_all_tags($data['city'] ): '',
				'state' => ( isset($data['state'])) ? wp_strip_all_tags($data['state'] ): '',
				'postcode' => ( isset($data['postcode'])) ? wp_strip_all_tags($data['postcode'] ): '',
				'country' => ( isset($data['country'])) ? wp_strip_all_tags($data['country'] ): '',
			];
			$order = wc_create_order(['customer_id' => $user_id]);
			$product = $data['product_data'];
			if(is_array($product) && !empty($product)){
				foreach($product as $p){
					$product_array = json_decode(base64_decode($p), true);
					$the_product = wc_get_product( (int)$product_array['id']);
					$the_price = ($product_array['stupid'] == 'not_really') ? 0 : $the_product->get_price();
					$order->add_product( $the_product  , (int)$product_array['quantity'],
						[
							'total' => floatval($the_price) * intval($product_array['quantity']),
							'subtotal' => floatval($the_price) * intval($product_array['quantity'])
						]
					);
				}
				
				$order->set_address( $address, 'shipping' );
				
				//::PENTEST:: ::CRLOYALTY::
				$address = $this->checkout_use_billing($address, $user_id);
				$order->set_address( $address, 'billing' );
				
				
				update_post_meta( $order->get_id(), 'city_id', wp_strip_all_tags($data['city_id'] ) );
				update_post_meta( $order->get_id(), 'advisor', $data['advisor'] );
				
				
				if(isset($data['notes']) && $data['notes'] != ''){
					$order->set_customer_note( $data['notes'] );
				}
				
				if(isset($data['coupon_code']) && $data['coupon_code'] != ''){
					$coupons = json_decode($data['coupon_code'], true);
					foreach($coupons as $c){
						$coupon = new \WC_Coupon( $c);
						if($coupon->is_valid()){
							$order->apply_coupon($coupon);
						}	
					}
				}
				
				$shipping_rate = new \WC_Shipping_Rate( '', $data['shipping_name'], $data['shipping'], [], 'custom_shipping_method' ); 
				$order->add_shipping($shipping_rate);
				$order->calculate_totals();
				$order->update_meta_data( '_expedition', $data['shipping_name'] );
				
				$courier = ['jne','jnt', 'rpx','pos', 'sicepat'];
				$selected_courier = (!in_array($data['shipping_courier'], $courier)) ? 'jne' : $data['shipping_courier'];
				update_post_meta( $order->get_id(), 'shipping_courier', $selected_courier );
				update_post_meta( $order->get_id(), 'shipping_service', $data['shipping_service'] );
				update_post_meta( $order->get_id(), 'order_weight', $data['product_weight'] );
				if(get_option('avoskin_disable_sms') != 'yes'){
					update_post_meta( $order->get_id(), '_wc_twilio_sms_optin', 1 );	
				}
				
				
				if(isset($data['referral']) && $data['referral'] != ''){
					update_post_meta( $order->get_id(), 'referral', $data['referral'] );
					update_post_meta( $order->get_id(), 'visitor', $data['visitor'] );
				}
				
				if(isset($data['avosbrf']) && $data['avosbrf'] != ''){
					update_post_meta($order->get_id(), 'avosbrf', $data['avosbrf']);
				}
				
				if(isset($data['dummy_ga']) && $data['dummy_ga'] != '0'){
					list( $custom_version, $custom_domainDepth, $custom_cid1, $custom_cid2 ) = preg_split( '[\.]', $data['dummy_ga'], 4 );

					$custom_contents = array( 'version' => $custom_version, 'domainDepth' => $custom_domainDepth, 'cid' => $custom_cid1 . '.' . $custom_cid2 );
					update_post_meta($order->get_id(), '_wc_google_analytics_pro_identity', trim($custom_contents['cid']));
				}
				
				WC()->session->order_awaiting_payment = $order->get_id();
				
				$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
				$payment = json_decode($data['payment'], true);
				update_post_meta( $order->get_id(), '_payment_method', $payment['name'] );
				update_post_meta( $order->get_id(), '_wc_google_analytics_pro_placed', 'yes' );
				
				//Process loyalty point // ::CRLOYALTY::
				/* TODO
				 * - update meta for point usage if any 'loyalty_point'
				 * - update phone in case loyalty_phone is not avail 'loyalty_phone'
				 * - update phone verified 'loyalty_phone_verified'
				 * - create cron job if order status is pending for 12 hours then return the point
				 * */
				
				if(isset($data['loyalty_point_final']) && $data['loyalty_point_final'] != '' && get_user_meta($user_id, 'loyalty_phone_verified', true) == 'yep'){
					update_post_meta($order->get_id(), 'loyalty_point', $data['loyalty_point_final']);
					$this->loyalty->add_point_fee($order, (float)$data['loyalty_point_final']);
				}else{
					update_post_meta($order->get_id(), 'loyalty_point', 0);
				}
				
				if(get_user_meta($user_id, 'loyalty_phone', true) == '' ){
					update_user_meta($user_id, 'loyalty_phone', $address['phone']);
					update_user_meta($user_id, 'loyalty_phone_verified', 'nope');
				}
				
				//Schedule order expired and return point
				@as_schedule_single_action( strtotime('+12 hours'), 'avoskin_loyalty_order_cancelled', [
					'payload' => [ 'order' => $order->get_id() ]
				]);
				$this->loyalty->process_point($order);
				
				
				if($payment['name'] == 'midtrans'){
					$channel = json_decode($data['channel'], true);
					update_post_meta( $order->get_id(), '_payment_method_title', $channel['title'] );
					$process_data = [
						'id' => $order->get_id(),
						'channel' => $channel['name']
					];
					$result = $available_gateways[ $payment['name'] ]->process_payment( $process_data );
				}elseif($payment['name'] == 'wfc_payment'){ // ::CRWFC::
					update_post_meta( $order->get_id(), '_payment_method_title', $payment['title'] );
					$result['result'] = 'success';
				}else{
					update_post_meta( $order->get_id(), '_payment_method_title', $payment['title'] );
					$result = $available_gateways[ $payment['name'] ]->process_payment( $order->get_id() );
				}
				
				
				if ( $result['result'] == 'success' ) { 
					if($payment['name'] == 'wfc_payment'){  //::CRWFC::
						$order->update_status( 'wc-processing' );
						$result['redirect'] = $order->get_view_order_url();
					}else{
						$result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->get_id() );
						$channel = json_decode($data['channel'], true);
						$channel = $channel['name'];
						$skip_channel = [
							'akulaku',
							'credit_card',
							//'alfamart',
							'gopay',
							'shopeepay'
						];
						if($payment['name'] == 'midtrans' && !in_array($channel, $skip_channel)){
							$parsed_url = parse_url($result['redirect']);
							parse_str($parsed_url['query'], $parsed_data);
							if(avoskin_midtrans_charge($parsed_data['snap_token'], wp_strip_all_tags($data['email']), $channel, $order->get_id())){
								$order_received_url = wc_get_endpoint_url( 'order-received', $order->get_id(), wc_get_checkout_url() );
								$order_received_url = add_query_arg( 'key', $order->get_order_key(), $order_received_url );
								$result['redirect'] = $order_received_url;	
							}
						}	
					}
					
					do_action( 'avoskin_order_processed', $order->get_id() );
				}
			}
			
			return $result;
		}
		
		private function verify_user($user){
			$user_raw = json_decode(base64_decode($user), true);
			$user_data = get_userdata( $user_raw['id'] );
			return ($user_data !== false && $user_data->data->user_login == $user_raw['login'] ) ? $user_raw['id'] : false;
		}
		
		public function order_update_resi($post_id){
			if(
				isset( $_POST['avoskin_resi_order']) &&
				$_POST['avoskin_resi_order'] != '' &&
				isset( $_POST['avoskin_shipping_courier']) &&
				$_POST['avoskin_shipping_courier'] != ''
			){
				if(get_post_meta($post_id, 'order_tracking_notif', true) != 'yes'){
					$order = wc_get_order( $post_id );
					$user = get_user_by('email', $order->get_billing_email());
					if($user){
						update_user_meta($user->ID, 'avo_is_verified', 'yes');	
					}
					$args = [
						'order_id' => $order->get_id(),
						'courier' => strtoupper($_POST['avoskin_shipping_courier']),
						'resi' => $_POST['avoskin_resi_order']
					];
					WC()->mailer()->emails['WC_Customer_Resi_order']->trigger($args);
					update_post_meta($post_id, 'order_tracking_notif', 'yes');
				}
			}	
		}
		
		public function add_verified_info($post_id){
			$order = wc_get_order( $post_id );
			$user = get_user_by('email', $order->get_billing_email());
			if($user){
				update_user_meta($user->ID, 'avo_is_verified', 'yes');	
			}
		}
		
		public function send_resi_info($request){
			$params = $request->get_params();
			$order = $params['order'];
			$args = [
				'order_id' => (int)$order,
				'courier' => strtoupper(get_post_meta((int)$order, 'shipping_courier', true)),
				'resi' => get_post_meta((int)$order, 'resi_order', true)
			];
			WC()->mailer()->emails['WC_Customer_Resi_order']->trigger($args);
			return __('Resi info has been sent!', 'avoskin');
		}
		
		public function track_order($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			$order = wc_get_order(preg_replace('/[^0-9]/', '', $data['order_id']));
			if($order !== false && is_array((array)$order) && !empty((array)$order)){
				if($order->get_billing_email() != $data['billing_email']){
					$results['title'] = __('Notice', 'avoskin');
					$results['msg'] = __('Order ID and Billing Email not match!', 'avoskin');
				}else{
					$order_data = [
						'order_id' => $order->get_id(),
						'resi' => get_post_meta($order->get_id(), 'resi_order', true),
						'courier' => get_post_meta($order->get_id(), 'shipping_courier', true)
					];
					$results['status'] = 'ok';
					$results['redirect'] = add_query_arg([
						'data' => base64_encode(wp_json_encode($order_data))
					], get_permalink((int)$data['result']));
				}
			}else{
				$results['title'] = __('Notice', 'avoskin');
				$results['msg'] = __('Order not found!', 'avoskin');
			}
			return $results;
		}
		
		public function update_user_info($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			
			// ::CRLOYALTY::
			if($user_id = $this->verify_user($data['user'])){
				$email = $data['email'];
				if(is_email($email) && $this->validate_email_tld($email)){
					$user = get_user_by('id', (int)$user_id);
					if($user->data->user_email == $email || null == email_exists( $email )){
						$results = $this->member->update_user($user_id, $data, $results);
						$args = $results['data'];
						$args['phone'] = $this->loyalty->get_loyalty_phone($user_id);
						$this->loyalty->update_user_info($args);
						unset($results['data']);		
					}else{
						$results['msg'] = __('Email address already used.', 'avoskin');	
					}
				}else{
					$results['msg'] = __('Invalid email address', 'avoskin');
				}
			}
			
			return $results;
		}
		
		private function validate_email_tld($email){ // ::CRLOYALTY::
			 return (filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\.com/', $email)
					|| filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\.id/', $email)) ? true : false;
		}
		
		public function update_user_password($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			
			// ::PENTEST::
			if(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $data['new_password'])){
				$results['msg'] = 'Password minimum 8 characters, at least one uppercase letter, one lowercase letter and one number!';
			}else{
				if($user_id = $this->verify_user($data['user'])){
					$results = $this->member->update_user_password($user_id, $data, $results);
				}	
			}
			
			return $results;
		}
		
		private function validate_address($data, $exclude = []){ // ::PENTEST::
			$valid = true;
			if(is_array($data) && !empty($data)){
				foreach($data as $k => $v){
					if(!in_array($k, $exclude) && $v != '' && !preg_match("#^[a-zA-Z0-9\(\),.\-\/ ]+$#", $v)){
						$valid = false;
					}
				}
			}
			return $valid;
		}
		
		public function update_user_address($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			if($this->validate_address($data, ['user', 'billing_email', 'shipping_email', 'billing_last_name', 'shipping_last_name'])){ // ::PENTEST::
				if($user_id = $this->verify_user($data['user'])){
					$results = $this->member->update_user_address($user_id, $data);
				}	
			}else{
				$results['msg'] = __('Invalid character inputted!', 'avoskin');
			}
			
			return $results;
		}
		
		public function add_to_wish($request){
			$params = $request->get_params();
			$results = [
				'title' => __('Info', 'avoskin'),
				'msg' => __('Product added to wishlist', 'avoskin'),
				'action' => 'added'
			];
			$user = $params['data']['user'];
			$product = $params['data']['product'];
			$wishlist = $this->model->get_user_meta($user, 'wishlist');
			if(is_array($wishlist)){
				if(!empty($wishlist)){
					if(!in_array($product,$wishlist)){
						array_push($wishlist, $product);	
					}else{
						$wishlist = array_diff($wishlist, [$product]);
						$results = [
							'title' => __('Info', 'avoskin'),
							'msg' => __('Product removed from wishlist', 'avoskin'),
							'action' => 'removed'
						];
					}
				}else{
					array_push($wishlist, $product);
				}
			}else{
				$wishlist = [$product];
			}
			
			update_user_meta((int)$user, 'wishlist', $wishlist);
			
			return $results;
		}
		
		public function emptying_cart(){
			WC()->cart->empty_cart();
		}
		
		public function record_referral($request){
			$params = $request->get_params();
			$referral = $params['referral'];
			$visitor = $params['visitor'];
			
			$user = $this->model->get_user_by_meta($referral);
			if($user != NULL && $user != ''){
				$this->model->record_referral([
					'type' => 'visit',
					'referral' => $user,
					'visitor' => base64_decode($visitor),
					'time' => current_time('Y-m-d H:i:s')
				]);
			}
		}
		
		public function add_referral_conversion($order_id){
			$referral = get_post_meta($order_id, 'referral', true);
			$visitor = get_post_meta($order_id, 'visitor', true);
			
			if($referral != ''){
				$user = $this->model->get_user_by_meta($referral);
				if($user != NULL && $user != ''){
					$this->model->record_referral([
						'type' => $order_id,
						'referral' => $user,
						'visitor' => base64_decode($visitor),
						'time' => current_time('Y-m-d H:i:s')
					]);
				}
			}
		}
		
		public function count_visit($referral){
			return $this->model->count_visit($referral);
		}
		
		public function count_unique_visit($referral){
			return $this->model->count_unique_visit($referral);
		}
		
		public function count_conversion($referral){
			return $this->model->count_conversion($referral);
		}
		
		public function get_referral_order($referral){
			return $this->model->get_referral_order($referral);
		}
		
		public function count_review_field($comment, $key, $value){
			return $this->model->count_review_field($comment, $key, $value);
		}
		
		//public function generate_airwaybill($order_id){
		//	if(get_post_meta($order_id, 'resi_order', true) != ''){}else{
		//		\AWB_Data::generate_airwaybill($order_id);
		//	}
		//}
		
		public function get_all_city(){
			$fileurl =  get_parent_theme_file_path("inc/libs/cities.json");
			$args = [
				'ssl' => [
					'verify_peer' => false,
					'verify_peer_name' => false,
				]
			];
			   
			$fileraw = file_get_contents( $fileurl, false, stream_context_create($args) );
			return json_decode( $fileraw, true );
		}
		
		public function join_avostore($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$result = $this->results;
			
			if(strlen($data['no_ktp'])  != 16 || !preg_match("/^\d+$/", $data['no_ktp'])){
				$result['msg'] = __('Invalid No. KTP', 'avoskin');
			}else{
				WC()->mailer()->emails['WC_Join_Avostore']->trigger($data);
				$this->add_member_avostore($data);
				$result = [
					'status' => 'ok',
					'title' => __('Success', 'avoskin'),
					'msg' => __('Thank you for your request! Redirecting to homepage ...', 'avoskin'),
					'redirect' => home_url('/')
				];
			}
			
			return $result;
		}
		
		private function add_member_avostore($data){
			$member_info = [
				'post_title' => esc_attr(strip_tags($data['name'])),
				'post_type' => 'avoskin-member',
				'post_status' => 'pending'
			];
			
			$post_id = wp_insert_post($member_info);
			if($post_id) {
				update_post_meta($post_id, 'name', $data['name']);
				update_post_meta($post_id, 'no_ktp', $data['no_ktp']);
				update_post_meta($post_id, 'email', $data['email']);
				update_post_meta($post_id, 'dob', $data['dob_day'].'/'.$data['dob_month'].'/'.$data['dob_year']);
				update_post_meta($post_id, 'phone', $data['phone']);
				if(isset($data['add_contact']) && $data['add_contact'] != ''){
					update_post_meta($post_id, 'add_contact', $data['add_contact']);	
				}
				update_post_meta($post_id, 'city', $data['city']);
				update_post_meta($post_id, 'address', $data['address']);
			}
		}
		
		public function subscribe_optin($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$result = $this->results;
			
			//$cred = json_decode(base64_decode($data['credential']), true);
			//$endpoint = 'https://api.sendgrid.com/v3/marketing/contacts';
			//$payload = [
			//	'list_ids' => [$cred['list']],
			//	'contacts' => [['email' => $data['email']]]
			//];
			//$args = [
			//	'headers' => [
			//		'Authorization' => 'Bearer '.$cred['api'],
			//		'Content-Type' => 'application/json'
			//	],
			//	'method'     => 'PUT',
			//	'body' => wp_json_encode($payload)
			//];
			//$sendgrid = wp_remote_request($endpoint, $args);
			//
			//if($sendgrid['response']['code'] == '202'){
			//	$result['status'] = 'ok';
			//}
			//
			
			//$cred = json_decode(base64_decode($data['credential']), true);
			//$args = [
			//	'headers' => [
			//		'Accept-Charset' => 'utf-8'
			//	],
			//	'method'     => 'POST',
			//	'body' => [
			//		'email' => $data['email']
			//	]
			//];
			//$req = wp_remote_request($cred['url'], $args);
			//if($req['response']['code'] == 200){
			//	$result['status'] = 'ok';
			//}
			
			$home = get_option('page_on_front');
			$time = time();
			$token = get_post_meta($home, 'subs_cred_token', true);
			$username = get_post_meta($home, 'subs_cred_username', true);
			$list = get_post_meta($home, 'subs_cred_list', true);
			$auth = hash_hmac("sha256","$username"."::"."$token"."::".$time,"$token");
			$url = 'https://api.kirim.email/v3/subscriber/';
			$args = [
				'headers' => [
					'Content-Type' => 'application/x-www-form-urlencoded',
					'Auth-Id' => $username,
					'Auth-Token' => $auth,
					'Timestamp' => $time
				],
				'body' => [
					'lists' => $list,
					'email' => $data['email']
				]
			];
			
			$req = wp_remote_post($url, $args);
			$req = json_decode(wp_remote_retrieve_body($req), true);
			
			if($req['status'] == 'success'){
				$result['status'] = 'ok';
			}
			
			return $result;
		}
		
		public function get_admin_order($status = ['wc-processing']){
			return $this->model->get_admin_order($status);
		}
		public function add_has_print_order($request){
			$params = $request->get_params();
			$data = $params['data'];
			
			if(is_array($data) && !empty($data)){
				foreach($data as $d){
					$d = preg_replace('/[^0-9]/', '', $d);
					$order = wc_get_order((int)$d);
					if (!empty($order)) {
						update_post_meta((int)$d, 'has_print', 'yep');
						update_post_meta((int)$d, 'jalan_status', 'sj-sorting');
						$order->update_status( 'wc-packing' );
					}
					
				}
			}
			
			return 'ok';
		}
		
		public function order_need_print($request){
			$params = $request->get_params();
			$data = $params['data'];
			$results = ['status' => 'fail', 'success' => []];
			
			if(is_array($data) && !empty($data)){
				foreach($data as $d){
					$d = substr($d, strpos($d, "-") + 1); 
					$order = wc_get_order((int)$d);
					if (!empty($order) && $order->get_status() == 'packing') {
						update_post_meta((int)$d, 'jalan_status', 'sj-need-print');
						array_push($results['success'], $d);
						$results['status'] = 'ok';
					}else{
						$results['status'] = 'empty';
					}
				}
			}else{
				$results['status'] = 'data_empty';
			}
			return $results;
		}
		
		public function order_to_ondelivery($request){
			$params = $request->get_params();
			$data = $params['data'];
			$results = ['status' => 'fail', 'success' => []];
			
			if(is_array($data) && !empty($data)){
				foreach($data as $d){
					$d = preg_replace('/[^0-9]/', '', $d);
					$order = wc_get_order((int)$d);
					if (!empty($order)) {
						$order->update_status( 'wc-on-delivery');
						array_push($results['success'], $d);
						$results['status'] = 'ok';
					}
				}
			}
			
			return $results;
		}
		
		public function order_print_surat_jalan($request){
			$params = $request->get_params();
			$data = $params['data'];
			$results = ['status' => 'fail'];
			
			if(is_array($data) && !empty($data)){
				foreach($data as $d){
					$d = preg_replace('/[^0-9]/', '', $d);
					$order = wc_get_order((int)$d);
					if (!empty($order)) {
						update_post_meta((int)$d, 'jalan_status', 'sj-has-print');
						if(get_post_meta($order->get_id(), 'resi_order', true) != ''){
							$order->update_status( 'wc-on-delivery');
						}
						$results['status'] = 'ok';
					}
				}
			}
			
			return $results;
		}
		
		public function user_order_received($request){
			$params = $request->get_params();
			$result = $this->results;
			$order_id = $params['order'];
			$order =  wc_get_order((int)$order_id);
			
			if (!empty($order) && $order->update_status( 'completed' )) {
				$result = [
					'status' => 'ok',
					'title' => __('Info', 'avoskin'),
					'msg' => __('Order status successfully updated!', 'avoskin')
				];
			}
			
			return $result;
		}
		
		public function send_awb_to_customer($order_id, $from_status, $to_status, $order){
			// ::CRLOYALTY:: 
			$this->loyalty->process_point($order);
			
			if($to_status == 'on-delivery'){
				//Create schedule for completing order
				$this->create_completed_order_schedule($order_id);
				
				$resi = get_post_meta($order_id, 'resi_order', true);
				$courier = get_post_meta($order_id, 'shipping_courier', true);				
				if($resi != '' && $courier != ''){
					$args = [
						'order_id' => $order_id,
						'courier' => $courier,
						'resi' => $resi
					];
					WC()->mailer()->emails['WC_Customer_Resi_order']->trigger($args);	
				}
			}
		}
		
		public function decrease_coupon_usage($order_id, $from_status, $to_status, $order){
			if($from_status == 'pending' && $to_status == 'on-hold'){
				$code = $order->get_used_coupons();
				if(is_array($code) && !empty($code)){
					foreach($code as $c){
						$coupon = new \WC_Coupon( $c );
						//$coupon->increase_usage_count(1);
						if(intval($coupon->get_usage_count()) > 0 ){
							$coupon->decrease_usage_count($order->get_user_id());
						}
					}
				}
			}
		}
		
		private function create_completed_order_schedule($order_id){
			return @as_schedule_single_action( strtotime('+1 week'), 'do_complete_scheduled_order', ['order_id' => $order_id]);
		}
		
		public function process_complete_scheduled_order($order_id){
			$order = wc_get_order(absint($order_id));
			if($order != false && $order->get_status() == 'on-delivery'){
				$order->update_status( 'completed' );
			}
		}
		
		public function admin_tracking_detail($request){
			$params = $request->get_params();
			$resi = $params['resi'];
			$courier = $params['courier'];
			$return = [
				'status' => 'fail'	
			];
			
			$tracking = avoskin_tracking_data($resi, $courier);
                        if(is_array($tracking) && !empty($tracking) && isset($tracking['status']) && $tracking['status'] == 'ok' && $tracking['data']['status']['code'] === 200):
				ob_start();
                                $result = $tracking['data']['result'];
				$return['status'] = 'ok';
                        ?>
                                <div class="tracking-data">
                                        <h3><?php _e('Tracking Info','avoskin');?></h3>
                                       <div class="wrapper">
                                               <div class="table-basic">
                                                       <table>
                                                               <thead>
                                                                       <tr>
                                                                               <th><?php _e('Status','avoskin');?></th>
                                                                               <th><?php _e('City','avoskin');?></th>
                                                                               <th><?php _e('Time','avoskin');?></th>
                                                                       </tr>
                                                               </thead>
                                                               <tbody>
                                                                        <?php foreach($result['manifest'] as $m):
                                                                                $time = strtotime($m['manifest_date'] . ' ' . $m['manifest_time']) ;
                                                                                $newformat = date('d-M-Y, H:i:s',$time);
                                                                        ?>
                                                                                <tr>
                                                                                        <td><?php echo $m['manifest_description'] ;?></td>
                                                                                        <td>
                                                                                                <?php if(isset($m['city_name']) && $m['city_name'] != ''):?>
                                                                                                        <?php echo $m['city_name'];?>
                                                                                                <?php else:?>
                                                                                                        <?php echo avoskin_get_string_between($m['manifest_description']) ;?>
                                                                                                <?php endif;?>
                                                                                        </td>
                                                                                        <td><?php echo $newformat ;?></td>
                                                                                </tr>
                                                                        <?php endforeach;?>
                                                               </tbody>
                                                       </table>
                                               </div><!-- end of table basic -->
                                       </div><!-- end of wrapper -->
                               </div><!-- end of tracking data -->
                        <?php else:?>
				<h3><?php _e('Tracking info is not available yet. ','avoskin')?></h3>
                        <?php endif;
			$return['content'] = ob_get_contents();
			ob_end_clean();
			
			return $return;
		}
		
		public function import_resi_order($request){
			$params = $request->get_params();
			$resi =   explode(',',$params['resi']);
			$order = wc_get_order(preg_replace('/[^0-9]/', '', $resi[0]));
			$results = '<li>Failed to import resi for order <b>'.$resi[0].'</b></li>';
			
			if($order != false){
				$courier = $params['courier'];
				$head =   explode(',',$params['head']);
				foreach($head as $h => $v){
					if('RESI' == preg_replace('/\s+/', '', $v)){
						update_post_meta($order->get_id(), 'resi_order', $resi[$h]);
						foreach( $order->get_items('shipping') as $item_id => $item_fee ){
							wc_delete_order_item($item_id);
						}
						update_post_meta( $order->get_id(), 'shipping_courier', $courier );
						update_post_meta( $order->get_id(), 'shipping_service', $courier );
						$order->update_meta_data( '_expedition', $courier );
						$shipping_rate = new \WC_Shipping_Rate( '', strtoupper($courier), $order->get_shipping_total(), [], 'custom_shipping_method' ); 
						$order->add_shipping($shipping_rate);		
					}
				}
				$results = '<li>Resi number for order <b>'.$resi[0].'</b> successfully imported.</li>';			
			}
			// ::TEMP:: CLEARER
			//foreach( $order->get_items('shipping') as $item_id => $item_fee ){
			//	wc_delete_order_item($item_id);
			//}
			//
			//update_post_meta($order->get_id(), 'resi_order', '');
			//update_post_meta( $order->get_id(), 'shipping_courier', '' );
			//update_post_meta( $order->get_id(), 'shipping_service', '' );	
			return $results;
		}
		
		private function replace_message_variables( $message, $order ) {
			$replacements = [
				'%shop_name%'       => get_bloginfo('name'),
				'%order_id%'        => $order->get_order_number(),
				'%order_count%'     => $order->get_item_count(),
				'%order_amount%'    => $order->get_total(),
				'%order_status%'    => ucfirst( $order->get_status() ),
				'%billing_name%'    => $order->get_formatted_billing_full_name(),
				'%shipping_name%'   => $order->get_formatted_shipping_full_name(),
				'%shipping_method%' => $order->get_shipping_method(),
				'%billing_first%'   => $order->get_billing_first_name( 'edit' ),
				'%billing_last%'    => $order->get_billing_last_name( 'edit' ),
			];
			return str_replace( array_keys( $replacements ), $replacements, $message );
		}
		
		public function send_whatsapp_message($order_id, $from_status, $to_status, $order){
			if ( in_array( 'wc-' . $to_status, get_option( 'wc_twilio_sms_send_sms_order_statuses' ) ) ) {
				// get message template
				$message = get_option( 'wc_twilio_sms_' . $to_status . '_sms_template', '' );
				
				// use the default template if status-specific one is blank
				if ( empty( $message ) ) {
					$message = get_option( 'wc_twilio_sms_default_sms_template' );
				}
				
				// allow modification of message before variable replace (add additional variables, etc)
				$message = apply_filters( 'wc_twilio_sms_customer_sms_before_variable_replace', $message, $order );
				
				// replace template variables
				$message = $this->replace_message_variables( $message, $order );
				
				// allow modification of message after variable replace
				$message = apply_filters( 'wc_twilio_sms_customer_sms_after_variable_replace', $message, $order);
				
				
				avoskin_send_whatsapp($order->get_billing_phone(), $message);
			}
		}
		
		public function add_order_email_schedule(){
			if ( false === as_next_scheduled_action( 'send_email_order_schedule' ) ) {
				@as_schedule_recurring_action( strtotime( 'Asia/Jakarta tomorrow', strtotime('UTC tomorrow') ), DAY_IN_SECONDS, 'send_email_order_schedule' );
			}
		}
		
		public function send_email_order_schedule_execute(){
			$data = [
				[
					'No Order',
					'Nama Buyer',
					'Jasa Kirim',
					'Tanggal Order',
					'Status Order'
				]
			];
			$has_order = false;
			$orders = $this->get_admin_order(['wc-packing', 'wc-on-delivery', 'wc-completed']);
			foreach($orders as $o){
				$order = wc_get_order($o['ID']);
				if($order){
					if(get_post_meta($order->get_id(), 'resi_order', true) == '' && (current_time('timestamp') - strtotime( $order->get_date_created())) > 172800){
						$courier = get_post_meta($order->get_id(), 'shipping_courier', true);
						$status = $order->get_status();
						if($status == 'packing'){
							$status = 'Packing';
						}elseif($status == 'completed'){
							$status = 'Completed';
						}else{
							$status = 'On Delivery';
						}
						$data[] = [
							'#'.$order->get_order_number(),
							$order->get_billing_first_name()  . ' ' . $order->get_billing_last_name() ,
							strtoupper($courier),
							esc_html( wc_format_datetime( $order->get_date_created(), 'd F Y' ) ),
							$status
						];
						$has_order = true;
					}
				}
			}
			if($has_order){
				send_csv_mail($data, "Berikut data order yang tidak memiliki nomor resi per-tanggal ". current_time('d F Y'), get_theme_mod('avoskin_order_email'), "Laporan pesanan tanpa resi ". current_time('d F Y'),  get_option('admin_email'));
			}

		}

		public function sicepat_reset_counter(){
			update_option('sicepat_used_awb',[]);
		}
		
		public function get_product_list($request){
			$params = $request->get_params();
			$total = (isset($params['total'])) ? $params['total'] : 10;
			$page = (isset($params['page'])) ? $params['page'] : 1;
			$args = [
				'page' => (int)$page,
				'per_page' => (int)$total,
				'status' => 'publish'				
			];
			if (isset($params['search'])){
				$args['search'] = $params['search'];
			}
			$raw_products = $this->wc_request('products', $args);
			$products = [];
			if(is_array($raw_products) && !empty($raw_products)){
				foreach($raw_products as $r){
					$thumbnail = '';
					if(isset($r['images'][0]) && !empty($r['images'][0])){
						$thumbnail = $r['images'][0]['src'];
					}
					$products[] = [
						'id' => $r['id'],
						'title' => $r['name'],
						'thumbnail' => $thumbnail,
						'price' => wp_strip_all_tags(wc_price($r['price'])),
						'stock' => $r['stock_quantity'],
						'purchase_limit' => get_post_meta($r['id'], 'product_purchase_limit', true),
						'category' => $r['categories']
					];						
				}
				if(!empty($products)){
					return [
						'status' => 'ok',
						'data' => $products
					];	
				}
			}
			return ['status' => 'failed'];
		}
		
		public function skinbae_product($request){
			$params = $request->get_params();
			$cat = $params['cat'];
			$prods = $params['product'];
			
			$args = [
				'post_type' => 'product',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'post__in' => $prods,
				'orderby' => 'post__in'
			];
			
			
			$posts = get_posts($args);
			$haspost = false;
			if(is_array($posts) && !empty($posts)){
				ob_start();
				foreach($posts as $p):
					$product = wc_get_product($p->ID);
					$term = [];
					$raw_terms = get_the_terms( $p->ID, 'product_cat' );
					foreach($raw_terms as $rt){
						array_push($term, $rt->slug);
					}
					if($cat == 'all' || $cat != 'all' && in_array($cat, $term)):
					$haspost = true;
					$stock = ceil(get_post_meta( $product->get_id(), '_stock', true ));
					$id = $product->get_id();
					$price = $product->get_regular_price();
					$sale = $product->get_sale_price();
					$percent = '';
					if($sale > 0){
						$percent = ceil((($price - $sale )  / $price ) * 100);
					}
					$stock = ceil(get_post_meta( $id, '_stock', true ));
				?>
						<div class="item">
							<figure>
								<?php if($product->get_stock_status() != 'instock'):?>
									<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
								<?php endif;?>
								<?php if($percent != '' ):?>
									<span class="disc"><?php echo ($percent == '' ) ? '' : $percent.'%' ;?></span>
								<?php endif;?>
								<?php echo $product->get_image([256,256]) ;?>
								<div class="action">
									<a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
									<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
									<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
								</div><!-- end of action -->
							</figure>
							<div class="caption">
								<h3><a href="<?php echo get_permalink($id) ;?>"><?php echo $product->get_name() ;?></a></h3>
								<p><?php echo $product->get_short_description() ;?></p>
							</div><!-- end of caption -->
						</div><!-- end of item -->
					<?php endif;?>
				<?php endforeach;
				$content = ob_get_contents();
				ob_end_clean();
			}
			
			if($haspost == false){
				$content = '<p style="text-align: center;">No product found!</p>';
			}
			
			return $content;
		}
		
		public function get_postcode($request){
			$params = $request->get_params();
			//delete_option('avoskin_postcode_'.$params['state']);
			//$zip_list = get_option('avoskin_postcode_'.$params['state'], []);
			$zip_list = [];
			if(empty($zip_list)){
				$fileurl =  get_parent_theme_file_path("inc/libs/zipcode/".strtolower($params['state']).".json");
				$args = [ 'ssl' => [ 'verify_peer' => false, 'verify_peer_name' => false, ] ];
				$zip_list = json_decode(file_get_contents( $fileurl, false, stream_context_create($args) ), true);
				update_option('avoskin_postcode_'.$params['state'], $zip_list);
			}
			
			$current = (isset($params['current'])) ? $params['current'] : '';
			ob_start();?>
				<option value="empty" <?php echo ($current == '') ? 'selected="selected"': '';?>><?php _e('Choose postal code','avoskin');?></option>
				<?php if(is_array($zip_list) && !empty($zip_list)) :
				foreach($zip_list as $z):?>
					<option value="<?php echo $z;?>" <?php selected($z, $current);?>><?php echo $z;?></option>
				<?php endforeach;
				endif;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		
		public function request_awb($request){
			$params = $request->get_params();
			$order = $params['order'];
			$result = '<span style="color:red">AWB untuk order #AVS-'.$order.' GAGAL direquest</span>';
			if(get_post_meta($order, 'resi_order', true) != ''){}else{
				$status = \AWB_Data::generate_airwaybill($order);
				$the_order = wc_get_order($order);
				if($status == 'ok'){
					$result = '<span style="color:green">AWB untuk order #AVS-'.$order.' BERHASIL direquest</span>';
					$the_order->add_order_note('Melakukan proses request AWB dengan status: BERHASIL');
				}else{
					$the_order->add_order_note('Melakukan proses request AWB dengan status: GAGAL');
				}
			}
			return $result;
		}
		
		public function move_awb($request){
			$params = $request->get_params();
			$order = $params['order'];
			$result = '<span style="color:green">Order #AVS-'.$order.' BERHASIL dipindahkan ke label</span>';
			update_post_meta($order, 'awb_status', 'awb-success');
			$the_order = wc_get_order($order);
			$the_order->add_order_note('Melakukan proses pemindahan order AWB dari tab ERROR ke tab LABEL');
			
			return $result;
		}
		
		public function update_awb($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			update_post_meta((int)$data['order_id'], $data['courier'].'_temp_request', wp_json_encode($data['param']));
			$the_order = wc_get_order((int)$data['order_id']);
			$the_order->add_order_note('Melakukan proses update parameter untuk request AWB');
			return 'ok';
		}
		
		public function avodash_customer_info_stats($request){
			return $this->model->avodash_customer_info_stats($request->get_params());
		}
		
		public function avodash_customer_info_city_stats($request){
			return $this->model->avodash_customer_info_city_stats($request->get_params());
		}
		
		public function generate_loyalty_otp($request){ // ::CRLOYALTY::
			$params = $request->get_params();
			$results = $this->results;
			if($params['user'] != 'nope'){
				if($this->validate_phone($params['new_phone'])){
					if($this->loyalty->check_phone_avail($params['new_phone'], $params['user'] )){
						$user = $params['user'];
						$phone = $params['new_phone'];
						$transient_key = base64_encode($user.'_'.$phone);
						if ( false !==  get_transient( $transient_key ) ) {
							delete_transient($transient_key);
						}
						$data = rand(1111,9999);
						set_transient( $transient_key, $data, strtotime('+3 minutes'));
						if($this->loyalty->send_otp_sms($phone, $data)){
							$results = [
								'status' => 'ok',
							];	
						}else{
							$results['title'] = 'Error';
							$results['msg'] = __('Unable to send OTP number via SMS. Please check your phone number!', 'avoskin');	
						}
					}else{
						$results['title'] = 'Error';
						$results['msg'] = __('Phone number already used!', 'avoskin');
					}	
				}else{
					$results['title'] = 'Error';
					$results['msg'] = __('Invalid phone number!', 'avoskin');
				}
			}else{
				$results['msg'] = __('Invalid user!', 'avoskin');
			}
			return $results;
		}
		
		
		public function validate_loyalty_otp($request){ // ::CRLOYALTY::
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			$results['msg'] = __('Invalid otp number!', 'avoskin');
			
			if($data['user'] != 'nope'){
				$user = $data['user'];
				$phone = $data['new_phone'];
				if ( false !== ( $otp = get_transient( base64_encode($user.'_'.$phone) ) )) {
					if(implode($data['otp_number']) == $otp){
						update_user_meta($user, 'loyalty_phone', $phone);
						if($this->loyalty->update_user_phone($user)){
							delete_transient(base64_encode($user.'_'.$phone));
							update_user_meta($user, 'loyalty_phone_verified', 'yep');
							update_user_meta($user, 'loyalty_phone_old', $phone);
							$results['status'] = 'ok';
							$results['title'] = 'Info';
							$results['msg'] = 'Phone number verified!';
							$results['phone'] = $phone;
						}else{
							$results['title'] = 'Error';
							$results['msg'] = 'New phone number already used!';
						}
					}
				}	
			}else{
				$results['title'] = 'Error';
				$results['msg'] = __('Invalid user!', 'avoskin');
			}
			
			return $results;
		}
		
		private function validate_phone($mobile){ // ::CRLOYALTY::
			if($mobile[0] == '0' ){
				$mobile = '62'. ltrim($mobile, '0');
			}elseif($mobile[0] == '+'){
				$mobile = str_replace('+','', $mobile);
			}
			return (preg_match('/^[0-9]{11}+$/', $mobile) || preg_match('/^[0-9]{12}+$/', $mobile) || preg_match('/^[0-9]{13}+$/', $mobile)) ? true : false;
		}
		
		public function avoskin_loyalty_do_order_cancelled($data){  // ::CRLOYALTY::
			$order = wc_get_order((int)$data['order']);
			if ($order) {
				$status = $order->get_status();
				if($status == 'pending' || $status == 'on-hold'){
					$order->update_status( 'cancelled' );	
				}				
			}
		}
		
		public function get_best_product($request){ // ::CRFRIDAY::
			$params = $request->get_params();
			$bpagi = $params['total'];
			$bproduct = $params['product'];
			ob_start();
			$raw = [];
			foreach($bproduct as $bp){
				$raw[] = $bp['product'];
			}
			$posts = get_posts([
				'fields' => 'ids',
				'post_type' => 'product',
				'posts_per_page' => (int)$bpagi,
				'post_status' => 'publish',
				'orderby' => 'post__in',
				'post__in' => $raw,
				'paged' => (int)$params['page']
			]);
			foreach($posts as $i ):
				$product = wc_get_product($i);
				$id = $product->get_id();
				$price = $product->get_regular_price();
				$sale = $product->get_sale_price();
				$percent = '';
				if(!$product->is_on_sale()){
					$sale = 0;
				}
				if($sale > 0){
					$percent = ceil((($price - $sale )  / $price ) * 100);
				}
				$stock = ceil(get_post_meta( $id, '_stock', true ));
		?>
			<div class="proditem">
				<figure>
					<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"></a>
					<?php if($percent != ''):?>
						<span class="disc">-<?php echo $percent ;?>%</span>
					<?php endif;?>
					<a href="<?php echo get_permalink($id);?>">
						<?php echo $product->get_image([600, 520]);?>
				       </a>
				       <?php if($product->get_stock_status() != 'instock'):?>
						<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
					<?php endif;?>
				</figure>
				<div class="caption">
					<h3><a href="<?php echo $product->get_permalink() ;?>"><?php echo $product->get_name() ;?></a></h3>
					<div class="cats">
						<?php echo wc_get_product_category_list($id);?>
					</div><!-- end of cats -->
					<div class="pricing"><?php echo $product->get_price_html(); ?></div>
					<div class="act">
						<div class="rated">
							<i class="fa-star"></i><b><?php echo $product->get_average_rating() ;?></b>
						</div><!-- end of rated -->
						<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"></a>
						<div class="clear"></div>
					</div><!--end of act -->
				</div><!-- end of caption -->
			</div><!-- end of item -->
		<?php endforeach;
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
		}
		
		// ::AVOMOB::
		public function get_user_point($request){
			$params = $request->get_params();
			return $this->loyalty->get_user_point($params['user_id']);
		}
		public function get_user_point_history($request){
			$params = $request->get_params();
			return $this->loyalty->get_user_point_timeline($params['user_id']);
		}
		
		// ::CRWFC::
		public function wfc_redeem($request){
			$params = $request->get_params();
			parse_str(wp_strip_all_tags($params['data']), $data);
			$results = $this->results;
			
			$results['msg'] = __('Invalid coupon code', 'avoskin');
			$coupons = get_posts([
				'fields' => 'ids',
				'post_type' => 'shop_coupon',
				'posts_per_page' => -1,
				'post_status' => 'publish'
			]);
			if(is_array($coupons) && !empty($coupons)){
				foreach($coupons as $c){
					if(strtolower($data['code']) == strtolower(get_the_title($c))){
						$coupon = new \WC_Coupon( $data['code'] );
						if($coupon && $coupon->get_discount_type() == 'avo_wfc'){
							$applied = WC()->cart->applied_coupons;
							$applied = (is_array($applied)) ? $applied : [];
							if(!in_array($data['code'], $applied)){
								$validate = $this->validate_wfc_coupon($params['user'], $coupon);
								if($validate['status'] == 'ok'){
									if(WC()->cart->apply_coupon($data['code'])){					
										if(isset($data['applied']) && $data['applied'] != 'nope'){
											WC()->cart->remove_coupon( $data['applied'] );
											avoskin_remove_wfc_product();
										}
										$results['status'] = 'ok';
										$results['title'] = __('Info', 'avoskin');
										$results['msg'] = __('Coupon successfully applied!', 'avoskin');
										$results['redirect'] = get_permalink(get_theme_mod('avoskin_wfc_redeem_page'));
										$this->wfc_cart_product_addition($data['code']);
									}else{
										$results['msg'] = __('Unable to apply coupon!', 'avoskin');
									}	
								}else{
									$results['msg'] = $validate['msg'];
								}
							}else{
								$results['status'] = 'ok';
								$results['title'] = __('Info', 'avoskin');
								$results['msg'] = __('Coupon successfully applied!', 'avoskin');
								$results['redirect'] = get_permalink(get_theme_mod('avoskin_wfc_redeem_page'));
								$this->wfc_cart_product_addition($data['code']);
							}
						}
					}
				}
			}
			
			return $results;
		}
		
		private function wfc_cart_product_addition($code){
			$cart = WC()->cart;
			if($cart->is_empty()){
				$products = get_post_meta(wc_get_coupon_id_by_code($code), 'wfc_products', true);
				if(is_array($products) && !empty($products) ){
					foreach($products as $p){
						$stock = get_post_meta($p, '_stock', true);
						if($stock != '' && (int)$stock > 0){
							$hash = $cart->add_to_cart( $p, 1);
							if($hash){
								$cart->remove_cart_item($hash);
								break;
							}
						}
					}
					
				}
			}
		}
		
		private function validate_wfc_coupon($user, $coupon){
			$is_eligible = true;
			$limit = $coupon->get_usage_limit_per_user();
			if( absint($limit) > 0){
				$is_eligible = false;
				$used = $coupon->get_used_by();
				if(is_array($used) && !empty($used)){
					$user_has = 0;
					foreach($used as $u){
						if($u == $user){
							$user_has++;
						}
					}
					if($user_has < $limit){
						$is_eligible = true;	
					}
				}else{
					$is_eligible = true;
				}
			}
			
			if($user != 'nope'){
				$emails = $coupon->get_email_restrictions();
				if(!empty($emails)){
					$is_eligible = false;
					$user = get_user_by('id', (int)$user);
					if(in_array($user->data->user_email, $emails)){
						$is_eligible = true;
					}
				}	
			}
			
			return $is_eligible ? [
				'status' => 'ok',
			] : [
				'status' => 'fail',
				'msg' => 'You\'re not eligible for this coupon code.'
			];
		}
        }
}