<?php
/*
  Handle request for static data files

  TODO: add postal code range for all cities, then we can use Postal Code field for city detection.
*/
class TRO_Data {
        private static $api_base = 'http://pro.rajaongkir.com/api';
      
        const PROVINCE_URL = '/province';
        const COST_URL = '/cost';

        function __construct() {
                
        }

        /*
          Translate Province Code into ID based on Raja Ongkir list.
      
          @param str $state - State / Province code from WooCommerce
          @return int - the province's ID
        */
        static function get_province_id( $state ) {
                $provinces = self::_get_data('provinces.json');
                $id = array_key_exists( $state, $provinces ) ? $provinces[$state] : 0;
                return $id;
        }

        /*
          Get all cities in the provice
      
          @param $prov_id - ID of the province
          @retun array
        */
        static function get_cities( $prov_id ) {
                $data = self::_get_data( 'cities/' . $prov_id . '.json' );
            
                // if exists, filter and return it
                if($data) {
                        $data = self::_filter_dupe_name( $prov_id, $data );
                        return $data;
                } else {
                        return $data;
                }
        }

        /*
          Get all couriers.
      
          @return array - List of couriers in (slug => name) format.
        */
        static function get_couriers() {
                $couriers_raw = self::_get_data( 'couriers.json' );
            
                // remap
                $couriers = [];
                foreach( $couriers_raw as $key => $value ) {
                        $couriers[$key] = $value['name'];
                }
            
                return $couriers;
        }

        /*
          Get the services provided by the courier.
          // TODO: code stinks. Remove hardcode for J&T and simple_format parameter
      
          @param string $name - Courier slug as listed in couriers.json
          @param bool $simple_format - *Optional*. If true, return a simplified `id => name` format. Default is false.
          @return array - The services this courier provided
        */
        static function get_services( $name, $simple_format = false ) {
                if($name === 'J&T') { $name = 'jnt'; } // for weird reason, the response code for 'jnt' is 'J&T'
            
                $couriers = self::_get_data( 'couriers.json' );
                $courier = isset( $couriers[$name] ) ? $couriers[$name] : null;
            
                // if courier found
                if( $courier ) {
                        $services = $courier['services'];
                        // simplify the data
                        if( $simple_format ) {
                                $parsed = [];
                                foreach( $courier['services'] as $key => $val ) {
                                        $parsed[$key] = $val['title'];
                                }
                                $services = $parsed;
                        }
                        return $services;
                }
        }
        
        static function get_selected_couriers(){
                $couriers = self::get_couriers();
                $selected_couriers = [];
                foreach($couriers as $id => $name) {
                        if(!empty(get_option('tro_'.$id . '_services')) ) {
                                $selected_couriers[] = $id;
                        }
                }
                return join(':', $selected_couriers);
        }
        
        static function get_rates($costs) {
                // format the costs from API to WooCommerce
                $rates = [];
                foreach($costs['results'] as $courier){
                        if(empty($courier) ) { break; }
                        // get full list of services
                        $code = ($courier['code'] === 'J&T') ? 'jnt' : $courier['code'];
                        $all_services = self::get_services($code);
            
                        $settings = get_option('tro_'.$code . '_services');
                        $allowed_services = (isset($settings) && !empty($settings)) ? $settings : [];
            
                        foreach($courier['costs'] as $service){
                                // check if this service is allowed
                                $is_allowed = false;
                                foreach($allowed_services as $as) {
                                        // if has variation
                                        if(isset($all_services[$as]['vars']) ) {
                                                $is_allowed = in_array($service['service'], $all_services[$as]['vars'] );
                                        }
                                        else {
                                                $is_allowed = $service['service'] === $as;
                                        }
            
                                        if($is_allowed) { break; }
                                }
            
                                if($is_allowed) {
                                        $rates[] = [
                                                'code' => $code,
                                                'label' => strtoupper($code) . ' ' . $service['service'],
                                                'cost' => $service['cost'][0]['value'],
                                                'etd' => $service['cost'][0]['etd'],
                                                'service' => $service['service']
                                        ];
                                }
                        }
                }
                
                return $rates;
        }

        
        static function get_costs($args){
                $query = http_build_query($args);
                $response = self::call(self::COST_URL, [
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => $query,
                        CURLOPT_HTTPHEADER => [
                                'content-type: application/x-www-form-urlencoded',
                                'key: ' . avoskin_split_apikey()
                        ],
                ]);
            
                if($response) {
                        $costs = $response;
                        return $costs;
                } else {
                        return $response;
                }
        }
        
         private static function call($endpoint, $extra_options = [] ) {
                $curl = curl_init();
                $curl_options = [
                        CURLOPT_URL => self::$api_base . $endpoint,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 10,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => [
                                'key: ' . avoskin_split_apikey()
                        ],
                ];
            
                foreach($extra_options as $option => $value) {
                        $curl_options[$option] = $value;
                }
            
                curl_setopt_array($curl, $curl_options);
            
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
            
                $response = json_decode($response, true);
                return $response['rajaongkir'];
        }
        
        /*
        Get JSON file inside /data directory
    
        @param string $filename
        @return array
      */
        private static function _get_data( $filename ) {
                $fileurl =  get_parent_theme_file_path("inc/rajaongkir/data/$filename");
                $args = [
                        'ssl' => [
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                        ]
                ];
            
                $fileraw = file_get_contents( $fileurl, false, stream_context_create($args) );
                return json_decode( $fileraw, true );
        }
        
        /*
          Add prefix to city and district that have the same name
      
          @param array $data - Cities data
          @param array $dupe_ids - ID of cities that share the same name
          @return array - Prefixed list
        */
        private static function _filter_dupe_name( $prov_id, $data ) {
                // Province that has same city and district name
                $city_dupe_name = [
                        3 => [402, 403, 455, 456], // Serang, Tangerang
                        9 => [22, 23, 54, 55, 78, 79, 108, 109, 430, 431, 468, 469], // Bandung, Bekasi, Bogor, Cirebon, Sukabumi, Tasikmalaya
                        10 => [249, 250, 348, 349, 472, 473, 398, 399], // Magelang, Pekalongan, Tegal, Semarang
                        11 => [74, 75, 178, 179, 255, 256, 247, 248, 289, 290, 342, 343, 369, 370], // Blitar, Kediri, Malang, Madiun, Mojokerto, Pasuruan, Probolinggo,
                  
                        32 => [420, 421], // Solok
                  
                        22 => [68, 69], // Bima
                        23 => [212, 213], // Kupang
                  
                        12 => [364, 365], // Pontianak
                        7 => [129, 130], // Gorontalo
                  
                        24 => [157, 158], // Jayapura
                        25 => [424, 425], // Sorong
                ];
            
                // if province has duplicate city name
                if( array_key_exists( $prov_id, $city_dupe_name ) ):
                        $dupe_ids = $city_dupe_name[$prov_id];
                  
                        foreach( $data as $id => &$value ) {
                                if( in_array($id, $dupe_ids) ) {
                                  $value['city_name'] = $value['city_name'] . ' (' . $value['type'] . ')';
                                }
                        }
                endif;
            
                return $data;
        }
}
