<?php

//Get template dir
function avoskin_dir(){
	echo esc_url( get_template_directory_uri() );
}

//Fetch metabox
function avoskin_get_meta($id, $meta_item){
	foreach($meta_item as $m => $v ){
		$meta_item[$m] = get_post_meta($id, $v, true);
	}
	return $meta_item;
}

//Get pages pair ID => Title
function avoskin_get_pages(){
        $pages = [];
        $pages_obj = get_posts([
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'post_type' => 'page',
                'orderby' => 'title',
                'order' => 'ASC'
        ]);
        if(is_array($pages_obj) && !empty($pages_obj)){
                foreach($pages_obj as $p){
			$lang = apply_filters( 'wpml_post_language_details', NULL, $p->ID ) ;
			$lang = ($lang['language_code'] == 'en') ? ' (EN)' : ' (ID)';
                        $pages[$p->ID] = $p->post_title . $lang;
                }
        }
        
        return $pages;
}

function avoskin_get_page($page = 'login', $get = false){
	$pages = [
		'avostore' => get_theme_mod('avoskin_avostore_page'),
		'id_avostore' => get_theme_mod('avoskin_id_avostore_page'),
		'tracking_result' => get_theme_mod('avoskin_tracking_result_page'),
		'tracking_form' => get_theme_mod('avoskin_tracking_form_page'),
	];
	
	if($get){
		return ($pages[$page] != '') ? get_permalink($pages[$page]) : '';	
	}else{
		echo ($pages[$page] != '') ? get_permalink($pages[$page]) : '';	
	}
}

function avoskin_get_product_menu($id){
	$thumb = get_post_thumbnail_id($id);
        $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
        $image = avoskin_resize( $img_url, 230, 230, true, true, true ); //resize & crop img
	$rating = get_post_meta( $id, '_wc_average_rating', true );
	ob_start();?>
		<div class="item">
                        <a href="<?php echo get_permalink($id);?>"><img src="<?php echo $image ;?>"/></a>
			<h4><a href="<?php echo get_permalink($id);?>"><?php echo get_the_title($id);?></a></h4>
			<?php echo avoskin_rating($rating) ;?>
		</div><!-- end of item -->
	<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

function avoskin_get_mira_menu($id, $desc){
	$thumb = get_post_thumbnail_id($id);
        $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
        $image = avoskin_resize( $img_url, 110, 140, true, true, true ); //resize & crop img
	ob_start();?>
		<li>
			<figure><a href="<?php echo get_permalink($id);?>"><img src="<?php echo $image ;?>" /></a></figure><!--
			--><div class="caption">
				<h3><a href="<?php echo get_permalink($id);?>"><?php echo get_the_title($id);?></a></h3>
				<div class="txt">
					<p><?php echo $desc ;?></p>
				</div><!-- end of txt -->
				<a class="morepost" href="<?php echo get_permalink($id);?>"><?php _e('See Product','avoskin');?></a>
			</div>
		</li>
	<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

function avoskin_rating($rate){
	ob_start();
	if(intval($rate) != 0):
		$star = '';
		if(intval($rate) != 5) {
			$star =  5 - intval($rate) ;
			$star = 'min'.$star;
		}		
	?>
		<div class="rate <?php echo $star ;?>">
			<i class="fa-star"></i><i class="fa-star"></i><i class="fa-star"></i><i class="fa-star"></i><i class="fa-star"></i>
		</div>
	<?php else:?>
		<div class="rate">
			<i class="fa-star-o"></i><i class="fa-star-o"></i><i class="fa-star-o"></i><i class="fa-star-o"></i><i class="fa-star-o"></i>
		</div>
	<?php endif;
	
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

function avoskin_rating_admin($rate){
	ob_start();
	if(intval($rate) != 0):
		$star = '';
		if(intval($rate) != 5) {
			$star =  5 - intval($rate) ;
			$star = 'min'.$star;
		}		
	?>
		<div class="rate <?php echo $star ;?>">
			<i class="dashicons-before dashicons-star-filled"></i><i class="dashicons-before dashicons-star-filled"></i><i class="dashicons-before dashicons-star-filled"></i><i class="dashicons-before dashicons-star-filled"></i><i class="dashicons-before dashicons-star-filled"></i>
		</div>
	<?php else:?>
		<div class="rate">
			<i class="dashicons-before dashicons-star-empty"></i><i class="dashicons-before dashicons-star-empty"></i><i class="dashicons-before dashicons-star-empty"></i><i class="dashicons-before dashicons-star-empty"></i><i class="dashicons-before dashicons-star-empty"></i>
		</div>
	<?php endif;
	
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}


function avoskin_get_cpt($type = 'product'){
	$posts_obj = get_posts([
		'post_type' => $type,
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'title',
		'order' => 'ASC'
	]);
	$posts = [];
	if(is_array($posts_obj) && !empty($posts_obj)){
		foreach($posts_obj as $p){
			$posts[$p->ID] = $p->post_title;
		}
	}
	return $posts;
}

function avoskin_get_tax($tax = 'product_cat', $hide = false){
        $cats = [];
        $cats_obj = get_terms([
                'taxonomy' => $tax,
                'hide_empty'=> $hide
        ]);
        if(is_array($cats_obj) && !empty($cats_obj)){
                foreach($cats_obj as $c){
                        $cats[$c->term_id] = $c->name;
                }
        }
        
        return $cats;
}

function avoskin_get_menu(){
	$menus_obj = wp_get_nav_menus();
	$menus = [];
	if(is_array($menus_obj) && !empty($menus_obj)){
		foreach($menus_obj as $m){
			$menus[$m->term_id] = $m->name;
		}
	}
	
	return $menus;
}

function avoskin_request($endpoint, $args = [], $method = 'GET'){
	$request = new WP_REST_Request( $method, '/avoskin/v1/'.$endpoint );
	if(!empty($args)){
		$request->set_query_params( $args );
	}
	$response = rest_do_request( $request );
	$server = rest_get_server();
	$data = $server->response_to_data( $response, false );
	return $data;
}

//Helper to get country list based on coutry
function avoskin_get_country_states($country = 'ID'){
	$country = ($country != '') ? $country : 'ID';
	$countries_obj = new WC_Countries();
        $states = $countries_obj->__get('states');
	return (isset($states[$country]) && !empty($states[$country])) ? $states[$country] : [];
}

function avoskin_get_states_cities($states){
	if($states != ''){
		// get cities data
		$cities_raw = TRO_Data::get_cities(TRO_DATA::get_province_id($states));
	    
		// parse raw data
		$cities = [];
		foreach($cities_raw as $id => $value) {
			$cities[$id] = $value['city_name'];
		}
		return $cities;
	}
}

function avoskin_markup($weight){
	if($weight >= 200 && $weight < 999){
		$weight = 1000;
	}else{
		if(strlen(strval($weight)) == 4){
			$weight = strval($weight);
			$first = $weight[0];
			$rest = substr($weight,1);
			if(absint($rest) > 199){
				$weight = strval((absint($first) + 1 )) . '000';
			}
		}elseif(strlen(strval($weight)) == 5){
			$weight = strval($weight);
			$first = $weight[0];
			$second = $weight[1];
			$rest = substr($weight,2);
			if(absint($rest) > 199){
				$weight = $first . strval((absint($second) + 1 )) . '000';
			}
		}
	}
	
	return absint($weight);
}

//Fetch couriers from API
function avoskin_display_couriers($args){
	$contents = '';
	$selected_couriers = TRO_Data::get_selected_couriers();
	$times = (get_option('woocommerce_weight_unit') == 'kg') ? 1000 : 1;
	$weight =  (float)$args['weight'] * $times;	
	$args = [
		'origin' => get_option('tro_city_origin'),
		'originType' => 'city',
		'destination' => $args['destination'],
		'destinationType' => 'city',
		'weight' => avoskin_markup($weight),
		'courier' => $selected_couriers
	];
	$result = TRO_Data::get_costs($args);
	$rates = TRO_Data::get_rates($result);
	
	if(is_array($rates) && !empty($rates)){
		$width = [
			'jne' => '51',
			'jnt' => '53',
			'rpx' => '37',
			'pos' => '42',
			'sicepat' => '55',
		];
		ob_start();?>
		<div class="dotdio blocky">
			<?php $numeric=1;foreach($rates as $r):?>
				<label>
					<input type="radio" name="shipping" value="<?php echo $r['cost'] ;?>" <?php echo ($numeric == 1) ? 'checked="checked"' : '';?> />
					<span><?php echo wp_strip_all_tags(strtoupper($r['code'])) ;?> [<?php echo wp_strip_all_tags($r['service']) ;?>] <?php echo wp_strip_all_tags(wc_price($r['cost']));?><?php echo ($r['etd'] != '') ? ' ('.wp_strip_all_tags($r['etd']).' Hari Kerja)' : '';?></span>
					<img src="<?php avoskin_dir();?>/assets/img/shipping/<?php echo $r['code'] ;?>.png" width="<?php echo $width[$r['code']] ;?>"/>
				</label>
			<?php $numeric++;endforeach;?>
		</div>
		<?php
		$contents = ob_get_contents();
		ob_end_clean();
	}
	
	return $contents;
}

//Encode user data for payload
function avoskin_user_data(){
	if(is_user_logged_in()){
		global $current_user;
		$data_user = [
			'id' => $current_user->data->ID,
			'login' => $current_user->data->user_login
		];
		return base64_encode(wp_json_encode($data_user));
	}else{
		return '';
	}
}

//Fetch tracking data
function avoskin_tracking_data($resi, $courier){
	$apikey = avoskin_split_apikey();
	$result = [
		'status' => 'fail'	
	];
	if($apikey != ''){
		$curl = curl_init();
	
		curl_setopt_array($curl, [
			CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "waybill=$resi&courier=$courier",
			CURLOPT_HTTPHEADER => [
				"content-type: application/x-www-form-urlencoded",
				"key: $apikey" 
			],
		]);
			
		$response = curl_exec($curl);
		$err = curl_error($curl);
			
		curl_close($curl);
			
		if (!$err) {
			$response =  json_decode($response, true);
			$result['data'] = $response['rajaongkir'];
			$result['status'] = 'ok';
		} 	
	}
	return $result;
}
function avoskin_get_string_between($string){
	$start = "[";
	$end = "]";
	$string = ' ' . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return '';
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}
function avoskin_validate_order($data){
	$data = json_decode(base64_decode($_GET['data']), true);
        return (wc_get_order($data['order_id']) != false) ? true : false;
}

function avoskin_fetch_blog($source, $total = 6, $style = 'home1'){
	$blog =  '';
	if ( false === ( $blog = get_transient( 'avoskin_blog_'.$style ) )) {
		$url = $source.'wp-json/avoskinblog/v1/fetch_blog';
		$request = wp_remote_post($url, [
			'timeout' => 120,
			'redirection' => 5,
			'blocking' => true,
			'headers' => [
				'Expect' => '',
			],
			'body' => [
				'total' => (int)$total,
				'style' => $style
			]
		]);
		try{
			$blog = json_decode(wp_remote_retrieve_body($request), true);
		}catch(Exception $ex){
			
		}
		
		if(NULL !== $blog){
			set_transient( 'avoskin_blog_'.$style, $blog, HOUR_IN_SECONDS );
		}
	}
	return $blog;
}

function avoskin_get_instagram($id = '', $total = 6){
	$data =  [];
	if ( false === ( $data = get_transient( 'avoskin_instagram' ) )) {
		require __DIR__ . '/libs/vendor/autoload.php';
		$instagram = new \InstagramScraper\Instagram();
		$data = avoskin_process_instagram($instagram->getMediasByUserId($id, $total));
		set_transient( 'avoskin_instagram', $data, HOUR_IN_SECONDS );        
	}
	return $data;
}

function avoskin_process_instagram($data){
	$media = [];
	foreach($data as $d){
		$media[] = [
			'src' => $d->getImageThumbnailUrl(),
			'url' => $d->getLink()
		];
	}
	return $media;
}

function avoskin_langswitch(){
	if (  function_exists( 'icl_get_languages' ) ) {
		$languages = icl_get_languages('skip_missing=N&orderby=KEY&order=asc&link_empty_to=str');
                $active = '';
                $country = [
                        'en' => [
                                'label' => 'EN',
                        ],
                        'id' => [
                                'label' => 'ID',
                        ]
                ];
		if(!empty($languages)){
			foreach($languages as $l){
                                $country[$l['code']]['url'] = $l['url'];
                                if($active == ''){
                                        $active = ( $l['active'] == 1) ? $l['code'] : '';
                                }
			}
		}
                return [
                        'active' => ($active != '') ?  $active : 'en',
                        'country' => array_reverse($country)
                ];
	}
}

function avoskin_is_verified($user_id){
	echo (get_user_meta($user_id, 'avo_is_verified', true) === 'yes') ? 'user-verified' : '';
}

function avoskin_upload_img($data){
	$upload_dir = wp_upload_dir();
	$upload_path = str_replace('/', DIRECTORY_SEPARATOR, $upload_dir['path']) . DIRECTORY_SEPARATOR;
	
	$img = substr($data['src'], strpos($data['src'], ",") + 1);
	$img = str_replace(' ', '+', $img);
	
	$decoded = base64_decode($img);
	$hashed_filename = md5($data['name'] . microtime()) . '_' . $data['name'];
	   
	// Save the image in the uploads directory.
	$upload_file = file_put_contents($upload_path . $hashed_filename, $decoded);
	   
	$attachment = [
		'post_mime_type' => $data['type'],
		'post_title' => $hashed_filename,
		'post_content' => '',
		'post_status' => 'publish',
		'guid' => $upload_dir['url'] . '/' . basename($hashed_filename)
	];

	return wp_insert_attachment($attachment, $upload_dir['path'] . '/' . $hashed_filename);
}

function avoskin_avatar($user_id, $size = 55){
	$dp = get_user_meta($user_id, 'user_dp', true);
	if($dp != ''){
		$image = avoskin_resize( wp_get_attachment_url($dp), $size, $size, true, true, true ); 
		return '<img src="'. $image.'" />';
	}else{
		return get_avatar($user_id, $size);
	}
}

function strip_tags_content($text, $tags = '', $invert = FALSE) {
	preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
	$tags = array_unique($tags[1]);
	
	if(is_array($tags) AND count($tags) > 0) {
		if($invert == FALSE) {
			return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
		}
		else {
			return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
		}
	}
	elseif($invert == FALSE) {
		return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
	}
	return $text;
}

function avoskin_split_apikey(){
	$rand = mt_rand(1,2);
	return get_option('tro_apikey_'.$rand);
}

function avoskin_generate_midtrans_snap($order, $snapToken){
	global $woocommerce;
	$midtrans = new WC_Gateway_Midtrans();
       $mixpanel_key_production = "17253088ed3a39b1e2bd2cbcfeca939a";
       $mixpanel_key_sandbox = "9dcba9b440c831d517e8ff1beff40bd9";
       $isProduction = $midtrans->environment == 'production';
       $snapToken = htmlspecialchars($snapToken, ENT_COMPAT,'ISO-8859-1', true);
       $mixpanel_key = $isProduction ? $mixpanel_key_production : $mixpanel_key_sandbox;
       $pluginName = 'fullpayment';
     
       // TODO evaluate whether finish & error url need to be hardcoded
       $wp_base_url = home_url( '/' );
       $finish_url = $wp_base_url."?wc-api=WC_Gateway_Midtrans";
       $finish_url = '"'.$finish_url.'&order_id="+result.order_id+"&status_code="+result.status_code+"&transaction_status="+result.transaction_status';
       if(isset($midtrans->enable_map_finish_url) && $midtrans->enable_map_finish_url == 'yes'){
	 $finish_url = 'result.finish_redirect_url';
       }
       $pending_url = $finish_url;
       if(property_exists($midtrans,'ignore_pending_status') && $midtrans->ignore_pending_status == 'yes'){
	 $pending_url = '"#"'; // prevent redirect onPending
       }
       $error_url = $wp_base_url."?wc-api=WC_Gateway_Midtrans";
       $snap_api_base_url = $isProduction ? "https://app.midtrans.com" : "https://app.sandbox.midtrans.com";
       $snap_script_url = $snap_api_base_url."/snap/snap.js";
	ob_start();
       // ## Print HTML
       ?>
       <!-- start Mixpanel -->
       <script data-cfasync="false" type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);mixpanel.init("<?php echo esc_js($mixpanel_key) ?>");</script>
       <!-- end Mixpanel -->
     
       <?php if(property_exists($midtrans,'ganalytics_id') && strlen($midtrans->ganalytics_id)>0){ ?>
       <!-- start Google Analytics -->
       <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
       })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
     
       ga('create', '<?php echo esc_attr($midtrans->ganalytics_id) ?>', 'auto');
       ga('send', 'pageview');
       </script>
       <!-- end Google Analytics -->
       <?php } ?>
     
       <script data-cfasync="false" id="snap_script" src="<?php echo esc_attr($snap_script_url);?>" data-client-key="<?php echo esc_attr($midtrans->client_key);?>"></script>
       <script data-cfasync="false" type="text/javascript">
       var mixpanel = mixpanel ? mixpanel : { init : function(){}, track : function(){} };
     
	 function MixpanelTrackResult(token, merchant_id, cms_name, cms_version, plugin_name, plugin_version, status, result) {
	   var eventNames = {
	     pay: 'pg-pay',
	     success: 'pg-success',
	     pending: 'pg-pending',
	     error: 'pg-error',
	     close: 'pg-close'
	   };
	   mixpanel.track(
	     eventNames[status], 
	     {
	       merchant_id: merchant_id,
	       cms_name: cms_name,
	       cms_version: cms_version,
	       plugin_name: plugin_name,
	       plugin_version: plugin_version,
	       snap_token: token,
	       payment_type: result ? result.payment_type: null,
	       order_id: result ? result.order_id: null,
	       status_code: result ? result.status_code: null,
	       gross_amount: result && result.gross_amount ? Number(result.gross_amount) : null,
	     }
	   );
	 }
	 var SNAP_TOKEN = "<?php echo esc_js($snapToken);?>";
	 var MERCHANT_ID = "<?php echo esc_js($midtrans->get_option('merchant_id'));?>";
	 var CMS_NAME = "woocommerce";
	 var CMS_VERSION = "<?php echo esc_js(WC_VERSION);?>";
	 var PLUGIN_NAME = "<?php echo esc_js($pluginName);?>";
	 var PLUGIN_VERSION = "<?php echo esc_js(MT_PLUGIN_VERSION);?>";
	 // Safely load the snap.js
	 function loadExtScript(src) {
	   // if snap.js is loaded from html script tag, don't load again
	   if (document.getElementById('snap_script'))
	     return;
	   // Append script to doc
	   var s = document.createElement("script");
	   s.src = src;
	   a = document.body.appendChild(s);
	   a.setAttribute('data-client-key','<?php echo esc_js($midtrans->client_key); ?>');
	   a.setAttribute('data-cfasync','false');
	 }
     
	 var retryCount = 0;
	 var snapExecuted = false;
	 var intervalFunction = 0;
	 // Continously retry to execute SNAP popup if fail, with 1000ms delay between retry
	 function execSnapCont(){
	   intervalFunction = setInterval(function() {
	     try{
	       snap.pay(SNAP_TOKEN, 
	       {
		 skipOrderSummary : true,
		 onSuccess: function(result){
		   MixpanelTrackResult(SNAP_TOKEN, MERCHANT_ID, CMS_NAME, CMS_VERSION, PLUGIN_NAME, PLUGIN_VERSION, 'success', result);
		   window.location = <?php echo $finish_url;?>;
		 },
		 onPending: function(result){ // on pending, instead of redirection, show PDF instruction link
		   MixpanelTrackResult(SNAP_TOKEN, MERCHANT_ID, CMS_NAME, CMS_VERSION, PLUGIN_NAME, PLUGIN_VERSION, 'pending', result);
		   // console.log(result?result:'no result');
		   if (result.fraud_status == 'challenge'){ // if challenge redirect to finish
		     window.location = <?php echo $pending_url;?>;
		   }
		   // redirect to thank you page
		   window.location = <?php echo $pending_url;?>;
		   // Show payment instruction and hide payment button
		   document.getElementById('payment-instruction-btn').href = result.pdf_url;
		   // document.getElementById('pay-button').style.display = "none";
		   document.getElementById('payment-instruction').style.display = "block";
		   // if no pdf instruction, hide the btn
		   if(!result.hasOwnProperty("pdf_url")){
		     document.getElementById('payment-instruction-btn').style.display = "none";
		   }
		   return;
		   // Below code is UNUSED
		   // Update order with PDF url to backend
		   try{
		     result['pdf_url_update'] = true;
		     result['snap_token_id'] = SNAP_TOKEN;
		     fetch('<?php echo esc_url($wp_base_url."?wc-api=WC_Gateway_Midtrans");?>', {
		       method: 'POST',
		       headers: {
			 'Accept': 'application/json',
			 'Content-Type': 'application/json'
		       },
		       body: JSON.stringify(result)
		     });
		   }catch(e){ console.log(e); }
		 },
		 onError: function(result){
		   MixpanelTrackResult(SNAP_TOKEN, MERCHANT_ID, CMS_NAME, CMS_VERSION, PLUGIN_NAME, PLUGIN_VERSION, 'error', result);
		   // console.log(result?result:'no result');
		   window.location = "<?php echo esc_url($error_url);?>&order_id="+result.order_id+"&status_code="+result.status_code+"&transaction_status="+result.transaction_status;
		 },
		 onClose: function(){
			window.location = '<?php echo $order->get_checkout_order_received_url();?>';
		   //MixpanelTrackResult(SNAP_TOKEN, MERCHANT_ID, CMS_NAME, CMS_VERSION, PLUGIN_NAME, PLUGIN_VERSION, 'close', null);
		   
		   // console.log(result?result:'no result');
		 }
	       });
	       snapExecuted = true; // if SNAP popup executed, change flag to stop the retry.
	     } catch (e){ 
	       retryCount++;
	       if(retryCount >= 10){
		 location.reload();
		 return;
	       }
	       console.log(e);
	       console.log("Snap not ready yet... Retrying in 1000ms!");
	     } finally {
	       if (snapExecuted) {
		 clearInterval(intervalFunction);
		 // record 'pay' event to Mixpanel
		 MixpanelTrackResult(SNAP_TOKEN, MERCHANT_ID, CMS_NAME, CMS_VERSION, PLUGIN_NAME, PLUGIN_VERSION, 'pay', null);
	       }
	     }
	   }, 1000);
	 };
     
	 console.log("Loading snap JS library now!");
	 // Loading SNAP JS Library to the page    
	 loadExtScript("<?php echo esc_js($snap_script_url);?>");
	 console.log("Snap library is loaded now");

       </script>
       
     <?php
     // ## End of print HTML
     $contents = ob_get_contents();
     ob_end_clean();
     return $contents;
}

function avoskin_generate_vanumber($order_id){
	$midtrans = get_option('woocommerce_midtrans_settings');
	$env = $midtrans['select_midtrans_environment'];
	$url =  ($env == 'sandbox') ? 'https://app.sandbox.midtrans.com' : 'https://app.midtrans.com' ;
	$snap = get_post_meta($order_id, '_mt_payment_snap_token', true);
	$tokenStatusUrl = $url.'/snap/v1/transactions/'.$snap.'/status';
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $tokenStatusUrl);
	curl_setopt($curl, CURLOPT_HTTPHEADER, [
		'Content-Type: application/json',
	]);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0); 
	curl_setopt($curl, CURLOPT_TIMEOUT, 400); //timeout in seconds
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	if (!$result) {
	    die("Connection Failure");
	}
	curl_close($curl);
	$data = json_decode($result, true);
	
	$meta = '';
	if($data['payment_type'] == 'bank_transfer'){
		if(isset($data['atm_channel'])){
			$bank = $data['atm_channel'];
			$vanumber = $data[$bank.'_va_number'];
			$meta = strtoupper($bank) . ' ' . $vanumber;
		}else{
			$meta = strtoupper($data['va_numbers'][0]['bank']) .' '. $data['va_numbers'][0]['va_number'];
		}
	}elseif($data['payment_type'] == 'cstore'){
		$meta = strtoupper($data['store']) .' '. $data['payment_code'];
	}elseif($data['payment_type'] == 'gopay' || $data['payment_type'] == 'akulaku'){
		$meta = strtoupper($data['payment_type']) .' '. get_post_meta($order_id, '_mt_payment_url', true);
	}else{
		$meta = 'Mandiri Code ' . $data['biller_code'] . ' Key ' . $data['bill_key'] ;
	}
	update_post_meta($order_id, 'bank_va_number', $meta);	
}

require_once get_parent_theme_file_path( '/inc/libs/twilio/autoload.php' );
use Twilio\Rest\Client;

function avoskin_send_whatsapp($phone, $message){
	if(get_option('avoskin_disable_wa') != 'yes'){
		$sid = get_option('wc_twilio_sms_account_sid');
		$token = get_option('wc_twilio_sms_auth_token');
		$twilio = new Client($sid, $token);
		
		$wa = $twilio->messages->create("whatsapp:$phone", // to
			[
			       "from" => "whatsapp:".get_option('avoskin_wa_twilio_number'),
			       "body" => $message
			]
		);	
	}
}

function create_csv_string($data) {
        // Open temp file pointer
        if (!$fp = fopen('php://temp', 'w+')) return FALSE;
        // Loop data and write to file pointer
        foreach ($data as $line) fputcsv($fp, $line);
        // Place stream pointer at beginning
        rewind($fp);
        // Return the data
        return stream_get_contents($fp);
}

function send_csv_mail($csvData, $body, $to = 'youraddress@example.com', $subject = 'Test email with attachment', $from = 'webmaster@example.com') {

	// This will provide plenty adequate entropy
	$multipartSep = '-----'.md5(time()).'-----';
	
	// Arrays are much more readable
	$headers = [
		"From: $from",
		"Reply-To: $from",
		"Content-Type: multipart/mixed; boundary=\"$multipartSep\""
	];
	
	// Make the attachment
	$attachment = chunk_split(base64_encode(create_csv_string($csvData))); 
	
	// Make the body of the message
	$body = "--$multipartSep\r\n"
	      . "Content-Type: text/HTML; charset=ISO-8859-1; format=flowed\r\n"
	      . "Content-Transfer-Encoding: 7bit\r\n"
	      . "\r\n"
	      . "$body\r\n"
	      . "--$multipartSep\r\n"
	      . "Content-Type: text/csv\r\n"
	      . "Content-Transfer-Encoding: base64\r\n"
	      . "Content-Disposition: attachment; filename=\"data-order.csv\"\r\n"
	      . "\r\n"
	      . "$attachment\r\n"
	      . "--$multipartSep--";
	
	// Send the email, return the result
	return @mail($to, $subject, $body, implode("\r\n", $headers)); 
}

function avoskin_check_product_limit($cart, $product_id, $qty){
	$limit = 'nope';
	$purchase_limit = get_post_meta($product_id, 'product_purchase_limit', true);
	if($purchase_limit != '' && $purchase_limit != '0' && $purchase_limit != 0){
		if(is_array($cart->cart_contents) && !empty($cart->cart_contents)){
			foreach($cart->cart_contents as $cart_key => $item){
				if($item['product_id'] == $product_id){
					$current_qty = $item['quantity'];
					if(((int)$current_qty + (int)$qty) > (int)$purchase_limit){
						$limit = $purchase_limit;
					}
				}
			}
		}else{
			if( (int)$qty > (int)$purchase_limit){
				$limit = $purchase_limit;
			}
		}	
	}
	
	return $limit;
}

function avoskin_check_cart_limit($data){
	$limit = 'nope';
	$purchase_limit = get_post_meta((int)$data['product'], 'product_purchase_limit', true);
	if($purchase_limit != '' && $purchase_limit != '0' && $purchase_limit != 0){
		if( (int)$data['quantity'] > (int)$purchase_limit){
			$limit = [
				'title' => get_the_title((int)$data['product']),
				'limit' => $purchase_limit
			];
		}
	}
	
	return $limit;
}


function avoskin_oos_text($product_id){
	echo (get_post_meta($product_id, 'product_empty_text', true) != '' ) ? get_post_meta($product_id, 'product_empty_text', true) : __('Out Of Stock', 'avoskin');
}

function avoskin_move_order_from_packaging_to_processing(){
	$orders = [
		'980','975','971','970','968','947'
	];
	foreach($orders as $o){
		$order = wc_get_order((int)$o);
		if($order){
			update_post_meta((int)$o, 'has_print', 'nope');
			update_post_meta((int)$o, 'jalan_status', '');
			update_post_meta((int)$o, 'awb_status', '');
			$order->update_status( 'wc-processing' );
		}
	}
}

function avoskin_midtrans_charge($snap_token, $user_email, $payment_channel, $order_id, $redirect = ''){
	
	if($payment_channel == 'credit_card' || $payment_channel == 'gopay'){
		update_post_meta($order_id, 'bank_va_number', $redirect);
		return true;
	}
	
	//Generate VA number from midtrans
	$midtrans = get_option('woocommerce_midtrans_settings');
	$env = $midtrans['select_midtrans_environment'];
	$url =  ($env == 'sandbox') ? 'https://app.sandbox.midtrans.com' : 'https://app.midtrans.com' ;
	$url .= "/snap/v2/transactions/$snap_token/charge";
	
	$payload = [
		'customer_details' => [
			'email' => $user_email
		],
		'payment_type' => $payment_channel
	];
	$request =  wp_remote_post($url, [
		'headers' => [
			'Content-Type' => 'application/json; charset=utf-8'
		],
		'body' => wp_json_encode($payload),
		'method'      => 'POST',
		'data_format' => 'body',
	]);
	
	if(!is_wp_error($request) && $request['response']['code'] == 200){
		$data =  json_decode(wp_remote_retrieve_body($request), true);
		if($data['status_code'] == 201){ //https://snap-docs.midtrans.com/#status-code
			$meta = '';
			
			if($data['payment_type'] == 'bank_transfer'){
				if(isset($data['atm_channel'])){
					$bank = $data['atm_channel'];
					$vanumber = $data[$bank.'_va_number'];
					$meta = $vanumber;
				}else{
					$meta =  $data['va_numbers'][0]['va_number'];
				}
			}elseif($data['payment_type'] == 'cstore'){
				$meta = $data['payment_code'];
			}elseif($data['payment_type'] == 'gopay' || $data['payment_type'] == 'akulaku'){
				$meta =  get_post_meta($order_id, '_mt_payment_url', true);
			}else{
				$meta =  $data['biller_code'] . ' - ' . $data['bill_key'] ;
			}
			update_post_meta($order_id, 'bank_va_number', $meta);	
			update_post_meta($order_id, 'payment_channel', $payment_channel);
			
			return true;
		}
	}
	
	return false;
}

function avoskin_get_instruction_data($order_id){
	$instruction = [
		'bca_va' =>[
			[
				'title' => 'Cara pembayaran via ATM BCA',
				'text' => '
					<ol>
						<li>Pada menu utama, pilih <b>Transaksi Lainnya</b></li>
						<li>Pilih <b>Transfer</b></li>
						<li>Pilih ke Rek <b>BCA Virtual Account</b></li>
						<li>Masukkan nomor <b>{vanumber}</b> lalu tekan <b>Benar</b></li>
						<li>Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan <b>Ya</b></li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via Klik BCA',
				'text' => '
					<ol>
						<li>Pilih menu <b>Transfer Dana</b></li>
						<li>Pilih Transfer ke <b>BCA Virtual Account</b></li>
						<li>Masukkan nomor BCA Virtual Account <b>{vanumber}</b></li>
						<li>Jumlah yang akan ditransfer, nomor rekening dan nama merchant akan muncul di halaman konfirmasi pembayaran, jika informasi benar klik <b>Lanjutkan</b></li>
						<li>Masukkan respon KEYBCA APPLI 1 yang muncul pada Token BCA Anda, lalu klik tombol <b>Kirim</b></li>
						<li>Transaksi Anda selesai</li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via m-BCA',
				'text' => '
					<ol>
						<li>Pilih <b>m-Transfer</b></li>
						<li>Pilih <b>Transfer</b></li>
						<li>Pilih <b>BCA Virtual Account</b></li>
						<li>Pilih nomor rekening yang akan digunakan untuk pembayaran</li>
						<li>Masukkan nomor BCA Virtual Account <b>{vanumber}</b>, lalu pilih <b>OK</b></li>
						<li>Nomor BCA Virtual Account dan nomor Rekening Anda akan terlihat di halaman konfirmasi rekening</li>
						<li>Pilih <b>OK</b> pada halaman konfirmasi pembayaran</li>
						<li>Masukkan PIN BCA untuk mengotorisasi pembayaran</li>
						<li>Transaksi Anda selesai</li>
					</ol>
				'
			],
		],
		'echannel' =>[
			[
				'title' => 'Pembayaran melalui ATM Mandiri',
				'text' => '
					<ol>
						<li>Masukkan PIN Anda</li>
						<li>Pada menu utama pilih menu <b>Pembayaran</b> kemudian pilih menu <b>Multi Payment</b></li>
						<li>Masukan <b>Kode Perusahaan {vacode}</b></li>
						<li>Masukan <b>Kode Pembayaran {vanumber}</b></li>
						<li>Konfirmasi pembayaran Anda</li>
					</ol>
				'
			],
			[
				'title' => 'Cara membayar melalui Internet Banking Mandiri',
				'text' => '
					<ol>
						<li>Login ke <a href="https://ib.bankmandiri.co.id/" target="_blank">Mandiri Internet Banking</a></li>
						<li>Di Menu Utama silakan pilih <b>Bayar</b> kemudian pilih <b>Multi Payment</b></li>
						<li>Pilih akun anda di <b>Dari Rekening</b>, kemudian di <b>Penyedia Jasa</b> pilih <b>midtrans</b></li>
						<li>Masukkan <b>Kode Pembayaran {vanumber}</b> dan klik <b>Lanjutkan</b></li>
						<li>Konfirmasi pembayaran anda menggunakan Mandiri Token</li>
					</ol>
				'
			]
		],
		'bni_va' =>[
			[
				'title' => 'Cara pembayaran via ATM BNI',
				'text' => '
					<ol>
						<li>Pada menu utama, pilih <b>Menu Lainnya</b></li>
						<li>Pilih <b>Transfer</b></li>
						<li>Pilih <b>Rekening Tabungan</b></li>
						<li>Pilih <b>Ke Rekening BNI</b></li>
						<li>Masukkan Nomor Rekening Pembayaran Anda <b>{vanumber}</b> lalu tekan <b>Benar</b></li>
						<li><b>Masukkan jumlah tagihan yang akan Anda bayar secara lengkap</b>. Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak</li>
						<li>Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening dan nama Merchant. Jika informasi telah sesuai tekan <b>Ya</b></li>
					</ol>
				'
			]
		],
		'bri_va' =>[
			[
				'title' => 'Cara pembayaran via ATM BRI',
				'text' => '
					<ol>
						<li>Pada menu utama, pilih <b>Transaksi Lain</b></li>
						<li>Pilih <b>Pembayaran</b></li>
						<li>Pilih <b>Lainnya</b></li>
						<li>Pilih <b>BRIVA</b></li>
						<li>Masukkan Nomor BRIVA <b>{vanumber}</b> dan pilih <b>Benar</b></li>
						<li>Jumlah pembayaran, nomor BRIVA dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih <b>Ya</b></li>
						<li>Pembayaran telah selesai. Simpan bukti pembayaran Anda.</li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via Internet Banking BRI',
				'text' => '
					<ol>
						<li>Masuk pada <b>Internet Banking BRI</b></li>
						<li>Pilih menu <b>Pembayaran dan Pembelian</b></li>
						<li>Pilih sub menu <b>BRIVA</b></li>
						<li>Masukkan Nomor BRIVA <b>{vanumber}</b></li>
						<li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih <b>Kirim</b></li>
						<li>Masukkan <b>password</b> dan <b>mToken</b>, pilih <b>Kirim</b></li>
						<li>Pembayaran telah selesai. Untuk mencetak bukti transaksi, pilih <b>Cetak</b></li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via BRI mobile',
				'text' => '
					<ol>
						<li>Masuk ke dalam aplikasi <b>BRI Mobile</b>, pilih <b>Mobile Banking BRI</b></li>
						<li>Pilih <b>Pembayaran</b>, lalu pilih <b>BRIVA</b></li>
						<li>Masukkan nomor BRIVA <b>{vanumber}</b></li>
						<li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih <b>Continue</b></li>
						<li>Masukkan Mobile Banking BRI PIN, pilih <b>Ok</b></li>
						<li>Pembayaran telah selesai. Simpan notifikasi sebagai bukti pembayaran</li>
					</ol>
				'
			],
		],
		'permata_va' =>[
			[
				'title' => 'Cara pembayaran via ATM Mandiri / Bersama',
				'text' => '
					<ol>
						<li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
						<li>Pilih <b>Transfer</b></li>
						<li>Pilih <b>Antar Bank Online</b></li>
						<li>Masukkan nomor <b>013 {vanumber}</b> (kode 013 dan 16 angka Virtual Account)</li>
						<li>Masukkan jumlah harga yang akan Anda bayar secara lengkap (tanpa pembulatan). Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</li>
						<li>Kosongkan nomor referensi dan tekan <b>Benar</b></li>
						<li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no.rekening tujuan. Jika informasinya telah cocok tekan <b>Benar</b></li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via ATM BCA / Prima',
				'text' => '
					<ol>
						<li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
						<li>Pilih <b>Transfer</b></li>
						<li>Pilih <b>Ke Rek Bank Lain</b></li>
						<li>Masukkan <b>013</b> (Kode Bank Permata) lalu tekan <b>Benar</b></li>
						<li>Masukkan jumlah harga yang akan Anda bayar secara lengkap (tanpa pembulatan), lalu tekan Benar. Penting: Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</li>
						<li>Masukkan <b>{vanumber}</b> (16 digit no. virtual account pembayaran) lalu tekan <b>Benar</b></li>
						<li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no.rekening tujuan. Jika informasinya telah cocok tekan <b>Benar</b></li>
					</ol>
				'
			],
			[
				'title' => 'Cara pembayaran via ATM Permata / Alto',
				'text' => '
					<ol>
						<li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
						<li>Pilih <b>Pembayaran</b></li>
						<li>Pilih <b>Pembayaran Lainnnya</b></li>
						<li>Pilih <b>Virtual Account</b></li>
						<li>Masukkan 16 digit no. Virtual Account <b>{vanumber}</b> dan tekan <b>Benar</b></li>
						<li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no. Virtual Account, dan nama toko. Jika informasinya telah cocok tekan <b>Benar</b></li>
						<li>Pilih rekening pembayaran Anda dan tekan <b>Benar</b></li>
					</ol>
				'
			],
		],
		'alfamart' =>[
			[
				'title' => 'Bayar di gerai Alfamart, Alfamidi, atau Dan+Dan.',
				'text' => '
					<ol>
						<li>Salinlah <b>Kode Pembayaran</b> Anda dan <b>jumlah pembayaran</b> yang akan dibayarkan. Jangan Khawatir, Kami juga akan mengirimkan salinan Kode Pembayaran dan Instruksi Pembayaran melalui email kepada Anda. </li>
						<li>Silakan pergi ke gerai <b>Alfamart</b>, <b>Alfamidi</b>, atau <b>Dan+Dan</b> terdekat dan berikanlah nomor <b>Kode Pembayaran Anda</b> ke kasir.</li>
						<li>Kasir akan mengkonfirmasi transaksi dengan menanyakan <b>jumlah transaksi</b> dan <b>nama merchant</b>.</li>
						<li>Konfirmasikan pembayaran Anda ke kasir.</li>
						<li>Transaksi Anda berhasil! Anda akan mendapatkan email konfirmasi pembayaran dan simpanlah struk transaksi Anda.</li>
					</ol>
				'
			]
		]
	];
	$channel = get_post_meta($order_id,'payment_channel', true) ;
	$vanumber = get_post_meta($order_id,'bank_va_number', true);
	$vacode = '';
	if($channel == 'echannel'){
		$vanumber = str_replace(' ', '', $vanumber);
		$vadata = explode('-', $vanumber);
		$vacode = $vadata[0];
		$vanumber = $vadata[1];
	}
	$selected = $instruction[$channel];
	$pattern = [ "{vanumber}","{vacode}"];
	$replace = [ $vanumber, $vacode ];
	
	foreach($selected as $k => $v){
		$selected[$k]['text'] = strtr($v['text'], array_combine($pattern, $replace));
	}
	
	return $selected;
	
}


function avoskin_generate_avail_awb(){ // ::TEMP::
	$range = get_option('sicepat_awb_range');
	$used = get_option('sicepat_used_awb', []);
	
	$range = explode('-', $range);
	$avail = [];
	if(is_array($range) && !empty($range)){
		$min = $range[0];
		$max = $range[1];
		$awb = str_pad(intval($min) + 1, strlen($min), '0', STR_PAD_LEFT); 
		while($awb != $max){
			if(!in_array($awb, $used)){
				array_push($avail, $awb);        
			}
			
			$awb = str_pad(intval($awb) + 1, strlen($awb), '0', STR_PAD_LEFT); 
		}
	}
	update_option('sicepat_avail_awb', $avail);
}

function avoskin_is_blog(){
	return (is_page_template('page-landing-blog.php') || is_home() || is_singular('post') || is_category() || is_author()) ? true : false;
}


// ::CRWFC::
function avoskin_has_wfc_coupon(){
	$applied = WC()->cart->applied_coupons;
	if(is_array($applied) && !empty($applied)){
		foreach($applied as $a){
			$coupon = new WC_Coupon($a);
			if($coupon->get_discount_type() == 'avo_wfc'){
				return $a;
			}
		}
	}
	return 'nope';
}
function avoskin_remove_wfc_product(){
	$cart = WC()->cart->get_cart();
	$coupon = WC()->cart->get_applied_coupons();
	$coupon = (is_array($coupon)) ? $coupon : [];
	if(is_array($cart) && !empty($cart)){
		foreach($cart as $key => $value){
			if(isset($value['wfc_code']) && $value['wfc_code'] != '' && !in_array($value['wfc_code'], $coupon)){
				WC()->cart->remove_cart_item($key);
			}
		}
	}
}

function avoskin_is_wfc_freeship(){
	$coupon = WC()->cart->get_applied_coupons();
	$cart = WC()->cart->get_cart();
	if(is_array($coupon) && !empty($coupon) && count($cart) == 1){
		foreach($coupon as $c){
			$freeship = get_post_meta(wc_get_coupon_id_by_code($c), 'wfc_freeship', true);
			return ($freeship == 'yes') ? true : false;
		}
	}
	
	return false;
}
function avoskin_has_wfc_coupon_applied(){
	$coupons = WC()->cart->get_applied_coupons();
	if(is_array($coupons) && !empty($coupons) ){
		foreach($coupons as $c){
			$coupon = new WC_Coupon($c);
			if($coupon->get_discount_type() == 'avo_wfc'){
				return true;
			}
		}
	}
	
	return false;
}

function avoskin_validate_wfc_after_login_register($user_id){
	$coupon = avoskin_has_wfc_coupon();
	$maybe_redirect_checkout = false;
	if($coupon != 'nope'){
		$maybe_redirect_checkout = true;
		$c = new WC_Coupon($coupon);
		if($c){
			$emails = $c->get_email_restrictions();
			if(!empty($emails)){
				$user = get_user_by('id', (int)$user_id);
				if(!in_array($user->data->user_email, $emails)){
					WC()->cart->remove_coupon( $coupon );
					$maybe_redirect_checkout = false;
				}
			}	
		}
	}
	if($maybe_redirect_checkout){
		$cart = WC()->cart->get_cart();
		$coupon = WC()->cart->get_applied_coupons();
		$coupon = (is_array($coupon)) ? $coupon : [];
		if(is_array($cart) && !empty($cart)){
			foreach($cart as $key => $value){
				if(isset($value['wfc_code']) && $value['wfc_code'] != '' && in_array($value['wfc_code'], $coupon)){
					return 'checkout';
				}
			}
		}	
	}
	
	return 'normal';
}

function avoskin_payment_method_html(){
	ob_start();
		$payment =  WC()->payment_gateways->get_available_payment_gateways();
	?>
		<div class="payment">
			<h3><?php _e('Payment','avoskin');?></h3>
			<div class="form-basic">
				<?php if(is_array($payment) && !empty($payment)):?>
					<?php foreach($payment as $p => $v):
						if($p == 'midtrans'):
							$method = ['name' => $p,'title' => $v->title];
							$midtrans = get_option('woocommerce_midtrans_settings');
							$channel = $midtrans['midtrans_payment_enabled'];
							if(is_array($channel) && !empty($channel)):
								$template = [
									'bca_va' => ['text' => 'Bank BCA','width' => '63'],
									'echannel' => ['text' => 'Bank Mandiri','width' => '68'],
									'bni_va' => ['text' => 'Bank BNI','width' => '62'],
									'bri_va' => ['text' => 'Bank BRI','width' => '80'],
									'permata_va' => ['text' => 'Bank Permata','width' => '99'],
									'credit_card' => ['text' => 'Credit Card','width' => '90'],
									'gopay' => ['text' => 'GoPay/QRIS','width' => '100'],
									'alfamart' => ['text' => 'Alfamart/Alfamidi/Dan+Dan','width' => '50'],
									'akulaku' => ['text' => 'Akulaku','width' => '95'],
									'shopeepay' => ['text' => 'ShopeePay','width' => '100']
								];
								$temp_channel = get_post_meta(get_option('page_on_front'), 'payment_order', true);
								foreach($temp_channel as $key => $t){
									if(!in_array($t, $channel)){
										unset($temp_channel[$key]);
									}
								}
								$channel = $temp_channel;
						?>
								<div class="dotdio blocky">
									<?php $i=1;foreach($channel as $c):
										$bank = [
											'name' => $c,
											'title' => wp_strip_all_tags($template[$c]['text'])
										];
									?>
										<label class="channel-<?php echo $c ;?>"> <!-- ::CRWFC:: -->
											<input type="radio" name="channel" value='<?php echo wp_json_encode($bank);?>' <?php echo ($i == 1) ? 'checked="checked"' : '';?> />
											<span>
												<?php echo $template[$c]['text'];?>
												<?php if($c == 'alfamart'):?>
													<small><?php echo $midtrans['midtrans_payment_inf0_'.$c] ;?></small>
												<?php endif;?>
											</span>
											<img src="<?php avoskin_dir();?>/assets/img/bank/<?php echo ($c != 'gopay') ? $c : $c.'-qris' ;?>.png" width="<?php echo $template[$c]['width'];?>" <?php echo ($c == 'gopay') ? 'style="position: relative;top:2px;"' : '' ;?>/>
											<?php if($c != 'alfamart'):?>
												<small><?php echo $midtrans['midtrans_payment_inf0_'.$c] ;?></small>
											<?php endif;?>
										</label>
									<?php $i++;endforeach;?>
								</div><!-- end of dotdio -->
								<input type="hidden" name="payment" value='<?php echo wp_json_encode($method);?>' />
							<?php endif;?>
						<?php endif;?>
					<?php endforeach;?>
				<?php endif;?>
			</div><!-- end of form basic -->
		</div><!-- end of payment -->
	<?php
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}