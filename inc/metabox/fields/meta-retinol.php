<?php
$retinol = new_cmb2_box([
        'id'            => 'retinol_content_settings',
        'title'         => __( 'Retinol Series Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-retinol.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'banner'    => [
                        'label' => __('Banner/Video', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'benefit' => [
                        'label' => __('Benefit', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'result' => [
                        'label' => __('Result', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);


$retinol->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at homepage',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
                'banner' => 'Banner',
                'video' => 'Video',
		'intro' => 'Intro',
                'benefit' => 'Benefit',
                'result' => 'Result'
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Banner',
        'id'   => 'banner_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Banner' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'banner',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Video Thumbnail',
        'id'   => 'banner_thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Thumbnail' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'banner',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Video URL',
        'id'      => 'banner_video',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'banner',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$retinol->add_field([
        'name' => 'Thumbnail',
        'id'   => 'intro_thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Thumbnail' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$retinol->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$retinol->add_field([
        'name' => 'Button Text',
        'id'      => 'intro_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Button URL',
        'id'      => 'intro_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$benefit = $retinol->add_field( [
        'id'          => 'benefit_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Benefit Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$retinol->add_group_field( $benefit, [
        'name' => 'Thumbnail',
        'id'   => 'thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Thumbnail' // Change upload button text. Default: "Add or Upload File"
        ]
] );

$retinol->add_group_field( $benefit, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$retinol->add_group_field( $benefit, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$retinol->add_field([
        'name' => 'Thumbnail',
        'id'   => 'result_thumbnail',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Thumbnail' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'result',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Title',
        'id'      => 'result_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'result',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Subtitle',
        'id'      => 'result_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'result',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Button Text',
        'id'      => 'result_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'result',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$retinol->add_field([
        'name' => 'Button URL',
        'id'      => 'result_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'result',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
