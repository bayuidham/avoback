<?php
//Create meta for fetaured product
add_action('add_meta_boxes', 'avoskin_product_gift_metabox');

function avoskin_product_gift_metabox(){
        add_meta_box(
                'avoskin_product_gift_metabox', // $id
                __('Hide this product?', 'avoskin'),
                'avoskin_product_gift_view', //callback
                'product', // $page
                'side', // $context
                'low'); // $priority
}

function avoskin_product_gift_view(){
        global $post;
        $gift = get_post_meta($post->ID, 'product_gift', true);
        $gift = ($gift != '') ? $gift : 'no';
        wp_nonce_field( 'avoskin_product_gift_meta_box_nonce', 'meta_box_nonce' );
        ?>
        <div class="meta-wrap-product-gift">
                <p>
                        <select class="widefat" name="avoskin_product_gift">
                                <option value="no" <?php selected('no', $gift);?>>No</option>
                                <option value="yes" <?php selected('yes', $gift);?>>Yes</option>       
                        </select>
                </p>
        </div>
        <?php
}

add_action('save_post', 'avoskin_product_gift_save_metabox');
function avoskin_product_gift_save_metabox($post_id){
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
                return;
        }
        // if our nonce isn't there, or we can't verify it, bail
        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'avoskin_product_gift_meta_box_nonce' ) ) {
                return;
        }
         
        // if our current user can't edit this post, bail
        if( !current_user_can( 'edit_post' ) ) {
                return;
        }
        if(isset( $_POST['avoskin_product_gift']) && $_POST['avoskin_product_gift'] != '') {
                update_post_meta( $post_id, 'product_gift', $_POST['avoskin_product_gift'] );        
        }
        
}