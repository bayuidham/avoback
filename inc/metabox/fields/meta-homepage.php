<?php
$homepage = new_cmb2_box([
        'id'            => 'homepage_content_settings',
        'title'         => __( 'Homepage Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-homepage.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'slider' => [
                        'label' => __('Slider', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'feat'    => [
                        'label' => __('Featured Product', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'avostore'    => [
                        'label' => __('Avo Store', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'testimonial'    => [
                        'label' => __('Testimonial', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'flash'    => [
                        'label' => __('Flash Sale', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'best'    => [
                        'label' => __('Best Seller Product', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'instagram'    => [
                        'label' => __('Instagram', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'blog'    => [
                        'label' => __('Blog', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);

$homepage->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at homepage',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'slider' => 'Slider',
                'feat' => 'Featured Product',
                'avostore' => 'Avo Store',
		'testimonial' => 'Testimonial',
		'flash' => 'Flash Sale',
		'best' => 'Best Seller Product',
		'instagram' => 'Instagram',
		'blog' => 'Blog'
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$slide = $homepage->add_field( [
        'id'          => 'slide_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Slide Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'slider',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_group_field( $slide, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage->add_group_field( $slide, [
        'name' => 'Mobile Image',
        'id'   => 'mobile_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage->add_group_field( $slide, [
        'name' => 'Link',
        'id'      => 'link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);


$homepage->add_field([
        'name' => 'Image',
        'id'   => 'feat_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
	'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'feat_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name'    => 'Text',
        'id'      => 'feat_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_field([
        'name' => 'Button Text',
        'id'      => 'feat_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Button Link',
        'id'      => 'feat_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$feature = $homepage->add_field( [
        'id'          => 'feat_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Feature Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'feat',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$homepage->add_group_field( $feature, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$homepage->add_field([
        'name' => 'Image',
        'id'   => 'avostore_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
	'tab'  => 'avostore',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'avostore_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'avostore',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name'    => 'Text',
        'id'      => 'avostore_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'avostore',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_field([
        'name' => 'Link',
        'id'      => 'avostore_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'avostore',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$testimonial = $homepage->add_field( [
        'id'          => 'testi_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Testimonial Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'testimonial',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_group_field( $testimonial, [
        'name' => 'Logo',
        'id'   => 'logo',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage->add_group_field( $testimonial, [
        'name' => 'From',
        'id'      => 'from',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$homepage->add_group_field( $testimonial, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'flash_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
	'name' => 'Flash Sale End-Time',
	'id'   => 'flash_end',
	'type' => 'text_datetime_timestamp',
	'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$flash_product = $homepage->add_field( [
        'id'          => 'flash_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Flash Sale Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_group_field( $flash_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'best_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name'    => 'Text',
        'id'      => 'best_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$best_product = $homepage->add_field( [
        'id'          => 'best_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Best Seller Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage->add_group_field( $best_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'ig_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'instagram',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
$homepage->add_field([
        'name' => 'Instagram Username',
        'id'      => 'ig_username',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'instagram',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Instagram ID',
        'id'      => 'ig_id',
        'desc'    => 'Get your Instagram user ID <a href="https://codeofaninja.com/tools/find-instagram-user-id" target="_blank">HERE</a>.',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'instagram',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$raw_account = get_option('sb_instagram_settings');
$account = [];
if(is_array($raw_account) && !empty($raw_account) && isset($raw_account['connected_accounts']) && is_array($raw_account['connected_accounts']) && !empty($raw_account['connected_accounts'])){
        $raw_account = $raw_account['connected_accounts'];
        foreach($raw_account as $ra){
                $account[$ra['username']] = $ra['username'];
        }        
}

$homepage->add_field([
        'name' => 'Instagram Account',
        'id'      => 'ig_account',
        'type'    => 'select',
        'options' => $account,
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'instagram',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Title',
        'id'      => 'blog_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'blog',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Total',
        'id'      => 'blog_total',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'blog',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Source',
        'id'      => 'blog_source',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'blog',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage->add_field([
        'name' => 'Link',
        'id'      => 'blog_link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'blog',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
