<?php

$landblog = new_cmb2_box( [
        'id'            => 'landblog_content_settings',
        'title'         => __( 'Landing Blog Settings', 'zahra' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => ['page-landing-blog.php'] ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
] );

$landblog->add_field([
        'name' => 'Top Story Title',
        'id'      => 'story_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$landblog->add_field([
        'name' => 'Top Story Posts',
        'id'      => 'story_post',
        'type'    => 'pw_multiselect',
        'options' => avoskin_get_cpt('post')
]);

$landblog->add_field([
        'name' => 'Category Title',
        'id'      => 'cat_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$cats = $landblog->add_field( [
        'id'          => 'cat_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Category Item {#}', 'zahra' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'zahra' ),
                'remove_button' => __( 'Remove Item', 'zahra' ),
                'sortable'      => true, // beta
                'closed'     => true, // true to have the groups closed by default
        ]
]);

$landblog->add_group_field( $cats, [
        'name' => 'Category',
        'id'      => 'id',
        'type'    => 'pw_select',
        'options' => avoskin_get_tax('category')
]);

$landblog->add_group_field( $cats, [
        'name' => 'Thumbnail',
        'id'   => 'thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ]
] );

$landblog->add_field([
        'name' => 'Latest Title',
        'id'      => 'latest_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$landblog->add_field([
        'name' => 'Show Latest Section?',
        'id'      => 'latest_show',
        'type'    => 'radio_inline',
        'default' => 'yes',
        'options' => ['yes' => 'Yes', 'no' => 'No']
] );

$landblog->add_field([
        'name' => 'Product Image',
        'id'      => 'product_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$landblog->add_field([
        'name' => 'Product Title',
        'id'      => 'product_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$landblog->add_field([
        'name' => 'Product Subtitle',
        'id'      => 'product_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$landblog->add_field([
        'name' => 'Product Button',
        'id'      => 'product_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$landblog->add_field([
        'name' => 'Product Link',
        'id'      => 'product_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

