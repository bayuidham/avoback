<?php

$halfpage = new_cmb2_box( [
        'id'            => 'meta_half_page_settings',
        'title'         => __( 'Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => ['page-half.php', 'page-faq.php', 'page-lady-boss.php'] ],
        'object_types'     => ['page' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );

$halfpage->add_field([
        'name' => 'Menu',
        'id'      => 'half_menu',
        'type'    => 'select',
        'default' => '',
        'options' => avoskin_get_menu(),
] );

$halfpage->add_field([
        'name' => 'Button Text',
        'id'      => 'half_button_txt',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$halfpage->add_field([
        'name' => 'Button Link',
        'id'      => 'half_button_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$halfpage->add_field([
        'name' => 'Button Color',
        'id'      => 'half_button_color',
        'type'    => 'select',
        'default' => 'green',
        'options' => [
                'green' => 'Green',
                'orange' => 'Orange',
                'pink' => 'Pink',
                'white' => 'White',
                'hollow' => 'Hollow'
        ]
] );
