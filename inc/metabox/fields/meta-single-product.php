<?php

$product = new_cmb2_box( [
        'id'            => 'meta_single_product_settings',
        'title'         => __( 'Tab Content', 'avoskin' ),
        'object_types'     => ['product' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'normal',
        'priority'      => 'default',
] );

$tab = $product->add_field( [
        'id'          => 'avotab_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Tab Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
]);

$product->add_group_field( $tab, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$product->add_group_field( $tab, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$product->add_group_field( $tab, [
        'name' => 'Video URL',
        'id'      => 'video',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$product->add_group_field( $tab, [
        'name' => 'Video Thumbnail',
        'id'   => 'thumbnail',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Thumbnail' // Change upload button text. Default: "Add or Upload File"
        ],
] );


$side_product = new_cmb2_box( [
        'id'            => 'meta_single_product_sidebar_settings',
        'title'         => __( 'Product Tagline', 'avoskin' ),
        'object_types'     => ['product' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'default',
] );


$side_product->add_field([
        'name' => '',
        'id'      => 'product_tagline',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$limit = new_cmb2_box( [
        'id'            => 'meta_single_product_pruchase_limit_settings',
        'title'         => __( 'Purchase Limit', 'avoskin' ),
        'object_types'     => ['product' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );


$limit->add_field([
        'name' => '',
        'id'      => 'product_purchase_limit',
        'type'    => 'text',
        'default' => '',
        'attributes' => [
                'type' => 'number',
                'pattern' => '\d*',
        ],
        'sanitization_cb' => 'absint',
        'escape_cb'       => 'absint',
]);

$empty = new_cmb2_box( [
        'id'            => 'meta_single_product_empty_settings',
        'title'         => __( 'Empty Stock Text', 'avoskin' ),
        'object_types'     => ['product' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );


$empty->add_field([
        'name' => '',
        'id'      => 'product_empty_text',
        'type'    => 'text',
        'default' => 'Out of Stock',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$hide_price = new_cmb2_box( [
        'id'            => 'meta_single_product_hide_price_settings',
        'title'         => __( 'Hide Price?', 'avoskin' ),
        'object_types'     => ['product' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );

$hide_price->add_field([
        'name' => '',
        'id'      => 'product_hide_price',
        'type'    => 'radio_inline',
        'default' => 'no',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes'
        ],
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

