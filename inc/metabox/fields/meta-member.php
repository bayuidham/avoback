<?php
//Create meta for fetaured product
add_action('add_meta_boxes', 'avoskin_join_member_metabox');

function avoskin_join_member_metabox(){
        add_meta_box(
                'avoskin_join_member_metabox', // $id
                __('Member Info', 'avoskin'),
                'avoskin_join_member_view', //callback
                'avoskin-member', // $page
                'normal', // $context
                'high'); // $priority
}

function avoskin_join_member_view(){
        global $post;
        $fields = [
                'name' => __('Name', 'avoskin'),
                'no_ktp' => __('No. KTP', 'avoskin'),
                'email' => __('Email', 'avoskin'),
                'dob' => __('Date of Birth', 'avoskin'),
                'phone' => __('Phone', 'avoskin'),
                'add_contact' => __('Additional Contact', 'avoskin'),
                'city' => __('City', 'avoskin'),
                'address' => __('Address', 'avoskin'),
        ]
        ?>
        <div class="meta-wrap-join-member">
                <style type="text/css">
                        .meta-wrap-join-member{
                                padding:10px 0;
                        }
                        .meta-wrap-join-member table{
                                width: 100%;
                        }
                        .meta-wrap-join-member table td{
                                width: calc(100% - 200px);
                                padding: 0 0 10px;
                        }
                        .meta-wrap-join-member table td:first-child{
                                width: 200px;
                                font-weight: 600;
                        }
                </style>
                <table>
                        <?php foreach($fields as $f => $v):?>
                                <?php if(get_post_meta($post->ID, $f, true) != ''):?>
                                        <tr>
                                                <td><?php echo $v ;?></td>
                                                <td>: <?php echo get_post_meta($post->ID, $f, true) ;?></td>
                                        </tr>
                                <?php endif;?>
                        <?php endforeach;?>
                </table>
        </div><!-- end of meta wrap join member -->
        <?php
}