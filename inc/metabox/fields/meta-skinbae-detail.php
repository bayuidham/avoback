<?php
$skinbaed = new_cmb2_box([
        'id'            => 'skinbaed_content_settings',
        'title'         => __( 'Skinbae Detail Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-skinbae-detail.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'navi' => [
                        'label' => __('Navigation', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'function'    => [
                        'label' => __('Function', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'cta'    => [
                        'label' => __('Call to Action', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);


$skinbaed->add_field([
        'name' => 'Yourskinbae',
        'id'      => 'navi_your',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbaed->add_field([
        'name' => 'Overview',
        'id'      => 'navi_over',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$skinbaed->add_field([
        'name' => 'Buy Link',
        'id'      => 'navi_buy',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbaed->add_field([
        'name' => 'Image',
        'id'      => 'intro_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbaed->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbaed->add_field([
        'name' => 'Subtitle',
        'id'      => 'intro_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbaed->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);



$function = $skinbaed->add_field( [
        'id'          => 'function_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Functions Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'function',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbaed->add_group_field( $function, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$skinbaed->add_group_field( $function, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$skinbaed->add_group_field( $function, [
        'name'    =>  ' Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);


$skinbaed->add_field([
	'name' => 'Image',
	'id'   => 'cta_img',
	'type' => 'file_list',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbaed->add_field([
        'name'    =>  'Text',
        'id'      => 'cta_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbaed->add_field([
        'name' => 'Action Label',
        'id'      => 'cta_action',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

