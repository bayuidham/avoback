<?php

$friday_order = new_cmb2_box([
        'id'            => 'friday_section_order_settings',
        'title'         => __( 'Section Order Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-landing-friday.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'side',
        'priority'      => 'high',
]);

$friday_order->add_field([
        'name'          => '',
        'id'            =>  'section_order',
        'type'          => 'order',
        // 'inline'        => true,
        'options'       =>  [
                'slider' => __('Slider', 'avoskin'),
                'pamflet' => __('Pamflet', 'avoskin'),
                'thumbnail' => __('Thumbnail', 'avoskin'),
                'flash' => __('Flash Sale', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
        ]
] );

// ::CRFRIDAY::
$friday = new_cmb2_box([
        'id'            => 'friday_content_settings',
        'title'         => __( 'Landing Friday Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-landing-friday.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'slider' => [
                        'label' => __('Slider', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'pamflet'    => [
                        'label' => __('Pamflet', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'trio'    => [
                        'label' => __('Thumbnail', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'flash'    => [
                        'label' => __('Flash Sale', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'best'    => [
                        'label' => __('Best Seller', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ]
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);

$friday->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at friday',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'slider' => __('Slider', 'avoskin'),
                'pamflet' => __('Pamflet', 'avoskin'),
                'trio' => __('Trio thumbnail', 'avoskin'),
                'flash' => __('Flash Sale', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$slide = $friday->add_field( [
        'id'          => 'slide_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Slide Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'slider',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$friday->add_group_field( $slide, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$friday->add_group_field( $slide, [
        'name' => 'Mobile Image',
        'id'   => 'mobile_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$friday->add_group_field( $slide, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$friday->add_group_field( $slide, [
        'name' => 'Subtitle',
        'id'      => 'subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$friday->add_group_field( $slide, [
        'name' => 'Link',
        'id'      => 'link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$friday->add_field([
        'name' => 'Image',
        'id'   => 'pamflet_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'pamflet',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Mobile Image',
        'id'   => 'pamflet_mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'pamflet',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Link',
        'id'   => 'pamflet_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'pamflet',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$trio = $friday->add_field( [
        'id'          => 'trio_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Thumbnail Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'trio',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$friday->add_group_field( $trio, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$friday->add_group_field( $trio, [
        'name' => 'Link',
        'id'   => 'url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$friday->add_field([
        'name' => 'Icon',
        'id'   => 'flas_icon',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Image',
        'id'   => 'flas_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Mobile Image',
        'id'   => 'flas_mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
// ::CRFLASH::
$friday->add_field([
	'name' => 'Flash Sale Start-Time',
	'id'   => 'flash_start',
	'type' => 'text_datetime_timestamp',
	'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
	'name' => 'Flash Sale End-Time',
	'id'   => 'flash_end',
	'type' => 'text_datetime_timestamp',
	'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Button',
        'id'      => 'flash_button',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name' => 'Link',
        'id'      => 'flash_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$flash_product = $friday->add_field( [
        'id'          => 'flash_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Flash Sale Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$friday->add_group_field( $flash_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$friday->add_field([
        'name' => 'Title',
        'id'      => 'best_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$friday->add_field([
        'name'    => 'Text',
        'id'      => 'best_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$friday->add_field([
        'name' => 'Pagination Total',
        'id'      => 'best_pagi',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$best_product = $friday->add_field( [
        'id'          => 'best_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Best Seller Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$friday->add_group_field( $best_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);