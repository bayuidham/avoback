<?php
//Create meta for fetaured product
add_action('add_meta_boxes', 'avoskin_package_status_metabox');

function avoskin_package_status_metabox(){
        add_meta_box(
                'avoskin_package_status_metabox', // $id
                __('Package Status', 'avoskin'),
                'avoskin_package_status_view', //callback
                'shop_order', // $page
                'side', // $context
                'high'); // $priority
}

function avoskin_package_status_view(){
        global $post;
        $current_status = get_post_meta($post->ID, 'package_status', true);
        $status = array_map( 'trim', explode( "\n", get_option( 'avo_package_status_list' ) ) );

        ?>
        <div class="meta-wrap-package-status">
                <p>
                        <select class="widefat" name="avoskin_package_status">
                                <?php foreach($status as $s):?>
                                        <option value="<?php echo $s;?>" <?php selected($current_status, $s);?>><?php echo $s;?></option>
                                <?php endforeach;?>
                        </select>
                </p>
        </div>
        <?php
}

add_action('save_post', 'avoskin_package_status_save_metabox');
function avoskin_package_status_save_metabox($post_id){
        
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
                return;
        }

        // if our current user can't edit this post, bail
        if( !current_user_can( 'edit_post' ) ) {
                return;
        }
        
        if(isset( $_POST['avoskin_package_status'])){
                update_post_meta( $post_id, 'package_status', $_POST['avoskin_package_status'] );        
        }
        
}