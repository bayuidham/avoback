<?php
$avostories = new_cmb2_box([
        'id'            => 'avostories_content_settings',
        'title'         => __( 'Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => ['page-avo-stories.php', 'page-lady-boss.php'] ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'slide' => [
                        'label' => __('Slide', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ]
]);

$avostories->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$avostories->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$slide = $avostories->add_field( [
        'id'          => 'slide_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Slide Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'slide',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$avostories->add_group_field( $slide, [
        'name' => 'Thumbnail',
        'id'   => 'thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$avostories->add_group_field( $slide, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$avostories->add_group_field( $slide, [
        'name' => 'Link',
        'id'      => 'url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);
