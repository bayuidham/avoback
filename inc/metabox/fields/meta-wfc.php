<?php

$wfc_order = new_cmb2_box([
        'id'            => 'wfc_section_order_settings',
        'title'         => __( 'Section Order Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-waste-for-change.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'side',
        'priority'      => 'high',
]);

$wfc_order->add_field([
        'name'          => '',
        'id'            =>  'section_order',
        'type'          => 'order',
        'options'       =>  [
                'intro' => __('Intro', 'avoskin'),
                'copy' => __('Copy', 'avoskin'),
                'redeem' => __('Redeem', 'avoskin'),
                'benefit' => __('Benefit', 'avoskin'),
                'info' => __('Info', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
                'cta' => __('Call to Action', 'avoskin'),
        ]
] );


// ::CRWFC::
$wfc = new_cmb2_box([
        'id'            => 'wfc_content_settings',
        'title'         => __( 'Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-waste-for-change.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'intro' => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'copy'    => [
                        'label' => __('Copy', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'redeem'    => [
                        'label' => __('Redeem', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'benefit'    => [
                        'label' => __('Benefit', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'info'    => [
                        'label' => __('Info', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'best'    => [
                        'label' => __('Best Seller', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'cta'    => [
                        'label' => __('Call to Action', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ]
        ]
]);

$wfc->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at waste for changes page',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'intro' => __('Intro', 'avoskin'),
                'copy' => __('Copy', 'avoskin'),
                'redeem' => __('Redeem', 'avoskin'),
                'benefit' => __('Benefit', 'avoskin'),
                'info' => __('Info', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
                'cta' => __('Call to Action', 'avoskin'),
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Banner',
        'id'   => 'intro_banner',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Mobile Banner',
        'id'   => 'intro_mbanner',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Title',
        'id'   => 'intro_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Button',
        'id'   => 'intro_btn',
        'type'    => 'text',
        'default' => '',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Link',
        'id'   => 'intro_url',
        'type'    => 'text',
        'default' => '',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$copy = $wfc->add_field( [
        'id'          => 'copy_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Copy Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'copy',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_group_field( $copy, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$wfc->add_group_field( $copy, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$wfc->add_group_field( $copy, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ]
]);

$wfc->add_field([
        'name' => 'Background',
        'id'   => 'redeem_bg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Mobile Background',
        'id'   => 'redeem_mbg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Title',
        'id'      => 'redeem_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name'    => 'Text',
        'id'      => 'redeem_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Placeholder',
        'id'      => 'redeem_placeholder',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Button',
        'id'      => 'redeem_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'redeem',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$wfc->add_field([
        'name' => 'Title',
        'id'      => 'benefit_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name'    => 'Text',
        'id'      => 'benefit_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Button',
        'id'      => 'benefit_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Link',
        'id'      => 'benefit_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$benefit = $wfc->add_field( [
        'id'          => 'benefit_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Benefit Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_group_field( $benefit, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$wfc->add_group_field( $benefit, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$wfc->add_field([
        'name' => 'Type',
        'id'      => 'info_type',
        'type'    => 'radio_inline',
        'options' => [
                'banner' => 'Banner',
                'text' => 'Text'
        ],
        'default' => 'banner',
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Banner',
        'id'   => 'info_banner',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Banner Mobile',
        'id'   => 'info_mbanner',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Image',
        'id'   => 'info_bg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Title',
        'id'      => 'info_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name'    => 'Text',
        'id'      => 'info_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'info',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Title',
        'id'      => 'best_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name'    => 'Text',
        'id'      => 'best_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Button',
        'id'      => 'best_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Link',
        'id'      => 'best_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$best_product = $wfc->add_field( [
        'id'          => 'best_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Best Seller Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_group_field( $best_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$wfc->add_field([
        'name' => 'Background',
        'id'   => 'cta_bg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Mobile Background',
        'id'   => 'cta_mbg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Thumbnail',
        'id'   => 'cta_thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$wfc->add_field([
        'name' => 'Title',
        'id'      => 'cta_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name'    => 'Text',
        'id'      => 'cta_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Shop Text',
        'id'      => 'cta_shop',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$wfc->add_field([
        'name' => 'Shop Link',
        'id'      => 'cta_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);