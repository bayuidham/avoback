<?php
$blp = new_cmb2_box([
        'id'            => 'blp_content_settings',
        'title'         => __( 'Avoskin BLP Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-avoskin-blp.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'cta'    => [
                        'label' => __('Call to Action', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'video' => [
                        'label' => __('Video', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'article' => [
                        'label' => __('Article', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'product' => [
                        'label' => __('Product', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);


$blp->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at Avoskin x BLP page',
	'id'      => 'blp_visibility',
	'type'    => 'multicheck',
	'options' => [
		'cta' => 'Call to Action',
                'intro' => 'Intro',
                'video' => 'Video',
                'article' => 'Article',
                'product' => 'Product'
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Title',
        'id'      => 'cta_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Button',
        'id'      => 'cta_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Link',
        'id'      => 'cta_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cta',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Image',
        'id'   => 'intro_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Subtitle',
        'id'      => 'intro_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$video = $blp->add_field( [
        'id'          => 'video_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Video Item {#}', 'zahra' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'zahra' ),
                'remove_button' => __( 'Remove Item', 'zahra' ),
                'sortable'      => true, // beta
                'closed'     => true, // true to have the groups closed by default
        ],
        'tab'  => 'video',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$blp->add_group_field( $video, [
        'name' => 'Thumbnail',
        'id'   => 'thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ]
] );

$blp->add_group_field( $video, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$blp->add_group_field( $video, [
        'name' => 'YouTube Video URL',
        'id'      => 'video',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$blp->add_field([
        'name' => 'Article Title',
        'id'      => 'article_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'article',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Article Posts',
        'id'      => 'article_post',
        'type'    => 'pw_multiselect',
        'options' => avoskin_get_cpt('post'),
        'tab'  => 'article',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$blp->add_field([
        'name' => 'Product Title',
        'id'      => 'product_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$blp->add_field([
        'name' => 'Product Item',
        'id'      => 'product_item',
        'type'    => 'pw_multiselect',
        'options' => avoskin_get_cpt(),
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$blp->add_field([
        'name' => 'See More Link',
        'id'      => 'article_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'article',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
