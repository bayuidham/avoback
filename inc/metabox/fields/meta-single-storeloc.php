<?php
// ::STORELOC::
$storeloc = new_cmb2_box( [
        'id'            => 'meta_single_storeloc_settings',
        'title'         => __( 'Location Settings', 'avoskin' ),
        'object_types'     => ['avoskin-storeloc' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'normal',
        'priority'      => 'default',
] );


$storeloc->add_field([
        'name' => 'Map Coordinate',
        'desc'    => 'Get your coordinate <a href="http://www.latlong.net/" target="_blank">HERE</a>. Copy and paste the "Latitude" and "Longitude" separated by comma.',
        'id'      => 'loc',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$storeloc->add_field([
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$storeloc->add_field([
        'name' => 'Subtitle',
        'id'      => 'subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$storeloc->add_field([
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);
