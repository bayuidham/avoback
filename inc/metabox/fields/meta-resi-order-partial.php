<div id="track-order" style="padding-top: 13px;margin-top: 15px;border-top: 1px solid #eee;">
        <?php
                $data_tracking = [
                        'resi' => $resi,
                        'courier' => $courier
                ];
        ?>
        <a href="#" class="button button-primary widefat" style="text-align: center;" data-tracking='<?php echo wp_json_encode($data_tracking);?>'>
                <?php _e('Tracking Order Detail','avoskin');?>
        </a>
        <style type="text/css">
                body.open-tracking{
                        height: 100vh;
                        overflow: hidden;
                }
                body.fetch-tracking:after{
                        display: block;
                        position: fixed;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                        background: rgba(255,255,255,.8);
                        content: '';
                        z-index: 99999;
                }
                .tracking-popup{
                        position: fixed;
                        z-index: 99999;
                        background: rgba(0,0,0,.8);
                        left: 0;
                        top: 0;
                        width: 100%;
                        height: 100%;
                        display: none;
                }
                body.open-tracking .tracking-popup{
                        display: block;
                }
                .tracking-popup .cls{
                        text-decoration: none;
                        position: absolute;
                        right: 30px;
                        top: 18px;
                        color: #fff;
                }
                .tracking-popup .cls .dashicons{
                        font-size: 24px;
                        height: auto;
                        width: auto;
                }
                .tracking-popup .layer{
                        margin: 50px auto 0;
                        padding: 0 20px 20px;
                        overflow: hidden;
                        -webkit-box-sizing: border-box;
                        -moz-box-sizing: border-box;
                        box-sizing: border-box;
                        background: #fff;
                        max-width: 850px;
                        -moz-border-radius: 10px ;
                        -webkit-border-radius: 10px ;
                        border-radius: 10px ;
                        position: relative;
                }
                .tracking-popup .layer .tracking-data .wrapper{
                        max-height: 60vh;
                        overflow-y: scroll;
                        padding: 10px 30px 10px 10px;
                        margin-right: -20px;
                }
                .table-basic table{
                        border-collapse: collapse;
                        border-spacing: 0;
                        width: 100%;
                        border-radius: 6px;
                        box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
                        background-color: #ffffff;
                        overflow: hidden;
                }
                .table-basic th{
                        background: #f5f5f5;
                        color: #22272b;
                        padding: 10px 15px;
                        font-weight: 600;
                        text-align: left;
                }
                .table-basic tbody td{
                        padding: 10px 15px;
                        text-align: left;
                        vertical-align: top;
                        border-top: 1px solid #eee;
                        font-size: 13px;
                        color: #22272b;
                }
                .table-basic tbody .topped td{
                        vertical-align: top;
                }
                .table-basic tbody tr:first-child td{
                        border: none;
                }
                .table-basic tbody td:last-child{
                        width: 140px;
                }
                .tracking-popup h3{
                        padding: 20px 35px;
                        margin: 0 -30px 20px;
                        color: #fff;
                        background: #007cba;
                }
        </style>
        <div class="tracking-popup">
                
                <div class="layer">
                        <a href="#" class="cls"><i class="dashicons dashicons-dismiss"></i></a>
                </div><!-- endof layer -->
        </div><!-- end of tracking popup -->
</div><!-- end of track order -->