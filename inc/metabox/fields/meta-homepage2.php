<?php

$homepage2_order = new_cmb2_box([
        'id'            => 'homepage_section_order_settings',
        'title'         => __( 'Section Order Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-homepage-2.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'side',
        'priority'      => 'low',
]);

$homepage2_order->add_field([
        'name'          => '',
        'id'            =>  'section_order',
        'type'          => 'order',
        // 'inline'        => true,
        'options'       =>  [
                'slider' => __('Slider', 'avoskin'),
                'series' => __('Special Series', 'avoskin'),
                'flash' => __('Flash Sale', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
		'recom' => __('Recommendation', 'avoskin'),
		'testi' => __('Testimonial', 'avoskin'),
		'caro' => __('Carousel', 'avoskin')
        ]
] );

$homepage2 = new_cmb2_box([
        'id'            => 'homepage2_content_settings',
        'title'         => __( 'Homepage Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-homepage-2.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'slider' => [
                        'label' => __('Slider', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'series'    => [
                        'label' => __('Series', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'flash'    => [
                        'label' => __('Flash Sale', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'best'    => [
                        'label' => __('Best Seller', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'recom'    => [
                        'label' => __('Recommendation', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'testi'    => [
                        'label' => __('Testimonial', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'caro'    => [
                        'label' => __('Carousel', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'subs'    => [
                        'label' => __('Subscribe', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);

$homepage2->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at homepage2',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'slider' => __('Slider', 'avoskin'),
                'series' => __('Special Series', 'avoskin'),
                'flash' => __('Flash Sale', 'avoskin'),
		'best' => __('Best Seller', 'avoskin'),
		'recom' => __('Recommendation', 'avoskin'),
		'testi' => __('Testimonial', 'avoskin'),
		'caro' => __('Carousel', 'avoskin'),
		'subs' => __('Subscribe', 'avoskin'),
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$slide = $homepage2->add_field( [
        'id'          => 'slide_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Slide Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'slider',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $slide, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage2->add_group_field( $slide, [
        'name' => 'Mobile Image',
        'id'   => 'mobile_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage2->add_group_field( $slide, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_group_field( $slide, [
        'name' => 'Subtitle',
        'id'      => 'subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_group_field( $slide, [
        'name' => 'Link',
        'id'      => 'link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_field([
        'name' => 'Title',
        'id'      => 'series_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'series',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name'    => 'Text',
        'id'      => 'series_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'series',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$series = $homepage2->add_field( [
        'id'          => 'series_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Series Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'series',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $series, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage2->add_group_field( $series, [
        'name' => 'Link',
        'id'      => 'link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_field([
        'name' => 'Icon',
        'id'   => 'flas_icon',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Image',
        'id'   => 'flas_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Mobile Image',
        'id'   => 'flas_mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );
// ::CRFLASH::
$homepage2->add_field([
	'name' => 'Flash Sale Start-Time',
	'id'   => 'flash_start',
	'type' => 'text_datetime_timestamp',
	'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
	'name' => 'Flash Sale End-Time',
	'id'   => 'flash_end',
	'type' => 'text_datetime_timestamp',
	'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Button',
        'id'      => 'flash_button',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Link',
        'id'      => 'flash_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$flash_product = $homepage2->add_field( [
        'id'          => 'flash_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Flash Sale Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'flash',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $flash_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$homepage2->add_field([
        'name' => 'Title',
        'id'      => 'best_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name'    => 'Text',
        'id'      => 'best_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_field([
        'name' => 'Button',
        'id'      => 'best_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Link',
        'id'      => 'best_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$best_product = $homepage2->add_field( [
        'id'          => 'best_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Best Seller Product {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'best',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $best_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);

$homepage2->add_field([
        'name' => 'Title',
        'id'      => 'recom_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'recom',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name'    => 'Text',
        'id'      => 'recom_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'recom',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$recom = $homepage2->add_field( [
        'id'          => 'recom_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Product Recommendation {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'recom',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $recom, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage2->add_group_field( $recom, [
        'name' => 'Mobile Image',
        'id'   => 'mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );


$homepage2->add_group_field( $recom, [
        'name' => 'Label',
        'id'      => 'label',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_group_field( $recom, [
        'name' => 'Products',
        'id'      => 'product',
        'type'    => 'pw_multiselect',
        'default' => '',
        'options'          =>  avoskin_get_cpt(),
] );

$homepage2->add_group_field( $recom, [
        'name' => 'Button',
        'id'      => 'button',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$homepage2->add_group_field( $recom, [
        'name' => 'Link',
        'id'      => 'url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$testimonial = $homepage2->add_field( [
        'id'          => 'testi_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Testimonial Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'testi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_group_field( $testimonial, [
        'name' => 'Logo',
        'id'   => 'logo',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$homepage2->add_group_field( $testimonial, [
        'name' => 'From',
        'id'      => 'from',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$homepage2->add_group_field( $testimonial, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$homepage2->add_field([
        'name' => 'Title',
        'id'      => 'caro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name'    => 'Text',
        'id'      => 'caro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$homepage2->add_field([
        'name' => 'Total',
        'id'      => 'caro_total',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Source',
        'id'      => 'caro_source',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Button',
        'id'      => 'caro_button',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Link',
        'id'      => 'caro_link',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'caro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

//$homepage2->add_field([
//        'name' => 'Form Action URL',
//        'id'      => 'subs_kirim_url',
//        'type'    => 'text',
//        'default' => '',
//        'sanitization_cb' => 'prefix_sanitize_text_callback',
//        'tab'  => 'subs',
//        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
//] );

$homepage2->add_field([
        'name' => 'Auth Token',
        'id'      => 'subs_cred_token',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'List ID',
        'id'      => 'subs_cred_list',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Username',
        'id'      => 'subs_cred_username',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Popup  Delay',
        'id'      => 'subs_popup_delay',
        'desc' => 'setup delay time before newsletter displayed (in second)',
        'type'    => 'text',
        'default' => '2',
        'attributes' => [
		'type' => 'number',
		'pattern' => '\d*',
	],
	'sanitization_cb' => 'absint',
        'escape_cb'       => 'absint',
        'tab'  => 'subs',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Image',
        'id'   => 'subs_popup_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
	'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Title',
        'id'      => 'subs_popup_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$homepage2->add_field([
        'name' => 'Text',
        'id'      => 'subs_popup_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'subs',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);
