<?php

$referral = new_cmb2_box( [
        'id'            => 'meta_single_referral_settings',
        'title'         => __( 'Referral Settings', 'avoskin' ),
        'object_types'     => ['avoskin-referral' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'normal',
        'priority'      => 'high',
] );

$referral->add_field([
        'name' => 'Referral Code',
        'id'      => 'referral_code',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Campaign Link',
        'id'      => 'campaign',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Referral Link',
        'id'      => 'url',
        'desc' => '<a href="#" class="button" id="generate">Generate and Copy Referral Link</a>',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Campaign Source',
        'id'      => 'campaign_source',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Campaign Medium',
        'id'      => 'campaign_medium',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Campaign Name',
        'id'      => 'campaign_name',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Email',
        'id'      => 'email',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$referral->add_field([
        'name' => 'Phone',
        'id'      => 'phone',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );
