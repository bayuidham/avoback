<?php

$coupon = new_cmb2_box( [
        'id'            => 'meta_single_coupon_settings',
        'title'         => __( 'Coupon Paramater', 'avoskin' ),
        'object_types'     => ['shop_coupon' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );

$coupon->add_field([
        'name' => 'Set as auto apply',
        'id'      => 'auto_apply',
        'type'    => 'radio_inline',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes'
        ],
        'default' => 'no',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$coupon->add_field([
        'name' => 'Apply coupon validation?',
        'id'      => 'combine_validation',
        'type'    => 'radio_inline',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes'
        ],
        'default' => 'no',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$courier = [];
$raw_courier = ['jne','pos','rpx','jnt','sicepat'];
foreach($raw_courier as $code){
        $service  =  TRO_Data::get_services($code);
        foreach($service as $s => $v){
		if($code != 'jne'){
			$courier[base64_encode($code.'_'.strtolower($s))] = strtoupper($code) .' ' . strtoupper($s);
		}else{
			if(strtolower($s) == 'jtr' ){
				$courier[base64_encode($code.'_'.strtolower($s))] = strtoupper($code) .' ' . strtoupper($s);	
			}else{
				foreach($v['vars'] as $var){
					$courier[base64_encode($code.'_'.strtolower($var))] = strtoupper($code) .' ' . strtoupper($var);		
				}	
			}
		}
        }
}

$coupon->add_field([
	'name'             => 'Select eligible courier',
	'id'               => 'combine_courier',
	'type'             => 'pw_multiselect',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  $courier,
]);

$payment = [
        'bca_va' =>'Bank BCA',
        'echannel' => 'Bank Mandiri',
        'bni_va' => 'Bank BNI',
        'bri_va' => 'Bank BRI',
        'permata_va' =>'Bank Permata',
        'credit_card' => 'Credit Card',
        'gopay' => 'GoPay/QRIS',
        'alfamart' => 'Alfamart/Alfamidi/Dan+Dan',
        'akulaku' => 'Akulaku',
        'shopeepay' => 'ShopeePay'
];

$coupon->add_field([
	'name'             => 'Select eligible payment',
	'id'               => 'combine_payment',
	'type'             => 'pw_multiselect',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  $payment,
]);


//Create meta for fetaured product
add_action('add_meta_boxes', 'avoskin_generate_coupon_metabox');

function avoskin_generate_coupon_metabox(){
        add_meta_box(
                'avoskin_generate_coupon_metabox', // $id
                __('Generate Coupon Code', 'avoskin'),
                'avoskin_generate_coupon_view', //callback
                'shop_coupon', // $page
                'side', // $context
                'low'); // $priority
}

function avoskin_generate_coupon_view(){
        global $post;
        $meta = avoskin_get_coupon_meta($post->ID);
        wp_nonce_field( 'avoskin_generate_coupon_meta_box_nonce', 'meta_box_nonce' );
	
        ?>
        <style type="text/css">
                .titem > div{
                        display: inline-block;
                        vertical-align: middle;
                        width: 48%;
                }
		.titem > div + div{
			float: right;
		}
                #gencop .titem label{
                        display: block;
                        margin: 5px 0;
                }
                #gencop .titem input{
                        max-width:98%;
                }
                #gencop .titem .button{
                        margin: 10px 0 0;
                }
		#gencop.fetching{
			position: relative;
		}
		#gencop.fetching:after{
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 9;
			content: '';
			background: rgba(255,255,255,.7);
		}
		#side-sortables .cmb-td > *{
			max-width:100% !important;
		}
        </style>
        <div id="gencop" class="meta-wrap-generate-coupon">
                <div class="tgencop">
                        <div  class="titem">
                                <div>
                                        <label>Max Character</label>
                                        <input type="number" id="maxchar" min="1" class="widefat" />
                                </div>
                                <div>
                                        <label>Total</label>
                                        <input type="number" id="total" min="1" class="widefat" />
                                </div>
				<span style="clear: both;display: block;width: 100%;"></span>
				<!-- ::CRWFC::-->
				<label>Prefix</label>
				<input type="text" id="prefix"  class="widefat" style="text-transform: uppercase;"/>
				<label>Coupon Code <small style="font-size:11px;font-style:italic;">(separated by comma)</small></label>
				<textarea class="widefat" id="coupon-code" style="height: 140px;"></textarea>
                                <a href="#" class="button" id="generate-code">Generate Code</a>
				<a href="#" class="button button-primary" id="duplicate-coupon" style="margin-left:5px;">Duplicate Coupon</a>
                        </div><!-- end of genrandom -->
			<div class="coupon-result"></div>
                </div><!-- end of tgencop -->
        </div>
        <script type="text/javascript">
                ;(function($){
			var ajaxQueue = $({});
			
			$.ajaxQueue = function(ajaxOpts) {
                                // hold the original complete function
                                var oldComplete = ajaxOpts.complete;
                              
                                // queue our ajax request
                                ajaxQueue.queue(function(next) {
                              
                                        // create a complete callback to fire the next event in the queue
                                        ajaxOpts.complete = function() {
                                                // fire the original complete if it was there
                                                if (oldComplete) oldComplete.apply(this, arguments);
                                                next(); // run the next query in the queue
                                        };
                                      
                                        // run the query
                                        $.ajax(ajaxOpts);
                                });
                        };
			
                        $(document).ready(function(){
                                
                                // ::CRWFC::
                                $('#generate-code').on('click', function(e){
                                        e.preventDefault();
                                        var self = $(this),
                                                parent = self.closest('.titem'),
						target = parent.find('textarea'),
						ances = $('#gencop'),
                                                valid = true;
                                        
                                        parent.find('input').each(function(){
                                                if ($(this).val() != '' || $(this).attr('id') == 'prefix') {}else{
                                                        valid = false;
                                                }
                                        });
                                        
                                        if (valid) {
                                                var data = {
                                                        maxchar: parent.find('#maxchar').val(),
                                                        total: parent.find('#total').val(),
							prefix: parent.find('#prefix').val()
                                                };
						if (!ances.hasClass('fetching')) {
							ances.addClass('fetching');
							$.post('<?php echo site_url('/wp-json/avoskin_coupon/v1/avoskin_generate_random_code/');?>', data, function(result){
								ances.removeClass('fetching');
								target.val(result);
							});	
						}
                                                
                                        }else{
                                                alert('Please input Max Character and Total coupon that you want to generate.')
                                        }
                                });
				
				$('#duplicate-coupon').on('click', function(e){
					e.preventDefault();
					var self = $(this),
                                                parent = self.closest('.titem'),
						target = parent.find('textarea'),
						ances = $('#gencop'),
						res = $('.coupon-result');
						
					if (target.val() != '') {
						var code = target.val().split(',');
						if (code.length > 0) {
							var length = code.length;
							ances.addClass('fetching');
							res.html('');
							$.each(code,function(k,v){
								   $.ajaxQueue({
									url: '<?php echo site_url('/wp-json/avoskin_coupon/v1/avoskin_duplicate_coupon_code/');?>',
									data: {
										code:v,
										meta: '<?php echo base64_encode(wp_json_encode($meta));?>'
									},
									type: 'POST',
									success: function(result) {
										$(result).appendTo(res);
										length--;
										if (length == 0) {
											target.val('');
											ances.removeClass('fetching');
										}
									}
								});
							});
						}
					}else{
						alert('Please input coupon code.')
					}
				});
                        });
                }(jQuery));
        </script>
      <?php
}

//CREATE REST ROUTE
add_action('rest_api_init', 'avoskin_register_generate_coupon_api');
function avoskin_register_generate_coupon_api(){
        $namespace = 'avoskin_coupon/v1';
        $route = [
                //User Login
                'avoskin_generate_random_code' => [
                        'method' => 'POST',
                        'args' => [
                                'maxchar' => ['required' => true],
                                'total' => ['required' => true]
                        ]
                ],
		'avoskin_duplicate_coupon_code' => [
                        'method' => 'POST',
                        'args' => [
                                'code' => ['required' => true],
                                'meta' => ['required' => true]
                        ]
                ],
		
        ];
                        
        foreach ($route as $r => $v) {
                register_rest_route($namespace, '/' . $r . '/', [
                        'methods' => $v['method'],
                        'callback' => $r,
                        'args' => $v['args']
                ]);
        }
}

function avoskin_generate_random_code($request){
        $params = $request->get_params();
	
	$code = '';
	$characters = "ABCDEFGHJKMNPQRSTUVWXYZ23456789";
	for($i=1;$i<= (int)$params['total'];$i++){
		$generated = substr( str_shuffle( $characters ),  0,  (int)$params['maxchar'] );
		if(avoskin_the_slug_exists(strtolower($generated))){
			$i--;	
		}else{
			$code .= $generated;
			if($i<(int)$params['total']){
				$code .= ',';
			}
		}
	}
        return $code;
}

function avoskin_duplicate_coupon_code($request){
        $params = $request->get_params();
	
	if(avoskin_the_slug_exists(strtolower($params['code']))){
		return '<p>Coupon '.$params['code'] .' already exists</p>';
	}else{
		$msg = '<p>Coupon '.$params['code'] .' failed to create</p>';
		$coupon_data = [
			'post_title' => strtoupper($params['code']),
			'post_author' => 1,
			'post_type' => 'shop_coupon',
			'post_status' => 'publish'
		];
		$post_id = wp_insert_post($coupon_data);
		if($post_id) {
			$msg = '<p>Coupon '.$params['code'] .' successfully created</p>';
			$meta = json_decode(base64_decode($params['meta']), true);
			foreach($meta as $m => $v){
				update_post_meta($post_id, $m, $v);
			}
		}
		return $msg;
	}
        
}

function avoskin_the_slug_exists($post_name) {
	global $wpdb;
	if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
		return true;
	} else {
		return false;
	}
}

function avoskin_get_coupon_meta($id){
	$meta = [];
	$key = get_post_custom_keys($id); // ::CRCOUPON::
	if(is_array($key) && !empty($key)){
		foreach($key as $k){
			if($k != '_edit_lock' && $k != '_edit_last'){
				$meta[$k] = get_post_meta($id, $k, true);	
			}
		}	
	}
	return $meta;
}


// ::CRWFC::
function avoskin_wfc_product(){
	$products = [];
	$posts_obj = get_posts([
		'fields' => 'ids',
		'post_type' => 'product',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'title',
		'order' => 'ASC',
		'meta_query' => [
			[
				'key' => 'product_gift',
				'value' => 'yes',
				'compare' => '='
			]
		]
	]);
	if(is_array($posts_obj) && !empty($posts_obj)){
		foreach($posts_obj as $p){
			$products[$p] = get_the_title($p);
		}
	}
	
	return $products;
}
$wfc = new_cmb2_box( [
        'id'            => 'meta_wfc_coupon_settings',
        'title'         => __( 'Waste for Change Setting', 'avoskin' ),
        'object_types'     => ['shop_coupon' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'side',
        'priority'      => 'low',
] );

$wfc->add_field([
        'name' => 'Set as Free Shipping?',
        'id'      => 'wfc_freeship',
        'type'    => 'radio_inline',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes'
        ],
        'default' => 'no',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$wfc->add_field([
	'name'             => 'Products',
	'id'               => 'wfc_products',
	'type'             => 'pw_multiselect',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_wfc_product(),
]);
