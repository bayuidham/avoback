<?php

$skinadvise = new_cmb2_box( [
        'id'            => 'meta_skin_advisor_page_settings',
        'title'         => __( 'Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' =>  'page-skin-advisor.php' ],
        'object_types'     => ['page' ], // Tells CMB2 to use term_meta vs post_meta
        'context'       => 'normal',
        'priority'      => 'high',
] );

$skinadvise->add_field([
        'name' => 'Title',
        'id'      => 'page_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );


$skinadvise->add_field([
        'name'    => 'Text',
        'id'      => 'page_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'love',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$skinadvise->add_field([
        'name' => 'Button Text',
        'id'      => 'btn_txt',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$skinadvise->add_field([
        'name' => 'Button Link',
        'id'      => 'btn_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
] );

$skinadvise->add_field([
        'name' => 'Enable Button?',
        'id'      => 'btn_enable',
        'type'    => 'radio_inline',
        'default' => 'no',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes',
        ]
] );
