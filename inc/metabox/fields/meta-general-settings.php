<?php

$general = new_cmb2_box([
        'id'            => 'general_content_settings',
        'title'         => __( 'General Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => ['page-homepage.php', 'page-homepage-2.php'] ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'header'    => [
                        'label' => __('Miscellaneous Text', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'optin' => [
                        'label' => __('Newsletter', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'login'    => [
                        'label' => __('Login', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'register'    => [
                        'label' => __('Register', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'forgot'    => [
                        'label' => __('Forgot Password', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'thank'    => [
                        'label' => __('Thank You Page Content', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ]
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);
$general->add_field([
        'name'    => 'Header Text',
        'id'      => 'header_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'header',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name'    => 'Qiscus Text',
        'id'      => 'qiscus_text',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
	'tab'  => 'header',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$general->add_field([
        'name' => 'Form Action URL',
        'id'      => 'optin_kirim_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

//
//$general->add_field([
//        'name' => 'Send Grid API',
//        'id'      => 'optin_sendgrid_api',
//        'type'    => 'text',
//        'default' => '',
//        'sanitization_cb' => 'prefix_sanitize_text_callback',
//        'tab'  => 'optin',
//        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
//] );
//
//$general->add_field([
//        'name' => 'Send Grid List ID',
//        'id'      => 'optin_sendgrid_list',
//        'type'    => 'text',
//        'default' => '',
//        'sanitization_cb' => 'prefix_sanitize_text_callback',
//        'tab'  => 'optin',
//        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
//] );

$general->add_field([
        'name' => 'Footer Newsletter Title',
        'id'      => 'optin_foot_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Footer Newsletter Text',
        'id'      => 'optin_foot_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Show Popup Newsletter?',
        'id'      => 'optin_popup_show',
        'type'    => 'radio_inline',
        'default' => 'no',
        'options' => [
                'no' => 'No',
                'yes' => 'Yes'
        ],
        'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Popup News Letter Delay',
        'id'      => 'optin_popup_delay',
        'desc' => 'setup delay time before newsletter displayed (in second)',
        'type'    => 'text',
        'default' => '2',
        'attributes' => [
		'type' => 'number',
		'pattern' => '\d*',
	],
	'sanitization_cb' => 'absint',
        'escape_cb'       => 'absint',
        'tab'  => 'optin',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Popup Newsletter Image',
        'id'   => 'optin_popup_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
	'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Popup Newsletter Title',
        'id'      => 'optin_popup_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Popup Newsletter Text',
        'id'      => 'optin_popup_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Popup Newsletter Info',
        'id'      => 'optin_popup_info',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'optin',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Login Title',
        'id'      => 'login_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'login',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Login Text',
        'id'      => 'login_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'login',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Login Info',
        'id'      => 'login_info',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'login',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$general->add_field([
        'name' => 'Register Title',
        'id'      => 'register_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'register',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Register Text',
        'id'      => 'register_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'register',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Register Info',
        'id'      => 'register_info',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'register',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$general->add_field([
        'name' => 'Forgot Password Title',
        'id'      => 'forgot_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'forgot',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name' => 'Forgot Password Text',
        'id'      => 'forgot_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'forgot',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Forgot Password Info',
        'id'      => 'forgot_info',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'forgot',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$general->add_field([
        'name' => 'Image',
        'id'   => 'thank_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
	'tab'  => 'thank',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$general->add_field([
        'name'    => 'Text',
        'id'      => 'thank_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
	'tab'  => 'thank',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$payment = new_cmb2_box([
        'id'            => 'homepage_payment_order_settings',
        'title'         => __( 'Payment Order Settings', 'anakecpr' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => ['page-homepage.php', 'page-homepage-2.php'] ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'side',
        'priority'      => 'low',
]);

$payment->add_field([
        'name'          => '',
        'id'            =>  'payment_order',
        'type'          => 'order',
        // 'inline'        => true,
        'options'       =>  [
                'bca_va' => 'Bank BCA',
                'echannel' =>'Bank Mandiri',
                'bni_va' => 'Bank BNI',
                'bri_va' => 'Bank BRI',
                'permata_va' =>'Bank Permata',
                'credit_card' => 'Credit Card',
                'gopay' => 'GoPay/QRIS',
                'alfamart' => 'Alfamart/Alfamidi/Dan+Dan',
                'akulaku' => 'Akulaku',
                'shopeepay' =>'ShopeePay'
        ]
] );