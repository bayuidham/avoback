<?php
//Create meta for fetaured product
add_action('add_meta_boxes', 'avoskin_resi_order_metabox');

function avoskin_resi_order_metabox(){
        add_meta_box(
                'avoskin_resi_order_metabox', // $id
                __('Order resi number', 'avoskin'),
                'avoskin_resi_order_view', //callback
                'shop_order', // $page
                'side', // $context
                'high'); // $priority
}

function avoskin_resi_order_view(){
        global $post;
        $resi = get_post_meta($post->ID, 'resi_order', true);
        $courier = get_post_meta($post->ID, 'shipping_courier', true);
        $couriers = [
                'jne' => 'JNE',
                'pos' => 'Pos Indonesia',
                'rpx' => 'RPX',
                'jnt' => 'J&T',
                'sicepat' => 'SiCepat',
        ];
        wp_nonce_field( 'avoskin_resi_meta_box_nonce', 'meta_box_nonce' );
        ?>
        <div class="meta-wrap-resi-number">
                <p>
                        <label style="display: block;margin: 0 0 5px;"><?php _e('Choose courier','avoskin');?></label>
                        <select class="widefat" name="avoskin_shipping_courier">
                                <?php foreach($couriers as $c => $v):?>
                                        <option value="<?php echo $c;?>" <?php selected($courier, $c);?>><?php echo $v;?></option>
                                <?php endforeach;?>
                        </select>
                </p>
                <p>
                        <label style="display: block;margin: 0 0 5px;"><?php _e('Input resi number','avoskin');?></label>
                        <input type="text" class="widefat" name="avoskin_resi_order" id="avoskin_resi_order" value="<?php echo $resi;?>" />
                </p>
        </div>
        <?php if($resi != ''):?>
                <div id="send-resi">
                        <input type="hidden" value="<?php echo $post->ID ;?>" name="order_id" />
                        <a href="#" class="button widefat" style="text-align: center;"><?php _e('Send Resi Number to Customer','avoskin');?></a>
                </div>
                <?php require_once get_parent_theme_file_path( '/inc/metabox/fields/meta-resi-order-partial.php' );?>
                <script type="text/javascript">
                        ;(function($){
                                $(document).ready(function(){
                                        $('#send-resi').each(function(){
                                                var self = $(this),
                                                        button = self.find('a'),
                                                        input = self.find('input');
                                                        
                                                button.on('click', function(e){
                                                        e.preventDefault();
                                                        $.post('<?php echo site_url('/wp-json/avoskin/v1/send_resi_info/');?>', {order: input.val()}, function(result){
                                                                alert(result);
                                                        });
                                                });
                                        });
                                        
                                        var fetched = false
                                        $('#track-order .button-primary').on('click', function(e){
                                                e.preventDefault();
                                                var self = $(this),
                                                        parent = self.closest('#track-order'),
                                                        target = parent.find('.tracking-popup .layer');
                                                
                                                if (fetched) {
                                                        $('body').addClass('open-tracking');
                                                }else{
                                                        $('body').addClass('fetch-tracking');
                                                        $.post('<?php echo site_url('/wp-json/avoskin/v1/admin_tracking_detail/');?>', self.data('tracking'), function(result){
                                                                $('body').removeClass('fetch-tracking');
                                                                fetched = true;
                                                                $(result.content).appendTo(target);
                                                                $('body').addClass('open-tracking');
                                                        });      
                                                }
                                                
                                        });
                                        
                                        $('.tracking-popup .cls').on('click', function(e){
                                                $('body').removeClass('open-tracking');
                                        });
                                });
                        }(jQuery));
                </script>
        <?php
        endif;
}

add_action('save_post', 'avoskin_resi_order_save_metabox');
function avoskin_resi_order_save_metabox($post_id){
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
                return;
        }
        // if our nonce isn't there, or we can't verify it, bail
        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'avoskin_resi_meta_box_nonce' ) ) {
                return;
        }
         
        // if our current user can't edit this post, bail
        if( !current_user_can( 'edit_post' ) ) {
                return;
        }
        update_post_meta( $post_id, 'resi_order', $_POST['avoskin_resi_order'] );
        update_post_meta( $post_id, 'shipping_courier', $_POST['avoskin_shipping_courier'] );        
        
        
}