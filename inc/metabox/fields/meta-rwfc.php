<?php

// ::CRWFC::
$rwfc = new_cmb2_box([
        'id'            => 'rwfc_content_settings',
        'title'         => __( 'Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-wfc-redeem.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'priority'      => 'high',
]);


$rwfc->add_field([
        'name' => 'Banner',
        'id'   => 'banner_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$rwfc->add_field([
        'name' => 'Banner Mobile',
        'id'   => 'banner_mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$rwfc->add_field([
        'name' => 'Title',
        'id'   => 'banner_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);
