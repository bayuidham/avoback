<?php
$miraculous = new_cmb2_box([
        'id'            => 'miraculous_content_settings',
        'title'         => __( 'Miraculous Series Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-miraculous.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'item' => [
                        'label' => __('Miraculous Item', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'product' => [
                        'label' => __('Related Products', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);


$miraculous->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$mira_item = $miraculous->add_field( [
        'id'          => 'mira_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Miraculous Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'item',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$miraculous->add_group_field( $mira_item, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ]
] );

$miraculous->add_group_field( $mira_item, [
        'name' => 'Mobile Image',
        'id'   => 'mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ]
] );

$miraculous->add_group_field( $mira_item, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$miraculous->add_group_field( $mira_item, [
        'name'    => 'Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$miraculous->add_group_field( $mira_item, [
        'name' => 'Link',
        'id'      => 'url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$miraculous->add_field([
        'name' => 'Title',
        'id'      => 'related_title',
        'type'    => 'text',
        'default' => 'Related Products',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$miraculous->add_field([
        'name' => 'See All Link',
        'id'      => 'related_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$related_product = $miraculous->add_field( [
        'id'          => 'related_product',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Related Products {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Product', 'avoskin' ),
                'remove_button' => __( 'Remove Product', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$miraculous->add_group_field( $related_product, [
	'name'             => 'Product',
	'id'               => 'product',
	'type'             => 'select',
	'show_option_none' => false,
	'default'          => '',
	'options'          =>  avoskin_get_cpt(),
]);
