<?php
$about = new_cmb2_box([
        'id'            => 'about_content_settings',
        'title'         => __( 'About Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-about.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                ],
                'discover' => [
                        'label' => __('Discover', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'cruelty' => [
                        'label' => __('Cruelty', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'love' => [
                        'label' => __('Love', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);


$about->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at homepage',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'intro' => 'Intro',
                'discover' => 'Discover',
                'cruelty' => 'Cruelty',
                'love' => 'Love',
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$about->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);


$about->add_field([
        'name' => 'Video Thumbnail',
        'id'   => 'disc_thumb',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Video URL',
        'id'      => 'disc_video',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Hastag',
        'id'      => 'disc_hash',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Title',
        'id'      => 'disc_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Button Text',
        'id'      => 'disc_btn',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Button URL',
        'id'      => 'disc_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'discover',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$about->add_field([
        'name' => 'Title',
        'id'      => 'cruelty_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'cruelty',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name'    => 'Text',
        'id'      => 'cruelty_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'cruelty',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$about->add_field([
        'name' => 'Image',
        'id'   => 'love_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'love',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name' => 'Title',
        'id'      => 'love_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'love',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$about->add_field([
        'name'    => 'Text',
        'id'      => 'love_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'love',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);
