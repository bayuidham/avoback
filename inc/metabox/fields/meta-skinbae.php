<?php
$skinbae = new_cmb2_box([
        'id'            => 'skinbae_content_settings',
        'title'         => __( 'Skinbae Settings', 'avoskin' ),
        'show_on'      => [ 'key' => 'page-template', 'value' => 'page-skinbae.php' ],
        'object_types'  => [ 'page'], // Post type
        'context'       => 'normal',
        'tab_style' => 'default',
        'priority'      => 'high',
        'tabs'      => [
                'visibility'    => [
                        'label' => __('Section Visibility', 'avoskin'),
                        'icon'  => 'dashicon-book-alt',
                        'show_on_cb' => 'cmb2_tabs_show_if_front_page',
                ],
                'navi' => [
                        'label' => __('Navigation', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'intro'    => [
                        'label' => __('Intro', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'variant'    => [
                        'label' => __('Variant', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'hero'    => [
                        'label' => __('Hero', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'product'    => [
                        'label' => __('Product', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'ingre'    => [
                        'label' => __('Ingredient', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'benefit'    => [
                        'label' => __('Benefit', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
		'function'    => [
                        'label' => __('Function', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'proof'    => [
                        'label' => __('Effiacy', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
                'person'    => [
                        'label' => __('Personalize', 'avoskin'),
                        'icon'  => 'dashicon-book-alt'
                ],
        ],
        // 'show_on_cb' => 'cmb2_tabs_show_if_front_page', // function should return a bool value
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
]);

$skinbae->add_field([
	'name'    => 'Choose Section',
	'desc'    => 'Checks multiple section to show it at skinbae',
	'id'      => 'visibility',
	'type'    => 'multicheck',
	'options' => [
		'intro' => 'Intro',
                'variant' => 'Variant',
                'hero' => 'Hero',
		'product'=> 'Product',
		'ingre' => 'Ingredient',
		'benefit' => 'Benefit',
		'function' => 'Function',
                'proof' => 'Effiacy',
                'person' => 'Personalize'
	],
        'tab'  => 'visibility',
	'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Yourskinbae',
        'id'      => 'navi_your',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Overview',
        'id'      => 'navi_over',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$skinbae->add_field([
        'name' => 'Buy Link',
        'id'      => 'navi_buy',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'navi',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Image',
        'id'      => 'intro_img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'intro_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'intro_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name'    => 'Text',
        'id'      => 'intro_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_field([
        'name' => 'YouTube Video URL',
        'id'      => 'intro_video',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'intro',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

foreach(['serum','toner'] as $v){
        $skinbae->add_field([
                'name' => ucfirst($v).' Image',
                'id'      => 'variant_'.$v.'_img',
                'type'    => 'file',
                // Optional:
                'options' => [
                    'url' => false, // Hide the text input for the url
                ],
                'text'    => [
                    'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
                ],
                'tab'  => 'variant',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ] );
        
        $skinbae->add_field([
                'name' =>  ucfirst($v). ' Title',
                'id'      => 'variant_'.$v.'_title',
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
                'tab'  => 'variant',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ] );
        
        $skinbae->add_field([
                'name' =>  ucfirst($v).' Subtitle',
                'id'      => 'variant_'.$v.'_subtitle',
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
                'tab'  => 'variant',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ] );
        
        $skinbae->add_field([
                'name'    =>  ucfirst($v).' Text',
                'id'      => 'variant_'.$v.'_text',
                'type'    => 'wysiwyg',
                'options' => [
                        'wpautop' => false, // use wpautop?
                        'media_buttons' => false, // show insert/upload button(s)
                        //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                        'textarea_rows' => 5, // rows="..."
                        'tabindex' => '',
                        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                        'editor_class' => '', // add extra class(es) to the editor textarea
                        'teeny' => true, // output the minimal editor config used in Press This
                        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                ],
                'tab'  => 'variant',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ]);
}

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'hero_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'hero',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'hero_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'hero',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$hero = $skinbae->add_field( [
        'id'          => 'hero_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Hero Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'hero',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_group_field( $hero, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$skinbae->add_group_field( $hero, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$skinbae->add_group_field( $hero, [
        'name' => 'Info',
        'id'      => 'info',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$skinbae->add_group_field( $hero, [
        'name' => 'Linked Product',
        'id'      => 'product',
        'type'    => 'pw_select',
        'options' => avoskin_get_cpt()
]);

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'product_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'product_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'product',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


foreach(['serum','toner'] as $v){
       ${'product_'.$v} = $skinbae->add_field( [
                'id'          => 'product_'.$v.'_item',
                'type'        => 'group',
                'repeatable'  => true, // use false if you want non-repeatable group
                'options'     => [
                        'group_title'   =>ucfirst($v) . __( ' Product Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                        'add_button'    => __( 'Add Another Item', 'avoskin' ),
                        'remove_button' => __( 'Remove Item', 'avoskin' ),
                        'sortable'      => true, // beta
                        'closed'     => false, // true to have the groups closed by default
                ],
                'tab'  => 'product',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ]);
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name' => 'Image',
                'id'   => 'img',
                'type'    => 'file',
                // Optional:
                'options' => [
                    'url' => false, // Hide the text input for the url
                ],
                'text'    => [
                    'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
                ],
        ] );
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name' => 'Title',
                'id'      => 'title',
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
        ]);
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name' => 'Info',
                'id'      => 'info',
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
        ]);
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name'    =>  ' Text',
                'id'      => 'text',
                'type'    => 'wysiwyg',
                'options' => [
                        'wpautop' => false, // use wpautop?
                        'media_buttons' => false, // show insert/upload button(s)
                        //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                        'textarea_rows' => 5, // rows="..."
                        'tabindex' => '',
                        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                        'editor_class' => '', // add extra class(es) to the editor textarea
                        'teeny' => true, // output the minimal editor config used in Press This
                        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                ],
        ]);
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name' => 'Link',
                'id'      => 'url',
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
        ]);
        
        $skinbae->add_group_field( ${'product_'.$v}, [
                'name'    => 'Color',
                'id'      => 'color',
                'type'    => 'colorpicker',
                'default' => '#fff3e4',
                'attributes' => [
                        'data-colorpicker' => json_encode( [ 'palettes' => ['#fff3e4', '#ffd4d1', '#edd4f4', '#e6f1fd','#dba07f'] ] ),
                ],
        ] );
        
}

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'ingre_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'ingre',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'ingre_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'ingre',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$ingre = $skinbae->add_field( [
        'id'          => 'ingre_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Ingredient Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'ingre',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_group_field( $ingre, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$skinbae->add_group_field( $ingre, [
        'name' => 'Mobile Image',
        'id'   => 'mimg',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$skinbae->add_group_field( $ingre, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$skinbae->add_group_field( $ingre, [
        'name'    =>  ' Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'benefit_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'benefit_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'benefit',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

for($i=1;$i<=3;$i++){
        $skinbae->add_field([
                'name' => 'Image '.$i,
                'id'   => 'benefit_img'.$i,
                'type'    => 'file',
                // Optional:
                'options' => [
                    'url' => false, // Hide the text input for the url
                ],
                'text'    => [
                    'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
                ],
                'tab'  => 'benefit',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ] );
        
        $skinbae->add_field([
                'name' => 'Title '.$i,
                'id'      => 'benefit_title'.$i,
                'type'    => 'text',
                'default' => '',
                'sanitization_cb' => 'prefix_sanitize_text_callback',
                'tab'  => 'benefit',
                'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
        ]);
}


$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'function_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'function',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'function_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'function',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );


$function = $skinbae->add_field( [
        'id'          => 'function_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Functions Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'function',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_group_field( $function, [
        'name' => 'Image',
        'id'   => 'img',
        'type'    => 'file',
        // Optional:
        'options' => [
            'url' => false, // Hide the text input for the url
        ],
        'text'    => [
            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ],
] );

$skinbae->add_group_field( $function, [
        'name' => 'Title',
        'id'      => 'title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
]);

$skinbae->add_group_field( $function, [
        'name'    =>  ' Text',
        'id'      => 'text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
]);

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'proof_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'proof',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'proof_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'proof',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name'    =>  ' Text',
        'id'      => 'proof_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'proof',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_field([
	'name' => 'Proof',
	'id'   => 'proof_item',
	'type' => 'file_list',
        'tab'  => 'proof',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_field([
        'name' => 'Title',
        'id'      => 'person_title',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name' => 'Subtitle',
        'id'      => 'person_subtitle',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$skinbae->add_field([
        'name'    =>  ' Text',
        'id'      => 'person_text',
        'type'    => 'wysiwyg',
        'options' => [
                'wpautop' => false, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                //'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => 5, // rows="..."
                'tabindex' => '',
                'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => true, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
        ],
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_field([
        'name' => 'Link',
        'id'      => 'person_url',
        'type'    => 'text',
        'default' => '',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
] );

$cats = $skinbae->add_field( [
        'id'          => 'cats_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Category Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);

$skinbae->add_group_field( $cats, [
        'name' => 'Product Category',
        'id'      => 'category',
        'type'    => 'pw_select',
        'options' => avoskin_get_tax()
]);

$person = $skinbae->add_field( [
        'id'          => 'person_item',
        'type'        => 'group',
        'repeatable'  => true, // use false if you want non-repeatable group
        'options'     => [
                'group_title'   => __( 'Personalize Item {#}', 'avoskin' ), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __( 'Add Another Item', 'avoskin' ),
                'remove_button' => __( 'Remove Item', 'avoskin' ),
                'sortable'      => true, // beta
                'closed'     => false, // true to have the groups closed by default
        ],
        'tab'  => 'person',
        'render_row_cb' => ['CMB2_Tabs', 'tabs_render_row_cb'],
]);
//
//$skinbae->add_group_field( $person, [
//        'name' => 'Image',
//        'id'   => 'img',
//        'type'    => 'file',
//        // Optional:
//        'options' => [
//            'url' => false, // Hide the text input for the url
//        ],
//        'text'    => [
//            'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
//        ],
//] );
//
//$skinbae->add_group_field( $person, [
//        'name' => 'Title',
//        'id'      => 'title',
//        'type'    => 'text',
//        'default' => '',
//        'sanitization_cb' => 'prefix_sanitize_text_callback',
//]);
//
//$skinbae->add_group_field( $person, [
//        'name' => 'Info',
//        'id'      => 'info',
//        'type'    => 'text',
//        'default' => '',
//        'sanitization_cb' => 'prefix_sanitize_text_callback',
//]);

$skinbae->add_group_field( $person, [
        'name' => 'Linked Product',
        'id'      => 'product',
        'type'    => 'pw_select',
        'options' => avoskin_get_cpt()
]);