<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * Be sure to replace all instances of 'avoskin_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */
/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */
if ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
}


/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function prefix_sanitize_text_callback( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization. 
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	$value = strip_tags( $value, '<button></button><b></b><span><span><br><br/><small></small><a></a><strong></strong><div></div><iframe></iframe><sup></sup><b></b><form></form><input>' );
	
	return $value;
}
/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array             $field_args Array of field parameters
 * @param  CMB2_Field object $field      Field object
 */
function avoskin_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}
function avoskin_show_page_meta($cmb) {
	$page = get_page_template_slug( $cmb->object_id() );
	if($page == true || empty($page)){
		if( empty($page) ){
			return true;
		}
	}
	return false;
}
function cmb2_tabs_show_if_front_page($cmb) {
	// Don't show this metabox if it's not the front page template.
	if (get_option('page_on_front') !== $cmb->object_id) {
		return false;
	}
	return true;
}

//Package status 
include dirname(__FILE__) .'/fields/meta-package-status.php';

//Resi Order metabox
include dirname(__FILE__) .'/fields/meta-resi-order.php';

//Join Member metabox
include dirname(__FILE__) .'/fields/meta-member.php';

//Produc gift
include dirname(__FILE__) .'/fields/meta-single-product-gift.php';

add_action( 'cmb2_init', 'avoskin_register_metabox' );
function avoskin_register_metabox() {
	
	//HOMEPAGE META
	include dirname(__FILE__) .'/fields/meta-homepage.php';
	include dirname(__FILE__) .'/fields/meta-homepage2.php';
	
	include dirname(__FILE__) .'/fields/meta-general-settings.php';
	
	//HALF PAGE
	include dirname(__FILE__) .'/fields/meta-halfpage.php';
	
	//FAQ PAGE
	include dirname(__FILE__) .'/fields/meta-faq.php';
	
	//ABOUT PAGE
	include dirname(__FILE__) .'/fields/meta-about.php';
	
	//AVO STORIES PAGE
	include dirname(__FILE__) .'/fields/meta-avo-stories.php';
	
	//SKIN ADVISOR PAGE
	include dirname(__FILE__) .'/fields/meta-skin-advisor.php';
	
	//PARTNER PAGE
	include dirname(__FILE__) .'/fields/meta-partner.php';
	
	//REFINING PAGE
	include dirname(__FILE__) .'/fields/meta-refining.php';
	
	//RETINOL PAGE
	include dirname(__FILE__) .'/fields/meta-retinol.php';
	
	//SINGLE PRODUCT
	include dirname(__FILE__) .'/fields/meta-single-product.php';
	
	//SINGLE REFERRAL
	include dirname(__FILE__) .'/fields/meta-single-referral.php';
	
	//MIRACULOUS PAGE
	include dirname(__FILE__) .'/fields/meta-miraculous.php';
	
	//SKINBAE PAGE
	include dirname(__FILE__) .'/fields/meta-skinbae.php';
	
	//SKINBAE PAGE
	include dirname(__FILE__) .'/fields/meta-skinbae-detail.php';
	
	//COUPON META
	include dirname(__FILE__) .'/fields/meta-coupon.php';
	
	//LANDING BLOG PAGE
	include dirname(__FILE__) .'/fields/meta-landing-blog.php';
	
	//AVOSKIN BLP PAGE
	include dirname(__FILE__) .'/fields/meta-avoskin-blp.php';
	
	//AVOSKIN LANDING FRIDAY PAGE
	include dirname(__FILE__) .'/fields/meta-friday.php'; // ::CRFRIDAY::
	
	//AVOSKIN WASTE FOR CHANGE
	include dirname(__FILE__) .'/fields/meta-wfc.php'; // ::CRWFC::
	
	//AVOSKIN REDEEM WASTE FOR CHANGE
	include dirname(__FILE__) .'/fields/meta-rwfc.php'; // ::CRWFC::
	
	// ::STORELOC::
	include dirname(__FILE__) .'/fields/meta-single-storeloc.php';
	
}

//Include AddOns
require_once dirname(__FILE__) .'/addons/tabs/tabs.php';
require_once dirname(__FILE__) .'/addons/order/cmb2-field-order.php';
require_once dirname(__FILE__) .'/addons/select2/select2.php';