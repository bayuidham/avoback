<?php

//HEADER
if ( ! function_exists( 'avoskin_before_head' ) ) {
	function avoskin_before_head() {
		$home_id = get_option('page_on_front');
		$text = get_post_meta($home_id, 'header_text', true);
	?>
                <div id="shell">
                        <header id="top" <?php echo ($text != '') ? 'class="has-popup"' : '';?>>
				<?php if($text != ''):?>
					<div class="popup-info">
						<div class="wrapper">
							<div class="txt">
								<?php echo $text ;?>
							</div><!-- end of txt -->
							<a href="#" class="cls"></a>
						</div><!-- end of wrapper -->
					</div><!-- end of popup info -->
				<?php endif;?>
        <?php
	}
}

if ( ! function_exists( 'avoskin_after_head' ) ) {
	function avoskin_after_head() {
	?>
                </header><!-- end of top -->
                <div id="body">
        <?php
	}
}

if ( ! function_exists( 'avoskin_topbar_head' ) ) {
	function avoskin_topbar_head() {
	?>
                <div class="top-head">
                        <div class="wrapper">
                                <div class="holder">
                                        <div class="logo">
                                                <a href="<?php echo home_url('/');?>">
                                                        <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" />
                                                </a>
                                        </div><!-- end of logo -->
                                        
                                        <div class="account">
                                                <a href="#"><?php _e('Account','avoskin');?></a>
                                                <div class="dropholder">
                                                        <span><?php _e('Welcome','avoskin');?></span>
                                                        <ul>
                                                                <?php
                                                                        $args= [
                                                                                'theme_location'	=> (is_user_logged_in()) ? 'account_login' : 'account_not_login',
                                                                                'container'		=> false,
                                                                                'menu_class'		=> '',
                                                                                'items_wrap' => '%3$s' 
                                                                        ];
                                                                        wp_nav_menu($args);
                                                                ?>
                                                                <?php if(is_user_logged_in()):?>
                                                                        <li class="logout"><a href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' )) ); ?>"><?php _e('Logout','avoskin');?></a></li>
                                                                <?php endif;?>
                                                        </ul>
                                                </div>
                                        </div><!-- end of account -->
                                        <div class="clear"></div>
                                </div><!-- end of holder -->
                        </div><!-- end of wrapper -->
                </div><!-- end of top head -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_botbar_head' ) ) {
	function avoskin_botbar_head() {
	?>
                <div class="bot-head">
                        <div class="wrapper">
                                <div class="bot-logo">
                                        <a href="<?php echo home_url('/');?>">
                                                <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" />
                                        </a>
                                </div><!-- end of bot logo -->
                                <nav class="mainmenu">
					<a href="#" class="mobile-menu-trigger"></a>
					<?php
						$args= [
							'theme_location'	=> 'primary',
							'container'		=> false,
							'menu_class'		=> '',
							'walker' => new avoskin_nav_walker(),
						];
						wp_nav_menu($args);
					?>
                                        <div class="clear"></div>
                                </nav><!-- end of main menu -->
                                <div class="util">
                                        <div class="search">
                                                <a href="#"></a>
                                        </div><!-- end of search 
                                        --><div class="cart">
                                                <a href="#" class="trigger"><b><?php echo  WC()->cart->get_cart_contents_count();?></b></a>
                                        </div><!-- end of cart 
                                        --><div class="location">
						<?php $code = (ICL_LANGUAGE_CODE == 'en') ? '' : 'id_';?>
                                                <a href="<?php avoskin_get_page($code.'avostore');?>"><span><?php _e('Avostore','avoskin');?></span></a>
                                        </div><!-- end of location
                                        --><div class="account">
                                                <a href="#"><span><?php _e('Account','avoskin');?></span></a>
                                                <div class="dropholder">
                                                        <span><?php _e('Welcome','avoskin');?></span>
                                                        <ul>
                                                                <?php
                                                                        $args= [
                                                                                'theme_location'	=> (is_user_logged_in()) ? 'account_login' : 'account_not_login',
                                                                                'container'		=> false,
                                                                                'menu_class'		=> '',
                                                                                'items_wrap' => '%3$s' 
                                                                        ];
                                                                        wp_nav_menu($args);
                                                                ?>
                                                                <?php if(is_user_logged_in()):?>
                                                                        <li class="logout"><a href="<?php echo wp_logout_url( home_url('/') ); ?>"><?php _e('Logout','avoskin');?></a></li>
                                                                <?php endif;?>
                                                        </ul>
                                                </div>
                                        </div><!-- end of account -->
                                </div><!-- end of util -->
                                <div class="clear"></div>
                        </div><!-- end of wrapper -->
                </div><!-- end of bot bar -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_before_foot' ) ) {
	function avoskin_before_foot() {
	?>
		</div><!-- end of body -->
		<footer id="bottom">
	<?php
	}
}

if ( ! function_exists( 'avoskin_after_foot' ) ) {
	function avoskin_after_foot() {
	?>
			</footer><!-- end of bottom -->
			<!--<div id="responsive"></div>-->
		</div><!-- end of shell -->
	<?php
	
		//MENU MOBILE
		get_template_part('views/menuside');
		
		//SEARH POPUP
		get_template_part('views/popup-search');
		
		//CART SIDEBAR
		get_template_part('views/cartside');
		
		//POPUP LOGIN ONLY IF USER NOT LOGGED IN
		if(!is_user_logged_in()) get_template_part('views/login-signup');
		
		//POPUP REVIEW ONLY FOR PAGE PRODUCT DETAIL AND LOGGED IN USER
		if(is_singular('product') && is_user_logged_in()) get_template_part('views/popup-review');
		
		//NEWSLETTER 
		get_template_part('views/popup-newsletter');	
		
		//TO REGISTER JS ON FOOTER
		wp_footer();
		
		//CUSTOM JS ON FOOTER
		get_template_part('views/phpjs');
	}
}

if ( ! function_exists( 'avoskin_optin_foot' ) ) {
	function avoskin_optin_foot() {
		$home_id = get_option('page_on_front');
		$meta = [
			'title' => 'optin_foot_title',
			'text' => 'optin_foot_text',
			'form' => 'optin_form',
			'url' => 'optin_kirim_url',
			//'api' => 'optin_sendgrid_api',
			//'list' => 'optin_sendgrid_list'
                ];
                extract(avoskin_get_meta($home_id, $meta));
		
		//$credential = [
		//	'api' => $api,
		//	'list' => $list
		//];
		
		$credential = [
			'url' => $url
		];
	?>
		<div class="optin">
			<div class="wrapper">
				<div class="layer rowflex">
					<div class="caption">
						<h2><?php echo $title ;?></h2>
						<?php echo $text ;?>
					</div><!-- end of caption -->
					<form>
						<input type="email" value="" name="email" placeholder="<?php _e('Your Email Address','avoskin');?>" required="required">
						<input type="hidden" name="credential"  value='<?php echo base64_encode(wp_json_encode($credential));?>'>
						<button type="submit" class="button btn-white slimy has-loading"><?php _e('Subscribe','avoskin');?></button>
					</form>
				</div><!-- end of layer -->
			</div><!-- end of wrapper -->
		</div><!-- end of optin -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_widget_foot' ) ) {
	function avoskin_widget_foot() {
	?>
		<div class="footer-widget">
			<div class="wrapper">
				<div class="rowflex">
					<div class="item">
						<?php if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('footer-1') ) : ?>
						<?php endif; ?>
					</div><!-- end of item -->
					<div class="item">
						<?php if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('footer-2') ) : ?>
						<?php endif; ?>
					</div><!-- end of item -->
					<div class="item">
						<?php if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('footer-3') ) : ?>
						<?php endif; ?>
					</div><!-- end of item -->
					<div class="item">
						<?php if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('footer-4') ) : ?>
						<?php endif; ?>
					</div><!-- end of item -->
				</div><!-- end of rowflex -->
			</div><!-- end of wrapper -->
		</div><!-- end of footer widget -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_tribute_foot' ) ) {
	function avoskin_tribute_foot() {
	?>
		<div class="tribute">
			<div class="wrapper">
				<div class="util">
					<?php
						$lang = avoskin_langswitch();
						if(is_array($lang) && !empty($lang) && isset($lang['country']) && !empty($lang['country'])):
					?>
						<ul class="lang">
							<?php foreach($lang['country'] as $l => $v):?>
								<li>
									<?php if($lang['active'] == $l):?>
										<span><?php echo $v['label'] ;?></span>
									<?php else:?>
										<a href="<?php echo $v['url'] ;?>"><?php echo $v['label'] ;?></a>
									<?php endif;?>
								</li>
							<?php endforeach;?>
						</ul>
					<?php endif;?>
					<a href="#" class="totop"><i class="fa-angle-up"></i></a>
				</div><!-- end of util -->
				<div class="copy">
					<p><?php echo get_theme_mod('avoskin_copy');?></p>
				</div><!-- end of copy -->
				<div class="clear"></div>
			</div><!-- end of wrapper -->
		</div><!--end of tribute -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_navside_display' ) ) {
	function avoskin_navside_display() {
		$menu = wp_get_nav_menu_items(get_post_meta(get_the_ID(), 'half_menu', true));
		$link = get_post_meta(get_the_ID(), 'half_button_url', true);
		$btn = get_post_meta(get_the_ID(), 'half_button_txt', true);
		$color = get_post_meta(get_the_ID(), 'half_button_color', true);
	?>
		<aside class="navside">
			<?php if($link != '' && $btn != ''):?>
				<div class="centered">
                                        <a href="<?php echo $link ;?>" class="button slimy btn-<?php echo $color ;?>"><?php echo $btn ;?></a>
                                </div><!-- end of centered -->
			<?php endif;?>
			<?php if(is_array($menu) && !empty($menu)):?>
				<ul>
					<?php foreach($menu as $m):?>
						<li <?php echo ($m->object_id == get_the_ID()) ? 'class="current-menu-item"' : '' ;?>><a href="<?php echo $m->url ;?>"><?php echo $m->title;?></a></li>
					<?php endforeach;?>
				</ul>
			<?php endif;?>
			<?php if(is_array($menu) && !empty($menu)):?>
				<div class="dropselect large dark">
					<select>
						<?php foreach($menu as $m):?>
							<option value="<?php echo $m->url ;?>" <?php echo ($m->object_id == get_the_ID()) ? 'selected="selected"' : '' ;?>><?php echo $m->title;?></option>
						<?php endforeach;?>
					</select>
				</div><!-- end of drop select -->
			<?php endif;?>
		</aside>
	<?php
	}
}

if ( ! function_exists( 'avoskin_intro_about' ) ) {
	function avoskin_intro_about() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('intro', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'intro_title',
			'text' => 'intro_text'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="intro">
			<div class="wrapper">
				<h1 class="line-title"><?php echo $title ;?></h1>
				<div class="format-text">
					<?php echo $text ;?>
				</div><!-- end of format text -->
			</div><!-- end of wrapper -->
		</div><!-- end of intro -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_discover_about' ) ) {
	function avoskin_discover_about() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('discover', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'thumb' => 'disc_thumb',
			'video' => 'disc_video',
			'hash' => 'disc_hash',
			'title' => 'disc_title',
			'btn' => 'disc_btn',
			'url' => 'disc_url',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="discover">
			<div class="wrapper">
				<div class="rowflex">
					<div class="caption">
						<strong><?php echo $hash ;?></strong>
						<h2><?php echo $title ;?></h2>
						<?php if($btn != '' && $url != ''):?>
							<a href="<?php echo $url ;?>" class="button slimy"><?php echo $btn ;?></a>
						<?php endif;?>
					</div><!-- end of caption -->
					<figure>
						<a <?php echo ($video != '') ? 'href="'.$video.'" data-fancybox' : '';?>>
							<?php if($video != ''):?>
								<b><i class="fa-play"></i></b>
							<?php endif;?>
							<img src="<?php echo $thumb ;?>" alt="<?php the_title_attribute();?>" />
						</a>
					</figure>
					<?php if($btn != '' && $url != ''):?>
						<div class="centered">
							<a href="<?php echo $url ;?>" class="button slimy"><?php echo $btn ;?></a>
						</div><!-- end of centered -->
					<?php endif;?>
				</div><!-- end of rowflex -->
			</div><!-- end of wrapper -->
			<div class="ornament"></div>
		</div><!-- end of discover -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_cruelty_about' ) ) {
	function avoskin_cruelty_about() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('cruelty', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'cruelty_title',
			'text' => 'cruelty_text'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="cruelty">
			<div class="wrapper">
				<h2 class="line-title"><?php echo $title ;?></h2>
				<div class="format-text nudge-left">
					<?php echo $text ;?>
				</div>
			</div><!-- end of wrapper -->
		</div><!-- end of cruelty -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_love_about' ) ) {
	function avoskin_love_about() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('love', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'love_img',
                        'title' => 'love_title',
			'text' => 'love_text'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="love">
			<?php if($thumb != ''):?>
				<img src="<?php echo $thumb ;?>" alt="<?php the_title_attribute();?>" />
			<?php endif;?>
			<div class="wrapper">
				<div class="caption white-txt">
					<h2 class="line-title"><?php echo $title ;?></h2>
					<div class="format-text nudge-left">
						<?php echo $text ;?>
					</div><!-- endof format text -->
				</div><!-- end of caption -->
			</div><!-- end of wrapper -->
		</div><!-- end of love -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_banner_refining' ) ) {
	function avoskin_banner_refining() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('banner', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'banner' => 'intro_banner'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="banner">
			<img src="<?php echo $banner ;?>" alt="<?php the_title_attribute();?>" />        
		</div>
	<?php
	}
}

if ( ! function_exists( 'avoskin_copy_refining' ) ) {
	function avoskin_copy_refining() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('intro', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'intro_thumbnail',
			'title' => 'intro_title',
                        'text' => 'intro_text',
			'btn' => 'intro_btn',
			'url' => 'intro_url'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="copy">
			<div class="wrapper">
				<div class="rowflex switch">
					<?php if($thumb != ''):?>
						<figure><img src="<?php echo $thumb ;?>" /></figure>
					<?php endif;?>
					<div class="caption">
						<h2 class="line-title pink"><?php echo $title ;?></h2>
						<div class="format-text">
							<?php echo $text ;?>
						</div><!-- end of format text -->
						<?php if($url != ''):?>
							<a href="<?php echo $url ;?>" class="button slimy btn-pink"><?php echo $btn ;?></a>
						<?php endif;?>
					</div><!-- end of caption -->
				</div><!-- end of row flex -->
			</div><!-- end of wrapper -->
		</div><!-- end of copy -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_benefit_refining' ) ) {
	function avoskin_benefit_refining() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('benefit', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'item' => 'benefit_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="benefit">
			<div class="wrapper">
				<div class="rowflex">
					
					<?php foreach($item as $i):?>
						<div class="item">
							<figure><img src="<?php echo $i['thumb'] ;?>" alt="mira" /></figure>
							<div class="caption white-txt">
								<h3 class="line-title"><?php echo $i['title'] ;?></h3>
								<div class="format-text">
									<?php echo $i['text'] ;?>
								</div><!-- end of format text -->
							</div><!-- end of caption -->
						</div><!-- end of item -->
					<?php endforeach;?>
					
				</div><!-- end of rowflex -->
			</div><!--e nd of wrapper -->
		</div><!-- end of benefit -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_action_refining' ) ) {
	function avoskin_action_refining() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('result', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'result_thumbnail',
			'title' => 'result_title',
			'subtitle' => 'result_subtitle',
			'btn' => 'result_btn',
			'url' => 'result_url'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="action">
			<div class="wrapper">
				<div class="rowflex">
					<div class="caption">
						<strong><?php echo $subtitle ;?></strong>
						<h2><?php echo $title ;?></h2>
						<?php if($url != ''):?>
							<a href="<?php echo $url ;?>" class="button slimy btn-pink"><?php echo $btn ;?></a>
						<?php endif;?>
					</div><!-- end of caption -->
					<?php if($thumb != ''):?>
						<figure>
							<img src="<?php echo $thumb ;?>" />
							<?php if($url != ''):?>
								<figcaption class="centered">
									<a href="<?php echo $url ;?>" class="button slimy btn-pink"><?php echo $btn ;?></a>
								</figcaption><!-- end of centered -->
							<?php endif;?>
						</figure>
					<?php endif;?>
				</div><!-- end of row flex -->
			</div><!-- end of wrapper -->
		</div><!-- end of action -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_banner_retinol' ) ) {
	function avoskin_banner_retinol() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('banner', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'banner' => 'banner_img',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="banner">
			<img src="<?php echo $banner ;?>" alt="<?php the_title_attribute();?>" />
		</div><!-- end of banner -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_video_retinol' ) ) {
	function avoskin_video_retinol() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('video', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'banner_thumb',
			'video' => 'banner_video',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="video">
			<div class="wrapper">
				<div class="pusher">
					<figure>
						<a <?php echo ($video != '') ? 'href="'.$video.'" data-fancybox=""' : '';?>>
							<?php if($video != '') :?><b><i class="fa-play"></i></b><?php endif;?>
							<img src="<?php echo $thumb ;?>" alt="<?php the_title_attribute();?>">
						</a>
					</figure>
				</div><!-- end of pusher -->
			</div><!-- end of wrapper -->
			<div class="ornament"></div>
		</div><!-- end of video -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_copy_retinol' ) ) {
	function avoskin_copy_retinol() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('intro', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'intro_thumb',
			'title' => 'intro_title',
                        'text' => 'intro_text',
			'btn' => 'intro_btn',
			'url' => 'intro_url'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="copy">
			<div class="wrapper">
				<div class="rowflex">
					<figure><img src="<?php echo $thumb ;?>" alt="<?php the_title_attribute();?>" /></figure>
					<div class="caption">
						<h2 class="line-title pink"><?php echo $title ;?></h2>
						<div class="format-text">
							<?php echo $text ;?>
						</div><!-- end of format text -->
						<a href="<?php echo $url ;?>" class="button btn-pink slimy"><?php echo $btn ;?></a>
					</div><!-- end of caption -->
				</div><!-- end of rowflex -->
			</div><!-- end of wrapper -->
		</div><!-- end of copy -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_benefit_retinol' ) ) {
	function avoskin_benefit_retinol() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('benefit', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'item' => 'benefit_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="feature">
			<div class="wrapper">
				<div class="layer">
					<?php $n=1;foreach($item as $i):?>
						<div class="item">
							<div class="rowflex <?php echo (($n % 2) == 0) ? 'switch' : '';?>">
								<figure><img src="<?php echo $i['thumb'] ;?>" alt="<?php the_title_attribute();?>" /></figure>
								<div class="caption">
									<h2 class="line-title pink"><?php echo $i['title'] ;?></h2>
									<div class="format-text">
										<?php echo $i['text'] ;?>
									</div><!-- endo f format text -->
								</div><!-- end of caption -->
							</div><!-- end of rowflex -->
						</div><!-- end of item -->
					<?php $n++;endforeach;?>
				</div><!-- end of layer -->
			</div><!-- end of wrapper -->
		</div><!-- end of feature -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_action_retinol' ) ) {
	function avoskin_action_retinol() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('result', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'thumb' => 'result_thumbnail',
			'title' => 'result_title',
			'subtitle' => 'result_subtitle',
			'btn' => 'result_btn',
			'url' => 'result_url'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="enjoy">
			<div class="wrapper">
				<div class="pusher">
					<img src="<?php echo $thumb ;?>" alt="<?php the_title_attribute();?>" />
					<strong><?php echo $subtitle ;?></strong>
					<h2><?php echo $title ;?></h2>
					<a href="<?php echo $url ;?>" class="button btn-pink slimy"><?php echo $btn ;?></a>
				</div><!-- end of pusher -->
			</div><!-- end of wrapper -->
		</div><!-- end of enjoy -->
	<?php
	}
}

//HOMEPAGE
if ( ! function_exists( 'avoskin_slider_homepage' ) ) {
	function avoskin_slider_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('slider', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'item' => 'slide_item'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="hero unloaded">
                        <div class="slidenav">
                                <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
                                <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
                        </div><!-- end of slidenav -->
                        <div class="slick-carousel">
				<?php foreach($item as $i):
					$desktop = avoskin_resize( $i['img'] , 2000, 820, true, true, true );
					$mobile = avoskin_resize( $i['mobile_img'] , 820, 820, true, true, true );
					$mobile = ( $mobile != '') ? $mobile : avoskin_resize( $i['img'] , 820, 820, true, true, true );
				?>
					<div class="item">
						<a href="<?php echo $i['link'] ;?>">
							<img src="<?php echo $desktop ;?>" alt="<?php the_title_attribute();?>"
								srcset="
									<?php echo $mobile ;?>  820w,
									<?php echo $desktop ;?>  2000w
								"
								sizes="(min-width: 769px) 100vw, 30vw"
							/>
						</a>
					</div><!-- end of item -->
				<?php endforeach;?>
                        </div><!-- end of slick carousel -->
                </div><!-- end of hero -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_feature_homepage' ) ) {
	function avoskin_feature_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('feat', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'img' => 'feat_img',
			'title' => 'feat_title',
			'text' => 'feat_text',
			'btn' => 'feat_btn',
			'url' => 'feat_url',
			'item' => 'feat_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="featured">
                        <div class="wrapper">
                                <div class="detail">
					<h2 class="line-title mobile"><?php echo $title ;?></h2>
                                        <div class="rowflex switch">
						<?php if($img != ''):?>
							<figure><a href="<?php echo $url ;?>"><img src="<?php echo $img ;?>" /></a></figure>
						<?php endif;?>
                                                <div class="caption">
                                                        <h2 class="line-title desktop"><?php echo $title ;?></h2>
                                                        <div class="format-text">
                                                                <?php echo $text ;?>
                                                        </div><!-- end of txt -->
							<?php if($btn != '' && $url != ''):?>
								<a href="<?php echo $url ;?>" class="button slimy"><?php echo $btn ;?></a>
							<?php endif;?>
                                                </div><!-- end of caption -->
                                        </div><!-- end of rowflex -->
                                </div><!-- end of  detail -->
				<?php if(is_array($item) && !empty($item)):?>
					<div class="info">
					       <div class="rowflex expand">
							<?php foreach($item as $i):?>
								<div class="item">
								       <div class="caption">
									       <?php echo $i['text'] ;?>
								       </div><!-- end of caption -->
							       </div><!-- end of item -->
							<?php endforeach;?>
					       </div><!-- end of rowflex -->
				       </div><!-- end of info -->
				<?php endif;?>
                        </div><!-- end of wrapper -->
                </div><!-- end of featured product -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_avostore_homepage' ) ) {
	function avoskin_avostore_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('avostore', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'img' => 'avostore_img',
			'title' => 'avostore_title',
			'text' => 'avostore_text',
			'url' => 'avostore_url',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="store">
                        <div class="wrapper">
                                <div class="section-title centered">
                                        <h2 class="line-title"><?php echo $title ;?></h2>
                                        <?php echo $text ;?>
                                </div><!-- end of secton title -->
				<?php if($img != ''):?>
					<div class="centered">
						<a href="<?php echo $url ;?>"><img src="<?php echo $img ;?>" /></a>
					</div>
				<?php endif;?>
                        </div><!-- end of wrapper -->
                        <div class="ornament"></div>
                </div><!-- end of store -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_testimonial_homepage' ) ) {
	function avoskin_testimonial_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('testimonial', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'item' => 'testi_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="testimonial">
                        <div class="wrapper">
                               <div class="pusher">
                                        <div class="said">
                                                <div class="slick-carousel">
                                                       <?php foreach($item as $i ):?>
                                                               <div class="item">
                                                                       <div class="holder">
                                                                               <div class="txt">
                                                                                       <p><?php echo $i['text'] ;?></p>
                                                                               </div><!-- end of txt -->
                                                                               <strong><?php echo $i['from'] ;?></strong>
                                                                       </div><!-- end of holder -->
                                                               </div><!-- end of item -->
                                                       <?php endforeach;?>
                                               </div><!-- end of slick -->
                                       </div><!-- end of said -->
                                        <div class="from">
                                                <div class="slick-carousel">
                                                        <?php foreach($item as $i ):?>
                                                                <div class="item">
                                                                        <div class="holder"><img src="<?php echo $i['logo'] ;?>" /></div>
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div><!-- end of slick carousel -->
                                        </div><!-- end of from -->
                               </div><!-- end of pusher -->
                        </div><!-- end of wrapper -->
                </div><!-- end of testimonial -->
	<?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_flash_sale_homepage' ) ) {
	function avoskin_flash_sale_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('flash', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'flash_title',
			'end' => 'flash_end',
			'item' => 'flash_product',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		$expired = date("Y/m/d H:i:s", $end);
	?>
		<div class="flash-sale">
                        <div class="wrapper">
                                <div class="flash-head">
                                        <h2><?php echo $title ;?></h2>
                                        <div class="fcount">
                                                <div class="ftime" data-time="<?php echo $expired;?>">
                                                </div><!-- end fo timing -->
                                        </div><!-- end of countdown -->
                                </div><!-- end of flash head -->
                                <?php if(is_array($item) && !empty($item)):?>
					<div class="slide">
					       <div class="slidenav">
						       <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
						       <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
					       </div><!-- end of slidenav -->
						<div class="slick-carousel">
							 <?php foreach($item as $i):
								$product = wc_get_product((int)$i['product']);
								$id = $product->get_id();
								$price = $product->get_regular_price();
								$sale = $product->get_sale_price();
								$percent = '';
								if(!$product->is_on_sale()){
									$sale = 0;
								}
								if($sale > 0){
									$percent = ceil((($price - $sale )  / $price ) * 100);
								}
							 ?>
								<div class="item">
									<div class="layer">
										<figure>
											<?php if($percent != ''):?>
												 <span class="disc"><?php echo $percent ;?>%</span>
											<?php endif;?>
											<a href="<?php echo get_permalink($id);?>">
												 <?php echo $product->get_image([186, 186]);?>
											</a>
										</figure>
										<div class="cat">
											<?php echo wc_get_product_category_list($id);?>
										</div><!-- end of cat -->
										<h3><a href="<?php echo get_permalink($id);?>"><?php echo $product->get_name();?></a></h3>
										<?php echo avoskin_rating($product->get_average_rating());?>
										<div class="pricing">
											<strong><?php echo ($percent != '') ? strip_tags(wc_price($sale)) : strip_tags(wc_price($price) );?></strong>
											<?php if($percent != '') :?>
												 <span><?php echo strip_tags(wc_price($price)) ;?></span>
											 <?php endif;?>
										</div><!-- end of pricing -->
										<?php
											$quantity = $product->get_stock_quantity();
											$quantity = ($quantity != 0) ? $quantity : 1;
											$low = wc_get_low_stock_amount($product);
											$status = ((int)$quantity > (int)$low) ? 'safe' : 'hurry';
											$percent  =  ((int)$low / (int)$quantity) * 100;
											$percent  = ($percent >= 100) ? ((int)$quantity / (int)$low) * 100 : $percent;
											$msg = ($status == 'safe') ? __('Still available', 'avoskin') : __('Low stocks', 'avoskin');
										?>
										<div class="stock-status <?php echo $status ;?>">
											<span><b style="width:<?php echo $percent ;?>%"></b></span>
											<p><?php echo $msg ;?></p>
										</div><!-- end of stock status -->
									</div><!-- end of layer -->
								</div><!-- end of item -->
							<?php endforeach;?>
						</div><!-- end of slick carousel -->
					</div><!-- end of slide -->
				<?php endif;?>
                        </div><!-- end of wrapper -->
                </div><!-- end of flash sale -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_best_seller_homepage' ) ) {
	function avoskin_best_seller_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('best', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'best_title',
			'text' => 'best_text',
			'item' => 'best_product',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="best-seller">
                        <div class="wrapper">
                                <div class="section-title centered">
                                        <h2 class="line-title"><?php echo $title ;?></h2>
                                        <div class="pusher">
                                                <?php echo $text ;?>
                                        </div>
                                </div><!-- end of secton title -->
				<?php if(is_array($item) && !empty($item)):?>
					<div class="rowflex">
						<?php foreach($item as $i):
							$product = wc_get_product((int)$i['product']);
							$id = $product->get_id();
							$price = $product->get_regular_price();
							$sale = $product->get_sale_price();
							$percent = '';
							if(!$product->is_on_sale()){
								$sale = 0;
							}
							if($sale > 0){
								$percent = ceil((($price - $sale )  / $price ) * 100);
							}
							$tagline = get_post_meta($id, 'product_tagline', true);
							$stock = ceil(get_post_meta( $id, '_stock', true ));
							$hide_price = (get_post_meta($product->get_id(), 'product_hide_price', true) == 'yes' ) ? 'hide-price' : '';
						?>
							<div class="product-item <?php echo $hide_price ;?>">
								<div class="holder">
									<div class="img-box">
										<?php if($product->get_stock_status() != 'instock'):?>
											<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
										<?php endif;?>
										<?php if($percent != '' || $tagline != ''):?>
											<span class="disc"><?php echo ($percent == '' || $tagline != '' && is_product() || $tagline != '' && is_page_template('page-homepage.php')) ? $tagline : $percent.'%' ;?></span>
										<?php endif;?>
										<?php echo $product->get_image([280,280]) ;?>
										<div class="action">
											<a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
											<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
											<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
										</div><!-- end of action -->
									</div><!-- end of img box -->
									<div class="cat">
										<?php echo wc_get_product_category_list($product->get_id());?>
									</div><!-- end of cat -->
									<h2 class="product-title"><a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a></h2>
									<?php echo avoskin_rating($product->get_average_rating());?>
									<?php if($percent != '') :?>
										<span class="sale"><?php echo wc_price($price) ;?></span>
									<?php endif;?>
									<strong class="price"><?php echo ($percent != '') ? wc_price($sale) : wc_price($price) ;?></strong>
								</div><!-- endof holder-->
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of rowflex -->
				<?php endif;?>
                                
                                <div class="centered">
                                        <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button slimy"><?php _e('See All','avoskin');?></a>
                                </div><!-- end of centered -->
                        </div><!-- end of wrapper -->
                </div><!-- end of best seller -->
	<?php
	}
}

/*
if ( ! function_exists( 'avoskin_instagram_homepage' ) ) {
	function avoskin_instagram_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('instagram', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'ig_title',
			'id' => 'ig_id',
			'username' => 'ig_username'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		//delete_transient( 'avoskin_instagram' );
		$instagram = avoskin_get_instagram($id);
		if(is_array($instagram) && !empty($instagram)):

	?>
			<div class="section-instagram">
				<div class="wrapper">
					<div class="section-title centered white-txt">
						<h2 class="line-title"><?php echo $title ;?></h2>
						<p><a href="https://www.instagram.com/<?php echo $username ;?>/" target="_blank"><i class="fa-instagram" style="position: relative;top: 2px;"></i> <?php echo $username ;?></a></p>
					</div><!-- end of section title -->
				</div><!-- end of wrapper -->
				<div class="wrap">
					<?php foreach($instagram as $i):?><div class="item">
						<a href="<?php echo $i['url'] ;?>" target="_blank"><img src="<?php echo $i['src'] ;?>" /></a>
					</div><!-- end of item --><?php endforeach;?>
				</div><!-- end of wrap -->
				<div class="wrapper centered">
					<a href="https://www.instagram.com/<?php echo $username ;?>/" target="_blank" class="button btn-white slimy"><?php _e('See All','avoskin');?></a>
				</div>
			</div><!-- end of section instagram -->
		<?php
		endif;
	}
}
*/

if ( ! function_exists( 'avoskin_instagram_homepage' ) ) {
	function avoskin_instagram_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('instagram', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'ig_title',
			'id' => 'ig_id',
			'username' => 'ig_username',
			'account' => 'ig_account'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		$instagram_html = '';
		if ( false === ( $instagram_html = get_transient( 'avoskin_instagram_html') )) :
		ob_start();
		do_shortcode('[instagram-feed user="'.$account.'" num="6"]');
		$instagram = get_option('avoskin_instagram_cache', []);
		if(is_array($instagram) && !empty($instagram)):

	?>
			<div class="section-instagram">
				<div class="wrapper">
					<div class="section-title centered white-txt">
						<h2 class="line-title"><?php echo $title ;?></h2>
						<p><a href="https://www.instagram.com/<?php echo $username ;?>/" target="_blank"><i class="fa-instagram" style="position: relative;top: 2px;"></i> <?php echo $username ;?></a></p>
					</div><!-- end of section title -->
				</div><!-- end of wrapper -->
				<div class="wrap">
					<?php foreach($instagram as $i):?><div class="item">
						<a href="<?php echo $i['permalink'] ;?>" target="_blank"><img src="<?php echo $i['media_url'] ;?>" /></a>
					</div><!-- end of item --><?php endforeach;?>
				</div><!-- end of wrap -->
				<div class="wrapper centered">
					<a href="https://www.instagram.com/<?php echo $username ;?>/" target="_blank" class="button btn-white slimy"><?php _e('See All','avoskin');?></a>
				</div>
			</div><!-- end of section instagram -->
		<?php
		endif;
			$instagram_html = ob_get_contents();
                        ob_end_clean();
                        set_transient( 'avoskin_instagram_html', $instagram_html, HOUR_IN_SECONDS );        
		endif;
		echo $instagram_html;
	}
}

if ( ! function_exists( 'avoskin_instagram_feed' ) ) {
	function avoskin_instagram_feed($posts, $settings) {
		update_option('avoskin_instagram_cache', $posts);
	}
}

if ( ! function_exists( 'avoskin_blog_homepage' ) ) {
	function avoskin_blog_homepage() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('blog', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'blog_title',
			'total' => 'blog_total',
			'source' => 'blog_source',
			'link' => 'blog_link'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		//delete_transient('avoskin_blog');
		$blog = ($source != '' && $total != '' ) ? avoskin_fetch_blog($source, $total) : '';
	?>
	
		<div class="section-blog">
                        <div class="wrapper">
                                <div class="section-title centered">
                                        <h2 class="line-title"><?php echo $title ;?></h2>
                                </div><!-- end of centered -->
                        </div><!-- end of wrapper -->
			<?php if($blog != ''):?>
				<div class="slide">
					<div class="slidenav">
						<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
						<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
					</div><!-- end of slidenav -->
					<div class="wrapper">
						<div class="slick-carousel">
							<?php echo $blog ;?>
						</div><!-- end of slick carousel -->
					</div><!-- end of wrapper -->
				</div><!-- end of slide -->
				<div class="wrapper centered">
					<a href="<?php echo $link ;?>" class="button slimy" target="_blank"><?php _e('See All','avoskin');?></a>
				</div>
			<?php endif;?>
                </div><!-- end of section blog -->
	<?php
	}
}

if ( ! function_exists( 'avoskin_navi_skinbae' ) ) {
	function avoskin_navi_skinbae() {
		$meta = [
			'your' => 'navi_your',
			'over' => 'navi_over',
			'buy' => 'navi_buy',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if($your != '' || $over != '' ||  $buy != '' ):
	?>
		<div class="navi">
		      <div class="wrapper">
				<?php if($your != ''):?>
					<a href="#"><?php echo $your ;?></a>
			      <?php endif;?>
			      <div class="action">
					<?php if($over != ''):?>
						<p><?php echo $over ;?></p>
				      <?php endif;?>
				      <?php if($buy != ''):?>
						<a href="<?php echo $buy ;?>" class="button btn-hollow"><?php _e('Buy','avosin');?></a>
				      <?php endif;?>
			      </div>
			      <div class="clear"></div>
		      </div>
	      </div><!-- end of navi -->
        <?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_intro_skinbae' ) ) {
	function avoskin_intro_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('intro', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'img' => 'intro_img',
			'title' => 'intro_title',
			'subtitle' => 'intro_subtitle',
			'text' => 'intro_text',
			'video' => 'intro_video',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="intro">
			<?php if($img != ''):?>
				<figure>
					<div class="forstick">
						<img src="<?php echo $img ;?>" />
					</div>
				</figure>
			<?php endif;?>
		      <div class="wrapper">
			      <div class="hintro">
					<?php if($title != ''):?>
						<h2 class="wow fadeInUp"><?php echo $title ;?></h2>
					<?php endif;?>
				      <?php if($subtitle != ''):?>
						<b class="wow fadeInUp" data-wow-delay="500ms"><?php echo $subtitle ;?></b>
				      <?php endif;?>
			      </div><!-- end of hentry -->
			      <?php if($text != ''):?>
					<div class="format-text wow fadeInUp" data-wow-delay="1s">
						<?php echo $text ;?>
					</div><!-- end of format text -->
			      <?php endif;?>
			      <?php if($video != ''):?>
			      <a href="<?php echo $video ;?>" class="wow fadeInUp"  data-wow-delay="1200ms" data-fancybox><?php _e('WATCH VIDEO','avoskin');?></a>
			      <?php endif;?>
		      </div><!-- end of wrapper -->
	      </div><!-- end of intro -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_variant_skinbae' ) ) {
	function avoskin_variant_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('variant', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'simg' => 'variant_serum_img',
			'stitle' => 'variant_serum_title',
			'ssubtitle' => 'variant_serum_subtitle',
			'stext' => 'variant_serum_text',
			'timg' => 'variant_toner_img',
			'ttitle' => 'variant_toner_title',
			'tsubtitle' => 'variant_toner_subtitle',
			'ttext' => 'variant_toner_text',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="variant">
			<div class="wrapper rowflex">
				<div class="item">
					<?php if($stitle != ''):?>
						<span class="wow fadeInLeft" data-wow-delay="1500ms"><?php echo $stitle ;?></span>
					<?php endif;?>
					<div class="emul">
						<?php if($simg != ''):?>
							<figure class="wow fadeInLeft" data-wow-delay="500ms"><img src="<?php echo $simg ;?>" /></figure>
						<?php endif;?>
						
						<div class="caption wow fadeInLeft" data-wow-delay="1500ms">
							<?php if($ssubtitle != ''):?>
								<b><?php echo $ssubtitle ;?></b>
							<?php endif;?>
							<?php if($stitle != ''):?>
								<h2><?php echo $stitle ;?></h2>
							<?php endif;?>
							<?php if($stext != ''):?>
								<div class="format-text">
									<?php echo $stext ;?>
								</div><!-- end of format text -->
							<?php endif;?>
						</div><!-- end of caption -->
					</div><!-- end of emul -->
				</div><!-- end of item -->
				<div class="item">
					<?php if($ttitle != ''):?>
						<span class="wow fadeInRight" data-wow-delay="1500ms"><?php echo $ttitle ;?></span>
					<?php endif;?>
					<div class="emul">
						<?php if($timg != ''):?>
							<figure class="wow fadeInRight" data-wow-delay="500ms"><img src="<?php echo $timg ;?>" /></figure>
						<?php endif;?>
						
						<div class="caption wow fadeInRight" data-wow-delay="1500ms">
							<?php if($tsubtitle != ''):?>
								<b><?php echo $tsubtitle ;?></b>
							<?php endif;?>
							<?php if($ttitle != ''):?>
								<h2><?php echo $ttitle ;?></h2>
							<?php endif;?>
							<?php if($ttext != ''):?>
								<div class="format-text">
									<?php echo $ttext ;?>
								</div><!-- end of format text -->
							<?php endif;?>
						</div><!-- end of caption -->
					</div><!-- end of emul -->
				</div><!-- end of item -->
			</div><!-- end of wrapper -->
		</div><!-- end of variant -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_hero_skinbae' ) ) {
	function avoskin_hero_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('hero', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'hero_title',
			'subtitle' => 'hero_subtitle',
			'item' => 'hero_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="hero">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
				</div><!-- end of hentry -->
				<?php if(is_array($item) && !empty($item)):?>
					<div class="list rowflex">
						<?php foreach($item as $i):?>
							<div class="item">
								<figure><a href="<?php echo get_permalink($i['product']) ;?>">
									<img src="<?php echo $i['img'] ;?>" width="115" /></a>
								</figure>
								<div class="caption">
									<h3><a href="<?php echo get_permalink($i['product']) ;?>"><?php echo $i['title'] ;?></a></h3>
									<p><?php echo $i['info'] ;?></p>
									<a href="<?php echo get_permalink($i['product']) ;?>" class="button btn-hollow darker"><?php _e('See More','avoskin');?></a>
								</div><!-- end of caption -->
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of list -->
				<?php endif;?>
				
			</div><!-- endf of wrapper -->
		</div><!-- end of hero -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_product_skinbae' ) ) {
	function avoskin_product_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('product', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'product_title',
			'subtitle' => 'product_subtitle',
			'serum_item' => 'product_serum_item',
			'toner_item' => 'product_toner_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="theprod">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
				</div><!-- end of hentry -->
				
				<div class="slide-holder">
					   <div class="slide-dots for-mobile">
						<div class="nav">
							<a href="#" class="active" data-dot="serum"><?php _e('Serum','avoskin');?></a>
							<a href="#"  data-dot="toner"><?php _e('Toner','avoskin');?></a>
						</div><!-- end of nav -->
					</div><!-- end of slide dots -->
					
					<?php foreach(['serum', 'toner'] as $s):?>
						<div id="<?php echo $s ;?>" class="slide <?php echo ($s == 'serum') ? 'active' : '' ;?>" data-slide="<?php echo $s ;?>">
							<div class="slick-carousel">
								<?php foreach(${$s.'_item'} as $i):?>
									<div class="item">
										<div class="stitle">
											<h3><?php echo $i['title'] ;?></h3>
											<span><?php echo $i['info'] ;?></span>
										</div><!-- end of stitle -->
										<div class="emul">
											<figure>
												<img src="<?php echo $i['img'] ;?>" />
												<div class="slidenav"><b class="prev"></b><b class="next"></b></div>
											</figure>
											<div class="caption">
												<div class="stitle for-mobile">
													<h3><?php echo $i['title'] ;?></h3>
													<span><?php echo $i['info'] ;?></span>
												</div><!-- end of stitle -->
												<div class="format-text">
													<?php echo $i['text'] ;?>
												</div><!-- end of format text -->
											</div><!-- end of caption-->
											<div class="slide-dots for-mobile dots-mobile">
												<div class="dots dots-<?php echo $s ;?> active">
													<?php $n=1;foreach(${$s.'_item'} as $color):?><a href="#" data-color="<?php echo $color['color'] ;?>" style="background: <?php echo $color['color'] ;?>" class="<?php echo ($n == 1) ? 'active' : '' ;?>"></a><?php $n++;endforeach;?>
												</div><!-- end of dots -->
											</div><!-- end of slide dots -->
											<a href="<?php echo $i['url'] ;?>"><?php _e('See Detail','avoskin');?></a>
										</div><!-- end of emul -->
										<div class="clear"></div>
									</div><!-- end of item -->
								<?php endforeach;?>
							</div><!-- end of slick -->
						</div><!-- end of slide -->
					<?php endforeach;?>
					<div class="slide-dots">
						<div class="nav">
							<a href="#" class="active" data-dot="serum">Serum</a>
							<a href="#"  data-dot="toner">Toner</a>
						</div><!-- end of nav -->
						<?php foreach(['serum', 'toner'] as $s):?>
							<div class="dots dots-<?php echo $s ;?> <?php echo ($s == 'serum') ? 'active': '' ;?>">
								<?php $n=1;foreach(${$s.'_item'} as $color):?><a href="#" data-color="<?php echo $color['color'] ;?>" style="background: <?php echo $color['color'] ;?>" class="<?php echo ($n == 1) ? 'active' : '' ;?>"></a><?php $n++;endforeach;?>
							</div><!-- end of dots -->
						<?php endforeach;?>
					</div><!-- end of slide dots -->
				</div><!-- end of slide holder -->
				
			</div><!-- endf of wrapper -->
		</div><!-- end of hero -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_ingre_skinbae' ) ) {
	function avoskin_ingre_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('ingre', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'ingre_title',
			'subtitle' => 'ingre_subtitle',
			'item' => 'ingre_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="ingre">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
				</div><!-- end of hentry -->
			</div><!-- endf of wrapper -->
			
			<?php if(is_array($item) && !empty($item)):?>
				<div class="slide">
					<div class="slick-carousel">
						<?php foreach($item as $i):?>
							<div class="item">
								<figure>
									<img src="<?php echo $i['img'] ;?>" class="for-desktop" />
									<img src="<?php echo $i['mimg'] ;?>" class="for-mobile" />
								</figure>
								<div class="caption">
									<div class="holder">
										<h3><?php echo $i['title'] ;?></h3>
										<div class="format-text">
											<?php echo $i['text'] ;?>
										</div><!-- end of txt -->
										<?php if(count($item) > 1):?>
											<div class="slidenav"><b class="prev"></b><b class="next"></b></div>
										<?php endif;?>
									</div><!-- end of holder -->
								</div><!-- end of caption -->
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of slick -->
				</div><!-- end of slide -->
			<?php endif;?>
		</div><!-- end of ingre -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_benefit_skinbae' ) ) {
	function avoskin_benefit_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('benefit', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'benefit_title',
			'subtitle' => 'benefit_subtitle',
			'img1' => 'benefit_img1',
			'title1' => 'benefit_title1',
			'img2' => 'benefit_img2',
			'title2' => 'benefit_title2',
			'img3' => 'benefit_img3',
			'title3' => 'benefit_title3',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="benefit">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
				</div><!-- end of hentry -->
				
				<?php for($i=1;$i<=3;$i++):?>
					<?php if(${'img'.$i} != '' && ${'title'.$i} != ''):?>
						<div class="img<?php echo $i ;?>">
							<figure><img src="<?php echo ${'img'.$i} ;?>" /></figure>
							<h3><?php echo ${'title'.$i} ;?></h3>
						</div><!-- end of img1 -->
					<?php endif;?>
				<?php endfor;?>
			</div><!-- endf of wrapper -->
		</div><!-- end of benefit -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_function_skinbae' ) ) {
	function avoskin_function_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('function', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'function_title',
			'subtitle' => 'function_subtitle',
			'item' => 'function_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
                <div class="function">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
				</div><!-- end of hentry -->
				
				<?php if(is_array($item) && !empty($item)):?>
					<div class="list rowflex">
						<?php foreach($item as $i):?>
							<div class="item">
								<figure><img src="<?php echo $i['img'] ;?>"  width="190"/></figure>
								<h3><?php echo $i['title'] ;?></h3>
								<div class="format-text">
									<?php echo $i['text'] ;?>
								</div><!-- end of txt -->
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of list -->
				<?php endif;?>
			</div><!-- endf of wrapper -->
		</div><!-- end of benefit -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_proof_skinbae' ) ) {
	function avoskin_proof_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('proof', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'proof_title',
			'subtitle' => 'proof_subtitle',
			'text' => 'proof_text',
			'item' => 'proof_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="effiacy">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
					<?php if($text != ''):?>
						<div class="format-text">
							<?php echo $text ;?>
						</div><!-- end of txt -->
					<?php endif;?>
				</div><!-- end of hentry -->
				
				<?php if(is_array($item) && !empty($item)):?>
					<div class="proof" data-count="<?php echo count($item) ;?>">
						<div class="slick-carousel">
							<?php foreach($item as $id => $v):?>
								<div class="item">
									<p><?php echo get_the_title($id) ;?></p>
									<figure><a href="<?php echo $v ;?>" data-fancybox="proof"><img src="<?php echo $v ;?>" /></a></figure>
								</div>
							<?php endforeach;?>
						</div>
					</div><!-- end of proof -->
				<?php endif;?>
				
				
			</div><!-- end f wrapper -->
		</div><!-- end of effiacy -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_person_skinbae' ) ) {
	function avoskin_person_skinbae() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('person', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'title' => 'person_title',
			'subtitle' => 'person_subtitle',
			'text' => 'person_text',
			'item' => 'person_item',
			'cats' => 'cats_item',
			'url' => 'person_url',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="person">
			<div class="wrapper">
				<div class="hentry">
					<?php if($subtitle != ''):?>
						<b><?php echo $subtitle ;?></b>
					<?php endif;?>
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
					<?php if($text != ''):?>
						<div class="format-text">
							<?php echo $text ;?>
						</div><!-- end of txt -->
					<?php endif;?>
				</div><!-- end of hentry -->
				
				<?php if(is_array($cats) && !empty($cats)):?>
					<div class="catstuff">
						<div class="cat-holder" data-item='<?php echo wp_json_encode($item) ;?>'>
							<div class="xwrap">
								<a href="#" data-cat="all" class="active"><?php _e('All','avoskin');?></a>
								<?php foreach($cats as $c):
									$term = get_term_by('term_id', (int)$c['category'], 'product_cat');
								?>
									<a href="#" data-cat="<?php echo $term->slug ;?>"><?php echo $term->name ;?></a>
								<?php endforeach;?>
							</div><!-- end of xwrap -->
						</div><!-- end of cat holder -->
					</div>
				<?php endif;?>
				
				<?php if(is_array($item) && !empty($item)):?>
					<div class="list rowflex">
						<?php foreach($item as $i):
							$product = wc_get_product($i['product']);
							$id = $product->get_id();
							$price = $product->get_regular_price();
							$sale = $product->get_sale_price();
							$percent = '';
							if(!$product->is_on_sale()){
								$sale = 0;
							}
							if($sale > 0){
								$percent = ceil((($price - $sale )  / $price ) * 100);
							}
							$stock = ceil(get_post_meta( $id, '_stock', true ));
						?>
							<div class="item">
								<figure>
									<?php if($product->get_stock_status() != 'instock'):?>
										<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
									<?php endif;?>
									<?php if($percent != '' ):?>
										<span class="disc"><?php echo ($percent == '' ) ? '' : $percent.'%' ;?></span>
									<?php endif;?>
									<?php echo $product->get_image([256,256]) ;?>
									<div class="action">
										<a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
										<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
										<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
									</div><!-- end of action -->
								</figure>
								<div class="caption">
									<h3><a href="<?php echo get_permalink($id) ;?>"><?php echo $product->get_name() ;?></a></h3>
									<p><?php echo $product->get_short_description() ;?></p>
								</div><!-- end of caption -->
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of list --->
				<?php endif;?>
				
				<?php if($url != ''):?>
					<div class="centered">
						<a href="<?php echo $url ;?>" class="button btn-hollow darker"><?php _e('See More','avoskin');?></a>
					</div>
				<?php endif;?>
				
			</div><!-- end of wrapper -->
		</div><!-- end of effiacy -->
        <?php
	}
}


if ( ! function_exists( 'avoskin_blog_header_view' ) ) {
	function avoskin_blog_header_view() {
	?>
		<div id="shell">
			<header id="blog-top">
				<div class="wrapper">
					<div class="logo"><a href="<?php echo get_permalink(get_theme_mod('avoskin_landing_blp_page')) ;?>"><img src="<?php echo get_theme_mod('avoskin_blog_logo');?>" alt="<?php bloginfo('name');?>" width="394" /></a></div>
				</div><!-- end of wrapper -->
			</header><!-- end of top -->
			<div id="body">
        <?php
	}
}
if ( ! function_exists( 'avoskin_blog_footer_view' ) ) {
	function avoskin_blog_footer_view() {
	?>
		</div><!-- end of body -->
		<footer id="blog-bottom">
			<div class="wrapper">
				<div class="copy">
					<p><?php echo get_theme_mod('avoskin_copy');?></p>
				</div>
			</div>
		</footer><!-- endof blog bottom -->
		<!--<div id="responsive"></div>-->
		</div><!-- end of shell -->
        <?php
		//MENU MOBILE
		get_template_part('views/menuside');
		
		//SEARH POPUP
		get_template_part('views/popup-search');
		
		//CART SIDEBAR
		get_template_part('views/cartside');
		
		//POPUP LOGIN ONLY IF USER NOT LOGGED IN
		if(!is_user_logged_in()) get_template_part('views/login-signup');
		
		//POPUP REVIEW ONLY FOR PAGE PRODUCT DETAIL AND LOGGED IN USER
		if(is_singular('product') && is_user_logged_in()) get_template_part('views/popup-review');
		
		//NEWSLETTER 
		get_template_part('views/popup-newsletter');	
		
		//TO REGISTER JS ON FOOTER
		wp_footer();
		
		//CUSTOM JS ON FOOTER
		get_template_part('views/phpjs');
	}
}

if ( ! function_exists( 'avoskin_slider_homepage2' ) ) {
	function avoskin_slider_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('slider', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'item' => 'slide_item'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="hero notload">
			<div class="slider">
				<div class="slick-carousel">
					<?php foreach($item as $i):
						$desktop = avoskin_resize( $i['img'] , 2560, 960, true, true, true );
						$mobile = avoskin_resize( $i['mobile_img'] , 830, 512, true, true, true );
						$mobile = ( $mobile != '') ? $mobile : avoskin_resize( $i['img'] , 830, 512, true, true, true );
					?>
						<div class="item">
							<a href="<?php echo $i['link'] ;?>">
								<img src="<?php echo $desktop ;?>" alt="home"
									srcset="
										<?php echo $mobile ;?>  500w,
										<?php echo $desktop ;?>  800w
									"
									sizes="(min-width: 769px) 100vw, 30vw"
								/>
								<div class="holder">
									<div class="wrapper">
										<h2><?php echo $i['title'] ;?></h2>
										<p><?php echo $i['subtitle'] ;?></p>
									</div><!-- end of wrapper -->
								</div><!-- end of holder -->
							</a>
						</div><!-- end of item -->
					<?php endforeach;?>
				</div><!-- end of slick carousel -->
				<?php if(count($item) > 1):?>
					<div class="slidenav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>
				<?php endif;?>
			</div><!-- end of slider -->
		</div><!-- end of hero -->
        <?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_series_homepage2' ) ) {
	function avoskin_series_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('series', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'series_title',
			'text' => 'series_text',
			'item' => 'series_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
        <div class="series">
		<div class="wrapper">
			<div class="hentry">
				<?php if($title != ''):?>
					<h2><?php echo $title ;?></h2>
				<?php endif;?>
				<?php if($text != ''):?>
				<div class="txt">
					<?php echo $text ;?>
				</div><!-- end of txt -->
				<?php endif;?>
			</div><!-- end of hentry -->
				
			<?php if(is_array($item) && !empty($item)):?>
				<div class="slider">
					<div class="slick-carousel">
						<?php foreach($item as $i):
							$img = avoskin_resize( $i['img'] , 800, 284, true, true, true );
						?>
							<div class="item">
								<a href="<?php echo $i['link'] ;?>"><img src="<?php echo $img ;?>" /></a>
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of slick carousel -->
					<?php if(count($item) > 3):?>
						<div class="slidenav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>
					<?php endif;?>
				</div><!-- end of slider -->
			<?php endif;?>
		</div>
	</div><!-- end of series -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_flash_homepage2' ) ) {
	// ::CRFLASH::
	function avoskin_flash_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('flash', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'icon' => 'flas_icon',
			'img' => 'flas_img',
			'mimg' => 'flas_mimg',
			'start' => 'flash_start',
			'end' => 'flash_end',
			'btn' => 'flash_button',
			'url' => 'flash_url',
			'item' => 'flash_product',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if((int)$end >= (int)current_time('timestamp')):
		$time = ($start != '' && (int)$start > (int)current_time('timestamp')) ?  $start : $end;
		$label = ($start != '' && (int)$start > (int)current_time('timestamp')) ?  __('Flash Sale Starting in', 'avoskin') : __('Flash Sale End in', 'avoskin');
		$time = $time - (7 * 60 * 60);
		$time = date("Y/m/d H:i:s", $time);
	?>
		<div class="flash">
			<div class="wrapper">
				<div class="hen">
					<?php if($icon != ''):?>
						<figure><img src="<?php echo $icon ;?>" width="215" /></figure>
					<?php endif;?>
					<div class="info">
						<p><?php echo $label ;?></p>
						<div class="timer" data-time="<?php echo $time ;?>"></div>
					</div><!-- end of info -->
					<div class="clear"></div>
				</div><!-- end of hen -->
			</div>
			<div class="wrapflash">
				<?php if($img != ''):?>
					<div class="thumb">
						<?php if(wp_is_mobile()):?>
							<img src="<?php echo $mimg ;?>" alt="home" />
						<?php else:?>
							<img src="<?php echo $img ;?>" alt="home" />
						<?php endif;?>
					</div><!-- end of thumb -->
				<?php endif;?>
				<?php if(is_array($item) && !empty($item)):?>
					<div class="flashlist">
						<div class="flash-scroll">
							<div class="prodlist">
								<?php foreach($item as $i ):
									$product = wc_get_product((int)$i['product']);
									$id = $product->get_id();
									$price = $product->get_regular_price();
									$sale = $product->get_sale_price();
									$percent = '';
									if(!$product->is_on_sale()){
										$sale = 0;
									}
									if($sale > 0){
										$percent = ceil((($price - $sale )  / $price ) * 100);
									}
									$stock = ceil(get_post_meta( $id, '_stock', true ));
								?>
									<div class="proditem">
										<figure>
											<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"></a>
											<?php if($percent != ''):?>
												<span class="disc">-<?php echo $percent ;?>%</span>
											<?php endif;?>
											<a href="<?php echo get_permalink($id);?>">
												<?php echo $product->get_image([600, 520]);?>
										       </a>
										       <?php if($product->get_stock_status() != 'instock'):?>
												<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
											<?php endif;?>
										</figure>
										<div class="caption">
											<h3><a href="<?php echo $product->get_permalink() ;?>"><?php echo $product->get_name() ;?></a></h3>
											<div class="cats">
												<?php echo wc_get_product_category_list($id);?>
											</div><!-- end of cats -->
											<div class="act">
												<div class="pricing"><?php echo $product->get_price_html(); ?></div>
												<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"></a>
												<div class="clear"></div>
											</div><!--end of act -->
											<div class="stockey">
												<?php
													$low = wc_get_low_stock_amount($product);
													$quantity = ($product->get_stock_quantity() != 0) ? $product->get_stock_quantity() : 1;
													$percent  =  100 - $quantity;
												?>
												<span class="bar"><b style="width:<?php echo $percent ;?>%"></b></span>
												<?php if($product->get_stock_quantity()  > 0):?>
													<?php if($product->get_stock_quantity() > (int)$low):?>
														<small><?php _e('Sisa','avoskin');?> <?php echo $quantity;?></small>
													<?php else:?>
														<small><?php _e('Hampir Habis','avoskin');?></small>
													<?php endif;?>
												<?php else:?>
													<small><?php _e('Stock Habis','avoskin');?></small>
												<?php endif;?>
											</div><!-- end of stockey -->
										</div><!-- end of caption -->
									</div><!-- end of item -->
								<?php endforeach;?>
								<div class="clear"></div>
							</div><!-- end of prodlist -->
						</div><!-- end of flash scroll -->
						<div class="scrollerx light">
							<div class="scrollerx-wrapper">
								<div class="scroll-element_size"></div>
								<div class="scroll-element_track"></div>
								<div class="scroll-bar"></div>
							</div>
						</div>
					</div><!-- end of flash list -->
				<?php endif;?>
			</div><!-- end of wrap flash -->
			<?php if($btn != '' && $url != ''):?>
				<div class="centered">
					<a href="<?php echo $url ;?>" class="button"><?php echo $btn ;?></a>
				</div><!-- end of centered -->
			<?php endif;?>
		</div><!-- end of flash -->
        <?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_recom_homepage2' ) ) {
	function avoskin_recom_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('recom', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'recom_title',
			'text' => 'recom_text',
			'item' => 'recom_product',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
	?>
		<div class="recom">
			<div class="hentry smaller">
				<?php if($title != ''):?>
					<h2><?php echo $title ;?></h2>
				<?php endif;?>
				<?php if($text != ''):?>
				<div class="txt">
					<?php echo $text ;?>
				</div><!-- end of txt -->
				<?php endif;?>
			</div><!-- end of hentry -->
			
			<?php if(is_array($item) && !empty($item)):
				$first = avoskin_resize( $item[0]['img'], 1122, 1240, true, true, true );
				$firstm = avoskin_resize( $item[0]['mimg'], 720, 444, true, true, true );
			?>
				<div class="recwrap">
					<div class="thbmob">
						<img src="<?php echo $first ;?>" alt="home"
							srcset="
								<?php echo $firstm ;?>  500w,
								<?php echo $first ;?>  800w
							"
							sizes="(min-width: 769px) 100vw, 30vw"
						/>
					</div><!-- end of thb mob -->
					<div class="rechead">
						<div class="wrapper">
							<div class="xscroll">
								<div class="holder">
									<?php $x=1;foreach($item as $i):?>
										<a href="#rec<?php echo $x ;?>" <?php echo ($x == 1) ? 'class="current"': '' ;?>><?php echo $i['label'] ;?></a>
									<?php $x++;endforeach;?>
								</div>
							</div>
						</div><!-- end of wrapper -->
					</div><!-- end of rechead -->
					<?php $x=1;foreach($item as $i):
						$desk = avoskin_resize( $i['img'], 1122, 1240, true, true, true );
						$mob = avoskin_resize( $i['mimg'], 720, 444, true, true, true );
					?>
						<div id="rec<?php echo $x ;?>" class="item <?php echo ($x == 1) ? 'current' : '' ;?>">
							<div class="thumb">
								<img src="<?php echo $desk ;?>" alt="home"
									srcset="
										<?php echo $mob ;?>  500w,
										<?php echo $desk ;?>  800w
									"
									sizes="(min-width: 769px) 100vw, 30vw"
								 />
							</div><!-- end of thumb -->
							<?php if(is_array($i['product']) && !empty($i['product'])):?>
								<div class="reclist">
									<div class="rec-scroll">
										<div class="prodlist">
											<?php foreach($i['product'] as $ip):
												$product = wc_get_product((int)$ip);
												$id = $product->get_id();
												$price = $product->get_regular_price();
												$sale = $product->get_sale_price();
												$percent = '';
												if(!$product->is_on_sale()){
													$sale = 0;
												}
												if($sale > 0){
													$percent = ceil((($price - $sale )  / $price ) * 100);
												}
												$stock = ceil(get_post_meta( $id, '_stock', true ));
											?>
												<div class="proditem">
													<figure>
														<a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"></a>
														<?php if($percent != ''):?>
															<span class="disc">-<?php echo $percent ;?>%</span>
														<?php endif;?>
														<a href="<?php echo get_permalink($id);?>">
															<?php echo $product->get_image([600, 520]);?>
													       </a>
													       <?php if($product->get_stock_status() != 'instock'):?>
															<span class="stock-empty"><?php avoskin_oos_text($id);?></span>
														<?php endif;?>
													</figure>
													<div class="caption">
														<h3><a href="<?php echo $product->get_permalink() ;?>"><?php echo $product->get_name() ;?></a></h3>
														<div class="cats">
															<?php echo wc_get_product_category_list($id);?>
														</div><!-- end of cats -->
														<div class="pricing"><?php echo $product->get_price_html(); ?></div>
														<div class="act">
															<div class="rated">
																<i class="fa-star"></i><b><?php echo $product->get_average_rating() ;?></b>
															</div><!-- end of rated -->
															<a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"></a>
															<div class="clear"></div>
														</div><!--end of act -->
													</div><!-- end of caption -->
												</div><!-- end of item -->
											<?php endforeach;?>
										</div>
									</div><!-- end of rec scroll -->
									<div class="scrollerx">
										<div class="scrollerx-wrapper">
											<div class="scroll-element_size"></div>
											<div class="scroll-element_track"></div>
											<div class="scroll-bar"></div>
										</div>
									</div>
								</div><!-- end of rec list -->
							<?php endif;?>
							<?php if($i['button'] != '' && $i['url'] != ''):?>
								<div class="centered">
									<div class="wrapper">
										<a href="<?php echo $i['url'] ;?>" class="button"><?php echo $i['button'] ;?></a>
									</div><!-- end of wrapper -->
								</div><!-- end of centered -->
							<?php endif;?>
						</div><!-- end of item -->	
					<?php $x++;endforeach;?>
				</div><!-- eend of recwrap -->
			<?php endif;?>
			
		</div><!-- end of recom -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_testi_homepage2' ) ) {
	function avoskin_testi_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('testi', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
			'item' => 'testi_item',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		if(is_array($item) && !empty($item)):
	?>
		<div class="quote">
			<div class="wrapper">
				<div class="quotext">
					<div class="slick-carousel">
						<?php foreach($item as $i):?>
							<div class="item">
								<div class="txt">
									<?php echo $i['text'] ;?>
								</div><!-- end of txt -->
								<b><?php echo $i['from'] ;?></b>
							</div><!-- end of item -->
						<?php endforeach;?>
					</div><!-- end of slick -->
				</div><!-- end of quotext -->
				
				<div class="qlogo">
					<div class="slick-carousel">
						<?php foreach($item as $i):?>
							<div class="item"><img src="<?php echo $i['logo'] ;?>" width="240" /></div>
						<?php endforeach;?>
					</div><!-- end of slick carousel -->
				</div><!-- end of qlogo -->
			</div><!-- end of wrapper -->
		</div><!-- end of quote -->
        <?php
		endif;
	}
}

if ( ! function_exists( 'avoskin_caro_homepage2' ) ) {
	function avoskin_caro_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('caro', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'caro_title',
			'text' => 'caro_text',
			'total' => 'caro_total',
			'source' => 'caro_source',
			'btn' => 'caro_button',
			'url' => 'caro_link',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		//delete_transient('avoskin_blog_home2');
		//delete_transient('avoskin_blog_home1');
		//delete_transient('avoskin_blog');
		$blog = ($source != '' && $total != '' ) ? avoskin_fetch_blog($source, $total, 'home2') : '';
	?>
		<div class="caro">
			<div class="wrapper">
				<div class="hentry smaller">
					<?php if($title != ''):?>
						<h2><?php echo $title ;?></h2>
					<?php endif;?>
					<?php if($text != ''):?>
					<div class="txt">
						<?php echo $text ;?>
					</div><!-- end of txt -->
					<?php endif;?>
				</div><!-- end of hentry -->
				<?php if($blog != ''):?>
					<div class="carolist">
						<div class="slick-carousel">
							<?php echo $blog ;?>
						</div><!-- end of slick carousel -->
						<?php if($total > 3):?>
							<div class="slidenav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>
						<?php endif;?>
					</div><!-- end of carolist -->
				<?php endif;?>
				<?php if($btn != '' && $url != ''):?>
					<div class="centered">
						<a href="<?php echo $url ;?>" class="button"><?php echo $btn ;?></a>
					</div><!-- end of centered -->
				<?php endif;?>
			</div>
		</div><!-- end of caro -->
        <?php
	}
}

if ( ! function_exists( 'avoskin_subs_homepage2' ) ) {
	function avoskin_subs_homepage2() {
		if(!is_array(get_post_meta(get_the_ID(), 'visibility', true)) || !in_array('subs', get_post_meta(get_the_ID(), 'visibility', true))) return;
		$meta = [
                        'title' => 'subs_popup_title',
			'text' => 'subs_popup_text',
			'img' => 'subs_popup_img',
			'action' => 'subs_kirim_url',
			'delay' => 'subs_popup_delay',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
		$credential = [
			'url' => $action
                ];
		$delay = ((int)$delay * 1000);
	?>
		<div class="subs notload" data-delay="<?php echo $delay ;?>">
			<?php if($img != ''):?>
				<figure><img src="<?php echo $img ;?>" /></figure><?php endif;?><!--
			--><div class="caption">
				<a href="#" class="cls"></a>
				<?php if($title != ''):?>
					<h2><?php echo $title ;?></h2>
				<?php endif;?>
				<?php if($text != ''):?>
					<div class="txt">
						<?php echo $text ;?>
					</div>
				<?php endif;?>
				<div class="form-basic">
					<form>
						<input type="email" value="" placeholder="<?php _e('your email address','ohace');?>" required="required" name="email" />
						<input type="hidden" name="credential"  value='<?php echo base64_encode(wp_json_encode($credential));?>'>
						<button type="submit" class="button has-loading"><?php _e('Subscribe','ohace');?></button>
					</form>
				</div>
			</div><!-- end of caption -->
		</div><!-- end of subs -->
        <?php
	}
}