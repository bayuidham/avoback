<?php
/*
Template Name: Page Redeem Waste for Change
*/
?>
<?php get_header();?>
        <?php
        // ::CRWFC::
        if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
                        'title' => 'banner_title',
                        'img' => 'banner_img',
                        'mimg' => 'banner_mimg',
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
                $coupon = WC()->cart->applied_coupons;
                $has_product = false;
        ?>
        
                <div class="inner-rwfc">
                        <div class="banner">
                                <?php if($img != ''):?>
                                        <span class="bg" style="background-image: url('<?php echo $img ;?>');"></span>
                                <?php endif;?>
                                <?php if($mimg != ''):?>
                                        <span class="bg-mobile" style="background-image: url('<?php echo $mimg ;?>');"></span>
                                <?php endif;?>
                                <?php if($title != ''):?>
                                        <div class="wrapper">
                                                <h2><?php echo $title ;?></h2>
                                        </div><!-- end of wrapper -->
                                <?php endif;?>
                        </div><!-- end of banner -->
                        
                        <?php if(is_array($coupon) && !empty($coupon)):?>
                                <?php foreach($coupon as $c):
                                        $wfc = new WC_Coupon($c);
                                        if($wfc->get_discount_type() == 'avo_wfc'):
                                                $products = get_post_meta(wc_get_coupon_id_by_code($c), 'wfc_products', true);
                                ?>
                                                <div class="best">
                                                        <div class="wrapper">
                                                                <?php if(is_array($products) && !empty($products)):
                                                                        $has_product = true;
                                                                ?>
                                                                        <div class="prodlist rowflex">
                                                                                 <?php foreach($products as $p ):
                                                                                        $product = wc_get_product((int)$p);
                                                                                        $id = $product->get_id();
                                                                                ?>
                                                                                        <div class="proditem">
                                                                                                <figure>
                                                                                                        <?php echo $product->get_image([600, 520]);?>
                                                                                                        <?php if($product->get_stock_status() != 'instock'):?>
                                                                                                                <span class="stock-empty"><?php avoskin_oos_text($id);?></span>
                                                                                                        <?php endif;?>
                                                                                                </figure>
                                                                                                <div class="caption">
                                                                                                        <h3><?php echo $product->get_name() ;?></h3>
                                                                                                        <div class="cats">
                                                                                                                <?php echo wc_get_product_category_list($id);?>
                                                                                                        </div><!-- end of cats -->
                                                                                                        <?php if($product->get_stock_status() == 'instock'):?>
                                                                                                        <div class="ract">
                                                                                                                <a href="#" class="button has-loading btn-add-to-cart-wfc" data-code="<?php echo $c ;?>" data-id="<?php echo $id ;?>"><?php _e('Ambil Barang','avoskin');?></a>
                                                                                                        </div>
                                                                                                        <?php endif;?>
                                                                                                </div><!-- end of caption -->
                                                                                        </div><!-- end of item -->
                                                                                <?php endforeach;?>
                                                                        </div><!-- end of prod list -->
                                                                <?php endif;?>
                                                        </div><!-- end of wrapper -->
                                                </div><!-- end of best -->
                                        <?php endif;?>
                                <?php endforeach;?>
                        <?php endif;?>
                        <?php if(!$has_product):?>
                                <div class="wrapper">
                                        <div class="pusher">
                                                <div class="format-text" style="text-align: center">
                                                        <?php _e('No coupon code applied','avoskin');?>
                                                </div><!-- end of format text -->
                                        </div><!-- end f pusher -->
                                </div><!-- end of wrapper -->
                        <?php endif;?>
                </div><!-- end of inner rwfc -->
                
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>