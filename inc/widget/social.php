<?php

class Widget_Social extends WP_Widget {

	// constructor
	function __construct() {
		parent::__construct(false, $name = __('Social', 'avoskin') , ['description' => __('Add social account widget for your website','avoskin')]);
	}

	// widget form creation
        function form($instance) {
                // Check values
                $social_pair = [
                        0 => [
                                'key' => 'fb_',
                                'name' => 'Facebook',
                        ],
                        1 => [
                                'key' => 'tw_',
                                'name' => 'Twitter',
                        ],
                        2 => [
                                'key' => 'ig_',
                                'name' => 'Instagram',
                        ],
			3 => [
                                'key' => 'wa_',
                                'name' => 'WhatsApp',
                        ]
                ];
        ?>
                
                <p>
                        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'avoskin'); ?></label>
                        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo (isset($instance['title'])) ? esc_attr($instance['title']) : '';  ?>" />
                </p>
                
                <?php foreach($social_pair as $s):?>
                        <p>
                                <label for="<?php echo $this->get_field_id($s['key'].'url'); ?>"><?php echo $s['name'] . __(' Account URL', 'avoskin'); ?></label>
                                <input class="widefat" id="<?php echo $this->get_field_id($s['key'].'url'); ?>"
                                                name="<?php echo $this->get_field_name($s['key'].'url'); ?>"
                                                type="text"
                                                value="<?php echo (isset($instance[$s['key'].'url'])) ? esc_attr($instance[$s['key'].'url']) : '';  ?>" />
                        </p>
                <?php endforeach;?>
                <?php
        }

	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
                // Fields
                $instance['title'] = strip_tags($new_instance['title']);
                $social_key = ['fb_','tw_','ig_','wa_'];
                foreach($social_key as $prefix){
                        $instance[$prefix.'url'] =$new_instance[$prefix.'url'];
                }
                
               return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
                // these are the widget options
                $title = (isset( $instance['title']) ) ?  apply_filters('widget_title', $instance['title']) : '';
                $social_key = [
                        0 => [
                                'key' => 'fb',
                                'title' => 'Facebook',
                                'icon' => 'facebook-square',
                        ],
                        1 => [
                                'key' => 'tw',
                                'title' => 'Twitter',
                                'icon' => 'twitter',
                        ],
                        2 => [
                                'key' => 'ig',
                                'title' => 'Instagram',
                                'icon' => 'instagram',
                        ],
                        3 => [
                                'key' => 'wa',
                                'title' => 'WhatsApp',
                                'icon' => 'whatsapp',
                        ]
                ];
                echo $before_widget;
                // Display the widget
             
                // Check if title is set
                if ( $title ) {
                   echo $before_title . $title . $after_title;
                }
                ?>
                <div class="holder">
                        <?php foreach($social_key as $s):?>
                                <?php if(isset($instance[$s['key'].'_url'] ) && $instance[$s['key'].'_url'] != '' ):?>
                                        <a href="<?php echo $instance[$s['key'].'_url'];?>" class="<?php echo $s['key'];?>" title="<?php echo $s['title'];?>">
                                                <i class="fa-<?php echo $s['icon'];?>"></i>
                                        </a>
                                <?php endif; ?>
                        <?php endforeach;?>
                </div>
                <?php
                echo $after_widget;
	}
	
}
// register widget
add_action('widgets_init', 'register_social_widget');
function register_social_widget(){
       register_widget("Widget_Social");
}