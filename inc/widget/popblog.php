<?php

class Widget_Popblog extends WP_Widget {

	// constructor
	function Widget_Popblog() {
		parent::__construct(false, $name = __('Popular Posts', 'avoskin') , array('description' => __('Add popular posts to widget','avoskin')));
	}

	// widget form creation
        function form($instance) {
        
                // Check values
                if( $instance) {
		     $term = esc_attr($instance['term']);
		     $order = esc_attr($instance['sort']);
                } else {
		     $term = 'date';
		     $order = 'DESC';
                }
		$opts = array(
			'date' => __('Date','avoskin'),
			'title' => __('Title','avoskin'),
			'rand' => __('Random','avoskin'),
		);
		$sort = array(
			'ASC' => 'ASC',
			'DESC' => 'DESC',
		);
        ?>
                
                <p>
                        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'avoskin'); ?></label>
                        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo (isset($instance['title'])) ? esc_attr($instance['title']) : '';  ?>" />
                </p>
                
                <p>
                        <label for="<?php echo $this->get_field_id('term'); ?>"><?php _e('Sort post by : ', 'avoskin'); ?></label>
			<select name="<?php echo $this->get_field_name('term'); ?>" id="<?php echo $this->get_field_id('term'); ?>" class="widefat">
			<?php
			foreach ($opts as $opt => $val ) {
				echo '<option value="' . $opt . '" id="' . $opt . '"', $term == $opt ? ' selected="selected"' : '', '>', $val, '</option>';
			}
			?>
			</select>
                </p>
		
		<p>
                        <label for="<?php echo $this->get_field_id('sort'); ?>"><?php _e('Order post by : ', 'avoskin'); ?></label>
			<select name="<?php echo $this->get_field_name('sort'); ?>" id="<?php echo $this->get_field_id('sort'); ?>" class="widefat">
			<?php
			foreach ($sort as $s => $v ) {
				echo '<option value="' . $s . '" id="' . $s . '"', $order == $s ? ' selected="selected"' : '', '>', $v, '</option>';
			}
			?>
			</select>
                </p>
                
                <p>
                        <label for="<?php echo $this->get_field_id('total'); ?>"><?php _e('Total posts to show', 'avoskin'); ?></label>
                        <input class="widefat" id="<?php echo $this->get_field_id('total'); ?>" name="<?php echo $this->get_field_name('total'); ?>" type="text" value="<?php echo (isset($instance['total'])) ? esc_attr($instance['total']) : '';  ?>" />
                </p>
                <?php
        }

	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
                // Fields
                $instance['title'] = strip_tags($new_instance['title']);
                $instance['term'] =$new_instance['term'];
                $instance['total'] =$new_instance['total'];
		$instance['sort'] =$new_instance['sort'];
               return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
                // these are the widget options
                $title = (isset( $instance['title']) ) ?  apply_filters('widget_title', $instance['title']) : '';
                $term = (isset( $instance['term']) ) ?  $instance['term'] : 'date';
		$sort =  (isset( $instance['sort']) ) ?  $instance['sort'] : 'DESC';
                $total =  (isset( $instance['total']) ) ?  $instance['total'] : -1;
                echo $before_widget;
                // Display the widget
             
                // Check if title is set
                if ( $title ) {
                   echo $before_title . $title . $after_title;
                }
                ?>
		<div class="list">
		<?php
			$loop = new WP_Query([
			'post_type' => 'post',
			'orderby' => $term,
			'order' => $sort,
			'posts_per_page' => $total,
		]);?>
		
		<?php if($loop->have_posts()) : while($loop->have_posts()):$loop->the_post();
			$thumb = get_post_thumbnail_id();
			$img_url = wp_get_attachment_url( $thumb,'full'); 
			$image = avoskin_resize( $img_url, 258, 254, true, true, true );
		?>
			<div class="item">
				<figure><a href="<?php the_permalink();?>"><img src="<?php echo $image ;?>" /></a></figure>
				<div class="caption">
					<div class="cat"><?php the_category(', ');?></div>
					<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
				</div><!-- end of caption -->
			</div><!-- end of item -->
		<?php endwhile; ?>
		
		<?php else:?>
			<div class="no-post-found">
				<div class="format-text the-content">
					<p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
				</div>
			</div>
		<?php endif;?>
		<?php wp_reset_query(); ?>
		</div><!-- end of holder -->
                <?php
                echo $after_widget;
	}
	
}
// register widget
add_action('widgets_init', 'register_popblog_widget');
function register_popblog_widget(){
       register_widget("Widget_Popblog");
}

