<?php

class Widget_Contact extends WP_Widget {

	// constructor
	function __construct() {
		parent::__construct(false, $name = __('Contact', 'avoskin') , ['description' => __('Add contact widget for your website','avoskin')]);
	}

	// widget form creation
        function form($instance) {
                // Check values
                $contact_pair = [
                        0 => [
                                'key' => 'email_',
                                'name' => 'Email',
                        ],
                        1 => [
                                'key' => 'phone_',
                                'name' => 'Phone',
                        ]
                ]
        ?>
                
                <p>
                        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'avoskin'); ?></label>
                        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo (isset($instance['title'])) ? esc_attr($instance['title']) : '';  ?>" />
                </p>
                
                <?php foreach($contact_pair as $s):?>
                        <p>
                                <label for="<?php echo $this->get_field_id($s['key'].'value'); ?>"><?php echo $s['name'];?></label>
                                <input class="widefat" id="<?php echo $this->get_field_id($s['key'].'value'); ?>"
                                                name="<?php echo $this->get_field_name($s['key'].'value'); ?>"
                                                type="text"
                                                value="<?php echo (isset($instance[$s['key'].'value'])) ? esc_attr($instance[$s['key'].'value']) : '';  ?>" />
                        </p>
                <?php endforeach;?>
                <?php
        }

	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
                // Fields
                $instance['title'] = strip_tags($new_instance['title']);
                $contact_key = ['email_','phone_'];
                foreach($contact_key as $prefix){
                        $instance[$prefix.'value'] =$new_instance[$prefix.'value'];
                }
                
               return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
                // these are the widget options
                $title = (isset( $instance['title']) ) ?  apply_filters('widget_title', $instance['title']) : '';
                echo $before_widget;
                // Display the widget
             
                // Check if title is set
                if ( $title ) {
                   echo $before_title . $title . $after_title;
                }
                ?>
                <div class="info">
			<?php if($instance['email_value'] != ''):?>
				<a href="mailto:<?php echo $instance['email_value'];?>" ><i class="fa-envelope"></i><span><?php echo $instance['email_value'];?></span></a>
			<?php endif;?>
			<?php if($instance['phone_value'] != ''):?>
				<a href="tel:<?php echo $instance['phone_value'];?>"><i class="fa-phone"></i><span><?php echo $instance['phone_value'];?></span></a>
			<?php endif;?>
                </div>
                <?php
                echo $after_widget;
	}
	
}
// register widget
add_action('widgets_init', 'register_contact_widget');
function register_contact_widget(){
       register_widget("Widget_Contact");
}