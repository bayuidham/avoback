<?php
/*
Template Name: Page Waste for Change
*/
?>
<?php get_header();?>
        <?php
        // ::CRWFC::
        if(have_posts()) : while(have_posts()) : the_post();
                $meta = [
                        'visibility' => 'visibility',
                        'order' => 'section_order',
                        
                        'ibanner' => 'intro_banner',
                        'imbanner' => 'intro_mbanner',
                        'isubtitle' => 'intro_subtitle',
                        'ibtn' => 'intro_btn',
                        'iurl' => 'intro_url',
                        
                        'copy' => 'copy_item',
                        
                        'rbg' => 'redeem_bg',
                        'rmbg' => 'redeem_mbg',
                        'rtitle' => 'redeem_title',
                        'rtext' => 'redeem_text',
                        'rplace' => 'redeem_placeholder',
                        'rbtn' => 'redeem_btn',
                        
                        'btitle' => 'benefit_title',
                        'btext' => 'benefit_text',
                        'bitem' => 'benefit_item',
                        'bbtn' => 'benefit_btn',
                        'burl' => 'benefit_url',
                        
                        'ftype' => 'info_type',
                        'fbanner' => 'info_banner',
                        'fmbanner' => 'info_mbanner',
                        'fbg' => 'info_bg',
                        'ftitle' => 'info_title',
                        'ftext' => 'info_text',
                        
                        
                        'ptitle' => 'best_title',
                        'ptext' => 'best_text',
                        'pbtn' => 'best_btn',
                        'purl' => 'best_url',
                        'pitem' => 'best_product',
                        
                        
                        'cbg' => 'cta_bg',
                        'cmbg' => 'cta_mbg',
                        'cthumb' => 'cta_thumb',
                        'ctitle' => 'cta_title',
                        'ctext' => 'cta_text',
                        'cshop' => 'cta_shop',
                        'curl' => 'cta_url'
                ];
                extract(avoskin_get_meta(get_the_ID(), $meta));
        ?>
        
                <div class="inner-wfc">
                        <?php if(is_array($order) && !empty($order)):?>
                                <?php foreach($order as $ord):?>
                                        <?php if($ord == 'intro'):?>
                                                <?php if(is_array($visibility) && in_array('intro', $visibility)):?>
                                                        <div class="intro">
                                                                <?php if($ibanner != ''):?>
                                                                        <figure>
                                                                                <img src="<?php echo $ibanner ;?>" alt="home"
                                                                                        srcset="
                                                                                                <?php echo $imbanner ;?>  500w,
                                                                                                <?php echo $ibanner ;?>  800w
                                                                                        "
                                                                                        sizes="(min-width: 769px) 100vw, 30vw"
                                                                                />
                                                                        </figure>
                                                                <?php endif;?>
                                                                <div class="wrapper">
                                                                        <div class="txt">
                                                                                <?php if($isubtitle != ''):?><p><?php echo $isubtitle ;?></p><?php endif;?>
                                                                        </div><!-- end of txt -->
                                                                        <?php if($ibtn != '' && $iurl != ''):?>
                                                                                <a href="<?php echo $iurl ;?>" class="button"><?php echo $ibtn ;?></a>
                                                                        <?php endif;?>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of intro -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'copy'):?>
                                                <?php if(is_array($copy) && !empty($copy) && is_array($visibility) && in_array('copy', $visibility)):?>
                                                        <div class="copy">
                                                                <div class="wrapper">
                                                                        <?php foreach($copy as $c):?>
                                                                                <div class="item">
                                                                                        <?php if($c['img'] != ''):?>
                                                                                                <figure><img src="<?php echo $c['img'] ;?>" /></figure>
                                                                                        <?php endif;?>
                                                                                        <div class="caption">
                                                                                                <?php if($c['title'] != ''):?>
                                                                                                        <h2><?php echo $c['title'];?></h2>
                                                                                                <?php endif;?>
                                                                                                <?php if($c['text'] != '') :?>
                                                                                                        <div class="txt"><?php echo $c['text'] ;?></div><!-- end of txt -->
                                                                                                <?php endif;?>
                                                                                        </div><!-- en dof caption -->
                                                                                </div><!-- end of item -->
                                                                        <?php endforeach;?>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of copy -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'redeem'):
                                                $applied = avoskin_has_wfc_coupon();
                                        ?>
                                                <?php if(is_array($visibility) && in_array('redeem', $visibility)):?>
                                                        <div class="redeem">
                                                                <div class="wrap">
                                                                        <div class="action">
                                                                                <div class="holder">
                                                                                        <div class="txt">
                                                                                                <?php if($rtitle != ''):?><h2><?php echo $rtitle ;?></h2><?php endif;?>
                                                                                                <?php if($rtext != ''):?>
                                                                                                        <div class="txt"><?php echo $rtext ;?></div><!-- end of txt -->
                                                                                                <?php endif;?>
                                                                                                <div class="form-basic">
                                                                                                        <form id="wfc-redeem">
                                                                                                                <input type="text" value="" placeholder="<?php echo $rplace ;?>" name="code" required="required" />
                                                                                                                <input type="hidden" name="applied" value="<?php echo $applied ;?>" />
                                                                                                                <input type="hidden" name="redeem" value="<?php echo  get_permalink(get_theme_mod('avoskin_wfc_redeem_page'));?>" />
                                                                                                                <button type="submit" class="button has-loading"><?php echo $rbtn ;?></button>
                                                                                                        </form>
                                                                                                </div><!-- end of form basic -->
                                                                                        </div><!-- end of txt -->
                                                                                </div><!-- end of holder -->
                                                                        </div><!-- end of action -->
                                                                        <?php if($rbg != ''):?>
                                                                                <figure>&nbsp;</figure>
                                                                        <?php endif;?>
                                                                </div><!-- end of wrap -->
                                                                <span class="bg" style="background-image: url('<?php echo $rbg ;?>')"></span>
                                                                <img src="<?php echo $rmbg ;?>" class="mbg" />
                                                        </div><!-- end of redeem -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'benefit'):?>
                                                <?php if(is_array($visibility) && in_array('benefit', $visibility)):?>
                                                        <div class="benefit">
                                                                <div class="wrapper">
                                                                        <?php if($btitle != '' || $btext != ''):?>
                                                                                <div class="hentry">
                                                                                        <?php if($btitle != ''):?><h2><?php echo $btitle ;?></h2><?php endif;?>
                                                                                        <?php if($btext != ''):?>
                                                                                                <div class="txt"><?php echo $btext ;?></div><!-- end of txt -->
                                                                                        <?php endif;?>
                                                                                </div><!-- end of hentry -->
                                                                        <?php endif;?>
                                                                        <?php if(is_array($bitem) && !empty($bitem)):?>
                                                                                <div class="rowflex">
                                                                                        <?php foreach($bitem as $b):?>
                                                                                                <div class="item">
                                                                                                        <figure><img src="<?php echo $b['img'] ;?>" /></figure>
                                                                                                        <h3><?php echo $b['title'] ;?></h3>
                                                                                                </div><!-- end of item -->
                                                                                        <?php endforeach;?>
                                                                                </div><!-- end of rowflex -->
                                                                        <?php endif;?>
                                                                        <?php if($bbtn != '' && $burl != ''):?>
                                                                                <div class="centered">
                                                                                        <a href="<?php echo $burl ;?>" class="button"><?php echo $bbtn ;?></a>
                                                                                </div><!-- end of centered -->
                                                                        <?php endif;?>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of benefit -->
                                                <?php endif;?>
                                                
                                        <?php elseif($ord == 'info'):?>
                                                <?php if(is_array($visibility) && in_array('info', $visibility)):?>
                                                        <div class="copy">
                                                                <?php if($ftype != 'banner'):?>
                                                                <div class="wrapper">
                                                                        <div class="item">
                                                                                <?php if($fbg != ''):?>
                                                                                        <figure><img src="<?php echo $fbg ;?>" /></figure>
                                                                                <?php endif;?>
                                                                                <div class="caption">
                                                                                        <?php if($ftitle != ''):?><h2><?php echo $ftitle ;?></h2><?php endif;?>
                                                                                        <?php if($ftext != ''):?>
                                                                                                <div class="txt"><?php echo $ftext ;?></div><!-- end of txt -->
                                                                                        <?php endif;?>
                                                                                </div><!-- en dof caption -->
                                                                        </div><!-- end of item -->
                                                                </div><!-- end of wrapper -->
                                                                <?php else:?>
                                                                        <div class="coban">
                                                                                <img src="<?php echo $fbanner ;?>" alt="home"
                                                                                        srcset="
                                                                                                <?php echo $fmbanner ;?>  500w,
                                                                                                <?php echo $fbanner ;?>  800w
                                                                                        "
                                                                                        sizes="(min-width: 769px) 100vw, 30vw"
                                                                                />
                                                                        </div>
                                                                <?php endif;?>
                                                        </div><!-- end of copy -->
                                                <?php endif;?>
                                                
                                        <?php elseif($ord == 'best'):?>
                                                <?php if(is_array($visibility) && in_array('best', $visibility)):?>
                                                        <div class="best">
                                                                <div class="wrapper">
                                                                        <?php if($ptitle != ''  || $ptext != ''):?>
                                                                                <div class="hentry">
                                                                                        <?php if($ptitle != ''):?><h2><?php echo $ptitle ;?></h2><?php endif;?>
                                                                                        <?php if($ptext != ''):?>
                                                                                                <div class="txt"><?php echo $ptext ;?></div><!-- end of txt -->
                                                                                        <?php endif;?>
                                                                                </div><!-- end of hentry -->
                                                                        <?php endif;?>
                                                                        <?php if(is_array($pitem) && !empty($pitem)):?>
                                                                                <div class="prodlist rowflex">
                                                                                        <?php foreach($pitem as $i ):
                                                                                                $product = wc_get_product((int)$i['product']);
                                                                                                $id = $product->get_id();
                                                                                                $price = $product->get_regular_price();
                                                                                                $sale = $product->get_sale_price();
                                                                                                $percent = '';
                                                                                                if(!$product->is_on_sale()){
                                                                                                        $sale = 0;
                                                                                                }
                                                                                                if($sale > 0){
                                                                                                        $percent = ceil((($price - $sale )  / $price ) * 100);
                                                                                                }
                                                                                                $stock = ceil(get_post_meta( $id, '_stock', true ));
                                                                                        ?>
                                                                                                <div class="proditem">
                                                                                                        <figure>
                                                                                                                <a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"></a>
                                                                                                                <?php if($percent != ''):?>
                                                                                                                        <span class="disc">-<?php echo $percent ;?>%</span>
                                                                                                                <?php endif;?>
                                                                                                                <a href="<?php echo get_permalink($id);?>">
                                                                                                                        <?php echo $product->get_image([600, 520]);?>
                                                                                                               </a>
                                                                                                               <?php if($product->get_stock_status() != 'instock'):?>
                                                                                                                        <span class="stock-empty"><?php avoskin_oos_text($id);?></span>
                                                                                                                <?php endif;?>
                                                                                                        </figure>
                                                                                                        <div class="caption">
                                                                                                                <h3><a href="<?php echo $product->get_permalink() ;?>"><?php echo $product->get_name() ;?></a></h3>
                                                                                                                <div class="cats">
                                                                                                                        <?php echo wc_get_product_category_list($id);?>
                                                                                                                </div><!-- end of cats -->
                                                                                                                <div class="pricing"><?php echo $product->get_price_html(); ?></div>
                                                                                                                <div class="act">
                                                                                                                        <div class="rated">
                                                                                                                                <i class="fa-star"></i><b><?php echo $product->get_average_rating() ;?></b>
                                                                                                                        </div><!-- end of rated -->
                                                                                                                        <a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"></a>
                                                                                                                        <div class="clear"></div>
                                                                                                                </div><!--end of act -->
                                                                                                        </div><!-- end of caption -->
                                                                                                </div><!-- end of item -->
                                                                                        <?php endforeach;?>
                                                                                </div><!-- end of prod list -->
                                                                        <?php endif;?>
                                                                        <?php if($pbtn != '' && $purl != ''):?>
                                                                                <div class="centered">
                                                                                        <a href="<?php echo $purl ;?>" class="button"><?php echo $pbtn ;?></a>
                                                                                </div><!-- end of centered -->
                                                                        <?php endif;?>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of best -->
                                                <?php endif;?>
                                        <?php elseif($ord == 'cta'):?>
                                                <?php if(is_array($visibility) && in_array('cta', $visibility)):?>
                                                        <div class="cta">
                                                                <div class="wrapper">
                                                                        <a href="<?php echo $curl ;?>">
                                                                                <div class="holder" >
                                                                                        <?php if($cbg != ''):?>
                                                                                                <span class="bg" style="background-image: url('<?php echo $cbg ;?>');"></span>
                                                                                        <?php endif;?>
                                                                                        <?php if($cmbg != ''):?>
                                                                                                <span class="bg-mobile" style="background-image: url('<?php echo $cmbg ;?>');"></span>
                                                                                        <?php endif;?>
                                                                                        <div class="caption">
                                                                                                <div class="txt">
                                                                                                        <?php if($ctitle != ''):?>
                                                                                                                <h2><?php echo $ctitle ;?></h2>
                                                                                                        <?php endif;?>
                                                                                                        <?php if($ctext != ''):?><?php echo $ctext ;?><?php endif;?>
                                                                                                </div><!-- end of txt -->
                                                                                                <div class="action">
                                                                                                        <figure>
                                                                                                                <?php if($cthumb != ''):?>
                                                                                                                        <img src="<?php echo $cthumb ;?>" width="115" />
                                                                                                                <?php endif;?>
                                                                                                                <?php if($cshop != ''):?><span><?php echo $cshop ;?></span><?php endif;?>
                                                                                                        </figure>
                                                                                                </div><!-- end of action -->
                                                                                        </div><!-- end  of caption -->
                                                                                        <div class="clear"></div>
                                                                                </div><!-- end of holder -->
                                                                        </a>
                                                                </div><!-- end of wrapper -->
                                                        </div><!-- end of cta -->
                                                <?php endif;?>
                                        <?php endif;?>
                                <?php endforeach;?>
                        <?php endif;?>
                </div><!-- end of inner wfc -->
        <?php endwhile;?>
        <?php else : ?>
                <div class="inner-page">
                        <div class="format-text wrapper">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                </div>
        <?php endif;?>
<?php get_footer();?>