<?php
/**
 * Avoskin Class
 *
 * @since    1.0.0
 * @package  avoskin
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Avoskin' ) ) {

	/**
	 * The main Avoskin class
	 */
	class Avoskin {
		private $controller;

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			$this->controller = \Avoskin\Controller::get_instance();
			
			//Setup basic settings
			add_action( 'after_setup_theme', [ $this, 'setup' ] );
			
			//Prepare widget placeholder
			add_action( 'widgets_init',  [$this, 'widgets_init' ] );
			
			//Register script
			add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ], 10 );
			
			//Filter search result for specific post type
			add_filter('pre_get_posts',[$this, 'search_filter']);
			
			//Rewrite search URL
			add_action( 'template_redirect', [$this, 'search_redirect'] );
			
			//Register customizer options
			add_action('customize_register' , [$this, 'register_customizer']);
			
			//Hide editor for some page template
			add_action('admin_init', [$this, 'hide_editor']); 
			
			//Disable adminbar
			add_filter('show_admin_bar', '__return_false');
			
			//Upload svg support
			add_filter('upload_mimes', [$this,'mime_types']);
			add_action('admin_head', [$this,'custom_admin_head']);
			
			//Enqueque script for admin
			add_action( 'admin_enqueue_scripts' ,[$this,'admin_scripts'], 9999 );
			
			//Reformat widget list
			add_filter('wp_list_categories', [$this,'cat_count_span']);
			
			//Register custom post type
			add_action( 'init', [ $this, 'post_type' ], 10 );
			
			//Add referral query var
			add_filter( 'query_vars', [$this, 'add_referral_query_var'] );
			
			//Create custom table for referral
			add_action('after_switch_theme', [$this, 'create_referral_table']);
			
			//Create custom admin menu
			add_action('admin_menu', [$this,'add_admin_menu']);
			
			//Add verified metabox for user
			add_action('show_user_profile', [$this,'add_verified_field']);
			add_action('edit_user_profile', [$this,'add_verified_field']);
			add_action('personal_options_update', [$this,'save_verified_field']);
			add_action('edit_user_profile_update', [$this,'save_verified_field']);
			
			//Add column to member archive list
			add_filter('manage_avoskin-member_posts_columns', [$this,'member_column_head']);
			add_action('manage_avoskin-member_posts_custom_column', [$this,'member_column_content'], 10, 2);
			
			//Add column to order archive list
			add_filter('manage_edit-shop_order_columns', [$this,'shop_order_column_head']);
			add_action('manage_shop_order_posts_custom_column', [$this,'shop_order_column_content'], 10, 2);
			
			//Change search text placeholder
			add_filter( 'get_search_form', [$this,'avoskin_search_form'] );
			
			//Generate shortcode customer
			add_shortcode( 'customer', [$this,'customer_order_name'] );
			
			//Disable password change emails
			add_filter( 'send_password_change_email', '__return_false' );
			
			add_action('admin_footer', [$this,'inject_script'], 9999);
		}
		
		public function inject_script(){
			$screen = get_current_screen();
			if('woocommerce_page_wc-settings' == $screen->id){
				?>
				<script type="text/javascript">
					;(function($){
						$(function(){
							$('#reset-sicepat-counter').on('click', function(e){
								e.preventDefault();
								$.post('<?php echo site_url('/wp-json/avoskin/v1/sicepat_reset_counter/');?>', function(result){
									window.location.reload();
								});      
							});
						});
					}(jQuery));
				</script>
				<?php
			}
			if('avoskin-referral' == $screen->id){
				?>
				<script type="text/javascript">
					;(function($){
						$(function(){
							var code = $('#referral_code'),
								camp = $('#campaign'),
								link = $('#url');
								
							$('#generate').on('click', function(e){
								e.preventDefault();
								if (code.val() != '') {
									if (camp.val() != '') {
										var url = camp.val() + '?referral='+code.val();
										link.val(url);
										var $temp = $("<input>");
										$("body").append($temp);
										$temp.val(link.val()).select();
										document.execCommand("copy");
										$temp.remove();
										alert('Referral link successfully copied!');
									}else{
										alert('Please input campaign link!');
									}
								}else{
									alert('Please input referral code!');
								}
							});
						});
					}(jQuery));
				</script>
				<?php
				
			}
			
			if('product' == $screen->id){
			?>
			<script type="text/javascript">
				;(function($){
					$(function(){
						$('form#post').on('submit', function(e){
							var valid = true,
								msg = '';
								
							if ($('#_weight').val() == '') {
								msg = 'Silahkan masukan berat/weight produk!';
								valid = false;
							}
							
							if ($('#_regular_price').val() == '') {
								msg = 'Silahkan masukan harga produk!';
								valid = false;
							}
							
							if (!valid) {
								e.preventDefault();
								alert(msg);
							}
						});
					});
				}(jQuery));
			</script>
			<?php
			}
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {
			load_theme_textdomain( 'avoskin', trailingslashit( WP_LANG_DIR ) . 'themes' );
			load_theme_textdomain( 'avoskin', get_stylesheet_directory() . '/languages' );
			load_theme_textdomain( 'avoskin', get_template_directory() . '/languages' );

			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'post-thumbnails' );
			
			register_nav_menus(
				apply_filters(
					'avoskin_register_nav_menus', [
						'primary'   => __( 'Primary Menu', 'avoskin' ),
						'primary_mobile'   => __( 'Primary Mobile Menu', 'avoskin' ),
						'account_login'   => __( 'Account Login Menu', 'avoskin' ),
						'account_not_login'   => __( 'Account Not Login Menu', 'avoskin' )
					]
				)
			);
			add_theme_support(
				'html5', apply_filters(
					'avoskin_html5_args', [
						'search-form',
						'comment-form',
						'comment-list',
						'gallery',
						'caption',
						'widgets',
					]
				)
			);
			add_theme_support( 'title-tag' );
			add_theme_support( 'customize-selective-refresh-widgets' );
			add_theme_support( 'responsive-embeds' );
			
		}

		/**
		 * Register widget area.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		public function widgets_init() {
			$sidebar_args['sidebar'] = [
				'name'        => __( 'Sidebar', 'avoskin' ),
				'id'          => 'sidebar-1',
				'description' => '',
			];
			
			$sidebar_args['blogsidebar'] = [
				'name'        => __( 'Blog Sidebar', 'avoskin' ),
				'id'          => 'sidebar-2',
				'description' => '',
			];
			
			$rows = intval( apply_filters( 'avoskin_footer_widget_rows', 1 ) );
			$regions = intval( apply_filters( 'avoskin_footer_widget_columns', 4 ) );

			for ( $row = 1; $row <= $rows; $row++ ) {
				for ( $region = 1; $region <= $regions; $region++ ) {
					$footer_n = $region + $regions * ( $row - 1 ); // Defines footer sidebar ID.
					$footer   = sprintf( 'footer_%d', $footer_n );

					if ( 1 === $rows ) {
						/* translators: 1: column number */
						$footer_region_name = sprintf( __( 'Footer Column %1$d', 'avoskin' ), $region );

						/* translators: 1: column number */
						$footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of the footer.', 'avoskin' ), $region );
					} else {
						/* translators: 1: row number, 2: column number */
						$footer_region_name = sprintf( __( 'Footer Row %1$d - Column %2$d', 'avoskin' ), $row, $region );

						/* translators: 1: column number, 2: row number */
						$footer_region_description = sprintf( __( 'Widgets added here will appear in column %1$d of footer row %2$d.', 'avoskin' ), $region, $row );
					}

					$sidebar_args[ $footer ] = [
						'name'        => $footer_region_name,
						'id'          => sprintf( 'footer-%d', $footer_n ),
						'description' => $footer_region_description,
					];
				}
			}

			$sidebar_args = apply_filters( 'avoskin_sidebar_args', $sidebar_args );

			foreach ( $sidebar_args as $sidebar => $args ) {
				$widget_tags = [
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h3 class="widget-title">',
					'after_title'   => '</h3>',
				];

				/**
				 * Dynamically generated filter hooks. Allow changing widget wrapper and title tags. See the list below.
				 *
				 * 'avoskin_sidebar_widget_tags'
				 *
				 * 'avoskin_footer_1_widget_tags'
				 * 'avoskin_footer_2_widget_tags'
				 * 'avoskin_footer_3_widget_tags'
				 * 'avoskin_footer_4_widget_tags'
				 */
				$filter_hook = sprintf( 'avoskin_%s_widget_tags', $sidebar );
				$widget_tags = apply_filters( $filter_hook, $widget_tags );

				if ( is_array( $widget_tags ) ) {
					register_sidebar( $args + $widget_tags );
				}
			}
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function scripts() {
			global $avoskin_version;
			
			//Prolly do some minification and combine assets in future
			
			/**
			 * Styles
			 */
			//wp_enqueue_style( 'avoskin-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-reset', get_template_directory_uri() . '/assets/css/reset.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-fonts', get_template_directory_uri() . '/assets/css/fonts.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-icon', get_template_directory_uri() . '/assets/css/icon.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-slick', get_template_directory_uri() . '/assets/css/slick.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-fancy', get_template_directory_uri() . '/assets/css/fancy.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-scrollbar', get_template_directory_uri() . '/assets/css/jquery.scrollbar.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-confirm', get_template_directory_uri() . '/assets/css/confirm.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-generic', get_template_directory_uri() . '/assets/css/generic.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-style', get_template_directory_uri() . '/assets/css/style.css', '', $avoskin_version );
			//wp_enqueue_style( 'avoskin-mobile', get_template_directory_uri() . '/assets/css/mobile.css', '', $avoskin_version );
			
			//Compressed
			wp_enqueue_style( 'avoskin-app', get_template_directory_uri() . '/assets/css/app.css', '', $avoskin_version );
			wp_enqueue_style( 'avoskin-extras', get_template_directory_uri() . '/assets/css/dev/extras.css', '', $avoskin_version );
			wp_enqueue_style( 'avoskin-blog', get_template_directory_uri() . '/assets/css/dev/blog.css', '', $avoskin_version );

			/**
			 * Scripts
			 */
			wp_dequeue_script('jquery');
			wp_deregister_script('jquery');
			
			wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js', [], $avoskin_version, false );
			//wp_enqueue_script( 'avoskin-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-sticky', get_template_directory_uri() . '/assets/js/sticky.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-slick', get_template_directory_uri() . '/assets/js/slick.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-fancy', get_template_directory_uri() . '/assets/js/fancy.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-countdown', get_template_directory_uri() . '/assets/js/countdown.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-numeral', get_template_directory_uri() . '/assets/js/numeral.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-scrollbar', get_template_directory_uri() . '/assets/js/jquery.scrollbar.min.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-dotdot', get_template_directory_uri() . '/assets/js/dotdot.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-axios', get_template_directory_uri() . '/assets/js/axios.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-confirm', get_template_directory_uri() . '/assets/js/confirm.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-cookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', ['jquery'], $avoskin_version, true );
			//wp_enqueue_script( 'avoskin-scripts', get_template_directory_uri() . '/assets/js/scripts.js', ['jquery'], $avoskin_version, true );
			
			//Compressed
			wp_enqueue_script( 'avoskin-libs', get_template_directory_uri() . '/assets/js/libs.js', ['jquery'], $avoskin_version, true );
			wp_enqueue_script( 'avoskin-app', get_template_directory_uri() . '/assets/js/app.js', ['jquery'], time(), true );
			wp_enqueue_script( 'avoskin-extra', get_template_directory_uri() . '/assets/js/dev/extra.js', ['jquery'], time(), true );
			wp_enqueue_script( 'avoskin-blog', get_template_directory_uri() . '/assets/js/dev/blog.js', ['jquery'], time(), true );
			
			$data = [
				'api_url'   => site_url('/wp-json/avoskin/v1/'),
				'nonce' => wp_create_nonce( 'wp_rest' ),
				'site_url' => site_url(),
				'user' => (is_user_logged_in()) ? get_current_user_id() : 'nope',
				'account' => get_permalink( wc_get_page_id( 'myaccount' ) ),
				'wishlist' => [],
				'optin' => [
					'title' => [
						'notice' => __('Notice', 'avoskin'),
						'success' => __('Success', 'avoskin'),
					],
					'msg' => [
						'success' => __('Thank you for subscribing!', 'avoskin'),
						'empty' => __('Please input email address!', 'avoskin'),
						'invalid' => __('Please input valid email address!', 'avoskin')
					]
				],
				'visitor' => base64_encode(WC_Geolocation::get_ip_address()),
				'ref' => (isset($_GET['referral']) && $_GET['referral'] != '') ? $_GET['referral'] : '',
				'avosbrf' => 'nope',
				'nostock' => [
					'title' => __('Notice', 'avoskin'),
					'msg' => __('Stock product is not available', 'avoskin')
				],
				'advisor' => 'nope'
			];
			
			if(is_user_logged_in()){
				$user = get_user_by('id', get_current_user_id());
				$month = [
					'' => '',
					'1' => __('January', 'avoskin'),
					'2' => __('February', 'avoskin'),
					'3' => __('March', 'avoskin'),
					'4' => __('April', 'avoskin'),
					'5' => __('May', 'avoskin'),
					'6' => __('June', 'avoskin'),
					'7' => __('July', 'avoskin'),
					'8' => __('August', 'avoskin'),
					'9' => __('September', 'avoskin'),
					'10' => __('October', 'avoskin'),
					'11' => __('November', 'avoskin'),
					'12' => __('December', 'avoskin')
				];
				$birth = date('d-m-Y', strtotime(get_user_meta(get_current_user_id(), 'dob_d', true) . ' '.$month[get_user_meta(get_current_user_id(), 'dob_m', true)] . ' ' . get_user_meta(get_current_user_id(), 'dob_y', true)));
				$advisor = [
					'name' => get_user_meta(get_current_user_id(), 'first_name', true) . ' ' . get_user_meta(get_current_user_id(), 'last_name', true),
					'email' => $user->data->user_email,
					'birth_date' => $birth,
					'gender' => (get_user_meta(get_current_user_id(), 'gender', true) == 'male') ? 'Man' : 'Woman'
				];
				$data['advisor'] = base64_encode(wp_json_encode($advisor));
			}
			
			if(isset($_GET['referral']) && $_GET['referral'] != '' && $_GET['referral'] == get_option('shopback_ref_field') && isset($_GET['uniqueID']) && $_GET['uniqueID'] != ''){
				$avosbrf = [
					'referral' => $_GET['referral'],
					'transaction_id' => $_GET['uniqueID']
				];
				$data['avosbrf'] = base64_encode(wp_json_encode($avosbrf));
			}
			if(!is_user_logged_in()){
				$data['login_alert'] = [
					'title' => __('Info', 'avoskin'),
					'msg' => __('You need to login first before able to submit a review', 'avoskin')
				];
				$data['wishlist_alert'] = [
					'title' => __('Info', 'avoskin'),
					'msg' => __('Please login/register to add product into your wishlist', 'avoskin')	
				];
			}else{
				$wishlist = get_user_meta(get_current_user_id(), 'wishlist', true);
				$data['wishlist']  = ($wishlist != '') ? $wishlist : [];
			}
			if(is_page_template('page-checkout.php')){
				$data['coupon_alert'] = [
					'title' => __('Info', 'avoskin'),
					'msg' => __('Please enter coupon code first.', 'avoskin')
				];
				
				$data['checkout_alert'] = [
					'title' => __('Notice', 'avoskin'),
					'msg' => __('Please accept our terms and conditions first.', 'avoskin')
				];
				
				// ::PENTEST::
				$pu_key = get_option('avo_enc_public', '');
				if($pu_key != ''){
					$data['public'] = base64_encode($pu_key);
				}
			}
			if(is_page_template('page-my-account.php')){
				$data['profile_alert'] = [
					'title' => __('Notice', 'avoskin'),
					'msg' => [
						'invalid_file' => __('Invalid file type. Please upload image only (*.jpg or *.png)', 'avoskin'),
						'invalid_size' => __('Image file too large. Maximum image size is 2MB.', 'avoskin'),
						'pass_too_short' => __('Password minimum 6 character.','avoskin'),
						'pass_not_match' => __('Password not match!', 'avoskin')
					]
				];
			}
			
			wp_localize_script( 'avoskin-app', 'avoskin_data', $data);	
			
		}
		
		public function admin_scripts(){
			global $avoskin_version;
			
			wp_enqueue_style( 'avoskin-admin', get_template_directory_uri() . '/assets/css/admins3.css', '', $avoskin_version );
		}
		
		public function search_filter($query) {
			if ($query->is_search && !is_admin()) {
				$query->set('post_type',[
					'product'
				]);
			}
			return $query;
		}
		
		public function search_redirect(){
			global $wp_rewrite;
			if ( !isset( $wp_rewrite ) || !is_object( $wp_rewrite ) || !$wp_rewrite->using_permalinks() ) return;
			$search_base = $wp_rewrite->search_base;
			if ( is_search() && !is_admin() && strpos( $_SERVER['REQUEST_URI'], "/{$search_base}/" ) === false ) {
				wp_redirect( home_url( "/{$search_base}/" . urlencode( get_query_var( 's' ) ) .'/' ) );
				exit();
			}
		}
		
		public function hide_editor(){
			global $pagenow;
			
			if(!('post.php' == $pagenow)){
				return;
			}
			// Get the Post ID.
			$post_id = filter_input(INPUT_GET, 'post') ? filter_input(INPUT_GET, 'post') : filter_input(INPUT_POST, 'post_ID');
			if(!isset($post_id)) {
				return;
			}
			
			// Hide the editor on a page with a specific page template
			$template_filename = get_post_meta($post_id, '_wp_page_template', true);
			$pages = [
				'page-homepage.php',
				'page-homepage-2.php',
				'page-faq.php',
				'page-about.php',
				'page-avo-stories.php',
				'page-skin-advisor.php',
				'page-partner.php',
				'page-refining.php',
				'page-retinol.php',
				'page-lady-boss.php',
				'page-miraculous.php',
				'page-skinbae.php',
				'page-skinbae-detail.php',
				'page-landing-blog.php',
				'page-avoskin-blp.php',
				'page-landing-friday.php', // ::CRFRIDAY::
				'page-waste-for-change.php', // ::CRWFC::
				'page-wfc-redeem.php', // ::CRWFC::
			];
		
			if(in_array($template_filename, $pages)) {
				remove_post_type_support('page', 'editor');
			}
		}
		
		public function register_customizer($wp_customize){
			
			$wp_customize->add_setting('avoskin_logo',[
				'default' => '',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' =>  [$this, 'avoskin_customizer_sanitize_text'],
			]);
			
			$wp_customize->add_control(
				new WP_Customize_Image_Control($wp_customize,'avoskin_logo',[
					'label'      => __( 'Logo', 'avoskin' ),
					'section'    => 'title_tagline',
					'settings'   => 'avoskin_logo',
				])
			);
			
			$wp_customize->add_setting('avoskin_blog_logo',[
				'default' => '',
				'capability'     => 'edit_theme_options',
				'sanitize_callback' =>  [$this, 'avoskin_customizer_sanitize_text'],
			]);
			
			$wp_customize->add_control(
				new WP_Customize_Image_Control($wp_customize,'avoskin_blog_logo',[
					'label'      => __( 'Blog Logo', 'avoskin' ),
					'section'    => 'title_tagline',
					'settings'   => 'avoskin_blog_logo',
				])
			);
			
			$wp_customize->add_setting(
				'avoskin_copy', [
					'default' => '',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' =>  [$this,'avoskin_customizer_sanitize_text'],
				]
			);
			
			$wp_customize->add_control(
				'avoskin_copy', [
					'section' => 'title_tagline',
					'label'      => __('Copyright Text','avoskin'),
					'type'       => 'textarea',
				]
			);
			
			$wp_customize->add_setting(
				'avoskin_order_email', [
					'default' => '',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' =>  [$this,'avoskin_customizer_sanitize_text'],
				]
			);
			
			$wp_customize->add_control(
				'avoskin_order_email', [
					'section' => 'title_tagline',
					'label'      => __('Schedule Order Email Receiver','avoskin'),
					'type'       => 'textarea',
				]
			);
			
			$pages_linking = [
				'error_404' => [
					'label' => __( 'Error 404 page', 'avoskin' ),
					'desc' =>  __( 'choose the page for error 404', 'avoskin' ),
				],
				'id_error_404' => [
					'label' => __( 'Error 404 ID page', 'avoskin' ),
					'desc' =>  __( 'choose the page for error 404 ID', 'avoskin' ),
				],
				'avostore' => [
					'label' => __( 'Avostore page', 'avoskin' ),
					'desc' =>  __( 'choose the page for avostore', 'avoskin' ),
				],
				'id_avostore' => [
					'label' => __( 'Avostore page ID', 'avoskin' ),
					'desc' =>  __( 'choose the page for avostore ID', 'avoskin' ),
				],
				'tracking_form' => [
					'label' => __( 'Tracking Form page', 'avoskin' ),
					'desc' =>  __( 'choose the page for tracking form', 'avoskin' ),
				],
				'id_tracking_form' => [
					'label' => __( 'Tracking Form page ID', 'avoskin' ),
					'desc' =>  __( 'choose the page for tracking form ID', 'avoskin' ),
				],
				'tracking_result' => [
					'label' => __( 'Tracking Result page', 'avoskin' ),
					'desc' =>  __( 'choose the page for tracking result', 'avoskin' ),
				],
				'id_tracking_result' => [
					'label' => __( 'Tracking Result page ID', 'avoskin' ),
					'desc' =>  __( 'choose the page for tracking result ID', 'avoskin' ),
				],
				'landing_blog' => [
					'label' => __( 'Landing Blog page', 'avoskin' ),
					'desc' =>  __( 'choose the page for landing blog', 'avoskin' ),
				],
				'id_landing_blog' => [
					'label' => __( 'Landing Blog page ID', 'avoskin' ),
					'desc' =>  __( 'choose the page for landing blog ID', 'avoskin' ),
				],
				'landing_blp' => [
					'label' => __( 'Avoskin BLP page', 'avoskin' ),
					'desc' =>  __( 'choose the page for Avoskin BLP', 'avoskin' ),
				],
				'id_landing_blp' => [
					'label' => __( 'Avoskin BLP page ID', 'avoskin' ),
					'desc' =>  __( 'choose the page for Avoskin BLP ID', 'avoskin' ),
				],
				// ::CRWFC::
				'wfc' => [
					'label' => __( 'Waste for Changes page', 'avoskin' ),
					'desc' =>  __( 'choose the page for Waste for Changes Page', 'avoskin' ),
				],
				'wfc_redeem' => [
					'label' => __( 'Waste for Changes Redeem page', 'avoskin' ),
					'desc' =>  __( 'choose the page for Waste for Changes Redeem Page', 'avoskin' ),
				],
			];
			foreach($pages_linking as $p => $v){
				$wp_customize->add_setting('avoskin_'.$p.'_page', [
					'capability' => 'edit_theme_options',
					'sanitize_callback' => [$this,'avoskin_sanitize_select'],
					'default' => '',
				]);
			      
				$wp_customize->add_control('avoskin_'.$p.'_page', [
					'type' => 'select',
					'section' => 'static_front_page', // Add a default or your own section
					'label' => $v['label'],
					'description' => $v['desc'],
					'choices' => avoskin_get_pages()
				]);	
			}
			
		}
		
		public function avoskin_customizer_sanitize_text($input){
			return wp_kses_post( force_balance_tags( $input ) );
		}
		
		public function avoskin_sanitize_select($input, $setting ){
			// Ensure input is a slug.
			$input = sanitize_key( $input );
		      
			// Get list of choices from the control associated with the setting.
			$choices = $setting->manager->get_control( $setting->id )->choices;
		      
			// If the input is a valid key, return it; otherwise, return the default.
			return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
		}
		
		public function mime_types($mimes) {
			$mimes['svg'] = 'image/svg+xml';
			return $mimes;
		}
		
		public function custom_admin_head() {
			$css = '';
			$css = 'img.components-responsive-wrapper__content[src$=".svg"] { width: 100% !important; height: auto !important; position: relative !important; }';
			echo '<style type="text/css">'.$css.'</style>';
		}
		
		public function  cat_count_span($links) {
			$links = str_replace('</a> (', '</a> <span>(', $links);
			$links = str_replace(')', ')</span>', $links);
			return $links;
		}
		
		public function post_type(){
			$labels = [
				'name'               => _x( 'Referral', 'post type general name', 'avoskin' ),
				'singular_name'      => _x( 'Referral', 'post type singular name', 'avoskin' ),
				'menu_name'          => _x( 'Referral', 'admin menu', 'avoskin' ),
				'name_admin_bar'     => _x( 'Referral', 'add new on admin bar', 'avoskin' ),
				'add_new'            => _x( 'Add New', 'avoskin', 'avoskin' ),
				'add_new_item'       => __( 'Add New Referral', 'avoskin' ),
				'new_item'           => __( 'New Referral', 'avoskin' ),
				'edit_item'          => __( 'Edit Referral', 'avoskin' ),
				'view_item'          => __( 'View Referral', 'avoskin' ),
				'all_items'          => __( 'All Referral', 'avoskin' ),
				'search_items'       => __( 'Search Referral', 'avoskin' ),
				'parent_item_colon'  => __( 'Parent Referral:', 'avoskin' ),
				'not_found'          => __( 'No Referral found.', 'avoskin' ),
				'not_found_in_trash' => __( 'No Referral found in Trash.', 'avoskin' )
			];
			
			$args = [
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_icon' => 'dashicons-admin-links',
				'query_var'          => true,
				'rewrite'            => [ 'slug' => 'referral'],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_position'      => 70,
				'supports'           => ['title']
			];
			
			register_post_type( 'avoskin-referral', $args );
			flush_rewrite_rules();
			
			$labels = [
				'name'               => _x( 'Avostore', 'post type general name', 'avoskin' ),
				'singular_name'      => _x( 'Avostore', 'post type singular name', 'avoskin' ),
				'menu_name'          => _x( 'Avostore', 'admin menu', 'avoskin' ),
				'name_admin_bar'     => _x( 'Avostore', 'add new on admin bar', 'avoskin' ),
				'add_new'            => _x( 'Add New', 'avoskin', 'avoskin' ),
				'add_new_item'       => __( 'Add New Avostore', 'avoskin' ),
				'new_item'           => __( 'New Avostore', 'avoskin' ),
				'edit_item'          => __( 'Edit Avostore', 'avoskin' ),
				'view_item'          => __( 'View Avostore', 'avoskin' ),
				'all_items'          => __( 'All Avostore', 'avoskin' ),
				'search_items'       => __( 'Search Avostore', 'avoskin' ),
				'parent_item_colon'  => __( 'Parent Avostore:', 'avoskin' ),
				'not_found'          => __( 'No Avostore found.', 'avoskin' ),
				'not_found_in_trash' => __( 'No Avostore found in Trash.', 'avoskin' )
			];
			
			$args = [
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_icon' => 'dashicons-groups',
				'query_var'          => true,
				'rewrite'            => [ 'slug' => 'member'],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_position'      => 70,
				'supports'           => ['title']
			];
			
			register_post_type( 'avoskin-member', $args );
			flush_rewrite_rules();
			
			// ::STORELOC::
			$labels = [
				'name'               => _x( 'Storeloc', 'post type general name', 'avoskin' ),
				'singular_name'      => _x( 'Storeloc', 'post type singular name', 'avoskin' ),
				'menu_name'          => _x( 'Storeloc', 'admin menu', 'avoskin' ),
				'name_admin_bar'     => _x( 'Storeloc', 'add new on admin bar', 'avoskin' ),
				'add_new'            => _x( 'Add New', 'avoskin', 'avoskin' ),
				'add_new_item'       => __( 'Add New Storeloc', 'avoskin' ),
				'new_item'           => __( 'New Storeloc', 'avoskin' ),
				'edit_item'          => __( 'Edit Storeloc', 'avoskin' ),
				'view_item'          => __( 'View Storeloc', 'avoskin' ),
				'all_items'          => __( 'All Storeloc', 'avoskin' ),
				'search_items'       => __( 'Search Storeloc', 'avoskin' ),
				'parent_item_colon'  => __( 'Parent Storeloc:', 'avoskin' ),
				'not_found'          => __( 'No Storeloc found.', 'avoskin' ),
				'not_found_in_trash' => __( 'No Storeloc found in Trash.', 'avoskin' )
			];
			
			$args = [
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'menu_icon' => 'dashicons-location-alt',
				'query_var'          => true,
				'rewrite'            => [ 'slug' => 'storeloc'],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_position'      => 70,
				'supports'           => ['title']
			];
			
			register_post_type( 'avoskin-storeloc', $args );
			flush_rewrite_rules();
			
			$labels = [
				'name'               => _x( 'Advisor Statistic', 'post type general name', 'avoskin' ),
				'singular_name'      => _x( 'Advisor Statistic', 'post type singular name', 'avoskin' ),
				'menu_name'          => _x( 'Advisor Statistic', 'admin menu', 'avoskin' ),
				'name_admin_bar'     => _x( 'Advisor Statistic', 'add new on admin bar', 'avoskin' ),
				'add_new'            => _x( 'Add New', 'avoskin', 'avoskin' ),
				'add_new_item'       => __( 'Add New Advisor Statistic', 'avoskin' ),
				'new_item'           => __( 'New Advisor Statistic', 'avoskin' ),
				'edit_item'          => __( 'Edit Advisor Statistic', 'avoskin' ),
				'view_item'          => __( 'View Advisor Statistic', 'avoskin' ),
				'all_items'          => __( 'All Advisor Statistic', 'avoskin' ),
				'search_items'       => __( 'Search Advisor Statistic', 'avoskin' ),
				'parent_item_colon'  => __( 'Parent Advisor Statistic:', 'avoskin' ),
				'not_found'          => __( 'No Advisor Statistic found.', 'avoskin' ),
				'not_found_in_trash' => __( 'No Advisor Statistic found in Trash.', 'avoskin' )
			];
			
			$args = [
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => false,
				'show_in_menu'       => false,
				'menu_icon' => 'dashicons-groups',
				'query_var'          => true,
				'rewrite'            => [ 'slug' => 'advisor-statistic'],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'hierarchical'       => false,
				'menu_position'      => 70,
				'supports'           => ['title']
			];
			
			register_post_type( 'advisor-statistic', $args );
			flush_rewrite_rules();
			
		}
		
		public function add_referral_query_var($vars){
			$vars[] .= 'ref';
			return $vars;
		}
		
		public function create_referral_table(){
			global $wpdb;

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		    
			$table_name = $wpdb->prefix . "avoskin_referral";  
		    
			$sql = "CREATE TABLE IF NOT EXISTS $table_name (
                                        id MEDIUMINT(20) NOT NULL AUTO_INCREMENT,
					type VARCHAR(20),
					referral VARCHAR(255),
					visitor VARCHAR(255),
                                        time DATETIME,
                                        
                                        UNIQUE KEY id (id)
                                );";
                        dbDelta($sql);
		}
		
		public function add_admin_menu(){
			add_submenu_page(
				'edit.php?post_type=avoskin-referral',
				__('Statistic', 'avoskin'),
				__('Statistic', 'avoskin'),
				'manage_options',
				'avoskin_referral',
				[$this, 'avoskin_referral_statistic']);
			
			add_submenu_page(
				'edit-comments.php',
				__('Reviews', 'avoskin'),
				__('Reviews', 'avoskin'),
				'manage_options',
				'avoskin_review',
				[$this, 'avoskin_product_review']);
			
			add_submenu_page(
				'options-general.php',
				__('Review', 'avoskin'),
				__('Review', 'avoskin'),
				'manage_options',
				'avoskin_review_settings',
				[$this, 'avoskin_review_settings']);
			
			//add_submenu_page(
			//	'woocommerce',
			//	__('Label', 'avoskin'),
			//	__('Label', 'avoskin'),
			//	'manage_options',
			//	'avoskin_label',
			//	[$this, 'avoskin_order_label'], 2);
			
			add_submenu_page(
				'woocommerce',
				__('Import Resi', 'avoskin'),
				__('Import Resi', 'avoskin'),
				'manage_options',
				'avoskin_resi',
				[$this, 'avoskin_resi_import'], 2);
			
			add_submenu_page(
				'woocommerce',
				__('Surat Jalan', 'avoskin'),
				__('Surat Jalan', 'avoskin'),
				'manage_options',
				'avoskin_surat_jalan',
				[$this, 'avoskin_order_surat_jalan'], 2);
			
			add_submenu_page(
				'woocommerce',
				__('Label', 'avoskin'),
				__('Label', 'avoskin'),
				'manage_options',
				'avoskin_awb',
				[$this, 'avoskin_order_awb'], 2);
		}
		
		public function avoskin_referral_statistic(){
			set_query_var( 'controller', $this->controller );
			get_template_part('inc/admin/referral-statistic');
		}
		
		public function avoskin_product_review(){
			set_query_var( 'controller', $this->controller );
			get_template_part('inc/admin/product-review');
		}
		
		public function avoskin_review_settings(){
			get_template_part('inc/admin/review-settings');
		}
		
		public function avoskin_order_label(){
			set_query_var( 'controller', $this->controller );
			get_template_part('inc/admin/order-label');
		}
		
		public function avoskin_order_awb(){
			set_query_var( 'controller', $this->controller );
			get_template_part('inc/admin/order-awb');
		}
		
		public function avoskin_order_surat_jalan(){
			set_query_var( 'controller', $this->controller );
			get_template_part('inc/admin/order-surat-jalan');
		}
		
		public function avoskin_resi_import(){
			get_template_part('inc/admin/resi-import');
		}
		
		public function add_verified_field($user){
		?>
			<h3 style="margin: 0;padding: 0;font-size:0;height: 0;opacity: 0;">&nbsp;</h3>
			<table class="form-table">
				<tbody>
					<tr>
						<th><?php _e('Verified user?','avoskin');?></th>
						<td>
							<label style="display: inline-block;vertical-align: middle;">
								<input type="radio" name="avo_is_verified" value="yes" <?php checked('yes', get_user_meta($user->ID, 'avo_is_verified', true));?> /><?php _e('Yes','avoskin');?>
							</label>
							<label style="display: inline-block;vertical-align: middle;margin-left: 10px;" >
								<input type="radio" name="avo_is_verified" value="no" <?php checked('no', get_user_meta($user->ID, 'avo_is_verified', true));?> /><?php _e('No','avoskin');?>
							</label>
						</td>
					</tr>
				</tbody>
			</table>
		<?php
		}
		
		public function save_verified_field($user_id){
			if(isset($_POST['avo_is_verified'])){
				 update_user_meta($user_id, 'avo_is_verified', $_POST['avo_is_verified']);
			}
		}
		public function member_column_head($defaults){
			$defaults['email'] = 'Email';
			$defaults['phone'] = 'Phone';
			$defaults['city'] = 'City';
			return $defaults;
		}
		
		public function member_column_content($column_name, $post_ID){
			if ($column_name == 'email') {
				echo get_post_meta($post_ID, 'email', true);
			}
			if ($column_name == 'phone') {
				echo get_post_meta($post_ID, 'phone', true);
			}
			if ($column_name == 'city') {
				echo get_post_meta($post_ID, 'city', true);
			}
		}
		
		public function shop_order_column_head($defaults){
			unset($defaults['order_total']);
			unset($defaults['wc_actions']);
			$defaults['printed'] = 'Printed';
			$defaults['package_status'] = 'Package Status';
			$defaults['order_total'] = 'Total';
			$defaults['wc_actions'] = 'Actions';
			
			return $defaults;
		}
		
		public function shop_order_column_content($column_name, $post_ID){
			if ($column_name == 'printed') {
				echo (get_post_meta($post_ID, 'has_print', true) != 'yep') ? '<i style="color:#ca4a1f;" class="dashicons dashicons-dismiss"></i>' : '<i style="color:#0073aa;" class="dashicons dashicons-yes-alt"></i>';
			}
			if ($column_name == 'package_status') {
				echo  (get_post_meta($post_ID, 'package_status', true) != '') ? get_post_meta($post_ID, 'package_status', true) : 'No Status';
			}
		}
		
		public function avoskin_search_form($html){
			$html = str_replace( 'placeholder="Search &hellip;"', 'placeholder="'.__('Type Keyword','avoskin').'" required="required"', $html );
			return $html;
		}
		
		public function customer_order_name(){
			$user = get_current_user_id();
			$first = get_user_meta($user, 'billing_first_name', true);
			$last = get_user_meta($user, 'billing_last_name', true);
			return '<strong>' . $first . ' ' . $last . '</strong>';
		}
	}
}

return new Avoskin();
