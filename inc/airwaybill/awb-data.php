<?php

class AWB_Data {       
        function __construct() {
                
        }
        
        static function jne_get_branch(){
                $branch = [];
                $branch_obj = self::_get_data("jne/branch.json");
                foreach($branch_obj as $b => $v){
                        $branch[$v['branch_code']] = $v['branch_name'];
                }
                
                return $branch;
        }
        
        static function jne_get_origin(){
                $origin = [];
                $origin_obj = self::_get_data("jne/origin.json");
                foreach($origin_obj as $o => $v){
                        $origin[$v['origin_code']] = $v['origin_name'];
                }
                
                return $origin;
        }
        
        static function jne_get_destination($province, $postcode){
                $province_id = TRO_Data::get_province_id($province);
                $destination = self::_get_data("jne/destination/$province_id.json");
                if(is_array($destination) && !empty($destination)){
                        foreach($destination as $d => $v){
                                if($v['zip'] == $postcode){
                                        return $destination[$d];
                                }
                        }
                }
                return 'invalid-postcode';
        }
        
        static function jnt_get_data($province, $postcode){
                $result = '';
                $province_id = TRO_Data::get_province_id($province);
                $raw = self::_get_data("jnt/$province_id.json");
                if(is_array($raw) && !empty($raw)){
                        foreach($raw as $r => $v){
                                if(
                                        is_array($v['kodepos']) && !empty($v['kodepos']) && in_array($postcode, $v['kodepos'])
                                        || !is_array($v['kodepos']) && $v['kodepos'] == $postcode)
                                {
                                        $result = $raw[$r];
                                        unset($result['kodepos']);
                                }
                        }
                }
                
                return $result;
        }
        static function sicepat_get_origin(){
                $city = get_option('tro_city_origin');
                $store_raw_country = get_option( 'woocommerce_default_country' );
                $split_country = explode( ":", $store_raw_country );
                $store_country = $split_country[0];
                $store_state   = $split_country[1];
                $origin_raw = self::_get_data("sicepat/$store_state/$city.json");
                $origin = [];
                if(is_array($origin_raw) && !empty($origin_raw)){
                        foreach($origin_raw as $or){
                                $key = [
                                        'province' => $or['province'],
                                        'city' => $or['city'],
                                        'subdistrict' => $or['subdistrict'],
                                        'ct_code' => $or['ct_code']
                                ];
                                $label = $or['subdistrict'] .' - '. $or['city'] .' - '. $or['province'] .' - '. $or['ct_code'];
                                $origin[base64_encode(wp_json_encode($key))]= $label;
                        }
                }
                return $origin;
        }
        
        private static function sicepat_get_destination($order){
                $province =  $order->get_shipping_state();
                $zip = $order->get_shipping_postcode();
                $city = get_post_meta($order->get_id(), 'city_id', true);
                $destination_raw = self::_get_data("sicepat/$province/$city.json");
                $selected = [
                        'destination_code' => '',
                        'province' => '',
                        'city' => '',
                        'subdistrict' => '',
                        'ct_code' => ''
                ];
                
                if(is_array($destination_raw) && !empty($destination_raw)){
                        foreach($destination_raw as $key => $d){
                                if(is_array($d['postal']) && !empty($d['postal'])){
                                        if(in_array($zip, $d['postal'])){
                                                $selected = $d;
                                                unset($selected['postal']);
                                        }
                                }
                        }
                }
                
                return  $selected;
        }


        /*
        Get JSON file inside /data directory
    
        @param string $filename
        @return array
      */
        private static function _get_data( $filename ) {
                $fileurl =  get_parent_theme_file_path("inc/airwaybill/data/$filename");
                $args = [
                        'ssl' => [
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                        ]
                ];
            
                $fileraw = file_get_contents( $fileurl, false, stream_context_create($args) );
                return json_decode( $fileraw, true );
        }
        
        
        static function generate_airwaybill($order_id){
                $order = wc_get_order($order_id);
                $courier = $order->get_meta('shipping_courier');
                $status = 'fail';
                switch($courier){
                        case 'jne' :
                                @$status = self::jne_generate_airwaybill($order);
                                break;
                        case 'rpx' :
                                @$status = self::rpx_generate_airwaybill($order);
                                break;
                        case 'jnt' :
                                @$status = self::jnt_generate_airwaybill($order);
                                break;
                        case 'sicepat' :
                                @$status = self::sicepat_generate_airwaybill($order);
                                break;
                }
                
                return $status;
        }
        
        
        private static function jne_generate_airwaybill($order){
                $store_raw_country = get_option( 'woocommerce_default_country' );
                $split_country = explode( ":", $store_raw_country );
                $store_country = $split_country[0];
                $store_state   = $split_country[1];
                $country = $order->get_billing_country();
                $state = $order->get_shipping_state();
                $shipper_region = strtoupper(WC()->countries->get_states( $store_country )[$store_state]);
                
                $destination = AWB_Data::jne_get_destination($state, $order->get_shipping_postcode());
                $times =  (get_option('woocommerce_weight_unit') == 'kg') ? 1 : 1000;
                //$url = 'http://apiv2.jne.co.id:10102/tracing/api/generatecnote';
                //$url = 'http://apiv2.jne.co.id:10101/tracing/api/generatecnote';
                $url = get_option('jne_awb_endpoint_url');
                $args = [
                        'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded'
                        ],
                        'body' => [
                                'username' => get_option('jne_awb_username'),
                                'api_key' => get_option('jne_awb_apikey'),
                                'OLSHOP_BRANCH' => 'JOG000',//get_option('jne_awb_branch'),
                                'OLSHOP_CUST' => get_option('jne_awb_cust'),
                                'OLSHOP_ORDERID' => time().'-'.$order->get_id(),
                                'OLSHOP_SHIPPER_NAME' => get_option('jne_awb_shipper_name'),
                                'OLSHOP_SHIPPER_ADDR1' => get_option( 'woocommerce_store_address' ),
                                'OLSHOP_SHIPPER_ADDR2' =>  strtoupper(get_option( 'woocommerce_store_city' )),
                                'OLSHOP_SHIPPER_ADDR3' => '',
                                'OLSHOP_SHIPPER_CITY' =>  strtoupper(get_option( 'woocommerce_store_city' )),
                                'OLSHOP_SHIPPER_REGION' =>  ($shipper_region == 'DAERAH ISTIMEWA YOGYAKARTA') ? 'DI YOGYAKARTA' : $shipper_region,
                                'OLSHOP_SHIPPER_ZIP' =>  get_option( 'woocommerce_store_postcode' ),
                                'OLSHOP_SHIPPER_PHONE' => get_option( 'jne_awb_shipper_phone' ),
                                'OLSHOP_RECEIVER_NAME' => strtoupper($order->get_shipping_first_name()) . ' ' . strtoupper($order->get_shipping_last_name()),
                                'OLSHOP_RECEIVER_ADDR1' => $order->get_shipping_address_1() ,
                                'OLSHOP_RECEIVER_ADDR2' => strtoupper($order->get_shipping_city()),
                                'OLSHOP_RECEIVER_ADDR3' => '',
                                'OLSHOP_RECEIVER_CITY' => $destination['city'],
                                'OLSHOP_RECEIVER_REGION' => $destination['region'],
                                'OLSHOP_RECEIVER_ZIP' => $order->get_shipping_postcode(),
                                'OLSHOP_RECEIVER_PHONE' => $order->get_billing_phone(),
                                'OLSHOP_QTY' => $order->get_item_count(),
                                'OLSHOP_WEIGHT' => round((float)$order->get_meta('order_weight') / $times, 1),
                                'OLSHOP_GOODSDESC' => 'AVOSKIN PRODUCT',
                                'OLSHOP_GOODSVALUE' => $order->get_total(),
                                'OLSHOP_GOODSTYPE' => '1',
                                'OLSHOP_INST' => 'NO INSTRUCTION',
                                'OLSHOP_INS_FLAG' => 'N',
                                'OLSHOP_ORIG' => get_option( 'jne_awb_origin' ),
                                'OLSHOP_DEST' => $destination['code'],
                                'OLSHOP_SERVICE' => $order->get_meta('shipping_service'),
                                'OLSHOP_COD_FLAG' => 'N',
                                'OLSHOP_COD_AMOUNT' => '0'
                        ]
                ];
                
                $body = get_post_meta($order->get_id(), 'jne_temp_request', true);
                if($body != ''){
                        $body = json_decode($body, true);
                        if(is_array($body) && !empty($body)){
                                $args['body'] = $body;
                        }
                }
                
                update_post_meta($order->get_id(), 'jne_temp_request', wp_json_encode($args['body']));
                
                $req = wp_remote_post($url, $args);
                update_post_meta($order->get_id(),'jne_response_awb_all', wp_json_encode($req));
                
                $req = json_decode(wp_remote_retrieve_body($req), true);
                update_post_meta($order->get_id(),'jne_response_awb_body', wp_json_encode($req));
                
                $result = $req['detail'][0];
                $status = 'fail';
                if($result['status'] == 'sukses'){
                        update_post_meta($order->get_id(), 'resi_order', $result['cnote_no']);
                        update_post_meta($order->get_id(), 'awb_status', 'awb-success');
                        $status = 'ok';
                }else{
                        update_post_meta($order->get_id(), 'awb_status', 'awb-error');
                }
                
                return $status;
        }
        
        private static function rpx_generate_airwaybill($order){
                require_once get_parent_theme_file_path( '/inc/libs/nusoap.php' );
                
                $user = get_option('rpx_awb_username');
                $password = get_option('rpx_awb_password');
                $account = get_option('rpx_awb_account');
        
                $wsdl= get_option('rpx_awb_endpoint_url');
                $client = new nusoap_client($wsdl,true);
                $err = $client->getError();
                $status = 'fail';
                
                if (!$err) {
                        $store_raw_country = get_option( 'woocommerce_default_country' );
                        $split_country = explode( ":", $store_raw_country );
                        $store_country = $split_country[0];
                        $store_state   = $split_country[1];
                        $country = $order->get_billing_country();
                        $state = $order->get_shipping_state();
                        $times =  (get_option('woocommerce_weight_unit') == 'kg') ? 1 : 1000;
                        $data = [
                                'user'=>$user,
                                'password'=>$password,
                                'order_type'=>'OS',
                                'service_type_id'=> $order->get_meta('shipping_service'),
                                'shipper_account'=>$account,
                                'shipper_name'=> get_option( 'rpx_awb_shipper_name' ),
                                'shipper_company'=> get_option( 'rpx_awb_company_name' ),
                                'shipper_address1' => get_option( 'woocommerce_store_address' ),
                                'shipper_city' => strtoupper(get_option( 'woocommerce_store_city' )),
                                'shipper_state' => strtoupper(WC()->countries->get_states( $store_country )[$store_state]),
                                'shipper_zip' => get_option( 'woocommerce_store_postcode' ),
                                'shipper_phone' => get_option( 'rpx_awb_shipper_phone' ),
                                'shipper_email'=> get_option( 'admin_email' ),
                                'shipper_mobile_no' => get_option( 'rpx_awb_shipper_phone' ),
                                'consignee_email' => $order->get_billing_email(),
                                'consignee_name' => strtoupper($order->get_shipping_first_name()) . ' ' . strtoupper($order->get_shipping_last_name()),
                                'consignee_address1' => $order->get_shipping_address_1() ,
                                'consignee_city' => strtoupper($order->get_shipping_city()),
                                'consignee_state' => strtoupper(WC()->countries->get_states( $country )[$state]),
                                'consignee_zip'=>$order->get_shipping_postcode(),
                                'consignee_phone'=> $order->get_billing_phone(),
                                'consignee_mobile_no' => $order->get_billing_phone(),
                                'desc_of_goods' => 'Avoskin Product',
                                'tot_package' => $order->get_item_count(),
                                'tot_weight' => round((float)$order->get_meta('order_weight') / $times, 1),
                                'tot_declare_value' => $order->get_total(),
                                'format' => 'JSON',
                        ];
                        
                        $temp_data = get_post_meta($order->get_id(), 'rpx_temp_request', true);
                        if($temp_data != ''){
                                $temp_data = json_decode($temp_data, true);
                                if(is_array($temp_data) && !empty($temp_data)){
                                        $data = $temp_data;
                                }
                        }
                        
                        update_post_meta($order->get_id(), 'rpx_temp_request', wp_json_encode($data));
                        
                        $awb_request = $client->call('sendShipmentData', $data);
                        if (!$client->fault) {
                                $err = $client->getError();
                                if (!$err) {
                                        $tmp = json_decode($awb_request, true);
                                        update_post_meta($order->get_id(),'rpx_response_awb_all', wp_json_encode($tmp));
                                        $awb = (isset($tmp['RPX']['DATA']) && $tmp['RPX']['DATA'] != '') ? $tmp['RPX']['DATA'][0]['AWB_RETURN'] : '';
                                        if($awb != '' ){
                                                update_post_meta($order->get_id(),'rpx_response_awb_body', wp_json_encode($tmp['RPX']));
                                                update_post_meta($order->get_id(), 'resi_order', $awb);
                                                
                                                update_post_meta($order->get_id(), 'awb_status', 'awb-success');
                                                $status = 'ok';
                                        }
                                } 
                        }
                }
                
                if($status != 'ok'){
                        update_post_meta($order->get_id(), 'awb_status', 'awb-error');
                }
                
                return $status;
        }
        
        private static function jnt_generate_airwaybill($order){
                //$url = 'http://159.138.2.151:22223/jandt_ecommerce/api/onlineOrder.action';
                //$url = 'http://jk.jet.co.id:22232/JandT_ecommerce/api/onlineOrder.action';
                $url = get_option('jnt_awb_endpoint_url');
                $store_raw_country = get_option( 'woocommerce_default_country' );
                $split_country = explode( ":", $store_raw_country );
                $store_country = $split_country[0];
                $store_state   = $split_country[1];
                $country = $order->get_billing_country();
                $state = $order->get_shipping_state();
                $key = get_option('jnt_awb_key');
                $times =  (get_option('woocommerce_weight_unit') == 'kg') ? 1 : 1000;
                $s = AWB_Data::jnt_get_data($store_state, get_option( 'woocommerce_store_postcode' ));
                $r = AWB_Data::jnt_get_data($state, $order->get_shipping_postcode());

                $data = [
                        'username'=> get_option('jnt_awb_username'),
                        'api_key'=>get_option('jnt_awb_api'),
                        'orderid'=>'AVSKIN-'.time().'-'.$order->get_id(),
                        'shipper_name'=>get_option('jnt_awb_shipper_name'),
                        'shipper_contact'=>get_option('jnt_awb_shipper_name'),
                        'shipper_phone'=> get_option('jnt_awb_shipper_phone'),
                        'shipper_addr'=> get_option( 'woocommerce_store_address' ),
                        'origin_code'=>$s['kode_kota'],
                        'receiver_name'=> strtoupper($order->get_shipping_first_name()) . ' ' . strtoupper($order->get_shipping_last_name()),
                        'receiver_phone'=> $order->get_billing_phone(),
                        'receiver_addr'=>$order->get_shipping_address_1() ,
                        'receiver_zip'=>$order->get_shipping_postcode(),
                        'destination_code'=>$r['kode_kota'],
                        'receiver_area'=>$r['receiver_area'],
                        'qty'=>$order->get_item_count(),
                        'weight' => round((float)$order->get_meta('order_weight') / $times, 1),
                        'goodsdesc'=>'AVOSKIN PRODUCT',
                        'servicetype'=>'1',
                        'insurance'=>'',
                        'orderdate'=> esc_html( wc_format_datetime( $order->get_date_paid(), 'Y-m-d H:i:s' ) ),
                        'item_name'=>'',
                        'cod'=>'',
                        'sendstarttime'=> date('Y-m-d H:i:s', current_time('timestamp')),
                        'sendendtime'=>date('Y-m-d H:i:s', strtotime('+1 day', current_time('timestamp'))),
                        'expresstype'=>'1',
                        'goodsvalue'=> $order->get_total()
                ];
                
                $temp_data = get_post_meta($order->get_id(), 'jnt_temp_request', true);
                if($temp_data != ''){
                        $temp_data = json_decode($temp_data, true);
                        if(is_array($temp_data) && !empty($temp_data)){
                                $data = $temp_data;
                        }
                }
                
                update_post_meta($order->get_id(), 'jnt_temp_request', wp_json_encode($data));

                $data_json = json_encode(['detail'=>[$data]]);
                $data_request = [
                        'data_param'=>$data_json,
                        'data_sign'=> base64_encode(md5($data_json.$key))
                ];
                
                $args = [
                        'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded'
                        ],
                        'body' => $data_request
                ];
                $result = wp_remote_post($url, $args);
                update_post_meta($order->get_id(),'jnt_response_awb_all', wp_json_encode($result));
                
                $result = json_decode(wp_remote_retrieve_body($result), true);
                update_post_meta($order->get_id(),'jnt_response_awb_body', wp_json_encode($result));
                
                $status = 'fail';
                
                if($result['success'] == 1 && $result['detail'][0]['status'] == 'Sukses'){
                        $resi = $result['detail'][0]['awb_no'];
                        update_post_meta($order->get_id(), 'resi_order', $resi);
                        update_post_meta($order->get_id(), 'awb_status', 'awb-success');
                        $status = 'ok';
                }else{
                        update_post_meta($order->get_id(), 'awb_status', 'awb-error');
                }
                
                return $status;
        }
        
        private static function sicepat_generate_airwaybill($order){
                $pickup_time = (current_time('H') > 17) ? '+24 hours'  : '+1 minutes';
                $pickup_time = date('Y-m-d 17:00', strtotime($pickup_time));
                
                $products = '';
                foreach ( $order->get_items() as $item_id => $item ) {
                        $products .= $item->get_name() .', ';
                }
                
                $times =  (get_option('woocommerce_weight_unit') == 'kg') ? 1 : 1000;
                
                $origin = get_option('sicepat_awb_shipper_origin_pickup');
                $destination = self::sicepat_get_destination($order);
                extract(json_decode(base64_decode($origin), true));
                $args = [
                        'body' => [
                                'auth_key' => get_option('sicepat_awb_key'),
                                'reference_number' => $order->get_id(),
                                'pickup_request_date' => $pickup_time,
                                'pickup_merchant_code' => '',
                                'pickup_merchant_name' => get_option('sicepat_awb_shipper_name'),
                                'pickup_address' => get_option('sicepat_awb_shipper_address_pickup'),
                                'pickup_city' => get_option('sicepat_awb_shipper_city_pickup'),
                                'pickup_merchant_phone' => get_option('sicepat_awb_shipper_phone'),
                                'pickup_method' => 'PICKUP',
                                'pickup_merchant_email' => get_option('admin_email'),
                                'notes' => '',
                                'PackageList' => [
                                        [
                                                'receipt_number' => self::sicepat_get_awb(),
                                                'origin_code' => get_option('sicepat_awb_shipper_code_pickup'),
                                                'delivery_type' => $order->get_meta('shipping_service'),
                                                'parcel_category' => 'Cosmetic',
                                                'parcel_content' => $products,
                                                'parcel_qty' => $order->get_item_count(),
                                                'parcel_uom' => 'Pcs',
                                                'parcel_value' => (float)$order->get_total(),
                                                'cod_value' => 0,
                                                'insurance_value' => 0,
                                                'total_weight' => round((float)$order->get_meta('order_weight') / $times, 1),
                                                'parcel_length' => 0,
                                                'parcel_width' => 0,
                                                'parcel_height' => 0,
                                                'shipper_code' => null,
                                                'shipper_name' => get_option('sicepat_awb_shipper_name'),
                                                'shipper_address' => get_option('sicepat_awb_shipper_address_pickup'),
                                                'shipper_province' => get_option('sicepat_awb_shipper_province_pickup'),
                                                'shipper_city' => get_option('sicepat_awb_shipper_city_pickup'),
                                                'shipper_district' => get_option('sicepat_awb_shipper_district_pickup'),
                                                'shipper_zip' => get_option('sicepat_awb_shipper_zip_pickup'),
                                                'shipper_phone' => get_option('sicepat_awb_shipper_phone'),
                                                'shipper_longitude' => null,
                                                'shipper_latitude' => null,
                                                'recipient_title' => 'Mrs.',
                                                'recipient_name' => strtoupper($order->get_shipping_first_name()) . ' ' . strtoupper($order->get_shipping_last_name()),
                                                'recipient_address' => $order->get_shipping_address_1(),
                                                'recipient_province' => $destination['province'],
                                                'recipient_city' => $destination['city'],
                                                'recipient_district' => $destination['subdistrict'],
                                                'recipient_zip' => $order->get_shipping_postcode(),
                                                'recipient_phone' => $order->get_billing_phone(),
                                                'recipient_longitude' => null,
                                                'recipient_latitude' => null,
                                                'destination_code' => $destination['destination_code'],
                                                'notes' => ''
                                        ]
                                ]
                        ]
                ];
                
                $body = get_post_meta($order->get_id(), 'sicepat_temp_request', true);
                if($body != ''){
                        $body = json_decode($body, true);
                        if(is_array($body) && !empty($body)){
                                $body['PackageList'][0]['receipt_number'] = self::sicepat_get_awb();
                                $args['body'] = $body;
                        }
                }
                
                update_post_meta($order->get_id(), 'sicepat_temp_request', wp_json_encode($args['body']));
                
                $url = get_option('sicepat_awb_endpoint_url_'.get_option('sicepat_awb_mode'));
                $req = wp_remote_post($url, $args);
                update_post_meta($order->get_id(),'sicepat_response_awb_all', wp_json_encode($req));
                $status = 'fail';
                
                if($req['response']['code'] == 200){
                        $result = json_decode(wp_remote_retrieve_body($req), true);
                        if(!empty($result['datas'])){
                                update_post_meta($order->get_id(),'sicepat_response_awb_body', wp_json_encode($result));
                                update_post_meta($order->get_id(), 'resi_order', $result['datas'][0]['receipt_number']);
                                update_post_meta($order->get_id(), 'sicepat_request_number', $result['request_number']);
                                
                                //Update used resi number
                                $used = get_option('sicepat_used_awb', []);
                                array_push($used,$result['datas'][0]['receipt_number']);
                                update_option('sicepat_used_awb', $used);
                                
                                update_post_meta($order->get_id(), 'awb_status', 'awb-success');
                                $status = 'ok';
                        }
                }
                
                if($status != 'ok'){
                        update_post_meta($order->get_id(), 'awb_status', 'awb-error');
                }
                
                return $status;
        }
        
        private static function sicepat_get_awb(){
                $avail = get_option('sicepat_avail_awb', []);
                $awb = (is_array($avail) && !empty($avail)) ? $avail[0] : '';
                unset($avail[0]);
                $avail = array_values($avail);
                update_option('sicepat_avail_awb', $avail);
                return $awb;
        
                //$used = get_option('sicepat_used_awb', []);
                //
                //$range = get_option('sicepat_awb_range');
                //if($range != ''){
                //        $range = explode('-', $range);
                //        if(is_array($range) && !empty($range)){
                //                $min = $range[0];
                //                $max = $range[1];
                //                $awb = str_pad(intval($min) + 1, strlen($min), '0', STR_PAD_LEFT); 
                //                while(self::sicepat_is_awb_avail($awb, $used) == false){
                //                        $awb = str_pad(intval($awb) + 1, strlen($awb), '0', STR_PAD_LEFT); 
                //                }
                //                return $awb;
                //        }
                //}
        }
        
        private static function sicepat_is_awb_avail($awb, $used){
                return (in_array($awb,$used)) ? false : true;
        }
}
