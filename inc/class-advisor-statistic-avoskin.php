<?php
/**
 * Class Advisor_Statistic file.
 *
 * @package WooCommerce\Reports
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Advisor_Statistic
 *
 * @package     WooCommerce/Admin/Reports
 * @version     2.1.0
 */
class Advisor_Statistic extends WC_Admin_Report {

	/**
	 * Chart colors.
	 *
	 * @var array
	 */
	public $chart_colours = array();

	/**
	 * Customers.
	 *
	 * @var array
	 */
	
	public $visit = array();
	
	public $conversion = array();

	/**
	 * Get the legend for the main chart sidebar.
	 *
	 * @return array
	 */
	public function get_chart_legend() {
		$legend = array();

		$legend[] = array(
			
			'title'            => sprintf( __( '%s visit in this period', 'woocommerce' ), '<strong>' . count( $this->visit ) . '</strong>' ),
			'color'            => $this->chart_colours['visit'],
			'highlight_series' => 2,
		);
		$legend[] = array(
			
			'title'            => sprintf( __( '%s conversion in this period', 'woocommerce' ), '<strong>' . count( $this->conversion ) . '</strong>' ),
			'color'            => $this->chart_colours['conversion'],
			'highlight_series' => 2,
		);

		return $legend;
	}

	

	/**
	 * Output the report.
	 */
	public function output_report() {

		$ranges = array(
			'year'       => __( 'Year', 'woocommerce' ),
			'last_month' => __( 'Last month', 'woocommerce' ),
			'month'      => __( 'This month', 'woocommerce' ),
			'7day'       => __( 'Last 7 days', 'woocommerce' ),
		);

		$this->chart_colours = array(
			'visit'    => '#3498db',
			'conversion'    => '#1abc9c',
		);

		$current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( wp_unslash( $_GET['range'] ) ) : '7day';

		if ( ! in_array( $current_range, array( 'custom', 'year', 'last_month', 'month', '7day' ), true ) ) {
			$current_range = '7day';
		}

		$this->check_current_range_nonce( $current_range );
		$this->calculate_current_range( $current_range );
		
		$query = [
			'post_type' => 'advisor-statistic',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		];
		$this->visit = get_posts($query);
		
		foreach ( $this->visit as $key => $visit ) {
			if ( strtotime( $visit->post_date ) < $this->start_date || strtotime( $visit->post_date ) > $this->end_date ) {
				unset( $this->visit[ $key ] );
			}
		}
		
		$query['meta_query'] = [
			[
				'key'     => 'conversion',
				'value'   => 'yes',
				'compare' => '='
			]
		];
		
		$this->conversion = get_posts($query);
		
		foreach ( $this->conversion as $key => $conversion ) {
			if ( strtotime( $conversion->post_date ) < $this->start_date || strtotime( $conversion->post_date ) > $this->end_date ) {
				unset( $this->conversion[ $key ] );
			}
		}
		include WC()->plugin_path() . '/includes/admin/views/html-report-by-date.php';
		$this->generate_order_table();
	}
	
	private function generate_order_table(){
		$conversion = $this->conversion;
		?>
			<style type="text/css">
				.table-basic table{
					border-collapse: collapse;
					border-spacing: 0;
					width: 100%;
					border-radius: 6px;
					box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
					background-color: #ffffff;
					overflow: hidden;
					font-family: "Open Sans";
				}
				.table-basic th{
					background: #0073aa;
					color: #fff;
					padding: 18px 24px;
					font-weight: 600;
					text-align: left;
				}
				.table-basic tfoot td,
				.table-basic tbody td{
					padding: 15px 24px;
					vertical-align: middle;
					border-top: 1px solid #eee;
					font-size: 13px;
					color: #22272b;
				}
				.table-basic tfoot td{
					font-weight:700;
					color: #000;
					font-size:14px;
				}
				.table-basic tbody tr:first-child td{
					border: none;
				}
				.conversion-table{
					max-width: 1000px;
				}
			</style>
			<div class="conversion-table table-basic">
				<table>
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Tanggal Order</th>
							<th>Status</th>
							<th>Nama</th>
							<th>Total Order</th>
						</tr>
					</thead>
					<tbody>
						<?php $total = 0;if(is_array($conversion) && !empty($conversion)):?>
							<?php foreach($conversion as $c):
								$order = wc_get_order(get_post_meta($c->ID, 'order', true));
								if($order):
								$total += $order->get_total();
							?>
								<tr>
									<td><a href="<?php echo get_edit_post_link($order->get_id()) ;?>">#<?php echo $order->get_order_number() ;?></a></td>
									<td><?php echo wc_format_datetime( $order->get_date_created(), 'd F Y' ) ;?></td>
									<td><?php echo ucfirst($order->get_status());?></td>
									<td><?php echo wp_strip_all_tags(esc_attr($order->get_billing_first_name()) . ' ' . esc_attr($order->get_billing_last_name())) ;?></td>
									<td><?php echo $order->get_formatted_order_total() ;?></td>
								</tr>
								<?php endif;?>
							<?php endforeach;?>
						<?php else:?>
							<tr>
								<td style="text-align: center" colspan="5">Order not found</td>
							</tr>
						<?php endif;?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4">Grand Total</td>
							<td><?php echo wc_price($total) ;?></td>
						</tr>
					</tfoot>
				</table>
			</div>
		<?php
	}

	/**
	 * Output an export link.
	 */
	public function get_export_button() {

		$current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( wp_unslash( $_GET['range'] ) ) : '7day';
		?>
		<a
			href="#"
			download="report-skin-advisor-<?php echo esc_attr( $current_range ); ?>-<?php echo esc_attr( date_i18n( 'Y-m-d', current_time( 'timestamp' ) ) ); ?>.csv"
			class="export_csv"
			data-export="chart"
			data-xaxes="<?php esc_attr_e( 'Date', 'woocommerce' ); ?>"
			data-groupby="<?php echo esc_attr( $this->chart_groupby ); ?>"
		>
			<?php esc_html_e( 'Export CSV', 'woocommerce' ); ?>
		</a>
		<?php
	}

	/**
	 * Output the main chart.
	 */
	public function get_main_chart() {
		global $wp_locale;

		$conversion_data    = $this->prepare_chart_data( $this->conversion, 'post_date', '', $this->chart_interval, $this->start_date, $this->chart_groupby );
		$visit_data    = $this->prepare_chart_data( $this->visit, 'post_date', '', $this->chart_interval, $this->start_date, $this->chart_groupby );
		
		$chart_data = wp_json_encode(
			array(
				'conversion' => array_values( $conversion_data ),
				'visit'    => array_values( $visit_data ),
			)
		);
		?>
		<div class="chart-container">
			<div class="chart-placeholder main"></div>
		</div>
		<script type="text/javascript">
			var main_chart;

			jQuery(function(){
				var chart_data = JSON.parse( decodeURIComponent( '<?php echo rawurlencode( $chart_data ); ?>' ) );

				var drawGraph = function( highlight ) {
					var series = [
							{
								label: "<?php echo esc_js( __( 'Visit', 'woocommerce' ) ); ?>",
								data: chart_data.visit,
								color: '<?php echo esc_html( $this->chart_colours['visit'] ); ?>',
								bars: { fillColor: '<?php echo esc_html( $this->chart_colours['visit'] ); ?>', fill: true, show: true, lineWidth: 0, barWidth: <?php echo esc_html( $this->barwidth ); ?> * 0.5, align: 'center' },
								shadowSize: 0,
								enable_tooltip: true,
								append_tooltip: "<?php echo esc_html( ' ' . __( 'visit', 'woocommerce' ) ); ?>",
								stack: true,
							},
							{
								label: "<?php echo esc_js( __( 'Conversion', 'woocommerce' ) ); ?>",
								data: chart_data.conversion,
								color: '<?php echo esc_html( $this->chart_colours['conversion'] ); ?>',
								bars: { fillColor: '<?php echo esc_html( $this->chart_colours['conversion'] ); ?>', fill: true, show: true, lineWidth: 0, barWidth: <?php echo esc_html( $this->barwidth ); ?> * 0.5, align: 'center' },
								shadowSize: 0,
								enable_tooltip: true,
								append_tooltip: "<?php echo esc_html( ' ' . __( 'conversion', 'woocommerce' ) ); ?>",
								stack: true,
							},
						];

					if ( highlight !== 'undefined' && series[ highlight ] ) {
						highlight_series = series[ highlight ];

						highlight_series.color = '#9c5d90';

						if ( highlight_series.bars )
							highlight_series.bars.fillColor = '#9c5d90';

						if ( highlight_series.lines ) {
							highlight_series.lines.lineWidth = 5;
						}
					}

					main_chart = jQuery.plot(
						jQuery('.chart-placeholder.main'),
						series,
						{
							legend: {
								show: false
							},
							grid: {
								color: '#aaa',
								borderColor: 'transparent',
								borderWidth: 0,
								hoverable: true
							},
							xaxes: [ {
								color: '#aaa',
								position: "bottom",
								tickColor: 'transparent',
								mode: "time",
								timeformat: "<?php echo ( 'day' === $this->chart_groupby ) ? '%d %b' : '%b'; ?>",
								monthNames: JSON.parse( decodeURIComponent( '<?php echo rawurlencode( wp_json_encode( array_values( $wp_locale->month_abbrev ) ) ); ?>' ) ),
								tickLength: 1,
								minTickSize: [1, "<?php echo esc_html( $this->chart_groupby ); ?>"],
								tickSize: [1, "<?php echo esc_html( $this->chart_groupby ); ?>"],
								font: {
									color: "#aaa"
								}
							} ],
							yaxes: [
								{
									min: 0,
									minTickSize: 1,
									tickDecimals: 0,
									color: '#ecf0f1',
									font: { color: "#aaa" }
								}
							],
						}
					);
					jQuery('.chart-placeholder').resize();
				}

				drawGraph();

				jQuery('.highlight_series').hover(
					function() {
						drawGraph( jQuery(this).data('series') );
					},
					function() {
						drawGraph();
					}
				);
			});
		</script>
		<?php
	}
}
