<?php

namespace Avoskin;
use Avoskin\Medoo;

class Model {
        
        private $db, $tbl;
        private static $instance;
        
        //Connect Model to DB with Medoo interface
        public function __construct(){
                global $wpdb;
                $this->db = new Medoo([
                        'database_type' => 'mysql',
                        'database_name' => DB_NAME,
                        'server' => DB_HOST, //FOR AVO PRODUCTION
                        //'server' => 'localhost',
                        'username' => DB_USER,
                        'password' => DB_PASSWORD,
                        'charset' => DB_CHARSET,
                        'prefix' => $wpdb->prefix,
                ]);
                $this->tbl = [
                        'referral' => 'avoskin_referral',
                        'meta' => 'postmeta',
                        'comment' => 'commentmeta',
                        'usermeta' => 'usermeta',
                        'posts' => 'posts',
                        'taxonomy' => 'term_taxonomy',
                        'term_relation' => 'term_relationships',
                        'terms' => 'terms',
                ];
        }
        
        public static function get_instance(){
                if(!isset(self::$instance)) {
                        self::$instance = new Model();
                }
                return self::$instance;
        }
        
        //Record referral to DB
        public function record_referral($data){
                $this->db->insert($this->tbl['referral'], $data);
                $data['id'] = $this->db->id();
                
                return $data['id'];
        }
        
        public function get_user_by_meta($meta){
                return $this->db->get($this->tbl['meta'], 'post_id', [
                        'meta_key' => 'referral_code',
                        'meta_value' => $meta
                ]);
        }
        
        public function count_visit($referral){
                return $this->db->count(
                        $this->tbl['referral'], 
                        [
                                'referral' => (int)$referral,
                                'type' => 'visit'
                        ]
                );
        }
        
        public function count_unique_visit($referral){
                $result =  $this->db->select(
                        $this->tbl['referral'], 'visitor',
                        [
                                'referral' => (int)$referral,
                                'type' => 'visit'
                        ]
                );
                return (is_array($result) && !empty($result)) ? count(array_unique($result)) : 0;
        }
        
        public function count_conversion($referral){
                return $this->db->count(
                        $this->tbl['referral'], 
                        [
                                'referral' => (int)$referral,
                                'type[!]' => 'visit'
                        ]
                );
        }
        
        public function get_referral_order($referral){
                return $this->db->select(
                        $this->tbl['referral'], 'type',
                        [
                                'referral' => (int)$referral,
                                'type[!]' => 'visit'
                        ]
                );
        }
        
        public function count_review_field($comment, $key, $value){
                if($key != 'skin_type'){
                        return $this->db->count(
                                $this->tbl['comment'], 
                                [
                                        'meta_key' => $key,
                                        'comment_id' => $comment,
                                        'meta_value' => $value
                                ]
                        );
                }else{
                        $skin_type = $this->db->get($this->tbl['comment'], 'meta_value', [
                                'meta_key' => $key,
                                'comment_id' => $comment
                        ]);                        
                        $skin_type =  maybe_unserialize($skin_type);
                        return (in_array($value, $skin_type)) ? 1 : 0;
                }
        }
        
        public function get_user_meta($user, $metakey){
                $result = $this->db->get(
                        $this->tbl['usermeta'], 'meta_value',
                        [
                                'user_id' => (int)$user,
                                'meta_key' => $metakey
                        ]
                );
                
                return maybe_unserialize($result);
        }
        
        public function search_product_by_cat($keyword){
                return $this->db->select(
                        $this->tbl['term_relation'],
                        [
                                "[>]".$this->tbl['taxonomy'] => ["term_taxonomy_id" => "term_id"],
                                "[>]".$this->tbl['terms'] => ["term_taxonomy_id" => "term_id"]
                        ],
                        [
                                'object_id(id)'
                        ],
                        [
                                $this->tbl['terms'].".name[~]" => $keyword,
                                $this->tbl['taxonomy'].".taxonomy" => 'product_cat'
                        ]
                );
        }
        
        public function get_admin_order($status){
                return $this->db->select(
                        $this->tbl['posts'],['ID'],
                        [
                                $this->tbl['posts'].".post_type" => 'shop_order',
                                $this->tbl['posts'].".post_status" => $status,
                                "ORDER" => [
                                        $this->tbl['posts'].".post_date" => "DESC"
                                ]
                        ]
                );
        }
        
        public function avodash_customer_info_stats($args){
                
                $final_user = [];
                $user = [];
                $raw_user =  $this->db->select(
                        $this->tbl['meta'],
                        [
                                "[>]".$this->tbl['posts'] => [$this->tbl['meta'].".post_id" => "ID"],
                        ],[
                                $this->tbl['meta'].".post_id",
                                $this->tbl['meta'].".meta_value",
                        ],
                        [
                                $this->tbl['meta'].".meta_key" => '_customer_user',
                                $this->tbl['posts'].".post_status" => ['wc-completed', 'wc-processing', 'wc-on-delivery', 'wc-packing'],
                                $this->tbl['posts'].".post_date[<>]" => [date('Y-m-d H:i:s', strtotime($args['start_date'])), date('Y-m-d H:i:s', strtotime($args['end_date']))]
                        ]
                );
                
                if(is_array($raw_user) && !empty($raw_user)){
                        foreach($raw_user as $r){
                                if($r['meta_value'] != '0'){
                                        $user[] = $r['meta_value'];
                                }
                        }
                }
                
                if(!empty($user)){
                        $user = array_unique($user);
                        $data = get_posts([
                                'fields' => 'ids',
                                'post_type' => 'shop_order',
                                'post_status' =>  ['wc-completed', 'wc-processing', 'wc-on-delivery', 'wc-packing'],
                                'posts_per_page' => -1,
                                'meta_query' => [
                                        [
                                                'key'  => '_customer_user',
                                                'value' => $user,
                                                'compare' => 'IN'
                                        ]
                                ]
                        ]);
                        
                        if(is_array($data) && !empty($data)){
                                foreach($data as $d){
                                        $customer = get_post_meta($d, '_customer_user', true);
                                        if(in_array($customer, $user)){
                                                $final_user[$customer][] = $d;
                                        }
                                }
                        }
                }
                
                return $final_user;
        }
        
        public function avodash_customer_info_city_stats($args){
                $data = [];
                $params = [                               
                        $this->tbl['meta'].".meta_key" => '_billing_city',
                        $this->tbl['posts'].".post_status" => ['wc-completed', 'wc-processing', 'wc-on-delivery', 'wc-packing'],
                        $this->tbl['posts'].".post_date[<>]" => [date('Y-m-d H:i:s', strtotime($args['start_date'])), date('Y-m-d H:i:s', strtotime($args['end_date']))]
                ];
                if(isset($args['search']) && $args['search'] != ''){
                        $params[$this->tbl['meta'].".meta_value[~]"] = $args['search'];
                }
                $city_raw =  $this->db->select(
                        $this->tbl['meta'],
                        [
                                "[>]".$this->tbl['posts'] => [$this->tbl['meta'].".post_id" => "ID"],
                        ],[
                                $this->tbl['meta'].".post_id(id)",
                                $this->tbl['meta'].".meta_value(city)",
                        ],
                        $params
                );
                if(is_array($city_raw) && !empty($city_raw)){
                        foreach($city_raw as $cr){
                                $valid = true;
                                if(isset($args['expedition']) && $args['expedition'] != ''){
                                        $valid = false;
                                        if($args['expedition'] == get_post_meta($cr['id'], 'shipping_courier', true)){
                                                $valid = true;
                                        }
                                }
                                if($valid){
                                        $data['city'][$cr['city']]['id'] = get_post_meta($cr['id'], 'city_id', true);
                                        $data['city'][$cr['city']]['order'][] = (int)$cr['id'];
                                        $data['city'][$cr['city']]['total'] = ($data['city'][$cr['city']]['total'] == '') ? 1 : $data['city'][$cr['city']]['total'] + 1;
                                        $data['total_order']= ($data['total_order'] == '') ? 1 : $data['total_order'] + 1;        
                                }
                                
                        }        
                }
                
                return $data;
        }
}