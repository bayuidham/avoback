<?php
/*
Template Name: Page Partner
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-partner">
                
                <?php if(have_posts()) : while(have_posts()) : the_post();
                        $meta = [
                                'title' => 'intro_title',
                                'text' => 'intro_text',
                                'how_txt' => 'btn_how_txt',
                                'how_url' => 'btn_how_url',
                                'join_txt' => 'btn_join_txt',
                                'join_url' => 'btn_join_url',
                        ];
                        extract(avoskin_get_meta(get_the_ID(), $meta));
			// ::STORELOC::
                        $store = get_posts([
                                'fields' => 'ids',
                                'post_type' => 'avoskin-storeloc',
                                'post_status' => 'publish',
                                'orderby' => 'title',
                                'order' => 'ASC',
                                'posts_per_page' => -1
                        ]);
                ?>
                
                        <div class="intro">
                                <div class="wrapper">
                                        <div class="caption">
                                                <h1><?php echo $title ;?></h1>
                                                <div class="format-text">
                                                        <?php echo $text ;?>
                                                </div><!-- end of format text -->
                                        </div><!-- end of caption -->
                                        <div class="action">
                                                <a href="<?php echo $how_url ;?>" class="button btn-hollow slimy has-icon"><i class="fa-question-circle"></i><span><?php echo $how_txt ;?></span></a>
                                                <a href="<?php echo $join_url ;?>" class="button slimy"><?php echo $join_txt ;?></a>
                                        </div><!-- end of action -->
                                        <div class="clear"></div>
                                </div><!-- end of wrapper -->
                        </div><!-- end of intro -->
                        
                        <?php if(is_array($store) && !empty($store)):?>
                                <div class="location">
                                        <div class="wrapper">
                                                <div class="bound">
                                                      <div class="map-area">
                                                              <div class="holder">
                                                                      <div class="map-search form-basic">
                                                                              <input type="text" value="" placeholder="<?php _e('Search Store','avoskin');?>" />
                                                                              <button><i class="fa-search"></i></button>
                                                                      </div><!-- endof map search -->
                                                                      <div class="iframe-holder">
                                                                              <div id="maps"></div>
                                                                      </div><!-- end of map -->
                                                              </div><!-- end of holder -->
                                                      </div><!-- end of map holder -->
                                                      <div class="store">
                                                              <div class="layer">
                                                                      <div class="scrollbar-inner">
                                                                                <?php foreach($store as $s):
                                                                                        $loc = explode(',', preg_replace('/\s+/', '', get_post_meta($s, 'loc', true)));
                                                                                        $data = [
                                                                                                'loc' => $loc,
                                                                                                'title' => esc_attr(get_post_meta($s, 'title', true)),
                                                                                                'subtitle' => esc_attr(get_post_meta($s, 'subtitle', true)),
                                                                                                'text' => esc_attr(get_post_meta($s, 'text', true)),
                                                                                        ];
                                                                                ?>
                                                                                      <div class="item" data-item='<?php echo json_encode($data);?>' data-filter-name="<?php echo strtolower($data['title']);?>" data-filter-item>
                                                                                              <h3><span><?php echo $data['title'] ;?></span> <b><?php echo $data['subtitle'] ;?></b></h3>
                                                                                              <div class="txt"><?php echo get_post_meta($s, 'text', true) ;?></div><!-- endof txt -->
                                                                                      </div><!-- end of item -->
                                                                              <?php endforeach;?>
                                                                      </div><!-- end of scrollbar inner -->
                                                              </div><!-- end of layer -->
                                                      </div><!-- end of store -->
                                                      <div class="clear"></div>
                                                </div><!-- end of bound -->
                                        </div><!-- end of wrapper -->
                                </div><!-- end of location-->
                        <?php endif;?>
                
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
                
        </div><!-- end of inner partner -->
<?php get_footer();?>