<?php
/*
Template Name: Page Lady Boss
*/
?>
<?php get_header();?>
        <div class="inner-page-half inner-lady-boss">
                <div class="wrapper">
                        <?php do_action('avoskin_navside');?>
                        <article class="main">
                                <?php if(have_posts()) : while(have_posts()) : the_post();
                                        $text = get_post_meta(get_the_ID(), 'intro_text', true);
                                        $slide = get_post_meta(get_the_ID(), 'slide_item', true);
                                ?>
                                        <div class="content">
                                                <h1 class="the-title"><?php the_title();?></h1>
                                                <?php if($text != ''):?>
                                                        <div class="format-text">
                                                                <?php echo $text ;?>
                                                        </div><!-- end of format text -->
                                                <?php endif;?>
                                                <?php if(is_array($slide) && !empty($slide)):?>
                                                        <div class="slide">
                                                                <div class="slick-carousel">
                                                                        <?php foreach($slide as $s):
                                                                                $image = avoskin_resize( $s['thumb'], 380, 260, true, true, true ); //resize & crop img
                                                                        ?>
                                                                                <div class="item">
                                                                                        <a href="<?php echo $s['url'] ;?>">
                                                                                                <figure>
                                                                                                        <img src="<?php echo $image ;?>" alt="<?php the_title_attribute();?>" />
                                                                                                        <figcaption><?php echo $s['title'] ;?></figcaption>
                                                                                                </figure>
                                                                                        </a>
                                                                                </div><!-- end of item -->
                                                                        <?php endforeach;?>
                                                                </div><!-- end of slick carousel -->
                                                        </div><!-- end of slide -->
                                                <?php endif;?>
                                        </div><!-- end of content -->
                                <?php endwhile;?>
                                <?php else : ?>
                                        <div class="format-text">
                                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                        </div>
                                <?php endif;?>
                        </article>
                        <div class="clear"></div>
                </div><!--end of wrapper -->
        </div><!-- end of inner page half -->
<?php get_footer();?>