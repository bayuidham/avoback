<?php get_header();?>
        <?php $the_post_id = 0;?>
        <div class="crumb">
                <div class="wrapper">
                        <a href="<?php echo get_permalink(get_theme_mod('avoskin_landing_blp_page')) ;?>"><?php _e('Home','avoskin');?></a><i class="fa-angle-right"></i>
                        <?php the_category(', ');?> <i class="fa-angle-right"></i>
                        <span><?php the_title();?></span>
                </div>
        </div>
        <div class="detail-blog">
                <div class="copytext">
                          <div class="wrapper rowflex">
                                <article class="main">
                                        <?php if(have_posts()) : while(have_posts()) : the_post();
                                                $the_post_id = get_the_ID();
                                        ?>
                                                <div class="content">
                                                        <div class="hentry">
                                                                <div class="cat"><?php the_category(', ');?></div>
                                                                <h1><?php the_title();?></h1>
                                                                <div class="meta">
                                                                        <?php the_author_posts_link();?>
                                                                        <a href="<?php the_permalink();?>"><?php the_time('F d, Y'); ?></a>
                                                                </div><!-- end of meta -->
                                                        </div><!-- end of hentry -->
                                                        <div class="format-text">
                                                                <?php the_content();?>
                                                        </div><!-- end of format text -->
                                                </div><!-- end of content -->
                                                <div class="util">
                                                        <div class="social">
                                                                <span><?php _e('Share','avoskin');?></span>
                                                                <?php
                                                                        foreach([
                                                                                '.heateorSssFacebookSvg' => 'fb',
                                                                                '.heateorSssTwitterSvg' => 'tw',
                                                                                '.heateorSssPinterestSvg' => 'pn'
                                                                        ] as $key => $s):?>
                                                                        <a href="#" class="<?php echo $s ;?>" onClick="(function(){ $('<?php echo $key;?>').trigger('click'); return false; })();return false;"></a>
                                                                <?php endforeach;?>
                                                                <div style="display: none;">
                                                                        <?php echo do_shortcode('[Sassy_Social_Share]');?>
                                                                </div>
                                                        </div><!-- end of social -->
                                                        <?php
                                                                $prev_post = get_adjacent_post(false, '', true);
                                                                $next_post = get_adjacent_post(false, '', false);
                                                                if(!empty($prev_post) || !empty($next_post)):
                                                        ?>
                                                                <div class="prevnext">
                                                                        <?php if(!empty($prev_post)):?>
                                                                                <a href="<?php echo get_the_permalink($prev_post->ID);?>"><?php _e('Prev Article','avoskin');?></a>
                                                                        <?php endif;?>
                                                                        <?php if(!empty($next_post)):?>
                                                                                <a href="<?php echo get_the_permalink($next_post->ID);?>"><?php _e('Next Article','avoskin');?></a>
                                                                        <?php endif;?>
                                                                </div><!-- end of prevnext -->
                                                        <?php endif;?>
                                                        <div class="clear"></div>
                                                </div><!-- end of utl -->
                                        <?php endwhile;?>
                                        <?php else : ?>
                                                <div class="format-text">
                                                        <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                                                </div>
                                        <?php endif;?>        
                                </article><!-- end of main -->
                                <aside class="sidebar">
                                        <?php if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('sidebar-2') ) : ?>
                                        <?php endif; ?>
                                </aside><!-- end of sidebar -->
                        </div><!-- end of wrapper -->
                </div><!-- end of copytext -->
                <?php
                        $meta = [
                                'pimg' => 'product_img',
                                'ptitle' => 'product_title',
                                'psub' => 'product_subtitle',
                                'pbtn' => 'product_btn',
                                'purl' => 'product_url',
                        ];
                        extract(avoskin_get_meta(get_theme_mod('avoskin_landing_blog_page'), $meta));
                ?>
                <div class="banner">
                        <div class="wrapper">
                                <div class="item" <?php echo ($pimg != '') ? 'style="background-image: url(\''.$pimg.'\');"' : '' ;?>>
                                        <?php if($psub != ''):?>
                                        <span><?php echo $psub ;?></span>
                                        <?php endif;?>
                                        <?php if($ptitle != ''):?>
                                                <h2><a href="<?php echo $purl ;?>"><?php echo $ptitle ;?></a></h2>
                                        <?php endif;?>
                                        <?php if($pbtn != '' && $purl != ''):?>
                                                <a href="<?php echo $purl ;?>" class="button btn-hollow"><?php echo $pbtn ;?></a>
                                        <?php endif;?>
                                </div><!-- end of item -->
                        </div><!-- endo f wrapper -->
                </div><!-- end of banner -->
            
                <div class="relblog">
                        <div class="wrapper">
                                <h2><?php _e('Related Posts','avoskin');?></h2>
                                <div class="rowflex">
                                             <?php
                                                $related = get_posts(
                                                         [
                                                               'category__in' => wp_get_post_categories($the_post_id),
                                                               'numberposts' => 3,
                                                               'post__not_in' => [$the_post_id],
                                                               'orderby'=> 'rand'
                                                        ]
                                                );
                                                if( $related ) foreach( $related as $post ) {
                                                        setup_postdata($post);
                                                                $thumb = get_post_thumbnail_id($post->ID);
                                                                $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                                                                $image = avoskin_resize( $img_url, 384, 312, true, true, true );
                                                                $cat = get_the_category($post->ID);
                                                        ?>
                                                           <div class="item">
                                                                <a href="<?php the_permalink();?>">
                                                                        <img src="<?php echo $image ;?>" />
                                                                        <div class="caption">
                                                                                <span><?php foreach($cat as $c):?>
                                                                                        <?php echo $c->name ;?>
                                                                                <?php endforeach;?></span>
                                                                                <h3><?php the_title();?></h3>
                                                                                <div class="meta">
                                                                                        <b><?php echo get_the_author_meta('display_name') ;?></b>
                                                                                        <b><?php the_time('F d, Y'); ?></b>
                                                                                </div><!-- end of meta -->
                                                                        </div>
                                                                </a>
                                                        </div><!-- end of item -->
                                                <?php  }else{
                                                    ?>
                                                    <div class="item fullwidth">
                                                               <?php _e('No related post available','avoskin');?>
                                                </div><!-- end of item --><?php
                                                }
                                                wp_reset_postdata(); ?>
                                </div><!-- end of rowflex -->
                        </div><!-- end of wrapper -->
                </div><!-- end of relblog -->
                
        </div><!-- end of inner page -->
<?php get_footer();?>