<?php
/*
Template Name: Page Skin Advisor Checkout
*/
?>
<?php get_header();?>
<?php
        //$data = [
        //        'name' => 'Bayu Idham',
        //        'product' => [
        //                [
        //                        'id' => 1265,
        //                        'qty' => 2
        //                ],
        //                [
        //                        'id' => 1215,
        //                        'qty' => 4
        //                ]
        //        ]
        //];
        //Local
        //eyJuYW1lIjoiQmF5dSBJZGhhbSIsInByb2R1Y3QiOlt7ImlkIjo2OSwicXR5IjoyfSx7ImlkIjo3OCwicXR5Ijo0fSx7ImlkIjo1NywicXR5IjoxfV19
        //http://avoskin.test/skin-advisor-checkout/?data=eyJuYW1lIjoiQmF5dSBJZGhhbSIsInByb2R1Y3QiOlt7ImlkIjo2OSwicXR5IjoyfSx7ImlkIjo3OCwicXR5Ijo0fSx7ImlkIjo1NywicXR5IjoxfV19
        
        //Staging
        //https://avoskin.demoapp.xyz/skin-advisor-checkout/?data=eyJuYW1lIjoiQmF5dSBJZGhhbSIsInByb2R1Y3QiOlt7ImlkIjoxMjY1LCJxdHkiOjJ9LHsiaWQiOjEyMTUsInF0eSI6NH1dfQ==
        
?>
        <div id="home-load">
               <style type="text/css">.lds-ellipsis{display:inline-block;position:relative;width:80px;height:80px}.lds-ellipsis div{position:absolute;top:33px;width:13px;height:13px;border-radius:50%;background:#b6c6b3;animation-timing-function:cubic-bezier(0,1,1,0)}.lds-ellipsis div:nth-child(1){left:8px;animation:lds-ellipsis1 .6s infinite}.lds-ellipsis div:nth-child(2){left:8px;animation:lds-ellipsis2 .6s infinite}.lds-ellipsis div:nth-child(3){left:32px;animation:lds-ellipsis2 .6s infinite}.lds-ellipsis div:nth-child(4){left:56px;animation:lds-ellipsis3 .6s infinite}@keyframes lds-ellipsis1{0%{transform:scale(0)}100%{transform:scale(1)}}@keyframes lds-ellipsis3{0%{transform:scale(1)}100%{transform:scale(0)}}@keyframes lds-ellipsis2{0%{transform:translate(0,0)}100%{transform:translate(24px,0)}}@media (max-width: 768px) {.avoskin-loading img{max-width:150px !important;}}</style>
               <div class="avoskin-loading" style="background: #fff;">
                       <div class="centered">
                               <img src="<?php echo get_theme_mod('avoskin_logo');?>" alt="<?php bloginfo('name');?>" style="display: block;width: 300px;margin: 0 auto;" />
                               <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                       </div><!-- end of centered -->
               </div><!-- end of avoskin loading -->
       </div>
<?php
        $data = (isset($_GET['data']) && $_GET['data'] != '') ? $_GET['data'] : '';
        if($data != ''):?>
        <script type="text/javascript">
                ;(function($){
                        $(document).ready(function(){
                                $.post( "<?php echo site_url('/wp-json/avoskin/v1/advisor_add_to_cart/');?>", {data: '<?php echo $data ;?>'}, function( result ) {
                                        window.location.href = result;
                                });
                        });
                }(jQuery));
        </script>
<?php else:?>
        <script type="text/javascript">
                ;(function($){
                        $(document).ready(function(){
                                window.location.href= '<?php echo home_url('/') ;?>';
                        });
                }(jQuery));
        </script>
<?php endif;?>
<?php get_footer();?>