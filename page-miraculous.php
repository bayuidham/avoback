<?php
/*
Template Name: Page Miraculous Series
*/
?>
<?php get_header();?>
        <?php woocommerce_breadcrumb();?>
        <div class="inner-mrcs">
                <?php if(have_posts()) : while(have_posts()) : the_post();
                        $meta = [
                                'text' => 'intro_text',
                                'item' => 'mira_item',
                                'title' => 'related_title',
                                'item_product' => 'related_product',
                                'rurl' => 'related_url',
                        ];
                        extract(avoskin_get_meta(get_the_ID(), $meta));
                ?>
                
                        <div class="wrapper">
                                <div class="section-title centered">
                                        <h2 class="line-title"><?php the_title();?></h2>
                                </div><!-- end of section title -->
                                <?php if($text != ''):?>
                                <div class="format-text">
                                        <?php echo $text ;?>
                                </div><!-- end of format text -->
                                <?php endif;?>
                                <?php if(is_array($item) && !empty($item)):?>
                                        <div class="wrap">
                                                <?php foreach($item as $i):?>
                                                        <div class="item">
                                                              <figure>
                                                                        <?php if($i['mimg'] != ''):?>
                                                                                <img src="<?php echo $i['mimg'] ;?>" />
                                                                      <?php endif;?>
                                                                      <?php if($i['img'] != ''):?>
                                                                                <span style="background-image: url('<?php echo $i['img'] ;?>');"></span>
                                                                      <?php endif;?>
                                                              </figure>
                                                              <div class="caption">
                                                                        <?php if($i['title'] != ''):?>
                                                                                <h2><?php echo $i['title'] ;?></h2>  
                                                                        <?php endif;?>
                                                                        <?php if($i['text'] != '') :?>
                                                                        <div class="txt">
                                                                                <?php echo $i['text'] ;?>
                                                                        </div><!-- end of txt -->  
                                                                        <?php endif;?>
                                                                        <?php if($i['url'] != ''):?>
                                                                                <a href="<?php echo $i['url'] ;?>" class="button btn-white"><?php _e('See All','avoskin');?></a>
                                                                      <?php endif;?>
                                                              </div><!-- end of caption -->
                                                      </div><!-- end of item -->
                                                <?php endforeach;?>
                                      </div><!-- end of wrap -->
                                <?php endif;?>
                        </div><!-- end of wrapper -->
                        <div class="best-seller">
                                <div class="wrapper">
                                        <?php if($title != ''):?>
                                                <div class="section-title centered">
                                                        <h2 class="line-title"><?php echo $title ;?></h2>
                                                </div><!-- end of secton title -->
                                        <?php endif;?>
                                        <?php if(is_array($item_product) && !empty($item_product)):?>
                                                <div class="rowflex">
                                                        <?php foreach($item_product as $i):
                                                                $product = wc_get_product((int)$i['product']);
                                                                $id = $product->get_id();
                                                                $price = $product->get_regular_price();
                                                                $sale = $product->get_sale_price();
                                                                $percent = '';
                                                                if($sale > 0){
                                                                        $percent = ceil((($price - $sale )  / $price ) * 100);
                                                                }
                                                                $tagline = get_post_meta($id, 'product_tagline', true);
                                                                $stock = ceil(get_post_meta( $id, '_stock', true ));
                                                                $hide_price = (get_post_meta($id, 'product_hide_price', true) == 'yes' ) ? 'hide-price' : '';
                                                        ?>
                                                                <div class="product-item <?php echo $hide_price ;?>">
                                                                        <div class="holder">
                                                                                <div class="img-box">
                                                                                        <?php if($product->get_stock_status() != 'instock'):?>
                                                                                                <span class="stock-empty"><?php avoskin_oos_text($id);?></span>
                                                                                        <?php endif;?>
                                                                                        <?php if($percent != '' || $tagline != ''):?>
                                                                                                <span class="disc"><?php echo ($percent == '' || $tagline != '' && is_product() || $tagline != '' && is_page_template('page-homepage.php')) ? $tagline : $percent.'%' ;?></span>
                                                                                        <?php endif;?>
                                                                                        <?php echo $product->get_image([280,280]) ;?>
                                                                                        <div class="action">
                                                                                                <a href="<?php echo get_permalink($product->get_id());?>" class="visit"><i></i></a>
                                                                                                <a href="#" class="add-to-cart act-add-to-cart" data-id="<?php echo $product->get_id();?>" data-quantity="1" data-stock="<?php echo $stock ;?>"><i></i></a>
                                                                                                <a href="#" class="add-to-wish" data-product="<?php echo $product->get_id();?>"><i></i></a>
                                                                                        </div><!-- end of action -->
                                                                                </div><!-- end of img box -->
                                                                                <div class="cat">
                                                                                        <?php echo wc_get_product_category_list($product->get_id());?>
                                                                                </div><!-- end of cat -->
                                                                                <h2 class="product-title"><a href="<?php echo get_permalink($product->get_id());?>"><?php echo $product->get_name() ;?></a></h2>
                                                                                <?php echo avoskin_rating($product->get_average_rating());?>
                                                                                <?php if($percent != '') :?>
                                                                                        <span class="sale"><?php echo wc_price($price) ;?></span>
                                                                                <?php endif;?>
                                                                                <strong class="price"><?php echo ($percent != '') ? wc_price($sale) : wc_price($price) ;?></strong>
                                                                        </div><!-- endof holder-->
                                                                </div><!-- end of item -->
                                                        <?php endforeach;?>
                                                </div><!-- end of rowflex -->
                                        <?php endif;?>
                                        <?php if($rurl != ''):?>
                                                <div class="centered">
                                                        <a href="<?php echo $rurl ;?>" class="button slimy"><?php _e('See All','avoskin');?></a>
                                                </div><!-- end of centered -->
                                        <?php endif;?>
                                </div><!-- end of wrapper -->
                        </div>
                <?php endwhile;?>
                <?php else : ?>
                        <div class="format-text">
                                <p><?php _e('Sorry, no posts matched your criteria. Try something else. ','avoskin')?></p>
                        </div>
                <?php endif;?>
        </div><!-- end of inner page -->
<?php get_footer();?>